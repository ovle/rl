package com.ovle.utils.gdx.math.dijkstraMap

import com.ovle.utils.gdx.math.array2d.Array2d.Companion.array
import com.ovle.utils.gdx.math.array2d.localMins
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMap.Companion.UNINITIALIZED_MAP_VALUE
import com.ovle.utils.gdx.math.array2d.gradientFloodFill
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class DijkstraMapTest {

    companion object {
        @JvmStatic
        fun args() = testCases

        @JvmStatic
        fun argsLocalMins() = localMinsTestCases
    }

    @ParameterizedTest
    @MethodSource("args")
    fun `test gradient flood fill`(testCase: TestCase) {
        val (goals, size, obstacles, expectedResult) = testCase
        val map = array(UNINITIALIZED_MAP_VALUE, size)
        gradientFloodFill(goals, obstacles, map)

        assertArrayEquals(expectedResult.data, map.data)
    }

    @ParameterizedTest
    @MethodSource("argsLocalMins")
    fun `test local minimums`(testCase: LocalMinsTestCase) {
        val (map, expectedResult) = testCase
        val result = map.localMins()

        assertIterableEquals(expectedResult.keys, result.keys)
        assertIterableEquals(expectedResult.values, result.values)
    }
}