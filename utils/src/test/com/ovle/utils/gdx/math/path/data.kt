package com.ovle.utils.gdx.math.path

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.point

data class TestCase(
    val path: List<GridPoint2>,
    val expectedResult: List<GridPoint2>
)

val testCases = arrayOf(
    TestCase(
        path = listOf(),
        expectedResult = listOf()
    ),
    TestCase(
        path = listOf(point(0, 0)),
        expectedResult = listOf(point(0, 0))
    ),
    TestCase(
        path = listOf(point(0, 0), point(0, 1)),
        expectedResult = listOf(point(0, 0), point(0, 1))
    ),

    TestCase(
        path = listOf(point(0, 0), point(0, 1), point(0, 2)),
        expectedResult = listOf(point(0, 0), point(0, 2))
    ),
    TestCase(
        path = listOf(point(0, 0), point(1, 0), point(2, 0)),
        expectedResult = listOf(point(0, 0), point(2, 0))
    ),

    TestCase(
        path = listOf(
            point(0, 0),
            point(1, 0), point(2, 0),
            point(2, 1), point(2, 2)
        ),
        expectedResult = listOf(point(0, 0), point(2, 0), point(2, 2))
    ),
    TestCase(
        path = listOf(
            point(0, 0),
            point(1, 0), point(2, 0),
            point(2, 1), point(2, 2),
            point(1, 2), point(0, 2),
            point(1, 2), point(-1, 2)
        ),
        expectedResult = listOf(point(0, 0), point(2, 0), point(2, 2), point(-1, 2))
    ),

    TestCase(
        path = listOf(point(0, 0), point(1, 1), point(2, 2)),
        expectedResult = listOf(point(0, 0), point(1, 1), point(2, 2))
    ),
    TestCase(
        path = listOf(point(0, 0), point(2, 5), point(4, 4)),
        expectedResult = listOf(point(0, 0), point(2, 5), point(4, 4))
    ),
)