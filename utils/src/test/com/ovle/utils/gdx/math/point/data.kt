package com.ovle.utils.gdx.math.point

import com.badlogic.gdx.math.GridPoint2

data class TestCase(
    val from: GridPoint2,
    val to: GridPoint2,
    val dst: Int,
    val expectedResult: GridPoint2?
)

val nextToTestCases = arrayOf(
    TestCase(
        from = point(0, 0), to = point(0, 0), dst = 1,
        expectedResult = null
    ),

    TestCase(
        from = point(0, 0), to = point(0, 1), dst = 0,
        expectedResult = point(0, 0)
    ),
    TestCase(
        from = point(0, 0), to = point(0, 1), dst = 1,
        expectedResult = point(0, 1)
    ),
    TestCase(
        from = point(0, 0), to = point(0, 1), dst = 2,
        expectedResult = null
    ),

    TestCase(
        from = point(0, 0), to = point(0, -1), dst = 0,
        expectedResult = point(0, 0)
    ),
    TestCase(
        from = point(0, 0), to = point(0, -1), dst = 1,
        expectedResult = point(0, -1)
    ),
    TestCase(
        from = point(0, 0), to = point(0, -1), dst = 2,
        expectedResult = null
    ),

    TestCase(
        from = point(0, 0), to = point(0, 2), dst = 1,
        expectedResult = point(0, 1)
    ),
    TestCase(
        from = point(0, 0), to = point(0, 2), dst = 2,
        expectedResult = point(0, 2)
    ),
    TestCase(
        from = point(0, 0), to = point(0, 2), dst = 3,
        expectedResult = null
    ),

    TestCase(
        from = point(0, 0), to = point(2, 2), dst = 2,
        expectedResult = point(1, 1)
    ),
    TestCase(
        from = point(0, 0), to = point(2, 2), dst = 4,
        expectedResult = point(2, 2)
    ),
    TestCase(
        from = point(0, 0), to = point(2, 2), dst = 5,
        expectedResult = null
    ),
)

val nextAfterTestCases = arrayOf(
    TestCase(
        from = point(0, 0), to = point(0, 2), dst = 0,
        expectedResult = point(0, 2)
    ),
    TestCase(
        from = point(0, 0), to = point(0, 2), dst = 1,
        expectedResult = point(0, 3)
    ),
    TestCase(
        from = point(0, 0), to = point(0, 2), dst = 2,
        expectedResult = point(0, 4)
    ),

    TestCase(
        from = point(0, 0), to = point(2, 2), dst = 2,
        expectedResult = point(3, 3)
    ),

    TestCase(
        from = point(1, 1), to = point(-1, -1), dst = 2,
        expectedResult = point(-2, -2)
    ),
)