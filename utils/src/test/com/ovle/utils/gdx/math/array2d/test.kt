package com.ovle.utils.gdx.math.array2d

import com.ovle.utils.gdx.math.point.point
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class Array2dTest {

    @Test
    fun `test get`() {
        val source = arrayOf(0, 1, 2, 3)
        val array = Array2d(source, 2)

        assertEquals(0, array[0, 0])
        assertEquals(1, array[1, 0])
        assertEquals(2, array[0, 1])
        assertEquals(3, array[1, 1])
    }

    @Test
    fun `test index and point`() {
        val source = arrayOf(0, 1, 2, 3)
        val array = Array2d(source, 2)

        source.forEachIndexed { index, _ ->
            val point = array.point(index)
            assertEquals(index, array.index(point.x, point.y))
        }
    }

    @Test
    fun `test match`() {
        val source = arrayOf(
            0, 1, 2,
            3, 4, 5,
            6, 7, 8
        )
        val array = Array2d(source, 3)
        val mask = Array2d(arrayOf(3, 4, 6, 7), 2)

        assertTrue(
            array.matches(mask, point(0, 1)) { v1, v2 -> v1 == v2 }
        )
        assertFalse(
            array.matches(mask, point(0, 0)) { v1, v2 -> v1 == v2 }
        )
    }
}