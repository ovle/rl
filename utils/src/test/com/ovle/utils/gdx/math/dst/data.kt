package com.ovle.utils.gdx.math.dst

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.point

data class TestCase(
    val from: GridPoint2,
    val to: GridPoint2,
    val expectedResult: Double
)

val testCases = arrayOf(
    //no dst
    TestCase(
        from = point(0, 0), to = point(0, 0),
        expectedResult = 0.0
    ),
    TestCase(
        from = point(27, 27), to = point(27, 27),
        expectedResult = 0.0
    ),

    //1 axis
    TestCase(
        from = point(0, 0), to = point(0, 1),
        expectedResult = 1.0
    ),
    TestCase(
        from = point(0, 0), to = point(27, 0),
        expectedResult = 27.0
    ),
    TestCase(
        from = point(-1, 0), to = point(1, 0),
        expectedResult = 2.0
    ),
    TestCase(
        from = point(1, 0), to = point(0, 0),
        expectedResult = 1.0
    ),
    TestCase(
        from = point(0, 0), to = point(-1, 0),
        expectedResult = 1.0
    ),

    //2 axis
    TestCase(
        from = point(0, 0), to = point(1, 1),
        expectedResult = 2.0
    ),
    TestCase(
        from = point(1, 2), to = point(2, 1),
        expectedResult = 2.0
    ),
    TestCase(
        from = point(0, 0), to = point(2, 2),
        expectedResult = 4.0
    ),
)