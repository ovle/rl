package com.ovle.utils.gdx.math.dst

import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType
import com.ovle.utils.gdx.math.discretization.bresenham.circle
import com.ovle.utils.gdx.math.distance
import com.ovle.utils.gdx.math.point.point
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.text.DecimalFormat

class DstTest {

    companion object {
        @JvmStatic
        fun args() = testCases
    }

    @ParameterizedTest
    @MethodSource("args")
    fun `test distance`(testCase: TestCase) {
        val (from, to, expectedResult) = testCase
        val df = DecimalFormat("#.##")
        val result = df.format(distance(from, to)).toDouble()
        assertEquals(expectedResult, result)
    }

    @Test
    fun `test circle`() {
        val radius = 3
        val center = point(0, 0)
        val circle = circle(center, radius, DiscreetCircleType.DISTANCE_BASED)
        circle.forEach {
            val dstInt = distance(it, center)
            assertEquals(dstInt, radius)
        }
    }
}