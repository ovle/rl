package com.ovle.utils.gdx.math.point

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class Test {

    companion object {
        @JvmStatic
        fun nextToArgs() = nextToTestCases
        @JvmStatic
        fun nextAfterArgs() = nextAfterTestCases
    }

    @ParameterizedTest
    @MethodSource("nextToArgs")
    fun `test next to`(testCase: TestCase) {
        val (from, to, dst, expectedResult) = testCase
        val point = nextTo(from, to, dst)
        Assertions.assertEquals(expectedResult, point)
    }

    @ParameterizedTest
    @MethodSource("nextAfterArgs")
    fun `test next after`(testCase: TestCase) {
        val (from, to, dst, expectedResult) = testCase
        val point = nextAfter(from, to, dst)
        Assertions.assertEquals(expectedResult, point)
    }
}