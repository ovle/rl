package com.ovle.utils.gdx.math.dijkstraMap

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.GoalsMap
import com.ovle.utils.DijkstraMapGoal
import com.ovle.utils.DijkstraMapValue
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMap.Companion.UNINITIALIZED_MAP_VALUE
import com.ovle.utils.gdx.math.point.point

data class TestCase(
    val goal: DijkstraMapGoal,
    val size: Int,
    val obstacles: Collection<GridPoint2>,
    val expectedResult: Array2d<DijkstraMapValue>
)

private const val X = UNINITIALIZED_MAP_VALUE
val testCases = arrayOf(
    TestCase(
        goal = point(0, 0) to 0f,
        size = 3,
        obstacles = emptyList(),
        expectedResult = Array2d(
            arrayOf(
                0f, 1f, 2f,
                1f, 2f, 3f,
                2f, 3f, 4f,
            ), 3
        )
    ),
    TestCase(
        goal = point(0, 0) to 0f,
        size = 3,
        obstacles = listOf(
            point(1, 0),
            point(1, 1),
        ),
        expectedResult = Array2d(
            arrayOf(
                0f, X, 6f,
                1f, X, 5f,
                2f, 3f, 4f,
            ), 3
        )
    ),
    TestCase(
        goal = point(1, 0) to 0f,
        size = 3,
        obstacles = listOf(
            point(1, 1),
        ),
        expectedResult = Array2d(
            arrayOf(
                1f, 0f, 1f,
                2f, X, 2f,
                3f, 4f, 3f,
            ), 3
        )
    ),
    TestCase(
        goal = point(1, 0) to 0f,
        size = 3,
        obstacles = listOf(
            point(0, 1),
            point(1, 1),
            point(2, 1),
        ),
        expectedResult = Array2d(
            arrayOf(
                1f, 0f, 1f,
                X, X, X,
                X, X, X,
            ), 3
        )
    ),
)

data class LocalMinsTestCase(
    val map: Array2d<DijkstraMapValue>,
    val expectedResult: GoalsMap
)

val localMinsTestCases = arrayOf(
    LocalMinsTestCase(
        map = Array2d(
            arrayOf(
                0f,
            ), 1
        ),
        expectedResult = mapOf(point(0, 0) to 0f)
    ),
    LocalMinsTestCase(
        map = Array2d(
            arrayOf(
                0f, 1f, 2f,
                1f, 2f, 3f,
                2f, 3f, 4f,
            ), 3
        ),
        expectedResult = mapOf(point(0, 0) to 0f)
    ),
    LocalMinsTestCase(
        map = Array2d(
            arrayOf(
                0f, 1f, 2f,
                1f, 2f, 3f,
                2f, 3f, 2f,
            ), 3
        ),
        expectedResult = mapOf(
            point(0, 0) to 0f,
            point(2, 2) to 2f
        )
    )
)