package com.ovle.utils.event

import com.ovle.utils.event.EventBus.clearHooks
import com.ovle.utils.event.EventBus.clearSubscriptions
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource


class EventTest {

    private var counter = 0

    companion object {
        @JvmStatic
        fun args() = testCases
    }

    @AfterEach
    fun cleanup() {
        clearSubscriptions(this)
        clearHooks()
    }

    @ParameterizedTest
    @MethodSource("args")
    fun `test events`(testCase: TestCase) {
        val eventBus = EventBus
        eventBus.addHook { debugHook(it) }
        testCase.subscribe(eventBus, ::eventCallback)
        testCase.events.forEach { eventBus.send(it) }

        assertEquals(testCase.callTimes, counter)    //todo check method calls instead (see Mockito)
    }

    @Test
    fun `test priority`() {
        val l = mutableListOf<Int>()
        val eventBus = EventBus
        eventBus.subscribe<Event>(this) { l += 0 } //priority = 0
        eventBus.subscribe<Event>(this, false, 2) { l += 2 }
        eventBus.subscribe<Event>(this, false, 1) { l += 1 }

        eventBus.send(Event())

        assertEquals(0, l[0])
        assertEquals(1, l[1])
        assertEquals(2, l[2])
    }

    private fun debugHook(e: Event) {
        println("event called: $e")
    }

    private fun eventCallback() {
        counter++
    }
}