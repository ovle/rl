package com.ovle.utils.event

data class TestCase(
    val subscribe: (EventBus, () -> Unit) -> Unit,
    val events: Collection<Event>,
    val callTimes: Int
)

val testCases = arrayOf(
    //simple cases
    TestCase(
        subscribe = { eb, cb -> eb.subscribe<TestEvent>(TestCase::class) { cb.invoke() } },
        events = listOf(),
        callTimes = 0
    ),
    TestCase(
        subscribe = { _, _ ->  },
        events = listOf(TestEvent()),
        callTimes = 0
    ),
    TestCase(
        subscribe = { eb, cb -> eb.subscribe<TestEvent>(TestCase::class) { cb.invoke() } },
        events = listOf(TestEvent()),
        callTimes = 1
    ),

    //single event, single sub
    TestCase(
        subscribe = { eb, cb -> eb.subscribe<ChildEvent1>(TestCase::class) { cb.invoke() } },
        events = listOf(TestEvent()),
        callTimes = 0
    ),
    TestCase(
        subscribe = { eb, cb -> eb.subscribe<TestEvent>(TestCase::class) { cb.invoke() } },
        events = listOf(ChildEvent1()),
        callTimes = 0
    ),
    TestCase(
        subscribe = { eb, cb -> eb.subscribe<TestEvent>(TestCase::class, checkInheritance = true) { cb.invoke() } },
        events = listOf(ChildEvent1()),
        callTimes = 1
    ),
)