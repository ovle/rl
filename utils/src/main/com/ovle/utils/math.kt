package com.ovle.utils

import kotlin.math.roundToInt

fun Float.roundToClosestByAbsInt() = if (this > 0) this.roundToInt() else -((-this).roundToInt())

fun <T, U> cartesianProduct(c1: Collection<T>, c2: Collection<U>): List<Pair<T, U>> {
    return c1.flatMap { c1Item -> c2.map { c2Item -> c1Item to c2Item } }
}

fun <T, U> cartesianProduct(c1: Array<T>, c2: Array<U>): Array<Pair<T, U>> {
    return c1.flatMap { c1Item -> c2.map { c2Item -> c1Item to c2Item } }.toTypedArray()
}