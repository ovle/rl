package com.ovle.utils

import kotlin.random.Random
import java.util.Random as JRandom

data class RandomParams(
    val seed: Long = 1
) {

    val kRandom: Random by lazy { Random(seed) }
    val jRandom: JRandom by lazy { JRandom(seed) }

    /**
     *
     */
    fun chance(threshold: Double) = kRandom.nextDouble() < threshold
    fun chance(threshold: Float) = kRandom.nextFloat() < threshold
}