package com.ovle.utils

import com.badlogic.gdx.math.GridPoint2
import kotlin.random.Random

typealias GridValueCombinator = (Float, Float, Random) -> Float
typealias ValueCheck<T> = (GridPoint2, T) -> Boolean

typealias DijkstraMapValue = Float
typealias DijkstraMapReducer = (DijkstraMapValue, DijkstraMapValue) -> DijkstraMapValue
typealias GoalsMap = Map<GridPoint2, DijkstraMapValue>
typealias DijkstraMapGoal = Pair<GridPoint2, DijkstraMapValue>