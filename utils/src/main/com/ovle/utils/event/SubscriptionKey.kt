package com.ovle.utils.event

data class SubscriptionKey(val clazz: EventClass, val checkInheritance: Boolean)

fun subKey(clazz: EventClass, checkInheritance: Boolean = false) = SubscriptionKey(clazz, checkInheritance)