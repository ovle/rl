package com.ovle.utils.event

open class Event {
    var next: Event? = null

    fun then(next: Event?) = this.apply { this@Event.next = next }
}