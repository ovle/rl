package com.ovle.utils.event

import com.ovle.utils.AppOptions
import ktx.log.debug
import kotlin.reflect.KClass
import kotlin.reflect.full.allSuperclasses
import kotlin.system.measureTimeMillis

/**
 * simple pub / sub implementation
 * no async, no storage, should be used mostly for decoupling
 */
object EventBus {

    val subscribers = mutableMapOf<SubscriptionKey, MutableList<SubscriptionElement>>()
    private val hooks: MutableCollection<Hook> = mutableListOf()

    fun addHook(hook: Hook) {
        hooks.add(hook)
    }

    fun clearHooks() = hooks.clear()

    /**
     * Send event to all subscribers
     */
    fun send(event: Event) {
        hooks.forEach { it.invoke(event) }

        val clazz = event.javaClass.kotlin
        val directSubscribers = directSubscribers(clazz)
        val indirectSubscribers = indirectSubscribers(clazz)
        val subscribers = directSubscribers + indirectSubscribers

        subscribers.forEach {
            if (AppOptions.IS_PROFILE_MODE) {
                val t = measureTimeMillis {
                    it.callback.invoke(event)
                }
                if (t > 1) {
                    debug { "${it.subscriber?.javaClass?.simpleName} / ${event.javaClass.simpleName}: updateTime = $t" }
                }
            } else {
                it.callback.invoke(event)
            }
        }

        event.next?.let { send(it) }
    }

    /**
     * Subscribe on event of type [T]
     *
     * @param subscriber        subscriber object
     * @param checkInheritance  if the event will trigger on subclasses of T
     * @param priority          define order in which receivers will get event (ascending)
     * @param callback          subscriber code
     */
    inline fun <reified T : Event> subscribe(
        subscriber: Subscriber,
        checkInheritance: Boolean = false,
        priority: Priority = DEFAULT_PRIORITY,
        noinline callback: (T) -> Unit
    ) {
        val clazz = T::class as EventClass
        val key = subKey(clazz, checkInheritance)
        val eventSubscribers = (subscribers[key] ?: mutableListOf())
            .apply { subscribers[key] = this }

        val subscription = SubscriptionElement(priority, subscriber, callback as (Event) -> Unit)
        eventSubscribers.add(subscription)
        eventSubscribers.sort() //use sortedSet instead sort on every add
    }

    fun clearSubscriptions(
        subscriber: Subscriber,
    ) = subscribers.values.forEach {
        eventSubscribers ->
        eventSubscribers.removeIf { it.subscriber == subscriber }
    }


    private fun directSubscribers(clazz: KClass<Event>) =
        subscribers[subKey(clazz)] ?: listOf()

    private fun indirectSubscribers(clazz: EventClass): Collection<SubscriptionElement> {
        val classesToCheck = clazz.allSuperclasses + clazz
        return classesToCheck
            .map { it as EventClass }
            .flatMap { subscribers[subKey(it, true)] ?: listOf() }
    }
}