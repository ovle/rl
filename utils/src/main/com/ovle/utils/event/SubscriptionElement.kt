package com.ovle.utils.event

const val DEFAULT_PRIORITY = 0

data class SubscriptionElement(
    val priority: Priority = DEFAULT_PRIORITY,
    val subscriber: Subscriber? = null,
    val callback: (Event) -> Unit
): Comparable<SubscriptionElement> {

    override fun compareTo(other: SubscriptionElement): Int = priority.compareTo(other.priority)
}