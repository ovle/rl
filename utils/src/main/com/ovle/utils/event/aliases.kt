package com.ovle.utils.event

import kotlin.reflect.KClass

typealias Priority = Int
typealias Subscriber = Any
typealias EventClass = KClass<Event>
typealias Hook = (Event) -> Unit
