package com.ovle.utils.gdx.math.point

import com.badlogic.gdx.math.GridPoint2


fun GridPoint2.left() = point(x - 1, y)
fun GridPoint2.right() = point(x + 1, y)
fun GridPoint2.top() = point(x, y + 1)
fun GridPoint2.bottom() = point(x, y - 1)

fun GridPoint2.adj() = adjHV() + adjD()
fun GridPoint2.adjHV() = adjHV(x, y)
fun GridPoint2.adjD() = adjD(x, y)

private fun adjHV(x: Int, y: Int) = arrayOf(
    point(x - 1, y),
    point(x + 1, y),
    point(x, y - 1),
    point(x, y + 1)
)

private fun adjD(x: Int, y: Int) = arrayOf(
    point(x - 1, y - 1),
    point(x + 1, y + 1),
    point(x + 1, y - 1),
    point(x - 1, y + 1)
)

fun isAdj(x1: Int, y1: Int, x2: Int, y2: Int): Boolean {
    return (x1 in ((x2 - 1)..(x2 + 1)))
        && (y1 in ((y2 - 1)..(y2 + 1)))
}