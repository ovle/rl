package com.ovle.utils.gdx.math.rect

import com.ovle.utils.cartesianProduct

class RectAreasHelper {

    fun rectAreas(area: RectArea, size: Int): Collection<RectArea> {
        val xRange = (area.x1 until area.x2)
        val yRange = (area.y1 until area.y2)
        val hChunks = xRange.chunked(size)
        val vChunks = yRange.chunked(size)

        val areas = cartesianProduct(hChunks, vChunks)
            .map { (hc, vc) ->
                RectArea(
                    x1 = hc.first(), y1 = vc.first(),
                    x2 = hc.last(), y2 = vc.last()
                )
            }

        return areas
    }

    fun borders(areas: Collection<RectArea>): Collection<RectArea> {
        return areas.flatMap {
            listOf(
                RectArea(xs = it.x1..it.x1, ys = it.y1..it.y2 + 1),
                RectArea(xs = it.x2 + 1..it.x2 + 1, ys = it.y1..it.y2 + 1),
                RectArea(xs = it.x1..it.x2 + 1, ys = it.y1..it.y1),
                RectArea(xs = it.x1..it.x2 + 1, ys = it.y2 + 1..it.y2 + 1)
            )
        }
    }
}