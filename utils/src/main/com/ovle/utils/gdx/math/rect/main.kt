package com.ovle.utils.gdx.math.rect

import com.ovle.utils.gdx.math.point.point

fun main() {
    val helper = RectAreasHelper()
    val width = 16
    val height = 16
    val areas = helper.rectAreas(
        RectArea(0, 0, width, height), 4
    )

    for (i in (0 until width)) {
        for (j in (0 until height)) {
            val p = point(i, j)
            val a = areas.find { p in it.points }
            val ai = areas.indexOf(a)
            val c = if (ai == -1) "." else Char(65 + ai)
            print(c)
        }
        println()
    }
}