package com.ovle.utils.gdx.math.discretization.bresenham

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.point

fun circle(center: GridPoint2, radius: Int, type: DiscreetCircleType) = type.calc(center, radius)

fun filledCircle(center: GridPoint2, radius: Int, type: DiscreetCircleType = DiscreetCircleType.DISTANCE_BASED) = circle(center, radius, type).fill()

//todo o(n2), use queue instead
private fun Collection<GridPoint2>.fill(): Collection<GridPoint2> {
    val minX = minOf { it.x }
    val maxX = maxOf { it.x }
    val minYs = (minX..maxX)
        .associateWith { x -> filter { it.x == x }.minOf { it.y } }
    val maxYs = (minX..maxX)
        .associateWith { x -> filter { it.x == x }.maxOf { it.y } }

    return (minX..maxX).flatMap { x ->
        val minY = minYs.getValue(x)
        val maxY = maxYs.getValue(x)

        (minY..maxY).map { y -> point(x, y) }
    }
}
