package com.ovle.utils.gdx.math.discretization.bresenham

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import com.ovle.utils.gdx.math.point.point
import kotlin.math.abs
import kotlin.math.sign


/**
 * bresenham line
 */
fun line(fromPosition: GridPoint2, toPosition: GridPoint2, isAllowDiagonals: Boolean = false): List<GridPoint2> {
    if (fromPosition == toPosition) {
        return listOf(fromPosition.cpy())
    }

    val (fromX, fromY) = fromPosition
    val (toX, toY) = toPosition
    val diffX = abs(toX - fromX)
    val diffY = abs(toY - fromY)
    val stepX = (toX - fromX).sign
    val stepY = (toY - fromY).sign

    var error = diffX - diffY
    var x = fromX
    var y = fromY
    val result = mutableListOf(point(x, y))

    while (x != toX || y != toY) {
        val errorX2 = error * 2
        val incY = errorX2 > -diffY
        val incX = errorX2 < diffX

        if (diffY > diffX) {
            if (incY) {
                error -= diffY
                x += stepX;
            }
            if (incX && (!incY || isAllowDiagonals)) {
                error += diffX
                y += stepY;
            }
        } else {
            if (incX) {
                error += diffX
                y += stepY;
            }
            if (incY && (!incX || isAllowDiagonals)) {
                error -= diffY
                x += stepX;
            }
        }

        result += point(x, y)
    }

    return result
}

