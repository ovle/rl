package com.ovle.utils.gdx.math

import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle
import com.ovle.utils.cartesianProduct
import com.ovle.utils.gdx.math.point.point

fun Float.between(v1: Float, v2: Float): Boolean {
    check(v2 > v1)

    return v1 < this && this <= v2
}

fun Int.between(v1: Int, v2: Int): Boolean {
    check(v2 > v1)

    return this in (v1 + 1)..v2
}

fun Int.hourBetween(v1: Int, v2: Int): Boolean {
    return if (v1 < v2) this in (v1 + 1)..v2
        else this !in (v2 + 1)..v1
}

//todo use Intersector
fun rectangle(x1: Int, y1: Int, x2: Int, y2: Int) = Rectangle(
    x1.toFloat(), y1.toFloat(), (x2 - x1).toFloat(), (y2 - y1).toFloat()
)

fun rectangle(p: GridPoint2) = Rectangle(p.x.toFloat(), p.y.toFloat(), 0.0f, 0.0f)

fun Rectangle.rangeX() = (x.toInt()..x.toInt() + width.toInt())
fun Rectangle.rangeY() = (y.toInt()..y.toInt() + height.toInt())

fun Rectangle.points() = cartesianProduct(rangeX().toList(), rangeY().toList())
    .map { (x, y) -> point(x, y) }

fun Rectangle.borders(): Collection<GridPoint2> {
    val h = height.toInt()
    val w = width.toInt()
    val x1 = x.toInt()
    val y1 = y.toInt()
    val x2 = x1 + w
    val y2 = y1 + h

    return (rangeX().flatMap { listOf(point(it, y1), point(it, y2)) } +
        rangeY().flatMap { listOf(point(x1, it), point(x2, it)) }).toSet()
}

fun Rectangle.innerPoints() = points() - borders().toSet()

fun Rectangle.overlapsInclusive(r: Rectangle) =
    x <= r.x + r.width && x + width >= r.x && y <= r.y + r.height && y + height >= r.y


fun linear(add: Float = 0.0f, mul: Float = 1.0f) = { x: Float -> add + mul * x }

fun reverseLinear(add: Float = 1.0f, mul: Float = 1.0f) = { x: Float -> 1.0f / (add + mul * x) }