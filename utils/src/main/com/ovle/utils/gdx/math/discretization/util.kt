package com.ovle.utils.gdx.math.discretization

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.point

fun Collection<GridPoint2>.toStringWithParams(
    stringify: (GridPoint2, Float) -> String,
    getValue: (GridPoint2) -> Float = { p -> if (p in this) 1.0f else 0.0f }
): String {
    if (isEmpty()) return ""

    val xs = this.map { it.x }
    val ys = this.map { it.y }
    val xRange = xs.minOrNull()!!..xs.maxOrNull()!!
    val yRange = ys.minOrNull()!!..ys.maxOrNull()!!

    val result = mutableListOf<String>()
    xRange.forEach { i ->
        val resultRow = mutableListOf<String>()
        yRange.forEach { j ->
            val p = point(i, j)
            val value = getValue.invoke(p)

            resultRow += stringify(p, value)
        }
        result += resultRow.joinToString(" ")
    }

    return result.joinToString("\n")
}