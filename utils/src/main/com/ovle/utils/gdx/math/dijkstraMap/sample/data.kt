package com.ovle.utils.gdx.math.dijkstraMap.sample

import com.ovle.utils.gdx.math.point.point

val samples = arrayOf(
    SampleMap(
        tilesStr = """
           _ _ _ _
           _ _ _ _
           _ _ _ _
           _ _ _ _
     """,
        goals = listOf(point(0, 0))
    ),
    SampleMap(
        tilesStr = """
           # # # #
           # # # #
           # # # #
           # # # #
     """,
        goals = listOf(point(0, 0))
    ),
    SampleMap(
        tilesStr = """
           _ _ _ _
           _ _ _ _
           _ _ _ _
           _ _ _ _
     """,
        goals = listOf(point(0, 0), point(1, 3))
    ),
    SampleMap(
        tilesStr = """
           _ _ _ _
           _ _ _ _
           _ _ _ _
           _ _ _ _
     """,
        goals = listOf(
            point(0, 0), point(3, 3), point(0, 3), point(3, 0)
        )
    ),
    SampleMap(
        tilesStr = """
           _ _ # _
           _ _ # _
           # # # _
           _ _ _ _
     """,
        goals = listOf(point(0, 0))
    ),
    SampleMap(
        tilesStr = """
           _ _ _ _ _ _ _ _
           _ _ _ _ _ _ _ _
           _ _ # # # # # #
           _ _ _ _ _ _ _ _
           _ _ _ _ _ _ _ _
           _ _ # # # # # #
           _ _ _ _ _ _ _ _
           _ _ _ _ _ _ _ _
     """,
        goals = listOf(point(4, 0))
    ),
    SampleMap(
        tilesStr = """
           _ _ _ _ _ _ _ _
           _ _ _ _ _ _ _ _
           _ _ # # # # # _
           _ _ _ _ _ _ _ _
           _ _ _ _ _ _ _ _
           _ _ _ _ _ _ _ _
           _ _ _ _ _ _ _ _
           _ _ _ _ _ _ _ _
     """,
        goals = listOf(point(6, 1)),
        bodySize = 2
    ),
    SampleMap(
        tilesStr = """
           # # # # # # # # # # # # # 
           # # # # # # # # # # # _ # 
           # # # # # # # _ _ _ # _ # 
           # # # # # # # _ # _ # _ # 
           # # # # # # # _ # _ # _ # 
           # # # # # # # _ # _ _ _ # 
           # # # # # # # _ # # # _ # 
           # # # # # # # _ # # # _ # 
           # # # # # # # _ # # # _ # 
           # # # # # # # _ # # # _ # 
           # # # # # # # _ # # # _ # 
           # # # # # # # _ # # # _ # 
           # # # # # # # # # # # # # 
     """,
        goals = listOf(point(11, 1))
    ),
)