package com.ovle.utils.gdx.math.array2d


fun normalize(array: Array2d<Float>, newMinValue: Float = 0.0f, newMaxValue: Float = 1.0f) {
    val maxValue = array.data.maxOrNull()!!
    val minValue = array.data.minOrNull()!!
    val size = array.size
    val diff = maxValue - minValue
    val newDiff = newMaxValue - newMinValue

    for (i in 0 until size) {
        for (j in 0 until size) {
            //move
            var normalizedValue = array[i, j] - minValue
            //squeeze / stretch
            normalizedValue = normalizedValue * newDiff / diff
            //move
            normalizedValue += newMinValue

            array[i, j] = normalizedValue
        }
    }
}
