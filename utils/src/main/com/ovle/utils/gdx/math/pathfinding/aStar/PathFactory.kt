package com.ovle.utils.gdx.math.pathfinding.aStar

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.*
import com.ovle.utils.gdx.math.distance
import com.ovle.utils.gdx.math.point.adjHV
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import com.ovle.utils.gdx.math.point.point

/*

class PathFactory {

    companion object {

        data class Params(
            val from: GridPoint2,
            val to: GridPoint2,
            val tiles: TileArray,

            val ignorePoints: Collection<GridPoint2> = emptySet(),
            val onlyPoints: Collection<GridPoint2> = emptySet(),

            val heuristicsFn: MoveCostFn2 = { p1, p2 -> distance(p1, p2) },
            val costFn: MoveCostFn = { _, _ -> 1 },
        )

        data class Result(
            val path: List<GridPoint2>,
            val success: Boolean
        )
    }

    fun path(params: Params): Result {
        val (from, to, tiles, ignorePoints, onlyPoints, heuristicsFn, costFn) = params

        val open = mutableSetOf(from)
        val closed = mutableSetOf<GridPoint2>()
        val costFromStart = mutableMapOf(from to 0)
        val estimatedTotalCost = mutableMapOf(from to heuristicsFn(from, from))
        val cameFrom = mutableMapOf<GridPoint2, GridPoint2>()

        var currentPosition: GridPoint2? = null
        while (open.isNotEmpty()) {
            currentPosition = open.minByOrNull(estimatedTotalCost::getValue)!!
            val (x, y) = currentPosition
            val currentTile = tiles[x, y]
            if (to == currentPosition) {
                val resultPath = path(currentPosition, cameFrom)
                return Result(resultPath, true)
            }

            open -= currentPosition
            closed += currentPosition

            val nearValues = currentPosition.adjHV()
                .filterNot { it in closed }

            for (neighbour in nearValues) {
                val (nX, nY) = neighbour
                if (!tiles.isValid(neighbour)) continue

                val nPoint = point(nX, nY)
                if (nPoint in ignorePoints) continue
                if (onlyPoints.isNotEmpty() && nPoint !in onlyPoints) continue

                val neighbourTile = tiles[nX, nY]
                val nextCost = costFn(currentTile, neighbourTile)
                val score = costFromStart.getValue(currentPosition) + nextCost
                if (score >= costFromStart.getOrDefault(neighbour, Int.MAX_VALUE)) continue

                val totalScore = score + heuristicsFn(currentPosition, neighbour)

                open += neighbour
                cameFrom[neighbour] = currentPosition
                costFromStart[neighbour] = score
                estimatedTotalCost[neighbour] = totalScore
            }
        }

        val failedPath = if (currentPosition == null) listOf() else path(currentPosition, cameFrom)
        return Result(failedPath, false)
    }


    private fun path(end: GridPoint2, cameFrom: Map<GridPoint2, GridPoint2>): List<GridPoint2> {
        val path = mutableListOf(end)
        var current = end
        while (cameFrom.containsKey(current)) {
            current = cameFrom.getValue(current)
            path.add(0, current)
        }
        return path.toList()
    }
}*/
