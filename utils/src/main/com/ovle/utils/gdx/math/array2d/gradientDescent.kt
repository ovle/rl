package com.ovle.utils.gdx.math.array2d

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.DijkstraMapValue
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMap
import com.ovle.utils.gdx.math.point.cpy
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.math.point.shift

/**
 * @return next point of gradient descent, adjacent to the [body]
 * values to compare is the sum of values for each point of [body] candidates
 *  null if [body] is at local max/min area
 */
fun Array2d<DijkstraMapValue>.gradientDescent(body: Collection<GridPoint2>): GridPoint2? {
    fun pointsSum(points: Collection<GridPoint2>) = points.sumOf { this[it.x, it.y].toDouble() }

    val candidates = listOf(
        point(-1, 0),
        point(1, 0),
        point(0, -1),
        point(0, 1)
    )

    val candidateToPoints = candidates.associateWith { body.cpy().shift(it) }
        .filter { isAllAccessible(it.value) }
    val pointsToSums = candidateToPoints.keys.associateWith { pointsSum(candidateToPoints[it]!!) }
    val curSum = pointsSum(body)

    val resultPoints = candidateToPoints.minByOrNull { pointsToSums[it.key]!! } ?: return null
    val result = resultPoints.key
    val resultPointsSum = pointsToSums[result]!!

    if (resultPointsSum >= curSum) return null

    return result
}

/**
 * @return is goal accessible from all the [points]
 */
private fun Array2d<DijkstraMapValue>.isAllAccessible(points: Collection<GridPoint2>) =
    points.all { isValid(it)
        && this[it.x, it.y] != DijkstraMap.UNINITIALIZED_MAP_VALUE }
