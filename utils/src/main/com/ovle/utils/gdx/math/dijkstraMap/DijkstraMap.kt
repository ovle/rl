package com.ovle.utils.gdx.math.dijkstraMap

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.DijkstraMapValue
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.discretization.toStringWithParams
import com.ovle.utils.gdx.math.point.point
import java.text.DecimalFormat


/**
 * simple dijkstra map implementation
 *
 * @property goals      points for which the scan will be performed
 * @property data        underlying gradient map
 */
@Suppress("MemberVisibilityCanBePrivate")
class DijkstraMap(
    val goals: Collection<GridPoint2>,
    val data: Array2d<DijkstraMapValue>
) {

    companion object {
        /**
         * value of points that has never been initialized during the scan (walls / inaccessible tiles)
         */
        const val UNINITIALIZED_MAP_VALUE: DijkstraMapValue = Float.MAX_VALUE
    }

    /**
     * print map values to console. use whitespaces for inaccessible points
     */
    override fun toString(): String {
        val displayedGoals = goals.map { point(it.y, it.x) }
        val formatter = DecimalFormat("#00.0").apply { positivePrefix = " " }
        val stringifier = { p: GridPoint2, v: Float ->
            if (v != UNINITIALIZED_MAP_VALUE) {
                val numberStr = formatter.format(v)
                if (p in displayedGoals) "!$numberStr" else " $numberStr"
            } else "   .  "
        }

        val getValue: (GridPoint2) -> Float = { data[it.y, it.x] }

        return "\ngradient map (goals: ${goals}):\n" +
                data.points.toStringWithParams(stringifier, getValue)
    }
}