package com.ovle.utils.gdx.math.dijkstraMap

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.array2d.Array2d.Companion.array
import com.ovle.utils.gdx.math.point.allInRange


/**
 * general-purpose factory for [DijkstraMap]
 */
@Suppress("MemberVisibilityCanBePrivate")
class DijkstraMapFactory {

    companion object {
        /**
         * value of points that has never been initialized during the scan (walls / inaccessible tiles)
         */
        const val GOAL_VALUE: Float = 0f
    }

    /**
     * create new dijkstra map of [mapSize], performing scan for each of [goals], ignore [inaccessiblePoints] and inaccessible tiles
     *
     * @return collection of maps for each of the [goals], reduced using [minValueReducer] to single map
     */
    fun dijkstraMap(
        mapSize: Int,
        bodySize: Int = 1,
        goals: Collection<GridPoint2>,
        inaccessiblePoints: Collection<GridPoint2> = setOf()
    ): DijkstraMap {
        require(mapSize > 0) { "mapSize should be positive" }
        require(bodySize > 0) { "bodySize should be positive" }
        require(goals.isNotEmpty()) { "goals shouldn't be empty" }
        require(goals.allInRange(0 until mapSize)) { "goals should be in map's range" }

        if (inaccessiblePoints.containsAll(goals)) {
            println("no accessible goals!")
            val inaccessibleMap = array(DijkstraMap.UNINITIALIZED_MAP_VALUE, mapSize)
            return DijkstraMap(goals, inaccessibleMap)
        }

        val accessibleGoals = goals - inaccessiblePoints.toSet()
        val goalsToValue = accessibleGoals.toSet().associateWith { GOAL_VALUE }
        val map = scanGoals(goalsToValue, inaccessiblePoints, mapSize)

        return DijkstraMap(goals, map)
    }
}