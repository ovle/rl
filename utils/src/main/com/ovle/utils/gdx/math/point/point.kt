package com.ovle.utils.gdx.math.point

import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2
import com.ovle.utils.gdx.math.discretization.bresenham.line
import com.ovle.utils.roundToClosestByAbsInt

object GridPointCache {
    val points = mutableMapOf<Int, GridPoint2>()

    private const val PRIME = 373
    fun get(x: Int, y: Int): GridPoint2 {
        return GridPoint2(x, y)
//        if (x >= PRIME || y >= PRIME) return GridPoint2(x, y)
//
//        val k = PRIME * x + y
//        return points.computeIfAbsent(k) { GridPoint2(x, y) }
    }
}

//private val gridPointPool: Pool<GridPoint2> = object : Pool<GridPoint2>() {
//    override fun newObject(): GridPoint2 {
//        return GridPoint2()
//    }
//}

operator fun GridPoint2.component1() = x
operator fun GridPoint2.component2() = y

fun point(x: Int = 0, y: Int = 0) = GridPointCache.get(x, y)
fun point(x: Float = 0.0f, y: Float = 0.0f) = GridPointCache.get(x.roundToClosestByAbsInt(), y.roundToClosestByAbsInt())
fun point(floatPoint: Vector2) = point(floatPoint.x, floatPoint.y)
fun point(point: GridPoint2) = point(point.x, point.y)

fun GridPoint2.isInSize(size: Int) = x in (0 until size) && y in (0 until size)


fun nextAfter(
    position: GridPoint2,
    afterPosition: GridPoint2,
    dst: Int = 1,
    isAllowDiagonals: Boolean = false,
): GridPoint2? {
    check(dst >= 0) { "dst should be >= 0" }

    val diff = afterPosition.cpy().sub(position)
    val toPosition = afterPosition.cpy().add(diff)

    return nextTo(afterPosition, toPosition, dst, isAllowDiagonals)
}

fun nextTo(
    position: GridPoint2,
    toPosition: GridPoint2,
    dst: Int = 1,
    isAllowDiagonals: Boolean = false,
): GridPoint2? {
    check(dst >= 0) { "dst should be >= 0" }

    val line = line(position, toPosition, isAllowDiagonals)
    if (line.size <= dst) return null

    return line[dst]
}