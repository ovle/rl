package com.ovle.utils.gdx.math.rect

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.point
import kotlin.math.max
import kotlin.math.min

data class RectArea(
    val x1: Int, val y1: Int,
    val x2: Int, val y2: Int,
) {

//    init {
//        check(x1 <= x2) { "x2 < x1 for $this" }
//        check(y1 <= y2) { "y2 < y1 for $this" }
//    }

    constructor(xs: IntRange, ys: IntRange): this(
        x1 = xs.first, y1 = ys.first, x2 = xs.last, y2 = ys.last
    )

    val width: Int = (x2 - x1) + 1

    val height: Int = (y2 - y1) + 1

    val size: Int = width * height

    val isSquare: Boolean = width == height

    val origin: GridPoint2 = point(x1, y1)

    val center: GridPoint2 = point((x1 + x2).toFloat() / 2, (y1 + y2).toFloat() / 2)


    val points: Collection<GridPoint2> by lazy {
        (x1..x2).flatMap { x ->
            (y1..y2).map { y ->
                point(x, y)
            }
        }
    }

    operator fun contains(p: GridPoint2) = p.x in x1..x2 && p.y in y1..y2


    fun isAtLeastOfSize(minSize: Int): Boolean =
        width >= minSize && height >= minSize

    fun isAllowMerge(ra: RectArea?): Boolean {
        if (ra == null) return false
        val isV = x1 == ra.x1 && x2 == ra.x2
        val isH = y1 == ra.y1 && y2 == ra.y2
        return isH || isV
    }

    fun merge(ra: RectArea): RectArea {
        check(isAllowMerge(ra))

        val isV = x1 == ra.x1 && x2 == ra.x2
        val isH = y1 == ra.y1 && y2 == ra.y2

        return when {
            isV -> RectArea(
                x1 = x1, y1 = min(y1, ra.y1),
                x2 = x2, y2 = max(y2, ra.y2)
            )
            isH -> RectArea(
                x1 = min(x1, ra.x1), y1 = y1,
                x2 = max(x2, ra.x2), y2 = y2
            )
            else -> throw IllegalArgumentException("cant merge $this with $ra")
        }
    }

/*
    fun grow(isX: Boolean = true, isY: Boolean = true) = RectArea(
        if (isX) x1-1 else x1,
        if (isY) y1-1 else y1,
        if (isX) x2+1 else x2,
        if (isY) y2+1 else y2
    )

    fun shrink(isX: Boolean = true, isY: Boolean = true) = RectArea(
        if (isX && width > 2) x1+1 else x1,
        if (isY && height > 2) y1+1 else y1,
        if (isX && width > 2) x2-1 else x2,
        if (isY && height > 2) y2-1 else y2
    )

    fun splitX(x: Int) = when (x) {
        x2 -> arrayOf(this)
        else -> arrayOf(
            RectArea(x1, y1, x, y2), RectArea(x + 1, y1, x2, y2)
        )
    }

    fun splitY(y: Int) = arrayOf(
        RectArea(x1, y1, x2, y), RectArea(x1, y + 1, x2, y2)
    )*/
}