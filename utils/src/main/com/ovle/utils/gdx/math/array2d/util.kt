package com.ovle.utils.gdx.math.array2d

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.adjHV
import com.ovle.utils.gdx.math.point.*
import java.text.DecimalFormat

/**
 * @return map of local minimums (key = point, value = map value of the point)
 */
fun <T: Comparable<T>> Array2d<T>.localMins(
    isPointUsable: (GridPoint2) -> Boolean = { true }
): Map<GridPoint2, T> {
    val result = points
        .filter { isPointUsable(it) }
        .filter {
            val currentPoint = this[it.x, it.y]
            it.adjHV().all { adjP ->
                val (x, y) = adjP
                !isValid(adjP) || this[x, y] >= currentPoint
            }
        }
    return result.associateWith { this[it.x, it.y] }
}

/**
 * parse [tilesStr] to [Array2d] of [TileChar]
 * works only for square maps!
 * @return parsed tiles
 */
fun parse(tilesStr: String?): Array2d<Char>? {
    if (tilesStr == null) return null

    val list = tilesStr.trim()
        .split("\n")
        .map {
            it.trim().split(" ")
                .map { chars -> chars[0] }
        }

    val size = list.size
    check(list[0].size == size) { "represented map should be square-shaped" }

    return Array2d(list.flatten().toTypedArray(), size)
}

fun <T> Array2d<T>.print(emptyValue: T) {
    val pattern = "#00.0"
    val formatter = DecimalFormat(pattern).apply { positivePrefix = " " }

    println()
    val s = this.size
    (0 until s).forEach { i ->
        println()
        (0 until s).forEach { j ->
            val v = this[j, s - i - 1]
            val vStr = if (v == emptyValue) "     "
                else formatter.format(v)
            print("$vStr ")
        }
    }
    println()
}