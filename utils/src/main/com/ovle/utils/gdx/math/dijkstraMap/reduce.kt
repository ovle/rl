package com.ovle.utils.gdx.math.dijkstraMap

import com.ovle.utils.DijkstraMapReducer
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.Array2d.Companion.array
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMap.Companion.UNINITIALIZED_MAP_VALUE
import kotlin.math.min

val minValueReducer: DijkstraMapReducer = { v1, v2 -> min(v1, v2) }

/**
 * reduce given collection of maps to one map, using given reducer
 */
fun Collection<Array2d<Float>>.reduceMaps(
    mapSize: Int, reducer: DijkstraMapReducer
): Array2d<Float> {
    val result = array(UNINITIALIZED_MAP_VALUE, mapSize)
    val dataSize = mapSize * mapSize
    forEach { map ->
        check(map.size <= mapSize)

        (0 until dataSize).forEach {
            result.data[it] = reducer.invoke(result.data[it], map.data[it])
        }
    }
    return result
}