package com.ovle.utils.gdx.math.array2d

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.isInSize
import com.ovle.utils.gdx.math.point.point

class Array2d<T>(
    val data: Array<T>,
    val size: Int
) {

    companion object {

        inline fun <reified T> array(init: T, size: Int) = Array2d(
            Array(size * size) { init }, size
        )

        inline fun <reified T> array(data: Collection<Collection<T>>): Array2d<T> {
            val size = data.size
            val a = data.flatten().toTypedArray()
            return Array2d(a, size)
        }
    }

    constructor(other: Array2d<T>) : this(other.data.copyOf(), other.size)

    val points: Collection<GridPoint2> by lazy {
        data.mapIndexed { index, _ -> point(index) }
    }

    fun index(x: Int, y: Int) = size * y + x
    fun point(index: Int) = point(
        index % size,
        index / size
    )

    operator fun get(index: Int): T = data[index]
    operator fun set(index: Int, el: T) { data[index] = el }

    operator fun get(x: Int, y: Int): T = get(index(x, y))
    operator fun set(x: Int, y: Int, el: T) = set(index(x, y), el)

    fun set(points: Collection<GridPoint2>, el: T) =
        points.filter { isValid(it) }
            .forEach { set(it.x, it.y, el) }

    fun getOrNull(p: GridPoint2): T? = if (isValid(p)) get(index(p.x, p.y)) else null

    fun indexedElements() = data.mapIndexed { index, el -> index to el }

    fun isValid(point: GridPoint2) = point.isInSize(size)

    fun check(p: GridPoint2, c: (T) -> Boolean) = isValid(p) && c(this[p.x, p.y])

    fun matches(mask: Array2d<T>, point: GridPoint2, matcher: (T, T) -> Boolean): Boolean {
        if (mask.size == 0) return true
        if (mask.size > size) return false

        val matchedPoints = mask.points.map { it.cpy().add(point) }
        if (matchedPoints.any { !isValid(it) }) return false

        val matchedValues = matchedPoints.map { p -> this[p.x, p.y] }
        val maskValues = mask.data

        return matchedValues.zip(maskValues)
            .all { (v, mv) -> matcher(v, mv) }
    }


    fun count(check: (T) -> Boolean): Int = data.count {
        check(it)
    }

    fun filter(check: (T) -> Boolean): Collection<GridPoint2> = points.filter {
        val value = this[it.x, it.y]
        check(value)
    }

    inline fun <reified R> map(mapper: (T) -> R) =
        Array2d(data.map { mapper.invoke(it) }.toTypedArray(), size)
}