package com.ovle.utils.gdx.math

import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2

fun vec2(point: GridPoint2) = Vector2(point.x.toFloat(), point.y.toFloat())

operator fun Vector2.component1() = x
operator fun Vector2.component2() = y