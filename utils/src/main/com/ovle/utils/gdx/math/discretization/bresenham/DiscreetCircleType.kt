package com.ovle.utils.gdx.math.discretization.bresenham

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.isAtDistance
import com.ovle.utils.gdx.math.point.point

enum class DiscreetCircleType {
    DISTANCE_BASED {
        override fun calc(center: GridPoint2, radius: Int): Collection<GridPoint2> {
            val result = mutableSetOf<GridPoint2>()
            val xRange = (center.x - radius..center.x + radius)
            val yRange = (center.y - radius..center.y + radius)
            xRange.forEach { x ->
                yRange.forEach { y ->
                    val p = point(x, y)
                    if (isAtDistance(center, p, radius, radius)) {
                        result += p
                    }
                }
            }
            return result
        }
    },
    NUMERICAL {
        override fun calc(center: GridPoint2, radius: Int): Collection<GridPoint2> {
            if (radius == 0) return setOf(center)

            val result = mutableSetOf<GridPoint2>()
            val cX = center.x
            val cY = center.y
            var d = ((5 - radius * 4) / 4.0f)
            var x = 0
            var y = radius

            do {
                //fill cross-wise
                result.addAll(
                        arrayOf(
                                point(cX + x, cY + y),
                                point(cX + x, cY - y),
                                point(cX - x, cY + y),
                                point(cX - x, cY - y),

                                point(cX + y, cY + x),
                                point(cX + y, cY - x),
                                point(cX - y, cY + x),
                                point(cX - y, cY - x)
                        )
                )
                //rotate
                if (d < 0) {
                    d += 2 * x + 1
                } else {
                    d += 2 * (x - y) + 1
                    y--
                }
                x++

            } while (x <= y)

            return result
        }
    };

    abstract fun calc(center: GridPoint2, radius: Int): Collection<GridPoint2>
}