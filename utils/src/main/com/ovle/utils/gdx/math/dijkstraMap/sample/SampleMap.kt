package com.ovle.utils.gdx.math.dijkstraMap.sample

import com.badlogic.gdx.math.GridPoint2

data class SampleMap(val tilesStr: String, val goals: Collection<GridPoint2>, val bodySize: Int = 1)