package com.ovle.utils.gdx.math.array2d

import com.ovle.utils.cartesianProduct
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.math.rect.RectArea

@Suppress("ReplaceRangeToWithUntil")
class ArrayValidationHelper {

    fun <T> validAreas(
        width: Int,
        height: Int,
        array: Array2d<T>,
        filter: (T) -> Boolean
    ): Collection<RectArea> {
        val result = mutableSetOf<RectArea>()
        val minX = 0
        val maxX = array.size - width
        val minY = height - 1
        val maxY = array.size - 1

        for (x in minX..maxX) {
            for (y in minY..maxY) {
                val x2 = x + (width - 1)
                val y2 = y - (height - 1)
                val isValid = isValidArea(x, x2, y, y2, array, filter)
                if (isValid) {
                    result += RectArea(x1 = x, y1 = y2, x2 = x2, y2 = y)
                }
            }
        }

        return result
    }

    fun <T> isValidArea(
        x: Int, x2: Int, y: Int, y2: Int,
        array: Array2d<T>, filter: (T) -> Boolean
    ): Boolean {
        val checkpointsX = arrayOf(x, x2)
        val checkpointsY = arrayOf(y, y2)
        val checkpoints = cartesianProduct(checkpointsX, checkpointsY)

        return checkpoints.all { (cx, cy) ->
            array.isValid(point(cx, cy)) && filter.invoke(array[cx, cy])
        }
    }
}