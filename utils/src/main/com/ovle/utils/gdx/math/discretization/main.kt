package com.ovle.utils.gdx.math.discretization

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType.DISTANCE_BASED
import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType.NUMERICAL
import com.ovle.utils.gdx.math.discretization.bresenham.circle
import com.ovle.utils.gdx.math.discretization.bresenham.line
import com.ovle.utils.gdx.math.discretization.bresenham.square
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.noise4j.grid.asciiDensityStringifier


fun main() {
    val center = point(0, 0)

    println("\nCircle (radius = 1, type = dst):")
    val circle = circle(center, 1, DISTANCE_BASED)
    print(circle)
    println("\nCircle (radius = 1, type = num):")
    print(circle(center, 1, NUMERICAL))

    println("\nCircle (radius = 3, type = dst):")
    print(circle(center, 3, DISTANCE_BASED))
    println("\nCircle (radius = 3, type = num):")
    print(circle(center, 3, NUMERICAL))

    println("\nCircle (radius = 8, type = dst):")
    print(circle(center, 8, DISTANCE_BASED))
    println("\nCircle (radius = 8, type = num):")
    print(circle(center, 8, NUMERICAL))

    println("\nsquare (size = 1):")
    print(square(center, 1))
    println("\nsquare (size = 3):")
    print(square(center, 3))
    println("\nsquare (size = 4):")
    print(square(center, 4))

    println("\nline 1:")
    print(line(point(0, 0), point(3, 3)))
    println("\nline 2:")
    print(line(point(0, 0), point(4, 4)))
    println("\nline 3:")
    print(line(point(0, 0), point(3, 4)))
    println("\nline 4:")
    print(line(point(0, 0), point(1, 5)))
    println("\nline 5:")
    print(line(point(0, 0), point(5, 1)))
    println("\nline 6:")
    print(line(point(0, 0), point(5, 0)))
    println("\nline 7:")
    print(line(point(-5, -4), point(0, 0)))
    println("\nline 8:")
    print(line(point(0, 0), point(-5, -4)))
}

private fun print(shape: Collection<GridPoint2>) {
    println(shape.toStringWithParams(::asciiDensityStringifier))
}