package com.ovle.utils.gdx.math

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.cartesianProduct
import kotlin.math.abs


private fun distanceHV(p1: GridPoint2, p2: GridPoint2) = abs(p1.x - p2.x) + abs (p1.y - p2.y)

/**
 * main function(others rely on it) to calc distance for distorted space where diag dst is 2x hor/vert dst
 */
fun distance(p1: GridPoint2, p2: GridPoint2) = distanceHV(p1, p2)
//fun distance(p1: GridPoint2, p2: GridPoint2) = p1.dst(p2).roundToClosestByAbsInt()

fun distances(ps1: Collection<GridPoint2>, ps2: Collection<GridPoint2>) = cartesianProduct(ps1, ps2)
    .map { distance(it.first, it.second) }

fun isAtDistance(p1: GridPoint2, p2: GridPoint2, minDistance: Int? = 1, maxDistance: Int? = 1): Boolean {
    val dst = distance(p1, p2)
    val minCheck = minDistance?.let { dst >= it } ?: true
    val maxCheck = maxDistance?.let { dst <= it } ?: true

    return minCheck && maxCheck
}

fun closestPoints(from: Collection<GridPoint2>, to: Collection<GridPoint2>) =
    cartesianProduct(from, to).minByOrNull { distance(it.first, it.second) }

fun minDistance(from: Collection<GridPoint2>, to: Collection<GridPoint2>) = distances(from, to).minOrNull()