package com.ovle.utils.gdx.math.lineOfSight.rayTracing

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.*
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType.*
import com.ovle.utils.gdx.math.discretization.bresenham.filledCircle
import com.ovle.utils.gdx.math.discretization.bresenham.line


fun hasLos(fromPosition: GridPoint2, toPosition: GridPoint2, obstacles: Collection<GridPoint2>): Boolean {
    val trace = trace(fromPosition, toPosition, obstacles)
    return trace.last() == toPosition
}

fun trace(fromPosition: GridPoint2, toPosition: GridPoint2, obstacles: Collection<GridPoint2>): List<GridPoint2> {
    val positions = line(fromPosition, toPosition)
    val result = positions.takeWhile { it !in obstacles }.toMutableList()
    //add last to see the walls
    if (result.size < positions.size) result += positions[result.size]
//    return positions.toList()
    return result
}