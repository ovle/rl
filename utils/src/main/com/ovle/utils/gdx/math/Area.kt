package com.ovle.utils.gdx.math

import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle

//todo remove?
//todo use Intersector
data class Area(
    val rectangles: MutableSet<Rectangle> = mutableSetOf()
) {

    constructor(r: Rectangle): this(mutableSetOf(r))

    val points: Collection<GridPoint2>
        get() = rectangles.flatMap { it.points() }.distinct()


    fun touches(r: Rectangle): Boolean {
        return rectangles.any { it.overlapsInclusive(r) || it.contains(r) || r.contains(it) }
    }

    operator fun contains(p: GridPoint2): Boolean {
        return rectangles.any { it.contains(p.x.toFloat(), p.y.toFloat()) }
    }

    operator fun plus(other: Area) {
        rectangles += other.rectangles
    }

    fun borders(): Collection<GridPoint2> {
        val directions = Direction.values()
        val result = directions.flatMap { borders(it, 1) } + directions.flatMap { borders(it, -1) }
        return result.distinct()
    }

    fun borders(d: Direction, delta: Int): Collection<GridPoint2> {
        val rectBorders = rectangles.flatMap { it.borders() }
        val result = rectBorders.filter { p ->
            val p2 = d.plus(p, delta)
            p2 !in points
        }
        return result.distinct()
    }
}