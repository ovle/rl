package com.ovle.utils.gdx.math.array2d

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.DijkstraMapGoal
import com.ovle.utils.DijkstraMapValue
import com.ovle.utils.gdx.math.point.adjHV
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import com.ovle.utils.gdx.math.point.isInSize
import com.ovle.utils.gdx.math.point.point


/**
 * Get separated subareas of area with size [size]
 * 'separation' defined by the [skipPoints] list
 * @see [floodFill]
 */
fun areas(size: Int, skipPoints: Collection<GridPoint2>): Collection<Collection<GridPoint2>> {
    val areas = mutableListOf<Collection<GridPoint2>>()
    val isValid: (GridPoint2) -> Boolean = { it !in skipPoints }

    for (x in (0 until size)) {
        for (y in (0 until size)) {
            val point = point(x, y)
            if (areas.any { point in it }) continue

            if (point !in skipPoints) {
                val newArea = floodFill(point, size, null, isValid)
                areas.add(newArea)
            }
        }
    }

    return areas
}

/**
 * Flood fill of area with size [size] started from point
 * @return filled area as collection of points with size up to [maxAreaSize]
 */
fun floodFill(
    startPoint: GridPoint2, size: Int, maxAreaSize: Int? = null, isValid: (GridPoint2) -> Boolean
): Collection<GridPoint2> {
    val areaPoints = mutableSetOf<GridPoint2>()
    val nextPoints = mutableSetOf(startPoint)

    while (nextPoints.isNotEmpty()) {
        val point = nextPoints.first()
        nextPoints.remove(point)

        if (!point.isInSize(size)) continue
        if (!isValid(point)) continue
        if (point in areaPoints) continue

        areaPoints.add(point)
        if (maxAreaSize != null && areaPoints.size >= maxAreaSize) break

        nextPoints.addAll(point.adjHV())
    }

    return areaPoints
}


/**
 * flood fill map with gradient values
 *
 * @param goal  start point. it is possible for it to be excluded,
 *      but only if the flood fill may be started from the adjacent point
 * @param skipPoints     points excluded from being processed
 * @param map   map to be filled
 */
fun gradientFloodFill(
    goal: DijkstraMapGoal, skipPoints: Collection<GridPoint2>, map: Array2d<DijkstraMapValue>
) {
    val (goalPoint, goalValue) = goal
    var current: GridPoint2
    val oldGoalPointValue = map[goalPoint.x, goalPoint.y]
    val open = linkedSetOf(goalPoint)
    val closed = skipPoints.toMutableSet()
    val cameFromMap = mutableMapOf<GridPoint2, GridPoint2>()

    while (open.isNotEmpty()) {
        current = open.first()
        val (x, y) = current

        val cameFromPoint = cameFromMap[current]
        val value = if (cameFromPoint == null) goalValue else map[cameFromPoint.x, cameFromPoint.y] + 1

        val isNeedProcess = map[x, y] > value
        if (isNeedProcess) {
            map[x, y] = value

            val adjPoints = current.adjHV()
                .filter {
                    map.isValid(it) && (it !in closed)
                }
            open += adjPoints
            adjPoints.forEach { cameFromMap[it] = current }
        }

        open -= current
        closed += current
    }

    val isNeedRestoreGoalValue = goalPoint in skipPoints
    if (isNeedRestoreGoalValue) {
        map[goalPoint.x, goalPoint.y] = oldGoalPointValue
    }
}