package com.ovle.utils.gdx.math.discretization.bresenham

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.cartesianProduct
import com.ovle.utils.gdx.math.point.point

fun square(origin: GridPoint2, edgeSize: Int): Collection<GridPoint2> {
    if (edgeSize == 1) return listOf(origin)

    val p1 = point(origin.x, origin.y)
    val d = edgeSize - 1
    val p2 = point(origin.x + d, origin.y)
    val p3 = point(origin.x, origin.y + d)
    val p4 = point(origin.x + d, origin.y + d)

    return quadrangle(p1, p2, p3, p4)
}

fun possibleSquaresForPoint(x: Int, y: Int, size: Int): Collection<Collection<GridPoint2>> {
    val xRange = x - (size - 1) until x + size
    val yRange = y - (size - 1) until y + size

    val xChunks = xRange.windowed(size)
    val yChunks = yRange.windowed(size)

    val result = xChunks.flatMap { xc ->
        yChunks.map { yc ->
            cartesianProduct(xc, yc).map {
                point(it.first, it.second)
            }
        }
    }
    return result
}