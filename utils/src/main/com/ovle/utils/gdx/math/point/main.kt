package com.ovle.utils.gdx.math.point


fun main() {
    val points = listOf(
        point(0, 0),
        point(1, 0),
        point(1, 1)
    )
    val shiftedPoints = points.cpy().shift(point(1, -2))

    println("points: $points")
    println("shifted: $shiftedPoints")
    println("inner borders: ${points.allBorders()}")
    println("outer borders: ${points.allOuterBorders()}")
}