package com.ovle.utils.gdx.math.point

import com.badlogic.gdx.math.GridPoint2
import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.cartesianProduct
import com.ovle.utils.gdx.math.Direction

//todo use Intersector
fun points(x: Int, ys: IntRange) = points(x..x, ys)
fun points(xs: IntRange, y: Int) = points(xs, y..y)
fun points(xs: IntRange, ys: IntRange) = cartesianProduct(xs.toList(), ys.toList())
    .map { (x, y) -> point(x, y) }

fun Collection<GridPoint2>.inRange(range: IntRange) = filter { it.x in range && it.y in range }
fun Collection<GridPoint2>.anyInRange(range: IntRange) = inRange(range).isNotEmpty()
fun Collection<GridPoint2>.allInRange(range: IntRange) = inRange(range).size == this.size

fun Collection<GridPoint2>.cpy() = this.map { it.cpy() }
fun Collection<GridPoint2>.shift(point: GridPoint2) = onEach { it.add(point) }

fun Collection<GridPoint2>.applyToGrid(grid: Grid, marker: Float) {
    forEach { p ->
        grid.set(p.x, p.y, marker)
    }
}