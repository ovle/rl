package com.ovle.utils.gdx.math.discretization.bresenham

import com.badlogic.gdx.math.GridPoint2

fun quadrangle(p1: GridPoint2, p2: GridPoint2, p3: GridPoint2, p4: GridPoint2) =
    (line(p1, p2) + line(p3, p4) + line(p1, p3) + line(p2, p4)).toSet()