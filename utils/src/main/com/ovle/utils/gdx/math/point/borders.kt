package com.ovle.utils.gdx.math.point

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.Direction


fun Collection<GridPoint2>.allBorders() = (
    borders(Direction.H, 1) + borders(Direction.H, -1) +
    borders(Direction.V, 1) + borders(Direction.V, -1))
    .distinct()

fun Collection<GridPoint2>.allOuterBorders() = (
    outerBorders(Direction.H, 1) + outerBorders(Direction.H, -1) +
    outerBorders(Direction.V, 1) + outerBorders(Direction.V, -1))
    .distinct()

fun Collection<GridPoint2>.allInnerBorders() = (
    innerBorders(Direction.H, 1) + innerBorders(Direction.H, -1) +
    innerBorders(Direction.V, 1) + innerBorders(Direction.V, -1))
    .distinct()


fun Collection<GridPoint2>.outerBorders(d: Direction, delta: Int): Collection<GridPoint2> {
    val result = this.filter { p ->
        val p2 = d.plus(p, delta)
        p2 !in this
    }
    return result.map { d.plus(it, delta) }
}

fun Collection<GridPoint2>.borders(d: Direction, delta: Int): Collection<GridPoint2> {
    val result = this.filter { p ->
        val p2 = d.plus(p, delta)
        p2 !in this
    }
    return result.distinct()
}

fun Collection<GridPoint2>.innerBorders(d: Direction, delta: Int): Collection<GridPoint2> {
    val result = this.filter { p ->
        val p2 = d.plus(p, delta)
        p2 !in this
    }
    return result.map { d.plus(it, -delta) }
}
