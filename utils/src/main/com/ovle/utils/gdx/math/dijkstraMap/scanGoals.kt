package com.ovle.utils.gdx.math.dijkstraMap

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.DijkstraMapValue
import com.ovle.utils.GoalsMap
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.Array2d.Companion.array
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.math.array2d.gradientFloodFill
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMap.Companion.UNINITIALIZED_MAP_VALUE
import kotlin.system.measureTimeMillis

/**
 * creates gradient map for new/rescanned dijkstra maps
 * more low-level compared to [DijkstraMapFactory.dijkstraMap]
 *
 * @return scanned gradient map
 */
fun scanGoals(
    goalsMap: GoalsMap,
    inaccessiblePoints: Collection<GridPoint2>,
    mapSize: Int,
    initArray: Array2d<DijkstraMapValue> = array(UNINITIALIZED_MAP_VALUE, mapSize)
): Array2d<Float> {
    return goalsMap.entries.fold(initArray) { acc, it ->
        gradientFloodFill(it.key to it.value, inaccessiblePoints, acc)
        acc
    }
}


fun main() {
    val mapSize = 128
    val goalsMap = (0 until mapSize).flatMap {
        x ->
        (0 until mapSize).map { y -> point(x, y) }
    }.associateWith { 0.0f }

    val time = measureTimeMillis { scanGoals(goalsMap, listOf(), mapSize) }

    println("goals count: ${goalsMap.size}, map size: ${mapSize}: $time millis")
}