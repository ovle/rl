package com.ovle.utils.gdx.math.array2d

import com.ovle.utils.ValueCheck


fun main() {
    val array = Array2d(arrayOf(
        ' ',' ','#','#',' ',
        ' ',' ',' ','#',' ',
        '#',' ',' ',' ','#',
        ' ',' ',' ','#',' ',
        ' ',' ',' ','#',' ',
    ), 5)

    val check: ValueCheck<Char> = { _, v -> v == ' ' }
    val skipPoints = array.points.filterNot { check.invoke(it, array[it.x, it.y]) }

    println("------ areas: -------")
    val areas1 = areas(array.size, skipPoints)
    areas1.forEach { println(it.joinToString()) }
}