package com.ovle.utils.gdx.math

import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2

enum class Direction {
    H {
        override fun compare(p1: GridPoint2, p2: GridPoint2) = p1.x.compareTo(p2.x)
        override fun equals(p1: GridPoint2, p2: GridPoint2) = p1.x == p2.x
        override fun plus(p: GridPoint2, v: Int): GridPoint2 = p.cpy().add(v, 0)
        override fun plus(p: Vector2, v: Float): Vector2 = p.cpy().add(v, 0.0f)
    },
    V {
        override fun compare(p1: GridPoint2, p2: GridPoint2) = p1.y.compareTo(p2.y)
        override fun equals(p1: GridPoint2, p2: GridPoint2) = p1.y == p2.y
        override fun plus(p: GridPoint2, v: Int): GridPoint2 = p.cpy().add(0, v)
        override fun plus(p: Vector2, v: Float): Vector2 = p.cpy().add(0.0f, v)
    };

    abstract fun compare(p1: GridPoint2, p2: GridPoint2): Int
    abstract fun equals(p1: GridPoint2, p2: GridPoint2): Boolean
    abstract fun plus(p: GridPoint2, v: Int): GridPoint2
    abstract fun plus(p: Vector2, v: Float): Vector2
}
