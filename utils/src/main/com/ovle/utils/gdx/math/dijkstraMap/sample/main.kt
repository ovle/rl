package com.ovle.utils.gdx.math.dijkstraMap.sample

import com.ovle.utils.gdx.math.array2d.parse
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMapFactory


fun main() {
    samples.forEach { printMap(it) }
}


private fun printMap(map: SampleMap) {
    val tiles = parse(map.tilesStr)!!
    val emptyTile = '_'
//    val wallTile = '#'
    val walls = tiles.points.filter { tiles[it.x, it.y] != emptyTile }
    val factory = DijkstraMapFactory()

    val goals = map.goals
    val dijkstraMap = factory.dijkstraMap(tiles.size, map.bodySize, goals, walls)

    println("---------------------------------------------------------------------------------------------------------")

    println("\nDIJKSTRA MAP, GOALS: $goals")
    println(dijkstraMap.toString())

//    println("\nFLEE DIJKSTRA MAP, INITIAL GOALS: $goals")
//    println(fleeMap(dijkstraMap).toString())
}