package com.ovle.utils.gdx

import com.badlogic.gdx.Gdx

class Preferences(val appKey: String) {

    private fun getString(key: String, defaultValue: String? = null) =
        preferences().getString(key, defaultValue)

    private fun setString(key: String, value: String) =
        preferences().putString(key, value)

    fun flush() {
        preferences().flush()
    }

    private fun preferences() = Gdx.app.getPreferences(appKey)
}