package com.ovle.utils.gdx.ashley.component
import com.badlogic.ashley.core.Component

interface BaseComponent: Component {
    val isModel: Boolean
        get() = true
}