package com.ovle.utils.gdx.ashley.component

import com.badlogic.ashley.core.ComponentMapper

inline fun <reified T : BaseComponent> mapper(): ComponentMapper<T> = ComponentMapper.getFor(T::class.java)
