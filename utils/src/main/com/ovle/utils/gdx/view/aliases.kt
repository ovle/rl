package com.ovle.utils.gdx.view

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureRegion


typealias TextureRegions = Array<Array<TextureRegion>>
typealias Palette = Array<Color>