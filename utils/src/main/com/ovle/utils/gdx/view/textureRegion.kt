package com.ovle.utils.gdx.view

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion


fun textureRegion(regions: TextureRegions, x: Int, y: Int): TextureRegion = regions[y][x] //switched

/**
 * create TextureRegion
 *
 * @param texture       region texture
 * @param x             x index (in tiles)
 * @param y             y index (in tiles)
 * @param tileSize      tile size
 * @param regionSize    region size (in tiles)
 */
fun textureRegion(texture: Texture, x: Int, y: Int, tileSize: Int, regionSize: Int = 1): TextureRegion {
    val size = tileSize * regionSize
    return TextureRegion(texture, x * tileSize, y  * tileSize, size, size)
}