package com.ovle.utils

import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
import java.io.Serializable


fun Serializable.bytes(): ByteArray {
    val bos = ByteArrayOutputStream()
    val oos = ObjectOutputStream(bos)
    oos.writeObject(this)
    oos.flush()
    return bos.toByteArray()
}

