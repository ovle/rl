package com.ovle.utils

import kotlin.math.roundToInt
import kotlin.random.Random


fun IntRange.random(r: Random) = (r.nextDouble() * (this.last - this.first)).roundToInt() + this.first

fun Int.withChance(chance: Float, r: Random): Int = if (r.nextDouble() <= chance) this else 0
fun Float.withChance(chance: Float, r: Random): Float = if (r.nextDouble() <= chance) this else 0.0f

fun <T> T.withChance(chance: Float, r: Random, defaultValue: T) = if (r.nextFloat() <= chance) this else defaultValue

fun List<Float>.chancedIndex(chance: Float): Int? {
    var result = 0
    var c = chance
    for (value in this) {
        c -= value
        if (c < 0) break

        result++
    }
    return if (result >= this.size) null else result
}

/**
 * should be used for mandatory selection based on weights
 */
fun Collection<Float>.weightedIndex(checkValue: Float): Int? {
    val sum = this.sum()
    val normValues = this.map { (it / sum) }

    return normValues.chancedIndex(checkValue)
}