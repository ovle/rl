package com.ovle.utils

fun <E: Enum<E>> E.next(): E {
    val enumConstants = this.declaringJavaClass.enumConstants
    val nextOrdinal = if (this.ordinal == enumConstants.size - 1) 0 else this.ordinal + 1
    return enumConstants[nextOrdinal]
}

fun <E: Enum<E>> E.prev(): E {
    val enumConstants = this.declaringJavaClass.enumConstants
    val nextOrdinal = if (this.ordinal == 0) enumConstants.size - 1 else this.ordinal - 1
    return enumConstants[nextOrdinal]
}