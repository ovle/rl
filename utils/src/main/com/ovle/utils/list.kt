package com.ovle.utils


fun <E> List<E>.firstAfter(element: E?): E {
    check(this.isNotEmpty())

    if (size == 1) return single()
    if (element == null) return first()
    if (element == last()) return first()

    return this[indexOf(element) + 1]
}