package com.ovle.utils.noise4j.grid.factory.path

import com.badlogic.gdx.math.GridPoint2

typealias GridPath = Collection<GridPoint2>
