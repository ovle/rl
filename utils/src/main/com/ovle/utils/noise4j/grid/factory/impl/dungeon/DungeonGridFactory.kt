package com.ovle.utils.noise4j.grid.factory.impl.dungeon

import com.github.czyzby.noise4j.map.Grid
import com.github.czyzby.noise4j.map.generator.room.RoomType
import com.github.czyzby.noise4j.map.generator.room.dungeon.DungeonGenerator
import com.github.czyzby.noise4j.map.generator.util.Generators
import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.factory.GridFactory
import com.ovle.utils.noise4j.grid.factory.GridFactoryParams


class DungeonGridFactory(params: Params): GridFactory<DungeonGridFactory.Companion.Params>(params) {

    companion object {
        /**
         * @param roomTypes possible room types
         * @param maxRoomSize max size of the rooms
         * @param minRoomSize min size of the rooms
         * @param tolerance maximum difference between room's width and height.
         * @param windingChance chance to wind the currently generated corridor in range of 0 to 1
         * @param randomConnectorChance chance of a random carved cell between two regions (rooms and corridors) in range of 0 to 1
         */
        class Params(
            size: Int,
            val roomTypes: Array<RoomType>,
            val maxRoomSize: Int,
            val minRoomSize: Int,
            val tolerance: Int = 2,
            val windingChance: Float = 0.25f,
            val randomConnectorChance: Float = 0.05f
        ) : GridFactoryParams(size)

        const val WALL_THRESHOLD = 1.0f
        const val FLOOR_THRESHOLD = 0.6f
        const val CORRIDOR_THRESHOLD = 0.2f
    }

    override fun create(random: RandomParams): Grid {
        val size = params.size
        val grid = Grid(size)
        val generator = DungeonGenerator.getInstance()
        Generators.setRandom(random.jRandom)

        generator.apply {
            params.roomTypes.forEach { addRoomType(it) }

            roomGenerationAttempts = 100
            maxRoomSize = params.maxRoomSize
            minRoomSize = params.minRoomSize
            tolerance = params.tolerance

            wallThreshold = WALL_THRESHOLD
            floorThreshold = FLOOR_THRESHOLD
            corridorThreshold = CORRIDOR_THRESHOLD

            windingChance = params.windingChance
            randomConnectorChance = params.randomConnectorChance
        }.generate(grid)

        return grid
    }
}