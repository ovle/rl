package com.ovle.utils.noise4j.grid.factory.impl.nop

import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.factory.GridFactory
import com.ovle.utils.noise4j.grid.factory.GridFactoryParams


class NopGridFactory(params: Params): GridFactory<NopGridFactory.Companion.Params>(params) {

    companion object {
        class Params(
            size: Int, val value: Float
        ) : GridFactoryParams(size)
    }

    override fun create(random: RandomParams): Grid {
        val size = params.size
        return Grid(params.value, size, size)
    }
}