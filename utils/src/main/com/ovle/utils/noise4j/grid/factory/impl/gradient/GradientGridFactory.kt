package com.ovle.utils.noise4j.grid.factory.impl.gradient

import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.factory.GridFactory
import com.ovle.utils.noise4j.grid.factory.GridFactoryParams
import com.ovle.utils.noise4j.grid.normalize
import kotlin.math.abs


class GradientGridFactory(params: Params): GridFactory<GradientGridFactory.Companion.Params>(params) {

    companion object {
        class Params(
            size: Int,
            val startValue: Float = 0.0f,
            val endValue: Float = 1.0f,
            var isMirrored: Boolean = true,
            val isHorizontal: Boolean = false
        ) : GridFactoryParams(size)
    }

    override fun create(random: RandomParams): Grid {
        val size = params.size
        val result = Grid(size)

        processArea(result)
        normalize(result)

        return result
    }

    private fun processArea(grid: Grid) {
        val width = grid.width
        val height = grid.height
        var averageStep: Float = abs(params.endValue - params.startValue) / height
        val isMirrored = params.isMirrored

        if (isMirrored) {
            averageStep *= 2f
        }

        for (i in 0 until width) {
            for (j in 0 until height) {
                grid.setValue(i, j, averageStep * j)
                if (isMirrored) {
                    grid.setValue(i, height - j - 1, averageStep * j)
                    val isHalf = j >= height / 2
                    if (isHalf) {
                        break
                    }
                }
            }
        }
    }

    private fun Grid.setValue(i: Int, j: Int, value: Float) {
        when {
            params.isHorizontal -> this[i, j] = value
            else -> this[j, i] = value
        }
    }
}