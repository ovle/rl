package com.ovle.utils.noise4j.grid

import com.github.czyzby.noise4j.map.Grid


fun normalize(grid: Grid, newMinValue: Float = 0.0f, newMaxValue: Float = 1.0f) {
    val maxValue = grid.array.maxOrNull()!!
    val minValue = grid.array.minOrNull()!!
    val width = grid.width
    val diff = maxValue - minValue
    val newDiff = newMaxValue - newMinValue

    for (i in 0 until width) {
        val height = grid.height
        for (j in 0 until height) {
            //move
            var normalizedValue = grid[i, j] - minValue
            //squeeze / stretch
            normalizedValue = normalizedValue * newDiff / diff
            //move
            normalizedValue += newMinValue

            grid[i, j] = normalizedValue
        }
    }
}
