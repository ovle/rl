package com.ovle.utils.noise4j.grid.factory.path.impl

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.adj
import com.ovle.utils.gdx.math.point.adjHV
import com.ovle.utils.noise4j.grid.factory.path.GridPath
import kotlin.random.Random

class RandomPathFactory {

    fun path(
        start: GridPoint2,
        startLastPoint: GridPoint2,
        gridSize: Int,
        length: Int,
        isAllowDiag: Boolean,
        isSelfIntersect: Boolean,
        r: Random
    ): GridPath {
        val result = mutableSetOf<GridPoint2>()

        var point = start
        var lastPoint: GridPoint2 = startLastPoint

        while (true) {
            result += point
            if (result.size >= length) break

            val allAdjPoints = if (isAllowDiag) point.adj() else point.adjHV()
            val adjPoints = allAdjPoints
                .filter {
                    it != lastPoint
                        && it.x in 0 until gridSize
                        && it.y in 0 until gridSize
                }
                .filter {
                    isSelfIntersect
                        || (it !in result && it.adjHV().all { a -> a == point || a !in result})
                }
            if (adjPoints.isEmpty()) break
            val newPoint = adjPoints.random(r)

            lastPoint = point
            point = newPoint
        }

        return result
    }
}