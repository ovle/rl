package com.ovle.utils.noise4j.grid.factory.impl.randomPath

import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.adjHV
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.noise4j.grid.factory.GridFactory
import com.ovle.utils.noise4j.grid.factory.GridFactoryParams
import com.ovle.utils.noise4j.grid.factory.path.GridPath
import com.ovle.utils.noise4j.grid.factory.path.impl.RandomPathFactory


class RandomPathGridFactory(
    params: Params,
    private val pathFactory: RandomPathFactory
): GridFactory<RandomPathGridFactory.Companion.Params>(params) {

    companion object {

        class Params(
            size: Int,
            val emptyValue: Float,
            val pathValue: Float,
            val countRange: IntRange,
            val lengthRange: IntRange,
            val isAllowDiag: Boolean = true,
            val isSelfIntersect: Boolean = true,
        ) : GridFactoryParams(size)
    }

    override fun create(random: RandomParams): Grid {
        val r = random.kRandom
        val size = params.size
        val emptyValue = params.emptyValue
        val grid = Grid(emptyValue, size, size)

        val paths = mutableListOf<GridPath>()
        val count = params.countRange.random(r)

        repeat(count) {
            //todo no length garanties here
            val length = params.lengthRange.random(r)
            val startX = (0 until grid.width).random(r)
            val startY = (0 until grid.height).random(r)
            val start = point(startX, startY)
            val startLastPoint = start.adjHV().random(r)

            paths += pathFactory.path(
                start = start,
                startLastPoint = startLastPoint,
                gridSize = grid.width,
                length = length,
                isAllowDiag = params.isAllowDiag,
                isSelfIntersect = params.isSelfIntersect,
                r = r
            )
        }

        val pathValue = params.pathValue
        paths.flatten().forEach {
            grid[it.x, it.y] = pathValue
        }

        return grid
    }
}