package com.ovle.utils.noise4j.grid.factory.impl.fractal

import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.noise4j.grid.factory.GridFactory
import com.ovle.utils.noise4j.grid.factory.GridFactoryParams
import com.ovle.utils.noise4j.grid.isPointValid
import com.ovle.utils.noise4j.grid.normalize
import com.ovle.utils.random
import kotlin.math.abs
import kotlin.random.Random


class FractalGridFactory(params: Params): GridFactory<FractalGridFactory.Companion.Params>(params) {

    companion object {
        /**
         * @property startIteration             when to start (values will be random on previous iterations)
         * @property initialBorderValues        initial grid in case of fractal use (usually from the grid of larger scale)
         * @property stopIteration              when to stop, debug-only
         * @property noise                      area size based noise
         * @property mediumDiffNoise            (cur value diff with [mediumValue]) based noise
         * @property mediumValue                base value for [mediumDiffNoise]
         * @property isNormalized               need normalize at the final
         * @property isDebug                    enable debug output
         */
        class Params(
            size: Int,
            val startIteration: IntRange = 1..1,
            val initialBorderValues: Array<FloatArray>? = null,
            val stopIteration: Int = -1,
            val noise: Float = 1.0f,
            val mediumDiffNoise: Float = 0.0f,
            val mediumValue: Float = 0.5f,
            val isNormalized: Boolean = false,
            val isDebug: Boolean = false
        ) : GridFactoryParams(size)


        private const val MIN_AREA_SIZE = 3

        private const val TILE_NOT_INITIALIZED_ID = 0.5f
    }

    private val isToroidal: Boolean
        get() = params.initialBorderValues == null


    override fun create(random: RandomParams): Grid {
        val r = random.kRandom
        val size = params.size
        val result = Grid(TILE_NOT_INITIALIZED_ID, size, size)

        if (params.initialBorderValues != null) {
            initMapWithBorderValues(result, params.initialBorderValues)
        } else {
            initMap(result, r)
        }

        diamondSquare(result, r)

        if (params.isNormalized) {
            normalize(result)
        }

        return result
    }

    private fun initMap(grid: Grid, random: Random) {
        val width = grid.width - 1
        val height = grid.height - 1

        grid[0, 0] = random.nextFloat()
        grid[width, 0] = random.nextFloat()
        grid[0, height] = random.nextFloat()
        grid[width, height] = random.nextFloat()
    }

    private fun initMapWithBorderValues(grid: Grid, initialBorderValues: Array<FloatArray>) {
        val width = grid.width - 1
        val height = grid.height - 1
        val halfWidth = width / 2
        val halfHeight = height / 2

        grid[0, 0] = initialBorderValues[0][0]
        grid[0, height] = initialBorderValues[0][2]
        grid[width, 0] = initialBorderValues[2][0]
        grid[width, height] = initialBorderValues[2][2]

        grid[0, halfHeight] = initialBorderValues[0][1]
        grid[halfWidth, 0] = initialBorderValues[1][0]
        grid[width, halfHeight] = initialBorderValues[2][1]
        grid[halfWidth, height] = initialBorderValues[1][2]

        grid[halfWidth, halfHeight] = initialBorderValues[1][1]
    }


    private fun diamondSquare(grid: Grid, random: Random) {
        var currCellWidth = grid.width
        var currCellHeight = grid.height
        val startIteration = params.startIteration.random(random)
        var iterationsCount = 0

        while (true) {
            val minWidthReached = currCellWidth < MIN_AREA_SIZE
            val minHeightReached = currCellHeight < MIN_AREA_SIZE
            val shouldStop = minWidthReached && minHeightReached
            if (shouldStop) break
            if (iterationsCount == params.stopIteration) break

            val cellsInGridRow = grid.width / (currCellWidth - 1)
            val cellsInGridColumn = grid.height / (currCellHeight - 1)
            val useRandomFilling = iterationsCount < startIteration

            if (params.isDebug) {
                println("\niteration: $iterationsCount\n")
            }

            if (params.isDebug) println("DIA $currCellWidth X $currCellHeight")
            for (i in 0 until cellsInGridRow) {
                for (j in 0 until cellsInGridColumn) {
                    val x = (currCellWidth - 1) * i
                    val y = (currCellHeight - 1) * j
                    val middleX = x + currCellWidth / 2
                    val middleY = y + currCellHeight / 2
                    val maxX = x + currCellWidth - 1
                    val maxY = y + currCellHeight - 1

                    if (params.isDebug) println("       [$x, $y, $maxX, $maxY]")

                    if (!useRandomFilling) {
                        setValue(grid, middleX, middleY, false, false, currCellWidth, currCellHeight, random)
                    } else {
                        initArea(grid, x, y, currCellWidth, currCellHeight, random)
                    }
                }
            }

            if (params.isDebug) println("SQR $currCellWidth X $currCellHeight")
            for (i in 0 until cellsInGridRow) {
                for (j in 0 until cellsInGridColumn) {
                    val x = (currCellWidth - 1) * i
                    val y = (currCellHeight - 1) * j
                    val middleX = x + currCellWidth / 2
                    val middleY = y + currCellHeight / 2
                    val maxX = x + currCellWidth - 1
                    val maxY = y + currCellHeight - 1

                    if (params.isDebug) println("       [$x, $y, $maxX, $maxY]")

                    if (!useRandomFilling) {
                        setValue(grid, middleX, y, false, true, currCellWidth, currCellHeight, random)
                        setValue(grid, middleX, maxY, false, true, currCellWidth, currCellHeight, random)
                        setValue(grid, x, middleY, true, false, currCellWidth, currCellHeight, random)
                        setValue(grid, maxX, middleY, true, false, currCellWidth, currCellHeight, random)
                    } else {
                        val cellInitWidth = currCellWidth / 2 + 1
                        val cellInitHeight = currCellHeight / 2 + 1

                        initArea(grid, x, y, cellInitWidth, cellInitHeight, random)
                        initArea(grid, middleX, y, cellInitWidth, cellInitHeight, random)
                        initArea(grid, x, middleY, cellInitWidth, cellInitHeight, random)
                        initArea(grid, middleX, middleY, cellInitWidth, cellInitHeight, random)
                    }
                }
            }

            iterationsCount++
            currCellWidth = currCellWidth / 2 + 1
            currCellHeight = currCellHeight / 2 + 1
        }
    }

    private fun initArea(grid: Grid, areaX: Int, areaY: Int, areaWidth: Int, areaHeight: Int, random: Random) {
        val width = areaWidth - 1
        val height = areaHeight - 1
        val maxX = areaX + width
        val maxY = areaY + height

        if (!grid.isInitialized(areaX, areaY)) {
            val value = random.nextFloat()
            grid[areaX, areaY] = value
            if (isToroidal) {
                if (areaX == 0) {
                    grid[grid.width - 1, areaY] = value
                }
                if (areaY == 0) {
                    grid[areaX, grid.height - 1] = value
                }
            }
        }
        if (!grid.isInitialized(maxX, areaY)) {
            val value = random.nextFloat()
            grid[maxX, areaY] = value
            if (isToroidal) {
                if (areaY == 0) {
                    grid[maxX, grid.height - 1] = value
                }
            }
        }
        if (!grid.isInitialized(areaX, maxY)) {
            val value = random.nextFloat()
            grid[areaX, maxY] = value
            if (isToroidal) {
                if (areaX == 0) {
                    grid[grid.width - 1, maxY] = value
                }
            }
        }
        if (!grid.isInitialized(maxX, maxY)) {
            grid[maxX, maxY] = random.nextFloat()
        }
    }

    private fun setValue(
        grid: Grid,
        x: Int, y: Int,
        isBorderX: Boolean, isBorderY: Boolean,
        cellWidth: Int, cellHeight: Int,
        random: Random
    ) {
        //corner points stay unaffected
        if (isBorderX && isBorderY) return
        //if already have value - return
        if (grid.isInitialized(x, y)) return

        val isCenterPoint = !isBorderX && !isBorderY
        val filledAdjValues = filledAdjValues(x, y, cellWidth, cellHeight, isCenterPoint, grid)

        if (filledAdjValues.isNotEmpty()) {
            val rawValue = filledAdjValues.average().toFloat()
            val noiseValue = totalNoise(rawValue, cellWidth, grid.width, random)
            val value = rawValue + noiseValue

            grid[x, y] = value

            if (isToroidal) {
                if (x == 0) grid[grid.width - 1, y] = value
                if (y == 0) grid[x, grid.height - 1] = value
            }
        }
    }


    private fun filledAdjValues(
        x: Int, y: Int,
        cellWidth: Int, cellHeight: Int,
        isCenterPoint: Boolean,
        grid: Grid
    ): List<Float> {
        val halfWidth = cellWidth / 2
        val halfHeight = cellHeight / 2
        val leftX = x - halfWidth
        val rightX = x + halfWidth
        val bottomY = y + halfHeight
        val topY = y - halfHeight

        val adjValues =
            if (isCenterPoint) listOf(
                point(leftX, topY), point(leftX, bottomY),
                point(rightX, topY), point(rightX, bottomY)
            ) else listOf(
                point(x, topY), point(x, bottomY),
                point(leftX, y), point(rightX, y)
            )

        return adjValues
            .filter {
                grid.isPointValid(it.x, it.y) && grid.isInitialized(it.x, it.y)
            }.map { grid[it.x, it.y] }
    }

    private fun totalNoise(
        rawValue: Float, cellWidth: Int, gridWidth: Int, random: Random
    ): Float {
        val randomLimit = random.nextInt(cellWidth) - cellWidth / 2
        val randomOffset = randomLimit.toFloat() / gridWidth.toFloat()
        val mediumNoise = abs(rawValue - params.mediumValue) * params.mediumDiffNoise

        return randomOffset * (params.noise + mediumNoise)
    }

    private fun Grid.isInitialized(x: Int, y: Int) = this[x, y] != TILE_NOT_INITIALIZED_ID
}