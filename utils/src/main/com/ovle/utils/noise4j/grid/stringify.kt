package com.ovle.utils.noise4j.grid

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.roundToClosestByAbsInt

fun nopStringifier(p: GridPoint2, v: Float) = v.toString()
fun numStringifier(p: GridPoint2, v: Float) = (10 * v).roundToClosestByAbsInt().toString()

fun asciiDensityStringifier(p: GridPoint2, v: Float) = when {
    v > 0.9f -> "@"
    v > 0.8f -> "%"
    v > 0.7f -> "#"
    v > 0.6f -> "*"
    v > 0.5f -> "+"
    v > 0.4f -> "="
    v > 0.3f -> "-"
    v > 0.2f -> ":"
    v > 0.1f -> "."
    else -> " "
}

fun defaultStringifier(p: GridPoint2, v: Float) = when (v) {
    1.0f -> "X"
    0.0f -> "0"
    else -> "."
}
