package com.ovle.utils.noise4j.grid.factory

import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.asciiDensityStringifier
import com.ovle.utils.noise4j.grid.print


fun main() {
    val seed = 612L
    val random = RandomParams(seed)
/*
    println("\nCelullar Automaton:")
    caFactory.create(random).print(::asciiDensityStringifier)
    println("\nNop:")
    nopFactory.create(random).print(::asciiDensityStringifier)
    println("\nFractal:")
    fractalFactory.create(random).print(::asciiDensityStringifier)
    println("\nGradient:")
    gradientFactory.create(random).print(::asciiDensityStringifier)
    println("\nDungeon:")
    dungeonFactory.create(random).print(::asciiDensityStringifier)
*/

    println("\nPath (no si, no br):")
    randomPathFactoryNoSiNoBr.create(random).print(::asciiDensityStringifier)
    println("\nPath (si, no br):")
    randomPathFactorySiNoBr.create(random).print(::asciiDensityStringifier)
}