package com.ovle.utils.noise4j.grid.factory.impl.cellularAutomaton

import com.github.czyzby.noise4j.map.Grid
import com.github.czyzby.noise4j.map.generator.cellular.CellularAutomataGenerator
import com.github.czyzby.noise4j.map.generator.util.Generators
import com.ovle.utils.noise4j.grid.connect
import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.ConnectionStrategy
import com.ovle.utils.noise4j.grid.factory.GridFactory
import com.ovle.utils.noise4j.grid.factory.GridFactoryParams


class CellularAutomatonGridFactory(params: Params): GridFactory<CellularAutomatonGridFactory.Companion.Params>(params) {

    companion object {
        class Params(
            size: Int,
            val iterationsAmount: Int = 3,
            val radius: Int = 1,
            val deathLimit: Int = 2,  //more will kill the walls
            val birthLimit: Int = 4,
            val aliveChance: Float = 0.6f,
            val lifeCellMarker: Float = 1.0f,
            val emptyMarker: Float = 0.0f,
            val connectionStrategy: ConnectionStrategy? = null
        ) : GridFactoryParams(size)
    }

    override fun create(random: RandomParams): Grid {
        val size = params.size
        val result = Grid(params.emptyMarker, size, size)

        val generator = CellularAutomataGenerator.getInstance()
        Generators.setRandom(random.jRandom)
        generator.apply {
            radius = params.radius
            iterationsAmount = params.iterationsAmount
            marker = params.lifeCellMarker
            deathLimit = params.deathLimit
            birthLimit = params.birthLimit
            aliveChance = 1.0f - params.aliveChance // this parameter is actually reversed in generator
        }

        generator.generate(result)
        params.connectionStrategy?.let {
            connect(result, params.emptyMarker, params.lifeCellMarker, it)
        }

        return result
    }
}