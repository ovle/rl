package com.ovle.utils.noise4j.grid.factory

import com.github.czyzby.noise4j.map.generator.room.RoomType
import com.ovle.utils.noise4j.grid.factory.impl.cellularAutomaton.CellularAutomatonGridFactory
import com.ovle.utils.noise4j.grid.factory.impl.dungeon.DungeonGridFactory
import com.ovle.utils.noise4j.grid.factory.impl.fractal.FractalGridFactory
import com.ovle.utils.noise4j.grid.factory.impl.gradient.GradientGridFactory
import com.ovle.utils.noise4j.grid.factory.impl.nop.NopGridFactory
import com.ovle.utils.noise4j.grid.factory.path.impl.RandomPathFactory
import com.ovle.utils.noise4j.grid.factory.impl.randomPath.RandomPathGridFactory

private const val SIZE = 16 + 1

val caFactory = CellularAutomatonGridFactory(
    CellularAutomatonGridFactory.Companion.Params(
        size = SIZE,
        aliveChance = 0.4f
    )
)

val nopFactory = NopGridFactory(
    NopGridFactory.Companion.Params(
        size = SIZE,
        value = 1.0f
    )
)

val fractalFactory = FractalGridFactory(
    FractalGridFactory.Companion.Params(
        size = SIZE
    )
)

val fractalFactoryWithParamsAllHigh = FractalGridFactory(
    FractalGridFactory.Companion.Params(
        size = SIZE,
        noise = 0.0f,
        startIteration = 1..1,
        initialBorderValues = arrayOf(
            FloatArray(3) { 0.9f },
            FloatArray(3) { 0.9f },
            FloatArray(3) { 0.9f }
        )
    )
)

val fractalFactoryWithParamsVertical = FractalGridFactory(
    FractalGridFactory.Companion.Params(
        size = SIZE,
        initialBorderValues = arrayOf(
            FloatArray(3) { 0.0f },
            FloatArray(3) { 0.9f },
            FloatArray(3) { 0.0f }
        )
    )
)

val gradientFactory = GradientGridFactory(
    GradientGridFactory.Companion.Params(
        size = SIZE,
    )
)

val dungeonFactory = DungeonGridFactory(
    DungeonGridFactory.Companion.Params(
        size = SIZE,
        roomTypes = arrayOf(RoomType.DefaultRoomType.SQUARE),
        maxRoomSize = 5,
        minRoomSize = 3
    )
)


val randomPathFactorySiNoBr = RandomPathGridFactory(
    RandomPathGridFactory.Companion.Params(
        size = SIZE,
        0.0f,
        1.0f,
        1..1,
        lengthRange = 20..20,
        isSelfIntersect = true
    ), RandomPathFactory()
)

val randomPathFactoryNoSiNoBr = RandomPathGridFactory(
    RandomPathGridFactory.Companion.Params(
        size = SIZE,
        0.0f,
        1.0f,
        1..1,
        lengthRange = 20..20,
        isSelfIntersect = false
    ), RandomPathFactory()
)