package com.ovle.utils.noise4j.grid.factory

import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.RandomParams


class Combine(params: Params): GridFactory<Combine.Companion.Params>(params) {

    companion object {
        class Params(
            size: Int,
            val factories: Array<GridFactory<*>>,
            val combine: (Array<Float>) -> Float
        ) : GridFactoryParams(size)
    }

    override fun create(random: RandomParams): Grid {
        val grids = params.factories.map { it.create(random) }
        val size = params.size
        val result = Grid(0.0f, size, size)

        for (x in 0 until size) {
            for (y in (0 until size)) {
                val v = grids.map { it.get(x, y) }.toTypedArray()
                val newV = params.combine(v)
                result.set(x, y, newV)
            }
        }

        return result
    }
}