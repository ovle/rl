package com.ovle.utils.noise4j.grid.factory.path.impl

import com.badlogic.gdx.math.GridPoint2
import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.noise4j.grid.GradientPathParams
import com.ovle.utils.noise4j.grid.factory.path.GridPath
import com.ovle.utils.noise4j.grid.gradientPath


class GradientPathFactory(
    private val params: Params
) {

    companion object {
        data class Params(
            val countRange: IntRange = 0..5,
            val minLength: Int = 10,
            val maxAttempts: Int = 50,
            val startValue: Float = 0.65f,
            val finishValue: Float = 0.45f,
            val erosionPower: Float = 0.02f,
        )

        data class Result(
            val paths: Collection<GridPath>
        )
    }


    fun create(grid: Grid, random: RandomParams): Result {
        val result = mutableListOf<GridPath>()

        val startPoints = getStartPoints(grid)
        var attempts = 0
        var count = 0
        val totalCount = params.countRange.random(random.kRandom)

        while (count < totalCount) {
            if (attempts > params.maxAttempts) break
            if (startPoints.isEmpty()) break

            val index = random.jRandom.nextInt(startPoints.size)
            val startPoint = startPoints[index]
            startPoints.remove(startPoint)

            val path = gradientPath(
                grid = grid,
                from = startPoint,
                params = GradientPathParams(
                    random = random.jRandom,
                    finishValue = params.finishValue,
                    erosionPower = params.erosionPower
                )
            )

            val isNeedAddPath = path.size > params.minLength
            if (isNeedAddPath) {
                result.add(path)
                startPoints.removeAll { it in path }
                count++
            }

            attempts++
        }

        return Result(result)
    }

    private fun getStartPoints(grid: Grid): MutableList<GridPoint2> {
        val result = mutableListOf<GridPoint2>()
        for (i in 0 until grid.width) {
            for (j in 0 until grid.height) {
                val p = point(i, j)
                if (grid.get(i, j) >= params.startValue) {
                    result.add(p)
                }
            }
        }
        return result
    }
}