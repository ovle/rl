package com.ovle.utils.noise4j.grid

import com.badlogic.gdx.math.GridPoint2
import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import com.ovle.utils.gdx.math.point.point

fun Grid.isPointValid(x: Int, y: Int) = x in (0 until width) && y in (0 until height)

fun Grid.print(stringify: (GridPoint2, Float) -> String) {
    println()
    (0 until width).forEach { i ->
        println()
        (0 until height).forEach { j ->
            val p = point(i, j)
            print(stringify(p, this[i, j]) + " ")
        }
    }
    println()
}

fun Grid.pointsNear(point: GridPoint2): Array<FloatArray>? {
    val (x, y) = point
    return arrayOf(
        FloatArray(3) { i -> this[x - 1, y - 1 + i] },
        FloatArray(3) { i -> this[x, y - 1 + i] },
        FloatArray(3) { i -> this[x + 1, y - 1 + i] }
    )
}