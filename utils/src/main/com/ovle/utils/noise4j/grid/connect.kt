package com.ovle.utils.noise4j.grid

import com.badlogic.gdx.math.GridPoint2
import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.gdx.math.distance
import com.ovle.utils.gdx.math.lineOfSight.rayTracing.trace
import com.ovle.utils.gdx.math.point.applyToGrid

//todo fix floodFill
enum class ConnectionStrategy {

    REMOVE_UNCONNECTED {
        override fun apply(
            grid: Grid, emptyTileMarker: Float, wallTileMarker: Float, areas: MutableList<Collection<GridPoint2>>
        ) {
            val resultArea = removeUnconnected(grid, wallTileMarker, areas)
            resultArea.applyToGrid(grid, emptyTileMarker)
        }

        private fun removeUnconnected(
            grid: Grid, wallTileMarker: Float, areas: MutableList<Collection<GridPoint2>>
        ): Collection<GridPoint2> {
            val mainArea = areas.maxByOrNull { it.size }!!
            val areasToRemove = areas.toMutableList().apply { remove(mainArea) }
            areasToRemove.forEach {
                it.applyToGrid(grid, wallTileMarker)
            }
            return mainArea
        }
    },

    CONNECT_UNCONNECTED_WITH_PATH {
        override fun apply(
            grid: Grid, emptyTileMarker: Float, wallTileMarker: Float, areas: MutableList<Collection<GridPoint2>>
        ) {
            val disconnectedAreas = areas.toMutableList()
            while (disconnectedAreas.size > 1) {
                val area = disconnectedAreas.first()
                val otherAreas = disconnectedAreas.filter { other -> area != other }
                val (from, to, otherArea) = closestPoints(area, otherAreas)
                val path = trace(from, to, emptyList()).toMutableSet() //todo diagonal steps?

                disconnectedAreas.remove(area)
                disconnectedAreas.remove(otherArea)
                val newArea = area + otherArea + path
                disconnectedAreas.add(newArea)
            }

            val result = disconnectedAreas.single()
            result.applyToGrid(grid, emptyTileMarker)
        }

        //todo O(n^3)
        private fun closestPoints(
            area: Collection<GridPoint2>, otherAreas: List<Collection<GridPoint2>>
        ): Triple<GridPoint2, GridPoint2, Collection<GridPoint2>> {
            val pointsInfo = area.map { point -> point to closestPointInAreas(point, otherAreas) }
            val closestPointsInfo = pointsInfo.minByOrNull {
                distance(it.first, it.second.first)
            }!!
            //todo rewrite, not readable
            return Triple(closestPointsInfo.first, closestPointsInfo.second.first, closestPointsInfo.second.second)
        }

        private fun closestPointInAreas(
            point: GridPoint2, otherAreas: List<Collection<GridPoint2>>
        ): Pair<GridPoint2, Collection<GridPoint2>> {
            val points = otherAreas.map { closestPointInArea(point, it)}
            return points.minByOrNull { distance(point, it.first) }!!
        }

        private fun closestPointInArea(
            point: GridPoint2, otherArea: Collection<GridPoint2>
        ): Pair<GridPoint2, Collection<GridPoint2>> {
            return otherArea.minByOrNull { distance(point, it) }!! to otherArea
        }
    };

    abstract fun apply(grid: Grid, emptyTileMarker: Float, wallTileMarker: Float, areas: MutableList<Collection<GridPoint2>>)
}

//todo
fun connect(grid: Grid, emptyTileMarker: Float, wallTileMarker: Float, connectionStrategy: ConnectionStrategy) {
    val areas = mutableListOf<Collection<GridPoint2>>() //areas(grid., emptyTileMarker)

//    println("areas: ${areas.size}")

    connectionStrategy.apply(grid, emptyTileMarker, wallTileMarker, areas)
}