package com.ovle.utils.noise4j.grid.factory

import com.github.czyzby.noise4j.map.Grid
import com.ovle.utils.RandomParams


abstract class GridFactory<T: GridFactoryParams>(val params: T) {

    abstract fun create(random: RandomParams): Grid
}