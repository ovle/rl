package com.ovle.rl.contentView.task

import com.badlogic.gdx.Input
import com.ovle.rl.content.task.type.build.buildTemplates
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.rl.view.game.render.gui.task.BuildRenderTaskHelper
import com.ovle.rl.view.game.scene2d.tabs.tasks.type.BuildTaskWidget
import com.ovle.utils.gdx.math.point.point

val buildTask = TaskViewTemplate(
    taskTemplate = com.ovle.rl.content.task.buildTask,
    displayName = "[b]ld",
    icon = point(6, 2),
    selectedIcon = point(6, 3),
    getWidget = { getGame -> BuildTaskWidget(getGame, buildTemplates) },
    getRenderHelper = { b, s, ttm -> BuildRenderTaskHelper(b, s, ttm) },
    getInfo = { ti ->
        val bt = (ti.target as TaskTarget.Build)
        "${bt.template.name} ${bt.point}"
    },
    keyCode = Input.Keys.B
)