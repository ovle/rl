package com.ovle.rl.contentView.task

import com.badlogic.gdx.Input
import com.ovle.rl.content.task.type.craft.craftTemplates
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.rl.view.game.scene2d.tabs.tasks.type.CraftTaskWidget
import com.ovle.utils.gdx.math.point.point

val craftTask = TaskViewTemplate(
    taskTemplate = com.ovle.rl.content.task.craftTask,
    displayName = "[c]ft",
    icon = point(6, 4),
    selectedIcon = point(6, 5),
    getWidget = { getGame -> CraftTaskWidget(getGame, craftTemplates) },
    getInfo = { ti -> (ti.target as TaskTarget.Craft).template.name },
    keyCode = Input.Keys.C
)