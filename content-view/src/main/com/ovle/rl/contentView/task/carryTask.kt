package com.ovle.rl.contentView.task

import com.badlogic.gdx.Input
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.utils.gdx.math.point.point

val carryTask = TaskViewTemplate(
    taskTemplate = com.ovle.rl.content.task.carryTask,
    displayName = "cr[y]",
    icon = point(7, 4),
    selectedIcon = point(7, 5),
    getInfo = { ti ->
        val ct = (ti.target as TaskTarget.Carry)
        "${ct.entity.name()} ${ct.point}"
    },
    keyCode = Input.Keys.Y
)