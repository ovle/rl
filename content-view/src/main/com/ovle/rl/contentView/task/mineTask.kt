package com.ovle.rl.contentView.task

import com.badlogic.gdx.Input
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.utils.gdx.math.point.point

val mineTask = TaskViewTemplate(
    taskTemplate = com.ovle.rl.content.task.mineTask,
    displayName = "[m]in",
    icon = point(7, 2),
    selectedIcon = point(7, 3),
    keyCode = Input.Keys.M
)