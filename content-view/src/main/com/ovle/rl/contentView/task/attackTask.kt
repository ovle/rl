package com.ovle.rl.contentView.task

import com.badlogic.gdx.Input
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.utils.gdx.math.distance
import com.ovle.utils.gdx.math.point.point

val attackTask = TaskViewTemplate(
    taskTemplate = com.ovle.rl.content.task.attackTask,
    displayName = "[a]tk",
    icon = point(4, 2),
    selectedIcon = point(4, 3),
    getInfo = { ti ->
        val entity = (ti.target as TaskTarget.Entity).entity
        "${entity.name()} ${entity.position()}"
    },
    keyCode = Input.Keys.A,
)