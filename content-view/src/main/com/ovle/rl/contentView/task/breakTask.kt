package com.ovle.rl.contentView.task

import com.badlogic.gdx.Input
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.utils.gdx.math.point.point

val breakTask = TaskViewTemplate(
    taskTemplate = com.ovle.rl.content.task.breakTask,
    displayName = "b[r]k",
    icon = point(5, 2),
    selectedIcon = point(5, 3),
    keyCode = Input.Keys.R
)