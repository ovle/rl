package com.ovle.rl.contentView.task

import com.badlogic.gdx.Input
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.utils.gdx.math.point.point

val exploreTask = TaskViewTemplate(
    taskTemplate = com.ovle.rl.content.task.exploreTask,
    displayName = "[e]xp",
    icon = point(0, 2),
    selectedIcon = point(0, 3),
    keyCode = Input.Keys.E
)