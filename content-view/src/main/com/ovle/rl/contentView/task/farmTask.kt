package com.ovle.rl.contentView.task

import com.badlogic.gdx.Input
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.utils.gdx.math.point.point

val farmTask = TaskViewTemplate(
    taskTemplate = com.ovle.rl.content.task.farmTask,
    displayName = "[f]rm",
    icon = point(3, 2),
    selectedIcon = point(3, 3),
//    getWidget = { getGame -> FarmTaskWidget(getGame, farmTemplates) },
    getInfo = { ti -> (ti.target as TaskTarget.Farm).template.name },
    keyCode = Input.Keys.F
)