package com.ovle.rl.contentView.task

val taskViews = listOf(
    attackTask,
    exploreTask,
    carryTask,
    buildTask,
    breakTask,
    craftTask,
    mineTask,
    gatherTask,
    farmTask
)