package com.ovle.rl.contentView.skill

import com.ovle.rl.content.skill.spawnChelSkill
import com.ovle.rl.view.game.render.effect.SkillViewTemplate
import com.ovle.rl.view.game.render.effect.config.actionSpriteEffect

val spawnWorkerSkillView = SkillViewTemplate(
    skillTemplate = spawnChelSkill,
    getSubjectSpriteEffect = { actionSpriteEffect() }
)