package com.ovle.rl.contentView.skill

import com.ovle.rl.content.skill.magicMissileSkill
import com.ovle.rl.model.game.time.MIN_ANIMATION_FRAME_LENGTH
import com.ovle.rl.view.game.render.effect.SkillViewTemplate
import com.ovle.rl.view.game.render.effect.config.FrameAnimationSpriteEffect
import com.ovle.rl.view.game.render.effect.config.OverflowSpriteEffect
import com.ovle.utils.gdx.math.point.point

val magicMissileSkillView = SkillViewTemplate(
    skillTemplate = magicMissileSkill,
    getSubjectSpriteEffect = { _ ->
        OverflowSpriteEffect(
            FrameAnimationSpriteEffect(
                MIN_ANIMATION_FRAME_LENGTH * 3,
                arrayOf(
                    point(16, 15),
                    point(17, 15),
                )
            )
        )
    }
)