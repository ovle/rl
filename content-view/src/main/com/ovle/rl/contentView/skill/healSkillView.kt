package com.ovle.rl.contentView.skill

import com.ovle.rl.content.skill.healSkill
import com.ovle.rl.model.game.time.MIN_ANIMATION_FRAME_LENGTH
import com.ovle.rl.view.game.render.effect.SkillViewTemplate
import com.ovle.rl.view.game.render.effect.config.FrameAnimationSpriteEffect
import com.ovle.rl.view.game.render.effect.config.OverflowSpriteEffect
import com.ovle.utils.gdx.math.point.point

val healSkillView = SkillViewTemplate(
    skillTemplate = healSkill,
    getObjectSpriteEffect = { _ ->
        OverflowSpriteEffect(
            FrameAnimationSpriteEffect(
                MIN_ANIMATION_FRAME_LENGTH * 2,
                arrayOf(
                    point(20, 15),
                    point(20, 14)
                )
            )
        )
    }
)