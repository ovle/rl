package com.ovle.rl.contentView.skill

import com.ovle.rl.content.skill.raiseDeadSkill
import com.ovle.rl.model.game.time.MIN_ANIMATION_FRAME_LENGTH
import com.ovle.rl.view.game.render.effect.SkillViewTemplate
import com.ovle.rl.view.game.render.effect.config.FrameAnimationSpriteEffect
import com.ovle.rl.view.game.render.effect.config.OverflowSpriteEffect
import com.ovle.utils.gdx.math.point.point

val raiseDeadSkillView = SkillViewTemplate(
    skillTemplate = raiseDeadSkill,
    getObjectSpriteEffect = { _ ->
        OverflowSpriteEffect(
            FrameAnimationSpriteEffect(
                MIN_ANIMATION_FRAME_LENGTH * 4,
                arrayOf(
                    point(19, 15),
                    point(19, 14),
                )
            )
        )
    }
)