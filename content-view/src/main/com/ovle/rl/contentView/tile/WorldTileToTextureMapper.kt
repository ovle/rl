package com.ovle.rl.contentView.tile

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.content.tile.*
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.view.game.render.mapper.TileToTextureMapper
import com.ovle.rl.view.game.render.tileMap.TileToTextureParams
import com.ovle.utils.gdx.math.point.point

class WorldTileToTextureMapper: TileToTextureMapper {

    companion object {
        private val TILES_TEXTURE_OFFSET = point(8, 21)
    }

    override fun map(params: TileToTextureParams): TextureRegion {
        val regions = params.textureRegions
        val texturePoint = texturePoint(params.tile)

        val x = texturePoint.y + TILES_TEXTURE_OFFSET.y
        val y = texturePoint.x + TILES_TEXTURE_OFFSET.x
        return regions[x][y]
    }

    private fun texturePoint(tile: TileTemplate?): GridPoint2 {
        return when (tile) {
            null -> point(0, 0)
            deepLava -> point(6, 1)
            shallowLava -> point(4, 0)
            deepWater -> point(0, 0)
            shallowWater -> point(2, 0)
            arcticDeepWater -> point(2, 0)
            iceFloor -> point(0, 1)
            pitFloor -> point(1, 0)

            naturalFloorGrass -> point(5, 0)
            naturalFloorTundraSoil -> point(1, 0)
            naturalFloorJungleGrass -> point(3, 1)

            naturalFloorSoil -> point(1, 0)
            naturalFloorSnow -> point(0, 1)
            naturalFloorSand -> point(6, 0)

            naturalFloorSedStone -> point(1, 0)
            naturalFloorMetaStone -> point(1, 0)
            naturalFloorIgnStone -> point(1, 0)

            naturalWallSoil -> point(3, 0)
            naturalWallSnow -> point(7, 1)
            naturalWallSand -> point(6, 0)

            naturalWallSedStone -> point(7, 0)
            naturalWallMetaStone -> point(7, 1)
            naturalWallIgnStone -> point(4, 0)

            else -> point(0, 0)
        }
    }
}