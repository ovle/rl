package com.ovle.rl.contentView.tile

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.ovle.rl.content.tile.*
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.view.game.render.mapper.TileToTextureMapper
import com.ovle.rl.view.game.render.tileMap.TileToTextureParams
import com.ovle.utils.gdx.math.point.point

class LocationTileToTextureMapper: TileToTextureMapper {

    companion object {
        private val TILES_TEXTURE_OFFSET = point(8, 0)
        private val EMPTY_REGION = point(0, 0)
        private val UNKNOWN_REGION = point(0, 0)
        private val INVISIBLE_REGION = point(1, 0)

        private val WALL_MASK_LEFT_TOP_DIAG = point(7, 0)
        private val WALL_MASK_LEFT_TOP = point(6, 0)
        private val WALL_MASK_LEFT = point(5, 0)
        private val WALL_MASK_TOP = point(4, 0)
    }


    override fun map(params: TileToTextureParams): TextureRegion {
        val regions = params.textureRegions
        val texturePoint = texturePoint(params)

        val x = texturePoint.y + TILES_TEXTURE_OFFSET.y
        val y = texturePoint.x + TILES_TEXTURE_OFFSET.x
        return regions[x][y]
    }


    private fun texturePoint(params: TileToTextureParams) = when {
        !params.isKnown -> UNKNOWN_REGION
        !params.isVisible -> INVISIBLE_REGION

        params.isFloorDown && params.isFloorRight -> WALL_MASK_LEFT_TOP
        params.isFloorDown -> WALL_MASK_TOP
        params.isFloorRight -> WALL_MASK_LEFT
        params.isFloorDiag -> WALL_MASK_LEFT_TOP_DIAG

        else -> {
            when (params.tile) {
                TileTemplate.NOTHING -> EMPTY_REGION

                shallowLava -> point(6, 15)
                deepLava -> point(7, 15)
                shallowWater -> point(6, 14)
                deepWater -> point(7, 14)
                arcticDeepWater -> point(7, 14)
                pitFloor -> point(0, 0)
                iceFloor -> point(7, 13)

                naturalWallMineralCopper -> point(0, 1)
                naturalWallMineralIron -> point(1, 1)
                naturalWallMineralGold -> point(2, 1)
                naturalWallMineralDiamond -> point(3, 1)

                naturalFloorGrass -> point((0..2).random(), 12)
                naturalFloorTundraSoil -> point((0..2).random(), 11)
                naturalFloorJungleGrass -> point((0..2).random(), 15)

                naturalFloorSoil -> point((0..1).random(), 7)
                naturalFloorSand -> point((0..2).random(), 14)
                naturalFloorSnow -> point((0..2).random(), 10)

                naturalFloorSedStone -> point((2..4).random(), 7)
                naturalFloorMetaStone -> point((5..6).random(), 7)
                naturalFloorIgnStone -> point((5..7).random(), 9)

                naturalWallSoil -> point((0..1).random(), 6)
                naturalWallSand -> point((3..5).random(), 14)
                naturalWallSnow -> point((3..5).random(), 10)

                naturalWallSedStone -> point((2..4).random(), 6)
                naturalWallMetaStone -> point((5..6).random(), 6)
                naturalWallIgnStone -> point((5..7).random(), 8)

                artFloorStoneRoad -> point((6..7).random(), 11)

                artFloorStone -> point(0, 5)
                artWallStone -> point(1, 5)
                doorStone -> point(2, 16)
                windowStone -> point(5, 16)
                artFloorWood -> point(0, 2)
                artWallWood -> point(1, 2)
                doorWood -> point(0, 16)
                windowWood -> point(3, 16)
                artFloorBrick -> point(2, 5)
                artWallBrick -> point(3, 5)

                else -> EMPTY_REGION
            }
        }
    }
/*
    private fun tiledPoint(x0: Int, y0: Int, point: GridPoint2): GridPoint2 {
        val x = if (point.x % 2 == 0) x0 else x0 + 1
        val y = if (point.y % 2 == 0) y0 else y0 + 1
        return point(x, y)
    }
*/
}