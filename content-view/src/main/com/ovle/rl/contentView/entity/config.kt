package com.ovle.rl.contentView.entity

import com.ovle.rl.contentView.entity.creature.*
import com.ovle.rl.contentView.entity.furniture.*
import com.ovle.rl.contentView.entity.other.*
import com.ovle.rl.contentView.entity.plant.*
import com.ovle.rl.contentView.entity.plant.tree.*
import com.ovle.rl.contentView.entity.resource.proc.*
import com.ovle.rl.contentView.entity.resource.raw.*
import com.ovle.rl.contentView.entity.skill.dragonBreathView
import com.ovle.rl.contentView.entity.skill.magicMissileView

val entityViewTemplates = listOf(
    portalView,
    keeperView,

    chelView,
    skeletonView,
    ogreView,

    batView,
    fishView,
    pigView,
    wolfView,
    dragonflyView,

    fruitsView,
    woodView,
    stoneView,
    copperView,
    ironView,
    goldView,
    diamondView,

    procWoodView,
    procStoneView,
    procCopperView,
    procIronView,
    procGoldView,

    cropStage1View,
    cropStage3View,
    grainView,
    peashooterView,

    breadView,

    borealTreeView,
    borealTreeSaplingView,
    temperateTreeView,
    temperateTreeSaplingView,
    jungleTreeView,
    jungleTreeSaplingView,
    jungleBigTreeView,
    desertTreeView,
    desertTreeSaplingView,
    fruitBushView,

//    altarView,
    bedView,
    dummyView,
    fenceView,
    stashView,
    tableView,
    chairView,
    closetView,
    barrelView,
    ovenView,
    anvilView,
    forgeView,
    carpWrkView,
    masonWrkView,

    graveView,
    bombView,
    magicMissileView,
    explosionView,
    dragonBreathView,
)