package com.ovle.rl.contentView.entity.furniture

import com.ovle.rl.content.entity.furniture.tableEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val tableView = EntityViewTemplate(
    entity = tableEntity,
    isTinted = true,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(2, 11),
    )
)