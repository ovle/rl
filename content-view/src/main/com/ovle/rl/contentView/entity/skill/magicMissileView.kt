package com.ovle.rl.contentView.entity.skill

import com.ovle.rl.content.entity.skill.magicMissileEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val magicMissileView = EntityViewTemplate(
    entity = magicMissileEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(17, 23),
    )
)