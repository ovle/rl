package com.ovle.rl.contentView.entity.plant.tree

import com.ovle.rl.content.entity.plant.tree.desertTreeEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val desertTreeView = EntityViewTemplate(
    entity = desertTreeEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(3, 19),
    )
)