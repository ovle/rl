package com.ovle.rl.contentView.entity.furniture

import com.ovle.rl.content.entity.furniture.forgeEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val forgeView = EntityViewTemplate(
    entity = forgeEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(5, 16),
    )
)