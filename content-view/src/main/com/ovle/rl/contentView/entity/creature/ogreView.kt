package com.ovle.rl.contentView.entity.creature

import com.ovle.rl.content.entity.creature.ogreEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val ogreView = EntityViewTemplate(
    entity = ogreEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(3, 3),
    )
)