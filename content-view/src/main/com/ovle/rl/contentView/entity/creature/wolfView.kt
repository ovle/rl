package com.ovle.rl.contentView.entity.creature

import com.ovle.rl.content.entity.creature.wolfEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val wolfView = EntityViewTemplate(
    entity = wolfEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(1, 3),
    )
)