package com.ovle.rl.contentView.entity.furniture

import com.ovle.rl.content.entity.furniture.masonWrkEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val masonWrkView = EntityViewTemplate(
    entity = masonWrkEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(4, 16),
    )
)