package com.ovle.rl.contentView.entity.furniture

import com.ovle.rl.content.entity.furniture.ovenEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val ovenView = EntityViewTemplate(
    entity = ovenEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(3, 16),
    )
)