package com.ovle.rl.contentView.entity.creature

import com.ovle.rl.content.entity.creature.pigEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val pigView = EntityViewTemplate(
    entity = pigEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(0, 3),
    )
)