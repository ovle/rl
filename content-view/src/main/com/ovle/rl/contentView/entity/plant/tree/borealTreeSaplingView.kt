package com.ovle.rl.contentView.entity.plant.tree

import com.ovle.rl.content.entity.plant.tree.borealTreeSaplingEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val borealTreeSaplingView = EntityViewTemplate(
    entity = borealTreeSaplingEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(0, 19),
    )
)