package com.ovle.rl.contentView.entity.creature

import com.ovle.rl.content.entity.creature.chelEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val chelView = EntityViewTemplate(
    entity = chelEntity,
    requireText = "-",
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(0, 0),
    )
)