package com.ovle.rl.contentView.entity.plant

import com.ovle.rl.content.entity.plant.peashooterEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val peashooterView = EntityViewTemplate(
    entity = peashooterEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(2, 21),
    )
)