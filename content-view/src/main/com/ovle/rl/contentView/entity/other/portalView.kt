package com.ovle.rl.contentView.entity.other

import com.ovle.rl.content.entity.other.portalEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val portalView = EntityViewTemplate(
    entity = portalEntity,
    requireText = "-",
    isBig = true,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(14, 1),
    )
)