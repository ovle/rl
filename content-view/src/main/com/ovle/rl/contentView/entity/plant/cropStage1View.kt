package com.ovle.rl.contentView.entity.plant

import com.ovle.rl.content.entity.plant.cropStage1Entity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val cropStage1View = EntityViewTemplate(
    entity = cropStage1Entity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(0, 22),
    )
)