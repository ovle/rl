package com.ovle.rl.contentView.entity.furniture

import com.ovle.rl.content.entity.furniture.barrelEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val barrelView = EntityViewTemplate(
    entity = barrelEntity,
    isTinted = true,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(6, 11),
    )
)