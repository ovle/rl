package com.ovle.rl.contentView.entity.plant.tree

import com.ovle.rl.content.entity.plant.tree.jungleTreeSaplingEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val jungleTreeSaplingView = EntityViewTemplate(
    entity = jungleTreeSaplingEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(2, 20),
    )
)