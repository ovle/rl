package com.ovle.rl.contentView.entity.resource.proc

import com.ovle.rl.content.entity.resource.proc.procStoneEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val procStoneView = EntityViewTemplate(
    entity = procStoneEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(0, 9)
    )
)