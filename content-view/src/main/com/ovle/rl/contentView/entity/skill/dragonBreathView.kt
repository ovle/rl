package com.ovle.rl.contentView.entity.skill

import com.ovle.rl.content.entity.skill.dragonBreathEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val dragonBreathView = EntityViewTemplate(
    entity = dragonBreathEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(8, 22),
    ),
    animation = mapOf(
        "idle" to listOf(
            point(8, 22), point(9, 22)
        )
    )
)