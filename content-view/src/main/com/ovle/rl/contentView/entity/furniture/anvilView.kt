package com.ovle.rl.contentView.entity.furniture

import com.ovle.rl.content.entity.furniture.anvilEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val anvilView = EntityViewTemplate(
    entity = anvilEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(6, 16),
    )
)