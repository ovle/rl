package com.ovle.rl.contentView.entity.resource.proc

import com.ovle.rl.content.entity.resource.proc.procIronEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val procIronView = EntityViewTemplate(
    entity = procIronEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(4, 9)
    )
)