package com.ovle.rl.contentView.entity.furniture

import com.ovle.rl.content.entity.furniture.fenceEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val fenceView = EntityViewTemplate(
    entity = fenceEntity,
    isTinted = true,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(5, 11),
    )
)