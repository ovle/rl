package com.ovle.rl.contentView.entity.plant

import com.ovle.rl.content.entity.plant.cropStage3Entity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val cropStage3View = EntityViewTemplate(
    entity = cropStage3Entity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(1, 22),
    )
)