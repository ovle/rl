package com.ovle.rl.contentView.entity.resource.proc

import com.ovle.rl.content.entity.resource.proc.procCopperEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val procCopperView = EntityViewTemplate(
    entity = procCopperEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(3, 9)
    )
)