package com.ovle.rl.contentView.entity.resource.raw

import com.ovle.rl.content.entity.resource.raw.copperEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val copperView = EntityViewTemplate(
    entity = copperEntity,
    sprite = mapOf(DEFAULT_SPRITE_KEY to point(3, 8))
)