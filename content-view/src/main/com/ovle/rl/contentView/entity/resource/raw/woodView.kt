package com.ovle.rl.contentView.entity.resource.raw

import com.ovle.rl.content.entity.resource.raw.woodEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val woodView = EntityViewTemplate(
    entity = woodEntity,
    sprite = mapOf(DEFAULT_SPRITE_KEY to point(2, 8))
)