package com.ovle.rl.contentView.entity.furniture

import com.ovle.rl.content.entity.furniture.bedEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val bedView = EntityViewTemplate(
    entity = bedEntity,
    isTinted = true,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(4, 11),
    )
)