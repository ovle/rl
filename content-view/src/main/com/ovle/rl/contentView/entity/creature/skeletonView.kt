package com.ovle.rl.contentView.entity.creature

import com.ovle.rl.content.entity.creature.skeletonEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val skeletonView = EntityViewTemplate(
    entity = skeletonEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(1, 2),
    )
)