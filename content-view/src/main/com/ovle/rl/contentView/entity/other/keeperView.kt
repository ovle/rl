package com.ovle.rl.contentView.entity.other

import com.ovle.rl.content.entity.other.keeperEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val keeperView = EntityViewTemplate(
    entity = keeperEntity,
    requireText = "-",
    isBig = true,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(11, 1),
    )
)