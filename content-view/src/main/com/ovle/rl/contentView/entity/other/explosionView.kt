package com.ovle.rl.contentView.entity.other

import com.ovle.rl.content.entity.other.explosionEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val explosionView = EntityViewTemplate(
    entity = explosionEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(18, 22),
    ),
    animation = mapOf(
        "idle" to listOf(
            point(18, 22), point(19, 22)
        )
    )
)