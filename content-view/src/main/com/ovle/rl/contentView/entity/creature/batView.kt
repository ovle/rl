package com.ovle.rl.contentView.entity.creature

import com.ovle.rl.content.entity.creature.batEntity
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.utils.gdx.math.point.point

val batView = EntityViewTemplate(
    entity = batEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(2, 3),
    )
)