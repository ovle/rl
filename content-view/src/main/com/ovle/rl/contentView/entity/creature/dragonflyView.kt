package com.ovle.rl.contentView.entity.creature

import com.ovle.rl.content.entity.creature.dragonflyEntity
import com.ovle.rl.view.game.render.DEFAULT_SPRITE_KEY
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.utils.gdx.math.point.point

val dragonflyView = EntityViewTemplate(
    entity = dragonflyEntity,
    sprite = mapOf(
        DEFAULT_SPRITE_KEY to point(1, 4),
    )
)