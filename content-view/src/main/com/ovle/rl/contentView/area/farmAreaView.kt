package com.ovle.rl.contentView.area

import com.badlogic.gdx.Input
import com.ovle.rl.content.area.farmArea
import com.ovle.rl.content.task.type.farm.farmTemplates
import com.ovle.rl.view.game.render.AreaTileView
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.rl.view.game.scene2d.tabs.areas.type.FarmAreaWidget
import com.ovle.utils.gdx.math.point.point

val farmAreaView = AreaViewTemplate(
    template = farmArea,
    displayName = "[f]rm",
    areaTileView = AreaTileView(
        floor = point(14, 10)
    ),
    getWidget = { getGame -> FarmAreaWidget(getGame, farmTemplates) },
    keyCode = Input.Keys.F,
)