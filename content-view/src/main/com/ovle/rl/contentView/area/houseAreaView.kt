package com.ovle.rl.contentView.area

import com.badlogic.gdx.Input
import com.ovle.rl.content.area.houseArea
import com.ovle.rl.view.game.render.AreaTileView
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.rl.view.game.scene2d.tabs.areas.type.HouseAreaWidget
import com.ovle.utils.gdx.math.point.point

val houseAreaView = AreaViewTemplate(
    template = houseArea,
    displayName = "[h]ou",
//    areaTileView = AreaTileView(
//        floor = point(8, 3)
//    ),
    getWidget = { getGame -> HouseAreaWidget(getGame) },
    keyCode = Input.Keys.R,
)