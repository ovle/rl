package com.ovle.rl.contentView.area

import com.badlogic.gdx.Input
import com.ovle.rl.content.area.storageArea
import com.ovle.rl.content.category.categoryTemplates
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.rl.view.game.scene2d.tabs.areas.type.StorageAreaWidget

val storageAreaView = AreaViewTemplate(
    template = storageArea,
    displayName = "[s]tg",
    getWidget = { getGame -> StorageAreaWidget(getGame, categoryTemplates) },
    keyCode = Input.Keys.S,
)