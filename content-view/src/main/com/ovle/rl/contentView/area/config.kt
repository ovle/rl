package com.ovle.rl.contentView.area

val areaViews = listOf(
    houseAreaView,
    storageAreaView,
    farmAreaView
)