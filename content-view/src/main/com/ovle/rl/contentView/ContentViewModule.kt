package com.ovle.rl.contentView

import com.ovle.rl.contentView.area.areaViews
import com.ovle.rl.contentView.entity.entityViewTemplates
import com.ovle.rl.contentView.skill.skillViews
import com.ovle.rl.contentView.task.taskViews
import com.ovle.rl.contentView.tile.LocationTileToTextureMapper
import com.ovle.rl.contentView.tile.WorldTileToTextureMapper
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.rl.view.game.render.effect.SkillViewTemplate
import com.ovle.rl.view.game.render.mapper.*
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.singleton


val contentViewModule = DI.Module("content-view") {
    bind<Collection<SkillViewTemplate>>() with singleton { skillViews }
    bind<Collection<TaskViewTemplate>>() with singleton { taskViews }
    bind<Collection<AreaViewTemplate>>() with singleton { areaViews }
    bind<Collection<EntityViewTemplate>>() with singleton { entityViewTemplates }

    bind<TileToTextureMapper>(LOCATION_TILE_MAPPER_TAG) with singleton {
        LocationTileToTextureMapper()
    }
    bind<TileToTextureMapper>(WORLD_TILE_MAPPER_TAG) with singleton {
        WorldTileToTextureMapper()
    }
}