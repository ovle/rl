package com.ovle.rl.content.material

import com.ovle.rl.model.game.material.MaterialTemplate

val woodMaterial = MaterialTemplate(
    name = "wood",
    grade = 1,
    color = "db16-brown"
)

val stoneMaterial = MaterialTemplate(
    name = "stone",
    grade = 2,
    color = "db16-grayBrown"
)

val copperMaterial = MaterialTemplate(
    name = "copper",
    grade = 3,
    color = "db16-orange"
)

val ironMaterial = MaterialTemplate(
    name = "iron",
    grade = 4,
    color = "db16-gray"
)

val goldMaterial = MaterialTemplate(
    name = "gold",
    grade = 5,
    color = "db16-yellow"
)

val meatMaterial = MaterialTemplate(
    name = "meat",
    grade = 1,
)
val cookMeatMaterial = MaterialTemplate(
    name = "cooked meat",
    grade = 2,
)

val plantMaterial = MaterialTemplate(
    name = "plant",
    grade = 1,
)
val cookPlantMaterial = MaterialTemplate(
    name = "cooked plant",
    grade = 2,
)

val fruitMaterial = MaterialTemplate(
    name = "fruit",
    grade = 1,
)

val diamondMaterial = MaterialTemplate(
    name = "diamond",
    grade = 6,
    color = "db16-white"
)


val materialTemplates = listOf(
    woodMaterial,
    stoneMaterial,
    copperMaterial,
    ironMaterial,
    goldMaterial,
    diamondMaterial,

    meatMaterial,
    plantMaterial,
    fruitMaterial,
)