package com.ovle.rl.content.location.instance.world.structure.purpose

import com.ovle.rl.content.area.houseArea
import com.ovle.rl.content.area.storageArea
import com.ovle.rl.content.category.categoryTemplates
import com.ovle.rl.content.category.plantCategory
import com.ovle.rl.content.category.stoneCategory
import com.ovle.rl.content.category.woodCategory
import com.ovle.rl.content.entity.creature.chelEntity
import com.ovle.rl.content.entity.furniture.*
import com.ovle.rl.content.entity.resource.proc.procStoneEntity
import com.ovle.rl.content.entity.resource.raw.grainEntity
import com.ovle.rl.content.entity.resource.raw.ironEntity
import com.ovle.rl.content.entity.resource.raw.stoneEntity
import com.ovle.rl.content.entity.resource.raw.woodEntity
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaPartPurpose
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaPurposeEntity

//todo food spawn
//todo items in containers
val livingAreaPurpose = AreaPartPurpose(
    name = "living",
    weight = 1.0f,
    creatures = listOf(
        AreaPurposeEntity(chelEntity, 0..2),
    ),
    entities = listOf(
        AreaPurposeEntity(stashEntity, 0..2),
        AreaPurposeEntity(bedEntity, 1..1),
        AreaPurposeEntity(tableEntity, 0..1),
        AreaPurposeEntity(chairEntity, 0..2)
    ),
    areaTemplate = houseArea,
    getAreaParams = { AreaParams.House() }
)

val socialAreaPurpose = AreaPartPurpose(
    name = "social",
    weight = 0.25f,
    creatures = listOf(
        AreaPurposeEntity(chelEntity, 0..4),
    ),
    entities = listOf(
        AreaPurposeEntity(tableEntity, 1..2),
        AreaPurposeEntity(chairEntity, 1..2),
        AreaPurposeEntity(barrelEntity, 1..1)
    )
)

val knowledgeAreaPurpose = AreaPartPurpose(
    name = "knowledge",
    weight = 0.125f,
    entities = listOf(
        AreaPurposeEntity(stashEntity, 0..1),
        AreaPurposeEntity(tableEntity, 0..1),
        AreaPurposeEntity(chairEntity, 1..2),
        AreaPurposeEntity(closetEntity, 1..2),
    )
)

val administrativeAreaPurpose = AreaPartPurpose(
    name = "administrative",
    weight = 0.125f,
    creatures = listOf(
        AreaPurposeEntity(chelEntity, 0..2),
    ),
    entities = listOf(
        AreaPurposeEntity(tableEntity, 1..1),
        AreaPurposeEntity(chairEntity, 1..2),
        AreaPurposeEntity(closetEntity, 0..1),
    )
)

val industryFoodAreaPurpose = AreaPartPurpose(
    name = "industry (foo)",
    weight = 0.25f,
    entities = listOf(
        AreaPurposeEntity(tableEntity, 1..1),
        AreaPurposeEntity(chairEntity, 1..1),
        AreaPurposeEntity(ovenEntity, 1..1),
    )
)

val industryStoneAreaPurpose = AreaPartPurpose(
    name = "industry (stn)",
    weight = 0.25f,
    entities = listOf(
        AreaPurposeEntity(chairEntity, 1..2),
        AreaPurposeEntity(masonWrkEntity, 1..1),
        AreaPurposeEntity(stoneEntity, 0..2),
    )
)

val industryWoodAreaPurpose = AreaPartPurpose(
    name = "industry (wod)",
    weight = 0.25f,
    entities = listOf(
        AreaPurposeEntity(chairEntity, 1..2),
        AreaPurposeEntity(carpWrkEntity, 1..1),
        AreaPurposeEntity(woodEntity, 0..2),
    )
)

val industryMetalAreaPurpose = AreaPartPurpose(
    name = "industry (mtl)",
    weight = 0.125f,
    entities = listOf(
        AreaPurposeEntity(chairEntity, 0..1),
        AreaPurposeEntity(anvilEntity, 1..1),
        AreaPurposeEntity(forgeEntity, 1..1),
        AreaPurposeEntity(ironEntity, 0..2),
    )
)

val industryChemistryAreaPurpose = AreaPartPurpose(
    name = "industry (che)",
    weight = 0.125f,
    entities = listOf()
)

val foodStorageAreaPurpose = AreaPartPurpose(
    name = "storage (fod)",
    weight = 0.5f,
    entities = listOf(
        AreaPurposeEntity(barrelEntity, 0..2),
        AreaPurposeEntity(grainEntity, 0..2),
    ),
    areaTemplate = storageArea,
    getAreaParams = {
        val template = plantCategory
        AreaParams.Storage(template)
    }
)

val woodStorageAreaPurpose = AreaPartPurpose(
    name = "storage (wod)",
    weight = 0.25f,
    entities = listOf(
        AreaPurposeEntity(woodEntity, 0..2),
    ),
    areaTemplate = storageArea,
    getAreaParams = {
        val template = woodCategory
        AreaParams.Storage(template)
    }
)

val stoneStorageAreaPurpose = AreaPartPurpose(
    name = "storage (stn)",
    weight = 0.25f,
    entities = listOf(
        AreaPurposeEntity(stoneEntity, 0..2),
        AreaPurposeEntity(procStoneEntity, 0..2),
    ),
    areaTemplate = storageArea,
    getAreaParams = {
        val template = stoneCategory
        AreaParams.Storage(template)
    }
)


val warfareAreaPurpose = AreaPartPurpose(
    name = "warfare",
    weight = 0.125f,
    entities = listOf(
        AreaPurposeEntity(stashEntity, 0..1),
        AreaPurposeEntity(dummyEntity, 0..1),
    )
)

val cultAreaPurpose = AreaPartPurpose(
    name = "cult",
    weight = 0.125f,
    entities = listOf()
)

val industryPurposes = setOf(
    industryFoodAreaPurpose,
    industryStoneAreaPurpose,
    industryWoodAreaPurpose,
    industryMetalAreaPurpose
)

val storagePurposes = setOf(
    foodStorageAreaPurpose,
    woodStorageAreaPurpose,
    stoneStorageAreaPurpose,
)


val areaPurposes = listOf(
    livingAreaPurpose,
    socialAreaPurpose, knowledgeAreaPurpose, administrativeAreaPurpose,
    industryFoodAreaPurpose, industryStoneAreaPurpose, industryWoodAreaPurpose, industryMetalAreaPurpose, industryChemistryAreaPurpose,
    foodStorageAreaPurpose, woodStorageAreaPurpose, stoneStorageAreaPurpose,
    warfareAreaPurpose,
    cultAreaPurpose,
)