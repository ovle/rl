package com.ovle.rl.content.location

import com.ovle.rl.content.location.instance.demo.testDemoLocation
import com.ovle.rl.content.location.instance.procDungeon.testProcDungeonLocation
import com.ovle.rl.content.location.instance.shooting.testShootingLocation


val playgroundLocationTemplates = listOf(
    testDemoLocation,
    testProcDungeonLocation,
    testShootingLocation,
//    testBattleLocation,
)