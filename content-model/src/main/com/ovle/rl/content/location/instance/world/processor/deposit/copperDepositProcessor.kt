package com.ovle.rl.content.location.instance.world.processor.deposit

import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.content.tile.naturalWallMineralCopper
import com.ovle.rl.model.procgen.grid.processor.location.LocalAreasLocationProcessor

val copperDepositProcessor = LocalAreasLocationProcessor(
    LocalAreasLocationProcessor.Companion.Params(
        baseTile = naturalWallMineralCopper,
        onePerTiles = 25,
        sizeRange = 3..24,
        tileCheck = { v -> TileMapChecker().isSedimentaryLayerDeposit(v) }
    )
)