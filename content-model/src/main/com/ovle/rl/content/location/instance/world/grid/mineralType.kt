package com.ovle.rl.content.location.instance.world.grid

import com.badlogic.gdx.math.GridPoint2
import com.github.czyzby.noise4j.map.Grid
import com.ovle.rl.content.location.instance.world.WORLD_LOCATION_SIZE
import com.ovle.utils.noise4j.grid.factory.impl.fractal.FractalGridFactory
import com.ovle.utils.noise4j.grid.pointsNear

fun mineralTypeGridFactory(locationPoint: GridPoint2, grid: Grid) = FractalGridFactory(
    params = FractalGridFactory.Companion.Params(
        size = WORLD_LOCATION_SIZE,
        startIteration = 1..1,
        noise = 1.0f,
        initialBorderValues = grid.pointsNear(locationPoint)
    )
)