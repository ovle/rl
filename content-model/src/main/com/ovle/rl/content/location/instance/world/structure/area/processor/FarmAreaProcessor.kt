package com.ovle.rl.content.location.instance.world.structure.area.processor

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.content.area.farmArea
import com.ovle.rl.content.task.type.farm.cropFarmTemplate
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LAYOUT_LEVEL_AVG
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaPartSpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaSpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaProcessor
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.allBorders
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.math.rect.RectArea

class FarmAreaProcessor(
    private val params: Params,
): AreaProcessor {

    companion object {
        class Params(
            val cropTemplates: Collection<FarmTemplate>,
            val fenceTemplate: EntityTemplate,
            val cropChancePerTile: Float,
        )
    }

    override fun weight(area: RectArea, layoutValue: Float) =
        if (layoutValue < LAYOUT_LEVEL_AVG) 0.1f else 0.0f

    override fun process(area: RectArea, layoutValue: Float, random: RandomParams): AreaSpawn {
        val r = random.kRandom

        val filters = listOf(::pFilterCross, ::pFilterHLine)
        val filter = filters.random(r)
        val points = area.points
        val center = area.center
        val gatePoints = setOf(
            point(center.x, area.y1), point(center.x, area.y2)
        )
        val borderPoints = points.allBorders().toSet()
        val fencePoints = borderPoints - gatePoints
        val waterPoints = (points - fencePoints).filter(filter).toSet()
        val soilPoints = points - waterPoints
        val farmAreaPoints = soilPoints - borderPoints
        val cropPoints = farmAreaPoints
            .filter { random.chance(params.cropChancePerTile) }
        val cropTemplate = params.cropTemplates.random(r)

        return AreaSpawn(
            area = area,
            parts = listOf(
                AreaPartSpawn(
                    points = farmAreaPoints,
                    areaTemplate = farmArea,
                    areaParams = AreaParams.Farm(cropTemplate)
                )
            ),
            pointsByTile = mapOf(
                TileType.WATER to waterPoints,
                TileType.SOIL to soilPoints,
            ),
            entitySpawns = cropPoints.map {
                EntitySpawn(
                    template = cropTemplate.crop,
                    position = it
                )
            } + fencePoints.map {
                EntitySpawn(
                    template = params.fenceTemplate,
                    position = it
                )
            }
        )
    }


    private fun pFilterCross(p: GridPoint2) = p.x % 2 == 0 && p.y % 2 == 0

    private fun pFilterHLine(p: GridPoint2) = p.y % 3 == 0
}