package com.ovle.rl.content.location.faction

import com.ovle.rl.model.game.social.dto.FactionFactionRelation
import com.ovle.rl.model.game.social.dto.RelationValue


internal val defaultRelations = listOf(
    FactionFactionRelation(
        faction1 = playerFaction,
        faction2 = enemyFaction,
        value = RelationValue.BAD
    )
)