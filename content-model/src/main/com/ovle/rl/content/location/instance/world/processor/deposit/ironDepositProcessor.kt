package com.ovle.rl.content.location.instance.world.processor.deposit

import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.content.tile.naturalWallMineralIron
import com.ovle.rl.model.procgen.grid.processor.location.LocalAreasLocationProcessor

val ironDepositProcessor = LocalAreasLocationProcessor(
    LocalAreasLocationProcessor.Companion.Params(
        baseTile = naturalWallMineralIron,
        onePerTiles = 50,
        sizeRange = 2..10,
        tileCheck = { v -> TileMapChecker().isMetaLayerDeposit(v) }
    )
)