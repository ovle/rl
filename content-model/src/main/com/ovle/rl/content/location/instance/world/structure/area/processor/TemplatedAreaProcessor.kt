package com.ovle.rl.content.location.instance.world.structure.area.processor

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType.UNKNOWN
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType.values
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.*
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.math.rect.RectArea
import com.ovle.utils.weightedIndex

class TemplatedAreaProcessor(
    private val areaTemplates: Collection<AreaTemplate>,
    private val roomEntitySpawnHelper: RoomEntitySpawnHelper,
): AreaProcessor {

    override fun weight(area: RectArea, layoutValue: Float) = 0.5f

    override fun process(area: RectArea, layoutValue: Float, random: RandomParams): AreaSpawn {
        val r = random.kRandom
        val areaTemplate = areaTemplates.filter {
            isValidTemplate(it, area)
        }.randomOrNull(r) ?: return AreaSpawn(
            area = area,
            parts = listOf(),
            pointsByTile = mapOf(UNKNOWN to area.points)
        )

        val mirrorX = r.nextBoolean()
        val mirrorY = r.nextBoolean()

        val pointsByTile = pointsByTile(area, areaTemplate, mirrorX, mirrorY)
        val rooms = areaTemplate.rooms
        val purposeByRoom = rooms.associateWith { purpose(it, random) }
        val entitySpawns = rooms.map {
            roomEntitySpawns(
                it, purposeByRoom[it], area, layoutValue,
                mirrorX, mirrorY, random
            )
        }.flatten()

        return AreaSpawn(
            area = area,
            parts = rooms.map {
                areaPartSpawn(
                    it, purposeByRoom[it],
                    area, mirrorX, mirrorY
                )
            },
            pointsByTile = pointsByTile,
            entitySpawns = entitySpawns
        )
    }

    private fun areaPartSpawn(
        room: AreaTemplateRoom,
        purpose: AreaPartPurpose?,
        area: RectArea,
        mirrorX: Boolean, mirrorY: Boolean
    ): AreaPartSpawn {
        val points = room.area.points.map {
            it.toAreaPoint(area, mirrorX, mirrorY)
        }
        return AreaPartSpawn(
            points = points,
            areaTemplate = purpose?.areaTemplate,
            areaParams = purpose?.getAreaParams?.invoke()
        )
    }



    private fun pointsByTile(
        area: RectArea,
        areaTemplate: AreaTemplate,
        mirrorX: Boolean, mirrorY: Boolean
    ): Map<TileType, Collection<GridPoint2>> {
        val x = if (mirrorX) area.x2 else area.x1
        val y = if (mirrorY) area.y2 else area.y1
        val mask = areaTemplate.parsedMask
        val result = values().associateWith { mutableListOf<GridPoint2>() }

        mask.forEachIndexed { i, _ ->
            mask[i].forEachIndexed { j, _ ->
                val tileChar = mask[i][j]
                val resultX = if (mirrorX) x - j else x + j
                val resultY = if (mirrorY) y - i else y + i
                val point = point(resultX, resultY)

                val tileType = TileType.byChar(tileChar)
                result[tileType]?.plusAssign(point)
            }
        }
        return result
    }

    private fun roomEntitySpawns(
        room: AreaTemplateRoom,
        purpose: AreaPartPurpose?,
        area: RectArea,
        layoutValue: Float,
        mirrorX: Boolean, mirrorY: Boolean,
        random: RandomParams
    ): Collection<EntitySpawn> {
        if (purpose == null) return emptyList()

        return roomEntitySpawnHelper.roomEntitySpawns(
            room, layoutValue, purpose, random.kRandom
        ).onEach {
            it.position = it.position.toAreaPoint(area, mirrorX, mirrorY)
        }
    }

    private fun purpose(
        room: AreaTemplateRoom, random: RandomParams
    ): AreaPartPurpose? {
        val purposes = room.purposes.toList()
        if (purposes.isEmpty()) return null

        val weights = purposes.map { it.weight }
        val checkValue = random.kRandom.nextFloat()
        val index = weights.weightedIndex(checkValue)
        index ?: return null

        return purposes[index]
    }

    private fun isValidTemplate(template: AreaTemplate, area: RectArea) =
        (template.w == area.width && template.h == area.height)

    private fun GridPoint2.toAreaPoint(
        area: RectArea, mirrorX: Boolean, mirrorY: Boolean
    ): GridPoint2 {
        val areaX = if (mirrorX) area.width - x - 1 else x
        val areaY = if (!mirrorY) area.height - y - 1 else y

        return point(areaX, areaY).add(area.origin)
    }
}