package com.ovle.rl.content.location.instance.world

import com.ovle.rl.content.entity.entityTemplates
import com.ovle.rl.content.game.condition.everyoneDiesLoseCondition
import com.ovle.rl.content.location.faction.defaultRelations
import com.ovle.rl.content.location.faction.neutralsFaction
import com.ovle.rl.content.location.faction.playerFaction
import com.ovle.rl.content.location.faction.wildFaction
import com.ovle.rl.content.location.instance.world.grid.*
import com.ovle.rl.content.location.instance.world.processor.deposit.copperDepositProcessor
import com.ovle.rl.content.location.instance.world.processor.deposit.diamondDepositProcessor
import com.ovle.rl.content.location.instance.world.processor.deposit.goldDepositProcessor
import com.ovle.rl.content.location.instance.world.processor.deposit.ironDepositProcessor
import com.ovle.rl.content.location.instance.world.processor.entitySpawnProcessor
import com.ovle.rl.content.location.instance.world.processor.predefinedStructureProcessor
import com.ovle.rl.content.location.instance.world.structure.embarkSimpleStructure
import com.ovle.rl.content.location.spawn.portalSpawns
import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.content.tile.mapper.WorldTileMapper
import com.ovle.rl.content.time.globalTimers
import com.ovle.rl.model.game.social.dto.FactionsConfig
import com.ovle.rl.model.game.spawn.LocationEntitySpawnConfig
import com.ovle.rl.model.procgen.config.LocationTemplate


val worldLocation = LocationTemplate(
    name = "Common",
    getGridFactories = { payload ->
        val locationPoint = payload!!.point
        val grids = payload.world.grids.grids

        arrayOf(
            altitudeGridFactory(locationPoint, grids[0]),
            temperatureGridFactory(locationPoint, grids[1]),
            humidityGridFactory(locationPoint, grids[2]),
            mineralTypeGridFactory(locationPoint, grids[3]),
            chasmGridFactory(locationPoint, grids[4]),
        )
    },
    postProcessors = arrayOf(
        copperDepositProcessor,
        ironDepositProcessor,
        goldDepositProcessor,
        diamondDepositProcessor,
        //lavaStreamsProcessor,
        //waterStreamsProcessor,
        predefinedStructureProcessor(
            listOf(
                embarkSimpleStructure,
                //portalStructure
            )
        ),
        entitySpawnProcessor
    ),
    tileMapper = WorldTileMapper(TileMapChecker()),
    globalTimerTemplates = globalTimers,
    finishGameConditionsCheck = listOf(
        everyoneDiesLoseCondition,
    ),
    factions = FactionsConfig(
        playerFaction = playerFaction,
        aiPlayerFaction = neutralsFaction,
        relations = defaultRelations,
        getSpawnFaction = { _ -> wildFaction }
    ),
    entitySpawnConfig = LocationEntitySpawnConfig(
        allowedEntities = entityTemplates,
        portalEntities = portalSpawns
    )
)