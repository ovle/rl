package com.ovle.rl.content.location.instance.battle

import com.ovle.rl.content.entity.creature.skeletonEntity
import com.ovle.rl.content.entity.creature.chelEntity
import com.ovle.rl.content.location.faction.enemyFaction
import com.ovle.rl.content.location.faction.playerFaction
import com.ovle.rl.model.template.structure.StructureEntities
import com.ovle.rl.model.template.structure.StructureEntity
import com.ovle.rl.model.template.structure.PredefinedStructureTemplate
import com.ovle.utils.gdx.math.point.point

val testBattleStructure = PredefinedStructureTemplate(
    name = "playground 8",
    mask = """  
      ` ` ` ` ` ` ` ` `
      ` ` ` ` ` ` ` ` `
      ` ` ` ` ` ` ` ` `
      ` ` ` ` ` ` ` ` `
     """,
    entities =
        StructureEntities(
            prototype = StructureEntity(
                template = skeletonEntity,
                faction = enemyFaction,
            ), positions = listOf(point(0, 1), point(0, 4))
        ).create() +
        listOf(
//            StructureEntity(
//                template = necromancerEntity,
//                structurePosition = point(1, 3),
//                factions = listOf(undeadFaction),
//            )
        ) +
        StructureEntities(
            prototype = StructureEntity(
                template = chelEntity,
                faction = playerFaction,
            ), positions = listOf(point(6, 2), point(6, 3))
        ).create() +
        listOf(
//            StructureEntity(
//                template = priestEntity,
//                structurePosition = point(6, 0),
//                factions = listOf(villagersFaction),
//            )
        )
)