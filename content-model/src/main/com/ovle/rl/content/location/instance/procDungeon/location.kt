package com.ovle.rl.content.location.instance.procDungeon

import com.github.czyzby.noise4j.map.generator.room.RoomType.DefaultRoomType
import com.ovle.rl.content.entity.entityTemplates
import com.ovle.rl.content.tile.artFloorStone
import com.ovle.rl.content.tile.artWallStone
import com.ovle.rl.content.tile.mapper.DungeonLocationTileMapper
import com.ovle.rl.content.tile.pitFloor
import com.ovle.rl.content.location.faction.defaultRelations
import com.ovle.rl.content.location.faction.neutralsFaction
import com.ovle.rl.content.location.faction.playerFaction
import com.ovle.rl.content.location.getNopGridFactory
import com.ovle.rl.content.location.instance.world.structure.embarkStructure
import com.ovle.rl.content.location.instance.world.processor.predefinedStructureProcessor
import com.ovle.rl.content.tile.mapper.TileMapChecker.Companion.SAMPLE_GROUND_LEVEL_V0
import com.ovle.rl.content.tile.naturalFloorSedStone
import com.ovle.rl.model.game.spawn.LocationEntitySpawnConfig
import com.ovle.rl.model.game.social.dto.FactionsConfig
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.game.spawn.EntitySpawnCheckHelper
import com.ovle.rl.model.procgen.grid.processor.location.entity.EntitySpawnProcessor
import com.ovle.rl.model.game.spawn.EntitySpawnHelper
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomsProcessor
import com.ovle.rl.content.location.instance.world.structure.room.ColonnadeRoomStructure
import com.ovle.rl.content.location.instance.world.structure.room.FilledCenterRoomStructure
import com.ovle.rl.content.location.instance.world.structure.room.NopRoomStructure
import com.ovle.utils.noise4j.grid.factory.impl.dungeon.DungeonGridFactory


private const val LOCATION_SIZE = 65

val testProcDungeonLocation = LocationTemplate(
    name = "Proc dungeon",
    getGridFactories = { _ ->
        arrayOf(
            DungeonGridFactory(
                params = DungeonGridFactory.Companion.Params(
                    size = LOCATION_SIZE,
                    roomTypes = arrayOf(
                        DefaultRoomType.SQUARE,
                    ),
                    maxRoomSize = 11,
                    minRoomSize = 3,
                    tolerance = 5,
                    windingChance = 0.25f,
                    randomConnectorChance = 0.05f
                )
            ),
            getNopGridFactory(LOCATION_SIZE, SAMPLE_GROUND_LEVEL_V0),
            getNopGridFactory(LOCATION_SIZE, SAMPLE_GROUND_LEVEL_V0)
        )
    },
    postProcessors = arrayOf(
        RoomsProcessor(
            structures = listOf(
                NopRoomStructure(),
                ColonnadeRoomStructure(artFloorStone, artWallStone),
                FilledCenterRoomStructure(pitFloor, artWallStone),
            ), baseTile = artFloorStone
        ),
        EntitySpawnProcessor(
            entityTemplates,
            EntitySpawnHelper(EntitySpawnCheckHelper())
        ),
        predefinedStructureProcessor(listOf(embarkStructure)),
    ),
    tileMapper = DungeonLocationTileMapper(
        DungeonLocationTileMapper.Companion.Params(
            artWallStone, artFloorStone, naturalFloorSedStone
        )
    ),
    finishGameConditionsCheck = listOf(),
    factions = FactionsConfig(
        playerFaction = playerFaction,
        aiPlayerFaction = neutralsFaction,
        relations = defaultRelations,
        getSpawnFaction = { _ -> neutralsFaction }
    ),
    entitySpawnConfig = LocationEntitySpawnConfig(
        entityTemplates, emptySet()
    )
)