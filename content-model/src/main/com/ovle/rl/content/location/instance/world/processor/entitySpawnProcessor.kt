package com.ovle.rl.content.location.instance.world.processor

import com.ovle.rl.content.entity.entityTemplates
import com.ovle.rl.model.game.spawn.EntitySpawnCheckHelper
import com.ovle.rl.model.procgen.grid.processor.location.entity.EntitySpawnProcessor
import com.ovle.rl.model.game.spawn.EntitySpawnHelper

val entitySpawnProcessor = EntitySpawnProcessor(
    entityTemplates,
    EntitySpawnHelper(EntitySpawnCheckHelper())
)