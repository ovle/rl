package com.ovle.rl.content.location.instance.world.structure

import com.ovle.rl.content.entity.creature.chelEntity
import com.ovle.rl.content.entity.other.keeperEntity
import com.ovle.rl.content.entity.resource.raw.grainEntity
import com.ovle.rl.content.location.faction.playerFaction
import com.ovle.rl.model.template.structure.StructureEntities
import com.ovle.rl.model.template.structure.StructureEntity
import com.ovle.rl.model.template.structure.PredefinedStructureTemplate
import com.ovle.utils.gdx.math.point.point

//todo select at world screen
val embarkStructure = PredefinedStructureTemplate(
    name = "embark place",
    mask = """
     x x X x x
     x X X X x 
     X X X X X
     x X X X x
     x x X x x 
     """,
    entities = listOf(
        StructureEntity(
            template = chelEntity,
            structurePosition = point(1, 1),
            faction = playerFaction,
        ),
        StructureEntity(
            template = chelEntity,
            structurePosition = point(3, 1),
            faction = playerFaction,
        ),
        StructureEntity(
            template = keeperEntity,
            structurePosition = point(2, 2),
            faction = playerFaction,
        ),
    ) + StructureEntities(
        prototype = StructureEntity(
            template = grainEntity,
            faction = playerFaction,
        ), positions = listOf(
            point(3, 3), point(3, 3), point(3, 3)
        )
    ).create(),

    tileFilter = { t ->
        val props = t.props
        (props.isFloor || props.isWall) && props.isNatural
    }
)