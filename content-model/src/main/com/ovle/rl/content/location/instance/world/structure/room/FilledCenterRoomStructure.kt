package com.ovle.rl.content.location.instance.world.structure.room

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.DirectionValue
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomInfo
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.DirectionValue.*
import com.ovle.rl.content.location.instance.world.structure.room.FilledCenterRoomStructure.Companion.Params
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomStructure
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2

//todo fix
//todo issue with castle, diamond, cross rooms
class FilledCenterRoomStructure(
    private val pitTile: TileTemplate,
    private val wallTile: TileTemplate
) : RoomStructure<Params> {

    companion object {
        data class Params(
            val centerSize: Int,
            val isHollow: Boolean,
            val isInverted: Boolean,
            val pathDirection: DirectionValue,
        )
    }

    override fun initParams(r: kotlin.random.Random): Params {
        val centerSize = arrayOf(3, 4, 5).random(r)
        val isInverted = arrayOf(true, false).random(r)
        val isHollow = arrayOf(true, false).random(r)
        val pathDirection = values()
            .filter { !isHollow || it != NO_DIRECTION }.random(r)

        return Params(centerSize, isHollow, isInverted, pathDirection)
    }

    override fun processTile(point: GridPoint2, room: RoomInfo, tiles: TileArray, params: Params, r: kotlin.random.Random) {
        val (x, y) = point

        val isFilledTile = isFilledTile(params, point, room)
        val isHPathTile = y == room.y + room.height / 2
        val isVPathTile = x == room.x + room.width / 2
        val haveHPath = params.pathDirection in setOf(H, HV)
        val haveVPath = params.pathDirection in setOf(V, HV)
        val isPathTile = haveHPath && isHPathTile || haveVPath && isVPathTile
        if (isPathTile) return

        if (isFilledTile) {
            val tile = if (params.isInverted) pitTile else wallTile
            tiles[x, y] = tile
        }
    }


    private fun isFilledTile(params: Params, point: GridPoint2, room: RoomInfo): Boolean {
        val (x, y) = point

        val roomCenterX = room.x + room.width / 2
        val roomCenterY = room.y + room.height / 2
        val sizeX = room.width / params.centerSize
        val sizeY = room.height / params.centerSize
        val xRange = (roomCenterX - sizeX..roomCenterX + sizeX)
        val yRange = (roomCenterY - sizeY..roomCenterY + sizeY)
        val xInRange = x in xRange
        val yInRange = y in yRange

        if (!params.isHollow) return xInRange && yInRange

        val isVWall = (x == xRange.first || x == xRange.last) && yInRange
        val isHWall = (y == yRange.first || y == yRange.last) && xInRange
        return isVWall || isHWall
    }
}