package com.ovle.rl.content.location.instance.battle

import com.ovle.rl.content.entity.entityTemplates
import com.ovle.rl.content.tile.mapper.WorldTileMapper
import com.ovle.rl.content.tile.tileTemplates
import com.ovle.rl.content.location.faction.defaultRelations
import com.ovle.rl.content.location.faction.neutralsFaction
import com.ovle.rl.content.location.faction.playerFaction
import com.ovle.rl.content.location.getDefaultGridFactories
import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.model.game.spawn.LocationEntitySpawnConfig
import com.ovle.rl.model.game.social.dto.FactionsConfig
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.utils.gdx.math.array2d.ArrayValidationHelper
import com.ovle.rl.model.procgen.grid.processor.location.structure.PredefinedStructureProcessor


val testBattleLocation = LocationTemplate(
    name = "Battle",
    getGridFactories = getDefaultGridFactories(),
    postProcessors = arrayOf(
        PredefinedStructureProcessor(
            listOf(testBattleStructure), tileTemplates, ArrayValidationHelper()
        )
    ),
    tileMapper = WorldTileMapper(TileMapChecker()),
    factions = FactionsConfig(
        playerFaction = playerFaction,
        aiPlayerFaction = neutralsFaction,
        relations = defaultRelations,
        getSpawnFaction = { _ -> neutralsFaction }
    ),
    entitySpawnConfig = LocationEntitySpawnConfig(
        entityTemplates, emptySet()
    )
)