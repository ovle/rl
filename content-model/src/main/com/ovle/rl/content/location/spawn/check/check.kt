package com.ovle.rl.content.location.spawn.check

import com.ovle.rl.content.area.houseArea
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.spawn.portal.PortalEntitySpawnCheck

val defaultSpawnCheck = PortalEntitySpawnCheck(
    check = { lp, _, _, _ -> hasFreeHouseSpawnCheck(lp) },
    description = "free house"
)

private fun hasFreeHouseSpawnCheck(lp: LocationPlayer): Boolean {
    val freeHouses = lp.areas
        .filter { it.template == houseArea }
        .filter {
            val p = it.params as AreaParams.House
            it.isValid && p.isActive && p.host == null
        }

    return freeHouses.isNotEmpty()
}