package com.ovle.rl.content.location.instance.world.structure.room

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.DirectionValue
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomInfo
import com.ovle.rl.content.location.instance.world.structure.room.ColonnadeRoomStructure.Companion
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.DirectionValue.*
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomStructure
import com.ovle.utils.gdx.math.point.adjHV
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2


class ColonnadeRoomStructure(
    private val floorTile: TileTemplate,
    private val wallTile: TileTemplate
): RoomStructure<Companion.Params> {

    companion object {
        data class Params(val direction: DirectionValue)
    }

    override fun initParams(r: kotlin.random.Random)
        = Params(values().random(r))

    override fun processTile(point: GridPoint2, room: RoomInfo, tiles: TileArray, params: Params, r: kotlin.random.Random) {
        val dirValue = params.direction
        val isFreeSpaceTile = point.adjHV()
            .map { tiles[it.x, it.y] }
            .all { it == floorTile }
        if (!isFreeSpaceTile) return

        val isColumn = isColumn(point, room, dirValue)
        if (isColumn) {
            tiles[point.x, point.y] = wallTile
        }
    }

    private fun isColumn(point: GridPoint2, room: RoomInfo, dirValue: DirectionValue): Boolean {
        val (x, y) = point
        val isHColumn = x % 2 == 0 && (y == room.y + 1 || y == room.y + room.height - 1)
        val isVColumn = y % 2 == 0 && (x == room.x + 1 || x == room.x + room.width - 1)
        return (isHColumn && (dirValue in setOf(H, HV))
            || isVColumn && (dirValue in setOf(V, HV))
            || isHColumn && isVColumn && dirValue == NO_DIRECTION)
    }
}