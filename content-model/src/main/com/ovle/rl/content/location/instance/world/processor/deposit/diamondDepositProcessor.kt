package com.ovle.rl.content.location.instance.world.processor.deposit

import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.content.tile.naturalWallMineralDiamond
import com.ovle.rl.model.procgen.grid.processor.location.LocalAreasLocationProcessor

val diamondDepositProcessor = LocalAreasLocationProcessor(
    LocalAreasLocationProcessor.Companion.Params(
        baseTile = naturalWallMineralDiamond,
        onePerTiles = 300,
        sizeRange = 1..3,
        tileCheck = { v -> TileMapChecker().isIgneousLayerDeposit(v) }
    )
)