package com.ovle.rl.content.location.instance.world.structure.area.processor

import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LAYOUT_LEVEL_AVG
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaSpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaProcessor
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.allBorders
import com.ovle.utils.gdx.math.rect.RectArea
import com.ovle.utils.random

class ParkAreaProcessor(
    private val params: Params,
): AreaProcessor {

    companion object {
        class Params(
            val treeTemplates: Collection<EntityTemplate>,
            val treeChancePerTile: Float,
        )
    }

    override fun weight(area: RectArea, layoutValue: Float) =
        if (layoutValue >= LAYOUT_LEVEL_AVG) 0.1f else 0.0f

    override fun process(area: RectArea, layoutValue: Float, random: RandomParams): AreaSpawn {
        val r = random.kRandom
        val waterPoints = randomInnerArea(area, (1..3), random)
            ?.points?.toSet() ?: emptySet()
        val roadPoints = area.points.allBorders().toSet()
        val soilPoints = area.points - roadPoints - waterPoints
        val treePoints = soilPoints.filter {
            random.chance(params.treeChancePerTile)
        }

        return AreaSpawn(
            area = area,
            parts = listOf(),
            pointsByTile = mapOf(
                TileType.WATER to waterPoints,
                TileType.ROAD to roadPoints,
                TileType.SOIL to soilPoints,
            ),
            entitySpawns = treePoints.map {
                EntitySpawn(
                    template = params.treeTemplates.random(r),
                    position = it
                )
            }
        )
    }


    private fun randomInnerArea(area: RectArea, sizeRange: IntRange, random: RandomParams): RectArea? {
        val r = random.kRandom
        val width = sizeRange.random(r)
        val height = sizeRange.random(r)
        val x1 = (area.x1..(area.x2 - width))
            .randomOrNull(r) ?: return null
        val y1 = (area.y1..(area.y2 - height))
            .randomOrNull(r) ?: return null

        return RectArea(
            xs = x1..x1 + width,
            ys = y1..y1 + height
        )
    }
}