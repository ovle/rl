package com.ovle.rl.content.location.instance.world.processor

import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.content.tile.mapper.TileMapChecker.Companion.WATER_LEVEL_V0
import com.ovle.rl.content.tile.shallowWater
import com.ovle.rl.model.procgen.grid.processor.location.GradientPathLocationProcessor
import com.ovle.utils.noise4j.grid.factory.path.impl.GradientPathFactory

val waterStreamsProcessor = GradientPathLocationProcessor(
    GradientPathLocationProcessor.Companion.Params(
        newTile = shallowWater,
        filter = { g, p ->
            val v = g.value(p)
            TileMapChecker().isAllowedWaterStreams(v)
        },
        factoryParams = GradientPathFactory.Companion.Params(
            countRange = 0..5,
            startValue = WATER_LEVEL_V0 + 0.1f,
            finishValue = WATER_LEVEL_V0,
            erosionPower = 0.02f
        ),
        getGrid = { g -> g.grids[0] }
    )
)