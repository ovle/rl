package com.ovle.rl.content.location.instance.world.structure.room

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomInfo
import com.ovle.rl.content.location.instance.world.structure.room.RandomRoomStructure.Companion.Params
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomStructure


class RandomRoomStructure(
    private val pitTile: TileTemplate,
    private val wallTile: TileTemplate
): RoomStructure<Params> {

    companion object {
        data class Params(val noiseAmount: Float)
    }

    override fun initParams(r: kotlin.random.Random) = Params(r.nextFloat())

    override fun processTile(point: GridPoint2, room: RoomInfo, tiles: TileArray, params: Params, r: kotlin.random.Random) {
        val noiseAmount = params.noiseAmount
        if (r.nextDouble() >= noiseAmount) return

        val tile = arrayOf(pitTile, wallTile).random(r)

        tiles[point.x, point.y] = tile
    }
}