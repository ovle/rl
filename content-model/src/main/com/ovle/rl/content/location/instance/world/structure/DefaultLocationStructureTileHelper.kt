package com.ovle.rl.content.location.instance.world.structure

import com.ovle.rl.content.tile.*
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LAYOUT_LEVEL_HIGH
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LAYOUT_LEVEL_LOW
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LocationStructureTileHelper
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType.*
import com.ovle.utils.RandomParams


class DefaultLocationStructureTileHelper: LocationStructureTileHelper {

    override fun tile(
        type: TileType, layoutValue: Float, random: RandomParams
    ): TileTemplate? {
        val isHigh = layoutValue >= LAYOUT_LEVEL_HIGH
        val isLow = layoutValue <= LAYOUT_LEVEL_LOW

        val isFloor = type == FLOOR
        val isWall = type == WALL
        val isDoor = type == DOOR
        val isWindow = type == WINDOW
        val isRoad = type == ROAD
        val isSquare = type == SQUARE
        val isWater = type == WATER
        val isSoil = type == SOIL

        val isLowLayoutEmptyTile = random.chance(0.2f)
        val isLowLayoutEmptyRoadTile = random.chance(0.5f)
        val isUnknown = type == UNKNOWN

        return when {
            isFloor -> when {
                isHigh -> artFloorStone
                isLow && isLowLayoutEmptyTile-> null
                else -> artFloorWood
            }
            isWall -> when {
                isHigh -> artWallStone
                isLow && isLowLayoutEmptyTile -> artFloorWood
                else -> artWallWood
            }
            isDoor -> when {
                isHigh -> doorStone
                isLow && isLowLayoutEmptyTile -> null
                else -> doorWood
            }
            isWindow -> when {
                isHigh -> windowStone
                isLow -> artWallWood
                else -> windowWood
            }
            isRoad -> when {
                //isHigh -> artFloorStone
                isLow && isLowLayoutEmptyRoadTile -> null
                else -> artFloorStoneRoad
            }
            isSquare -> when {
                isHigh -> artFloorStone
                else -> artFloorStoneRoad
            }
            isWater -> shallowWater
            isSoil -> naturalFloorSoil

            isUnknown -> pitFloor
            else -> throw IllegalStateException("unexpected unknown tile material, type: $type")
        }
    }
}