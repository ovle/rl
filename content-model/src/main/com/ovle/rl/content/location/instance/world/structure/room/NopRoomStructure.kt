package com.ovle.rl.content.location.instance.world.structure.room

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomInfo
import com.ovle.rl.content.location.instance.world.structure.room.NopRoomStructure.Companion.Params
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.room.RoomStructure


class NopRoomStructure: RoomStructure<Params> {

    companion object {
        object Params
    }

    override fun initParams(r: kotlin.random.Random) = Params

    override fun processTile(point: GridPoint2, room: RoomInfo, tiles: TileArray, params: Params, r: kotlin.random.Random) {}
}