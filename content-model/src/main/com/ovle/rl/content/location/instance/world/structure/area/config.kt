package com.ovle.rl.content.location.instance.world.structure.area

import com.ovle.rl.content.location.instance.world.structure.purpose.*
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaTemplate
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaTemplateRoom
import com.ovle.utils.gdx.math.rect.RectArea


val houseSq1 = AreaTemplate(
    name = "h-sq-1",
    h = 5, w = 5,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(1, 1, 3, 3),
            entitiesCount = 2..3,
            purposes = setOf(livingAreaPurpose) + industryPurposes + storagePurposes
        )
    ),
    mask = """  
          # # # # #
          # _ _ _ #
          d _ _ _ d
          # _ _ _ #
          # # D # #
     """
)

val houseSq2 = AreaTemplate(
    name = "h-sq-2",
    h = 5, w = 5,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(1, 1, 2, 3),
            entitiesCount = 2..3,
            purposes = setOf(livingAreaPurpose) + industryPurposes
        )
    ),
    mask = """  
          # # # # .
          # _ _ # .
          # _ _ d .
          # _ _ # .
          # # D # .
     """
)

val houseSq3 = AreaTemplate(
    name = "h-sq-3",
    h = 5, w = 5,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(1, 2, 3, 3),
            entitiesCount = 1..2,
            purposes = setOf(livingAreaPurpose)
        )
    ),
    mask = """  
          # # # # #
          # _ _ _ #
          # _ _ _ d
          # # D # #
          . . . . .
     """
)

val houseSq4 = AreaTemplate(
    name = "h-sq-4",
    h = 5, w = 5,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(1, 1, 3, 3),
            entitiesCount = 2..4,
            purposes = industryPurposes
        )
    ),
    mask = """  
          . # # # .
          # _ _ _ #
          d _ # _ d
          # _ _ _ #
          . # D # .
     """
)

val houseSq5 = AreaTemplate(
    name = "h-sq-5",
    h = 5, w = 5,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(1, 2, 3, 3),
            entitiesCount = 1..2,
            purposes = setOf(livingAreaPurpose)
        )
    ),
    mask = """  
          # # # # #
          # _ _ _ #
          # _ _ _ #
          # d # _ #
          . . # D #
     """
)

val houseH1 = AreaTemplate(
    name = "h-h-1",
    h = 5, w = 11,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(2, 1, 4, 3),
            entitiesCount = 1..2,
            purposes = setOf(livingAreaPurpose)
        ),
        AreaTemplateRoom(
            area = RectArea(6, 1, 8, 3),
            entitiesCount = 2..4,
            purposes = setOf(livingAreaPurpose)
        ),
    ),
    mask = """  
          . # # # # # # # # # .
          . # _ _ _ # _ _ _ # .
          . # _ _ _ _ _ _ _ # .
          . # _ _ _ # _ _ _ # .
          . # # D # # d # d # .
     """
)

val houseH2 = AreaTemplate(
    name = "h-h-2",
    h = 5, w = 11,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(1, 1, 2, 3),
            entitiesCount = 2..3,
            purposes = setOf(livingAreaPurpose)
        ),
        AreaTemplateRoom(
            area = RectArea(8, 1, 9, 3),
            entitiesCount = 2..3,
            purposes = setOf(livingAreaPurpose)
        ),
    ),
    mask = """  
          # # # # # # # # # # # 
          # _ _ _ _ _ _ _ _ _ #
          # _ _ # _ _ _ # _ _ #
          # _ _ # # D # # _ _ #
          # # d # . . . # d # #
     """
)

val houseV1 = AreaTemplate(
    name = "h-v-1",
    h = 11, w = 5,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(1, 2, 3, 4),
            entitiesCount = 1..2,
            purposes = setOf(socialAreaPurpose) + industryPurposes
        ),
        AreaTemplateRoom(
            area = RectArea(1, 6, 3, 8),
            entitiesCount = 2..4,
            purposes = setOf(livingAreaPurpose) + storagePurposes
        ),
    ),
    mask = """ 
          . . . . .
          # # # # #
          # _ _ _ #
          d _ _ _ d
          # _ _ _ #
          # # D # #
          # _ _ _ #
          d _ _ _ d
          # _ _ _ #
          # # D # #
          . . . . .
     """
)

val houseV2 = AreaTemplate(
    name = "h-v-2",
    h = 11, w = 5,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(1, 1, 3, 2),
            entitiesCount = 0..1,
            purposes = setOf()
        ),
        AreaTemplateRoom(
            area = RectArea(1, 4, 3, 6),
            entitiesCount = 2..3,
            purposes = industryPurposes + storagePurposes
        ),
        AreaTemplateRoom(
            area = RectArea(1, 8, 3, 9),
            entitiesCount = 2..3,
            purposes = setOf(livingAreaPurpose)
        )
    ),
    mask = """  
          # # # # #
          # _ _ _ #
          # _ _ _ #
          # # _ # #
          d _ _ _ d
          # _ _ _ #
          d _ _ _ d
          # # D # #
          # _ _ _ #
          # _ _ _ #
          # # D # #
     """
)

val houseLs1 = AreaTemplate(
    name = "h-ls-1",
    h = 11, w = 11,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(0, 0, 11, 11),
            entitiesCount = 0..0,
            purposes = emptySet()
        ),
        AreaTemplateRoom(
            area = RectArea(3, 3, 7, 7),
            entitiesCount = 4..8,
            purposes = setOf(warfareAreaPurpose)
        )
    ),
    mask = """  
          . . . # # # # # . . .
          . # # # _ _ _ # # # .
          . # _ _ _ _ _ _ _ # .
          # # _ _ _ _ _ _ _ # #
          # _ _ _ # # # _ _ _ #
          # _ _ _ # # # _ _ _ #
          # _ _ _ # # # _ _ _ #
          # # _ _ _ _ _ _ _ # #
          . # _ _ _ _ _ _ _ # .
          . # # # _ _ _ # # # .
          . . . # # D # # . . .
     """
)

val houseLs2 = AreaTemplate(
    name = "h-ls-2",
    h = 11, w = 11,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(2, 6, 4, 8),
            entitiesCount = 2..3,
            purposes = setOf(livingAreaPurpose) + storagePurposes
        ),
        AreaTemplateRoom(
            area = RectArea(6, 2, 8, 8),
            entitiesCount = 2..6,
            purposes = setOf(socialAreaPurpose) + industryPurposes
        ),
    ),
    mask = """  
          . . . . . . . . . . .
          . # # # # # # # # # .
          . # _ _ _ # _ _ _ # .
          . # _ _ _ D _ _ _ d .
          . # _ _ _ # _ _ _ # .
          . # # d # # _ _ _ # .
          . . . . . # _ _ _ # .
          . . . . . # _ _ _ d .
          . . . . . # _ _ _ # .         
          . . . . . # # D # # .
          . . . . . . . . . . .
     """
)

val houseLs3 = AreaTemplate(
    name = "h-ls-3",
    h = 11, w = 11,
    rooms = listOf(
        AreaTemplateRoom(
            area = RectArea(2, 1, 8, 9),
            entitiesCount = 4..8,
            purposes = setOf(administrativeAreaPurpose)
        )
    ),
    mask = """  
          . # # # # # # # # # .
          . # _ _ _ _ _ _ _ # .
          . # _ # _ _ _ # _ # .
          . d _ _ _ _ _ _ _ d .
          . # _ # _ _ _ # _ # .
          . d _ _ _ _ _ _ _ d .
          . # _ # _ _ _ # _ # .
          . d _ _ _ _ _ _ _ d .
          . # _ # _ _ _ # _ # .
          . # _ _ _ _ _ _ _ # .
          . # # # D # D # # # .
     """
)

val squareSq1 = AreaTemplate(
    name = "s-sq-1",
    h = 5, w = 5,
    rooms = listOf(),
    mask = """  
          . . . . .
          . 0 0 0 .
          . 0 0 0 .
          . 0 0 0 .
          . . . . .
     """
)

val squareLs1 = AreaTemplate(
    name = "s-ls-1",
    h = 11, w = 11,
    rooms = listOf(),
    mask = """  
          . . . . . . . . . . .
          . . . . . . . . . . .
          . . # 0 0 0 0 0 # . .
          . . 0 0 0 0 0 0 0 . .
          . . 0 0 0 # 0 0 0 . .
          . . 0 0 # # # 0 0 . .
          . . 0 0 0 # 0 0 0 . .
          . . 0 0 0 0 0 0 0 . .
          . . # 0 0 0 0 0 # . .
          . . . . . . . . . . .
          . . . . . . . . . . .
     """
)


val areaTemplates = listOf(
    houseSq1, houseSq2, houseSq3, houseSq4, houseSq5,
    houseH1, houseH2,
    houseV1, houseV2,
    houseLs1, houseLs2, houseLs3,
    squareLs1, squareSq1
)