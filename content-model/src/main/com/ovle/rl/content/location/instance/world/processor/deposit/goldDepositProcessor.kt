package com.ovle.rl.content.location.instance.world.processor.deposit

import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.content.tile.naturalWallMineralGold
import com.ovle.rl.model.procgen.grid.processor.location.LocalAreasLocationProcessor

val goldDepositProcessor = LocalAreasLocationProcessor(
    LocalAreasLocationProcessor.Companion.Params(
        baseTile = naturalWallMineralGold,
        onePerTiles = 100,
        sizeRange = 2..8,
        tileCheck = { v -> TileMapChecker().isMetaLayerDeposit(v) }
    )
)