package com.ovle.rl.content.location.instance.demo

import com.ovle.rl.content.entity.creature.*
import com.ovle.rl.content.entity.furniture.stashEntity
import com.ovle.rl.content.entity.other.keeperEntity
import com.ovle.rl.content.entity.other.portalEntity
import com.ovle.rl.content.entity.resource.raw.grainEntity
import com.ovle.rl.content.location.faction.enemyFaction
import com.ovle.rl.content.location.faction.neutralsFaction
import com.ovle.rl.content.location.faction.playerFaction
import com.ovle.rl.model.template.structure.StructureEntity
import com.ovle.rl.model.template.structure.PredefinedStructureTemplate
import com.ovle.utils.gdx.math.point.point


val testDemoStructure = PredefinedStructureTemplate(
    name = "demo playground",
    mask = """  
      O O O O O O O O O O O O O O O O
      w w w : : : = = = = = = i i i O
      w w w : : = = = = = c = . . . O
      : w w w = = = = = c c c . . . O
      : : . w : = = : : : = = = = = O
      . . . w : : : : : : : = = = = O
      : : w w . . . . . . : . . = = O
      : : w : . . . . . X X X : : = O
      : w : : : : . . . X X X : = = O
      = = = = : : . . . X X X = = = O
      = = = = = = : : : : : : = = = O
      = = = = = = = = = = = = = O O O
      O O = O O O = = = = = O O O O O
      O . . . O O O O O O O O O O O O
      O . . . O O O . . . O O O O O O
      O O O O O O O O O O O O O O O O
     """,
    entities =
        listOf(
            StructureEntity(
                template = keeperEntity,
                structurePosition = point(10, 7),
                faction = playerFaction,
            ),
            StructureEntity(
                template = chelEntity,
                structurePosition = point(6, 6),
                faction = playerFaction,
            ),
            StructureEntity(
                template = chelEntity,
                structurePosition = point(7, 6),
                faction = playerFaction,
            ),
            StructureEntity(
                template = portalEntity,
                structurePosition = point(2, 1),
            ),

            StructureEntity(
                template = batEntity,
                structurePosition = point(1, 13),
                faction = neutralsFaction,
            ),
            StructureEntity(
                template = grainEntity,
                structurePosition = point(6, 8),
            ),
            StructureEntity(
                template = stashEntity,
                structurePosition = point(8, 1),
            ),

            StructureEntity(
                template = skeletonEntity,
                structurePosition = point(13, 13),
                faction = enemyFaction,
            ),
            StructureEntity(
                template = ogreEntity,
                structurePosition = point(9, 1),
                faction = enemyFaction,
            ),
        )
)