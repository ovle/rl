package com.ovle.rl.content.location.instance.world.structure

import com.ovle.rl.content.entity.other.portalEntity
import com.ovle.rl.model.template.structure.StructureEntity
import com.ovle.rl.model.template.structure.PredefinedStructureTemplate
import com.ovle.utils.gdx.math.point.point

val portalStructure = PredefinedStructureTemplate(
    name = "portal",
    mask = """
     X X X 
     X X X
     X X X
     """,
    entities = listOf(
        StructureEntity(
            template = portalEntity,
            structurePosition = point(1, 1),
        ),
    ),
    tileFilter = { t ->
        val props = t.props
        (props.isFloor || props.isWall) && props.isNatural
    }
)