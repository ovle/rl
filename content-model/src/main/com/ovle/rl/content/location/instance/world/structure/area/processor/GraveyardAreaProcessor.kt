package com.ovle.rl.content.location.instance.world.structure.area.processor

import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LAYOUT_LEVEL_AVG
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaSpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaProcessor
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.allBorders
import com.ovle.utils.gdx.math.rect.RectArea

class GraveyardAreaProcessor(
    private val params: Params,
): AreaProcessor {

    companion object {
        class Params(
            val graveTemplates: Collection<EntityTemplate>,
        )
    }

    override fun weight(area: RectArea, layoutValue: Float) =
        if (layoutValue < LAYOUT_LEVEL_AVG) 0.1f else 0.0f


    override fun process(area: RectArea, layoutValue: Float, random: RandomParams): AreaSpawn {
        val r = random.kRandom
        val roadPoints = area.points.allBorders().toSet()
        val soilPoints = area.points - roadPoints
        val gravePoints = soilPoints.filter { it.x % 2 == 0 && it.y % 2 == 0}

        return AreaSpawn(
            area = area,
            parts = listOf(),
            pointsByTile = mapOf(
                TileType.ROAD to roadPoints,
                TileType.SOIL to soilPoints,
            ),
            entitySpawns = gravePoints.map {
                EntitySpawn(
                    template = params.graveTemplates.random(r),
                    position = it
                )
            }
        )
    }
}