package com.ovle.rl.content.location

import com.ovle.rl.content.tile.mapper.TileMapChecker.Companion.SAMPLE_GROUND_LEVEL_V0
import com.ovle.rl.model.procgen.config.GridFactoryPayload
import com.ovle.utils.noise4j.grid.factory.GridFactory
import com.ovle.utils.noise4j.grid.factory.impl.nop.NopGridFactory

fun getNopGridFactory(size: Int, value: Float): GridFactory<*> =
    NopGridFactory(
        params = NopGridFactory.Companion.Params(
            size = size,
            value = value
        )
    )

private const val DEFAULT_LOCATION_SIZE = 16

fun getDefaultGridFactories(): (GridFactoryPayload?) -> Array<GridFactory<*>> = { _ ->
    arrayOf(
        getNopGridFactory(DEFAULT_LOCATION_SIZE, SAMPLE_GROUND_LEVEL_V0),
        getNopGridFactory(DEFAULT_LOCATION_SIZE, 0.9f),
        getNopGridFactory(DEFAULT_LOCATION_SIZE, SAMPLE_GROUND_LEVEL_V0),
        getNopGridFactory(DEFAULT_LOCATION_SIZE, SAMPLE_GROUND_LEVEL_V0),
        getNopGridFactory(DEFAULT_LOCATION_SIZE, SAMPLE_GROUND_LEVEL_V0),
    )
}