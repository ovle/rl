package com.ovle.rl.content.location.instance.world.processor

import com.ovle.rl.content.tile.tileTemplates
import com.ovle.utils.gdx.math.array2d.ArrayValidationHelper
import com.ovle.rl.model.procgen.grid.processor.location.structure.PredefinedStructureProcessor
import com.ovle.rl.model.template.structure.PredefinedStructureTemplate

fun predefinedStructureProcessor(templates: Collection<PredefinedStructureTemplate>) = PredefinedStructureProcessor(
    templates, tileTemplates, ArrayValidationHelper()
)