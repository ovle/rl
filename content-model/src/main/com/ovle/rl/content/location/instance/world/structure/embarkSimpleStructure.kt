package com.ovle.rl.content.location.instance.world.structure

import com.ovle.rl.content.entity.creature.chelEntity
import com.ovle.rl.content.entity.other.keeperEntity
import com.ovle.rl.content.entity.resource.raw.grainEntity
import com.ovle.rl.content.location.faction.playerFaction
import com.ovle.rl.model.template.structure.StructureEntities
import com.ovle.rl.model.template.structure.StructureEntity
import com.ovle.rl.model.template.structure.PredefinedStructureTemplate
import com.ovle.utils.gdx.math.point.point

//todo select at world screen
val embarkSimpleStructure = PredefinedStructureTemplate(
    name = "embark place",
    mask = """
     X X X 
     X X X
     X X X
     """,
    entities = listOf(
        StructureEntity(
            template = chelEntity,
            structurePosition = point(0, 0),
            faction = playerFaction,
        ),
        StructureEntity(
            template = chelEntity,
            structurePosition = point(0, 2),
            faction = playerFaction,
        ),
        StructureEntity(
            template = keeperEntity,
            structurePosition = point(1, 1),
            faction = playerFaction,
        ),
    ) + StructureEntities(
        prototype = StructureEntity(
            template = grainEntity,
        ), positions = listOf(
            point(0, 1), point(0, 1)
        )
    ).create(),

    tileFilter = { t ->
        val props = t.props
        (props.isFloor || props.isWall) && props.isNatural
    }
)