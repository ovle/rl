package com.ovle.rl.content.location.spawn

import com.ovle.rl.content.entity.creature.chelEntity
import com.ovle.rl.content.location.spawn.check.defaultSpawnCheck
import com.ovle.rl.model.game.spawn.portal.PortalEntitySpawnTemplate

val portalSpawnChel = PortalEntitySpawnTemplate(
    entity = chelEntity,
    check = defaultSpawnCheck,
    chancePerHour = { i -> 0.2f / i }
)