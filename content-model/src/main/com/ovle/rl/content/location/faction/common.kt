package com.ovle.rl.content.location.faction

import com.ovle.rl.model.game.social.dto.FactionTemplate


internal val playerFaction = FactionTemplate(
    name = "player",
)

internal val enemyFaction = FactionTemplate(
    name = "enemy"
)

internal val neutralsFaction = FactionTemplate(
    name = "neutral"
)

internal val wildFaction = FactionTemplate(
    name = "wild"
)