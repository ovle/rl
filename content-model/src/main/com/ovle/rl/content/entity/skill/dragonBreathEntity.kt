package com.ovle.rl.content.entity.skill

import com.ovle.rl.content.aoe.lineAoe
import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.trigger.damageObstacleOnCollideTrigger
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.space.aoe.AOEComponent
import com.ovle.rl.model.game.trigger.TriggerComponent
import com.ovle.rl.model.template.entity.EntityTemplate


val dragonBreathEntity = EntityTemplate(
    name = "dragon breath",
    parent = objectEntity,
    getState = {
        listOf(
            AOEComponent(template = lineAoe, duration = 3),
            CollisionComponent(hasBody = false),
            TriggerComponent(
                triggers = mutableListOf(
                    damageObstacleOnCollideTrigger
                )
            ),
        )
    }
)