package com.ovle.rl.content.entity.plant.tree

import com.ovle.rl.content.entity.plant.treeEntity
import com.ovle.rl.content.tile.naturalFloorTundraSoil
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val borealTreeEntity = EntityTemplate(
    name = "boreal tree",
    shortName = "brt",
    parent = treeEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.1f,
        tileCheck = { t -> t == naturalFloorTundraSoil }
    )
)