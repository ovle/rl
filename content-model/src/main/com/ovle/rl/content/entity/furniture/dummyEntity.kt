package com.ovle.rl.content.entity.furniture

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.model.game.container.CarriableComponent
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val dummyEntity = EntityTemplate(
    name = "dummy",
    shortName = "dum",
    description = "training dummy",
    parent = objectEntity,
    getState = {
        listOf(
            //todo not really alive, but destructable
            HealthComponent(maxHealth = Int.MAX_VALUE),
            CarriableComponent(),
            MaterialComponent(),
        )
    }
)