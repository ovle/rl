package com.ovle.rl.content.entity.plant

import com.ovle.rl.content.ai.task.CombatAITaskFactory
import com.ovle.rl.content.ai.task.IdleAITaskFactory
import com.ovle.rl.content.location.faction.enemyFaction
import com.ovle.rl.content.skill.magicMissileSkill
import com.ovle.rl.content.tile.naturalFloorJungleGrass
import com.ovle.rl.model.game.ai.AIComponent
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.perception.PerceptionComponent
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.social.SocialComponent
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val peashooterEntity = EntityTemplate(
    name = "peashooter",
    shortName = "psr",
    parent = plantEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.0025f,
        tileCheck = { t -> t in setOf(
            naturalFloorJungleGrass
        )},
        nearRadius = 4
    ),
    getState = {
        listOf(
            HealthComponent(),
            SkillComponent(skills = setOf(magicMissileSkill)),
            AIComponent(
                aiTaskFactories = listOf(
                    CombatAITaskFactory(),
                    IdleAITaskFactory()
                )
            ),
            PerceptionComponent(sightRadius = 5),
            SocialComponent(faction = enemyFaction),
        )
    }
)