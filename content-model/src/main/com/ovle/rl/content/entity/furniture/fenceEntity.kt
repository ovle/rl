package com.ovle.rl.content.entity.furniture

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.material.woodMaterial
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.container.CarriableComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val fenceEntity = EntityTemplate(
    name = "fence",
    shortName = "fnc",
    description = "fence",
    parent = objectEntity,
    getState = {
        listOf(
            CollisionComponent(hasBody = true),
            CarriableComponent(),
            MaterialComponent(woodMaterial),
        )
    }
)