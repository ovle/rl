package com.ovle.rl.content.entity

import com.ovle.rl.content.entity.creature.*
import com.ovle.rl.content.entity.furniture.*
import com.ovle.rl.content.entity.other.*
import com.ovle.rl.content.entity.plant.cropStage1Entity
import com.ovle.rl.content.entity.plant.cropStage3Entity
import com.ovle.rl.content.entity.plant.peashooterEntity
import com.ovle.rl.content.entity.plant.tree.*
import com.ovle.rl.content.entity.resource.proc.*
import com.ovle.rl.content.entity.resource.raw.*
import com.ovle.rl.content.entity.skill.dragonBreathEntity
import com.ovle.rl.content.entity.skill.magicMissileEntity
import com.ovle.rl.content.entity.skill.peaEntity


val entityTemplates = listOf(
//not used directly
//    objectEntity,
//    creatureEntity,
//    treeEntity,

    portalEntity,
    keeperEntity,

    chelEntity,
    skeletonEntity,
    ogreEntity,

    batEntity,
    fishEntity,
    pigEntity,
    wolfEntity,
    dragonflyEntity,

    woodEntity,
    stoneEntity,
    copperEntity,
    ironEntity,
    goldEntity,
    diamondEntity,

    procWoodEntity,
    procStoneEntity,
    procCopperEntity,
    procIronEntity,
    procGoldEntity,

    cropStage1Entity,
    cropStage3Entity,
    grainEntity,
    peashooterEntity,

    breadEntity,

    borealTreeEntity,
    borealTreeSaplingEntity,
    temperateTreeEntity,
    temperateTreeSaplingEntity,
    jungleTreeEntity,
    jungleTreeSaplingEntity,
    jungleBigTreeEntity,
    desertTreeEntity,
    desertTreeSaplingEntity,
    fruitBushEntity,

    //altarEntity,
    bedEntity,
    dummyEntity,
    fenceEntity,
    stashEntity,
    tableEntity,
    chairEntity,
    closetEntity,
    barrelEntity,
    ovenEntity,
    anvilEntity,
    forgeEntity,
    masonWrkEntity,
    carpWrkEntity,

    graveEntity,
    bombEntity,

    magicMissileEntity,
    peaEntity,
    explosionEntity,
    dragonBreathEntity
)