package com.ovle.rl.content.entity.plant.tree

import com.ovle.rl.content.entity.plant.plantEntity
import com.ovle.rl.content.tile.naturalFloorSand
import com.ovle.rl.content.tile.shallowWater
import com.ovle.rl.content.time.TREE_GROW_CHANCE_PER_HOUR
import com.ovle.rl.content.time.TREE_SPAWN_CHANCE_PER_HOUR
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.game.life.age.TransformConfig
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val desertTreeSaplingEntity = EntityTemplate(
    name = "desert tree sap.",
    shortName = "dss",
    parent = plantEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.1f,
        chancePerHour = TREE_SPAWN_CHANCE_PER_HOUR,
        tileCheck = { t -> t == naturalFloorSand },
        nearTiles = setOf(shallowWater),
        nearRadius = 3
    ),
    getState = {
        listOf(
            AgeComponent(
                transform = TransformConfig(
                    entity = desertTreeEntity,
                    chancePerHour = TREE_GROW_CHANCE_PER_HOUR
                )
            )
        )
    }
)