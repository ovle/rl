package com.ovle.rl.content.entity

import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.effect.StaticEffectComponent
import com.ovle.rl.model.game.space.body.BodyComponent
import com.ovle.rl.model.game.time.TimerComponent
import com.ovle.rl.model.game.trigger.TriggerComponent
import com.ovle.rl.model.template.entity.EntityTemplate


val objectEntity = EntityTemplate(
    name = "object",
    description = "physical object",
    getState = {
        listOf(
            BodyComponent(),
            CollisionComponent(hasBody = false),
            TriggerComponent(),
            TimerComponent(),
            StaticEffectComponent()
        )
    }
)