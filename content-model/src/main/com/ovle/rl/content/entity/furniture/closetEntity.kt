package com.ovle.rl.content.entity.furniture

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.container.CarriableComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate


val closetEntity = EntityTemplate(
    name = "closet",
    shortName = "cls",
    parent = objectEntity,
    getState = {
        listOf(
            CollisionComponent(hasBody = false),
            CarriableComponent(),
            MaterialComponent()
        )
    }
)