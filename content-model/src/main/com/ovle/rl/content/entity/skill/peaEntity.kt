package com.ovle.rl.content.entity.skill

import com.ovle.rl.content.path.linePath
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val peaEntity = EntityTemplate(
    name = "pea",
    parent = projectileEntity,
    getState = {
        listOf(
            MoveComponent(
                defaultSpeed = 15.0, pathTemplate = linePath
            )
        )
    }
)