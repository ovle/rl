package com.ovle.rl.content.entity.plant

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.material.plantMaterial
import com.ovle.rl.content.time.CROP_GROW_CHANCE_PER_HOUR
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.game.life.age.TransformConfig
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val cropStage1Entity = EntityTemplate(
    name = "crop 1",
    shortName = "cr1",
    parent = objectEntity,
    getState = {
        listOf(
            CollisionComponent(hasBody = false),
            MaterialComponent(plantMaterial),
            AgeComponent(
                transform = TransformConfig(
                    entity = cropStage3Entity,
                    chancePerHour = CROP_GROW_CHANCE_PER_HOUR
                )
            )
        )
    }
)