package com.ovle.rl.content.entity.plant.tree

import com.ovle.rl.content.entity.plant.treeEntity
import com.ovle.rl.content.tile.naturalFloorJungleGrass
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val jungleBigTreeEntity = EntityTemplate(
    name = "big jungle tree",
    shortName = "jbt",
    parent = treeEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.05f,
        tileCheck = { t -> t == naturalFloorJungleGrass },
    ),
    getState = {
        listOf(
            AgeComponent(dieChancePerHour = null)
        )
    }
)