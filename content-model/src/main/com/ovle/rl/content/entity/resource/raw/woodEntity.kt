package com.ovle.rl.content.entity.resource.raw

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.woodMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val woodEntity = EntityTemplate(
    name = "wood (raw)",
    shortName = "wod-",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(woodMaterial)
        )
    }
)