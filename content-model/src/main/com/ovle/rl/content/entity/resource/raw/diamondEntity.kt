package com.ovle.rl.content.entity.resource.raw

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.diamondMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val diamondEntity = EntityTemplate(
    name = "diamond",
    shortName = "dia-",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(diamondMaterial)
        )
    }
)