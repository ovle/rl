package com.ovle.rl.content.entity.plant.tree

import com.ovle.rl.content.entity.plant.treeEntity
import com.ovle.rl.content.tile.naturalFloorGrass
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val temperateTreeEntity = EntityTemplate(
    name = "temperate tree",
    shortName = "tmt",
    parent = treeEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.15f,
        tileCheck = { t -> t == naturalFloorGrass },
    )
)