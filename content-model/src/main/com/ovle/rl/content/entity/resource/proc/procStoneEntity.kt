package com.ovle.rl.content.entity.resource.proc

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.stoneMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val procStoneEntity = EntityTemplate(
    name = "stone (cut)",
    shortName = "stn+",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(stoneMaterial)
        )
    }
)