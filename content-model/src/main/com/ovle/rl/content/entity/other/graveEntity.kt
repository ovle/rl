package com.ovle.rl.content.entity.other

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.container.ContainerComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val graveEntity = EntityTemplate(
    name = "grave",
    shortName = "grv",
    parent = objectEntity,
    getState = {
        listOf(
            CollisionComponent(hasBody = false),
            ContainerComponent(),
            MaterialComponent(),
        )
    }
)