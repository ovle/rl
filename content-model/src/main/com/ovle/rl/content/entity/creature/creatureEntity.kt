package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.ai.task.*
import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.material.meatMaterial
import com.ovle.rl.content.task.type.craft.craftTemplates
import com.ovle.rl.model.game.ai.AIComponent
import com.ovle.rl.model.game.container.ContainerComponent
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.game.perception.PerceptionComponent
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.skill.helper.PositionHelper
import com.ovle.rl.model.game.skill.helper.SkillHelper
import com.ovle.rl.model.game.social.SocialComponent
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.template.entity.EntityTemplate

val creatureEntity = EntityTemplate(
    name = "creature",
    description = "living creature",
    parent = objectEntity,
    getState = {
        listOf(
            AgeComponent(),
            HealthComponent(),
            MaterialComponent(meatMaterial),
            ContainerComponent(),
            AIComponent(aiTaskFactories = listOf(
                FleeAITaskFactory(
                    enemyRadius = 5,
                    belowHealth = 0.33f
                ),
                CombatAITaskFactory(), //todo task performer/combat priority
                EatAITaskFactory(
                    skillHelper = SkillHelper(PositionHelper((PassabilityHelper()))),
                ),
                SleepAITaskFactory(),
                TaskPerformerAITaskFactory(),
                IdleAITaskFactory()
            )),
            SkillComponent(),
            PerceptionComponent(sightRadius = 5),
            SocialComponent(),
        )
    }
)