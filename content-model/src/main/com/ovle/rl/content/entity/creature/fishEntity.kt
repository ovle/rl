package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.ai.task.*
import com.ovle.rl.content.time.FISH_DIE_CHANCE_PER_HOUR
import com.ovle.rl.content.time.FISH_SPAWN_CHANCE_PER_HOUR
import com.ovle.rl.model.game.ai.AIComponent
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val fishEntity = EntityTemplate(
    name = "fish",
    shortName = "fsh",
    parent = creatureEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.0025f,
        chancePerHour = FISH_SPAWN_CHANCE_PER_HOUR,
        tileCheck = { t -> t.props.isWater },
    ),
    getState = {
        listOf(
            AgeComponent(dieChancePerHour = FISH_DIE_CHANCE_PER_HOUR),
            MoveComponent(defaultSpeed = 2.0, type = MoveType.SWIMMING),
            AIComponent(aiTaskFactories = listOf(
                FleeAITaskFactory(),
                //EatDecision(),
                IdleAITaskFactory()
            )),
            HealthComponent(maxHealth = 1)
        )
    }
)