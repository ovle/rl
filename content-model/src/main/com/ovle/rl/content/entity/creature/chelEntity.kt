package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.material.cookMeatMaterial
import com.ovle.rl.content.material.cookPlantMaterial
import com.ovle.rl.content.material.fruitMaterial
import com.ovle.rl.content.material.meatMaterial
import com.ovle.rl.content.skill.*
import com.ovle.rl.model.game.container.CarrierComponent
import com.ovle.rl.model.game.house.HouseAwareComponent
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.schedule.DayScheduleComponent
import com.ovle.rl.model.game.life.hunger.HungerComponent
import com.ovle.rl.model.game.life.sleep.SleepComponent
import com.ovle.rl.model.game.schedule.DaySchedule
import com.ovle.rl.model.game.schedule.DayScheduleType
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.task.TaskPerformerComponent
import com.ovle.rl.model.game.time.SUNSET_HOUR
import com.ovle.rl.model.template.entity.EntityTemplate

val chelEntity = EntityTemplate(
    name = "chel",
    shortName = "chl",
    parent = creatureEntity,
    getState = {
        listOf(
            HealthComponent(maxHealth = 5),
            MoveComponent(defaultSpeed = 1.0),
            HungerComponent(
                maxHunger = 24,
                consumes = setOf(
                    fruitMaterial,
                    cookPlantMaterial,
                    cookMeatMaterial
                )
            ),
            SleepComponent(),
            HouseAwareComponent(),
            CarrierComponent(),
            TaskPerformerComponent(),
            DayScheduleComponent(
                config = listOf(
                    DaySchedule(
                        type = DayScheduleType.SLEEP,
                        start = SUNSET_HOUR,
                        duration = 8
                    )
                )
            ),
            SkillComponent(
                skills = setOf(
                    basicAttackSkill,
                    mineSkill,
                    gatherSkill,
                    buildSkill,
                    craftSkill,
                    farmSkill
                )
            )
        )
    }
)