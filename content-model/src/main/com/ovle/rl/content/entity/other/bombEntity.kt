package com.ovle.rl.content.entity.other

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.time.explodeTimer
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.container.CarriableComponent
import com.ovle.rl.model.game.time.TimerComponent
import com.ovle.rl.model.game.time.dto.Timer
import com.ovle.rl.model.template.entity.EntityTemplate

val bombEntity = EntityTemplate(
    name = "bomb",
    shortName = "bmb",
    parent = objectEntity,
    getState = {
        listOf(
            TimerComponent(
                timers = mutableListOf(
                    Timer(template = explodeTimer)
                )
            ),
            CollisionComponent(hasBody = false),
            CarriableComponent()
        )
    }
)

