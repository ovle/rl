package com.ovle.rl.content.entity.other

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.perception.PerceptionComponent
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.social.SocialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val keeperEntity = EntityTemplate(
    name = "keeper",
    shortName = "kep",
    description = "keeper of the dungeon",
    parent = objectEntity,
    getState = {
        listOf(
            HealthComponent(maxHealth = 20),
//            MaterialComponent(MaterialType.STONE),
            PerceptionComponent(sightRadius = 10),
            SocialComponent(isKeeper = true),
            SkillComponent() //doesn't own skills itself
        )
    }
)