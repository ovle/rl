package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.material.meatMaterial
import com.ovle.rl.content.skill.basicAttackSkill
import com.ovle.rl.content.tile.naturalFloorGrass
import com.ovle.rl.content.tile.naturalFloorTundraSoil
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.life.hunger.HungerComponent
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val wolfEntity = EntityTemplate(
    name = "wolf",
    shortName = "wlf",
    parent = creatureEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.0025f,
        tileCheck = { t -> t in setOf(
            naturalFloorGrass, naturalFloorTundraSoil
        )},
    ),
    getState = {
        listOf(
            MoveComponent(defaultSpeed = 2.0),
            HealthComponent(maxHealth = 3),
            SkillComponent(skills = listOf(basicAttackSkill)),
            HungerComponent(
                maxHunger = 24,
                consumes = listOf(meatMaterial)
            )
        )
    }
)