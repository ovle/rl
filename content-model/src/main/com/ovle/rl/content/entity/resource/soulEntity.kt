package com.ovle.rl.content.entity.resource

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.model.game.container.CarriableComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

/*
val soulEntity = EntityTemplate(
    name = "soul (simple)",
    description = "soul of the living being",
    parent = objectEntity,
    getState = {
        listOf(
            MaterialComponent(MaterialType.AETHER),
            CarriableComponent()
        )
    }
)
*/
