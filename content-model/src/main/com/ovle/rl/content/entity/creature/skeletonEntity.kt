package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.skill.basicAttackSkill
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.task.TaskPerformerComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val skeletonEntity = EntityTemplate(
    name = "skeleton",
    shortName = "skl",
    parent = creatureEntity,
    getState = {
        listOf(
            MoveComponent(defaultSpeed = 1.0),
            TaskPerformerComponent(),
            SkillComponent(skills = setOf(basicAttackSkill)),
        )
    }
)