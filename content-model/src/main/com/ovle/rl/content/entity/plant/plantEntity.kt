package com.ovle.rl.content.entity.plant

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.material.plantMaterial
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val plantEntity = EntityTemplate(
    name = "plant",
    parent = objectEntity,
    getState = {
        listOf(
            HealthComponent(maxHealth = 2),
            MaterialComponent(plantMaterial),
            CollisionComponent(hasBody = false),
        )
    }
)