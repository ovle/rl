package com.ovle.rl.content.entity.resource.proc

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.copperMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val procCopperEntity = EntityTemplate(
    name = "copper (bar)",
    shortName = "cop+",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(copperMaterial)
        )
    }
)