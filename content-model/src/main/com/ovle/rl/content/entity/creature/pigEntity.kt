package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.ai.task.*
import com.ovle.rl.content.material.fruitMaterial
import com.ovle.rl.content.material.meatMaterial
import com.ovle.rl.content.material.plantMaterial
import com.ovle.rl.content.task.type.craft.craftTemplates
import com.ovle.rl.content.tile.naturalFloorGrass
import com.ovle.rl.model.game.ai.AIComponent
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.life.hunger.HungerComponent
import com.ovle.rl.model.game.skill.helper.PositionHelper
import com.ovle.rl.model.game.skill.helper.SkillHelper
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val pigEntity = EntityTemplate(
    name = "pig",
    parent = creatureEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.005f,
        tileCheck = { t -> t == naturalFloorGrass },
    ),
    getState = {
        listOf(
            HealthComponent(maxHealth = 3),
            MoveComponent(defaultSpeed = 0.5),
            AIComponent(aiTaskFactories = listOf(
                FleeAITaskFactory(
                    enemyRadius = 2,
                    belowHealth = 0.5f
                ),
                EatAITaskFactory(
                    skillHelper = SkillHelper(PositionHelper((PassabilityHelper()))),
                ),
                IdleAITaskFactory()
            )),
            HungerComponent(
                maxHunger = 24,
                consumes = listOf(
                    meatMaterial, fruitMaterial, plantMaterial
                )
            )
        )
    }
)