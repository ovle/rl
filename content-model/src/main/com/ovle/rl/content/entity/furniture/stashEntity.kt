package com.ovle.rl.content.entity.furniture

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.container.CarriableComponent
import com.ovle.rl.model.game.container.ContainerComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val stashEntity = EntityTemplate(
    name = "stash",
    shortName = "sts",
    parent = objectEntity,
    getState = {
        listOf(
            CollisionComponent(hasBody = false),
            CarriableComponent(),
            ContainerComponent(),
            MaterialComponent(),
        )
    }
)