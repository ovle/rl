package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.material.fruitMaterial
import com.ovle.rl.content.material.meatMaterial
import com.ovle.rl.content.skill.basicAttackSkill
import com.ovle.rl.content.tile.naturalFloorGrass
import com.ovle.rl.content.tile.naturalFloorSoil
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.life.hunger.HungerComponent
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val ogreEntity = EntityTemplate(
    name = "ogre",
    shortName = "ogr",
    parent = creatureEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.0025f,
        tileCheck = { t -> t in setOf(
            naturalFloorSoil, naturalFloorGrass
        )},
    ),
    getState = {
        listOf(
            HealthComponent(maxHealth = 10),
            MoveComponent(defaultSpeed = 0.5),
            HungerComponent(
                maxHunger = 24 * 3,
                consumes = listOf(
                    meatMaterial, fruitMaterial
                )
            ),
            SkillComponent(skills = setOf(
                basicAttackSkill
            )),
        )
    }
)