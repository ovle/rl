package com.ovle.rl.content.entity.plant

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.entity.resource.raw.woodEntity
import com.ovle.rl.content.material.woodMaterial
import com.ovle.rl.content.time.TREE_WITHER_CHANCE_PER_HOUR
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.gather.SourceComponent
import com.ovle.rl.model.game.gather.SourceInfo
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val treeEntity = EntityTemplate(
    name = "tree",
    parent = objectEntity,
    getState = {
        listOf(
            AgeComponent(dieChancePerHour = TREE_WITHER_CHANCE_PER_HOUR),
            CollisionComponent(hasBody = true),
            HealthComponent(),
            MaterialComponent(woodMaterial),
            SourceComponent(
                SourceInfo(woodEntity, 1..3),
            )
        )
    }
)