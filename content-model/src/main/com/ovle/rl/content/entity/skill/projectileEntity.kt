package com.ovle.rl.content.entity.skill

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.path.linePath
import com.ovle.rl.content.trigger.damageObstacleOnCollideTrigger
import com.ovle.rl.content.trigger.destroyEntityOnEntityCollide
import com.ovle.rl.content.trigger.destroyEntityOnStopMovementTrigger
import com.ovle.rl.content.trigger.destroyEntityOnTileCollideTrigger
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.trigger.TriggerComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val projectileEntity = EntityTemplate(
    name = "projectile",
    parent = objectEntity,
    getState = {
        listOf(
            MoveComponent(
                defaultSpeed = 15.0, pathTemplate = linePath
            ),
            CollisionComponent(active = false, hasBody = false), //'active' will be enabled programmatically on move start
            TriggerComponent(
                triggers = mutableListOf(
                    damageObstacleOnCollideTrigger,
                    destroyEntityOnEntityCollide,
                    destroyEntityOnStopMovementTrigger,
                    destroyEntityOnTileCollideTrigger
                )
            ),
        )
    }
)