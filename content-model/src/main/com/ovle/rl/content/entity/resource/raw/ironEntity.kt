package com.ovle.rl.content.entity.resource.raw

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.ironMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val ironEntity = EntityTemplate(
    name = "iron (raw)",
    shortName = "irn-",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(ironMaterial)
        )
    }
)