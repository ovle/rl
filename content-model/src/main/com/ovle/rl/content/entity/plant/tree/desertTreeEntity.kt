package com.ovle.rl.content.entity.plant.tree

import com.ovle.rl.content.entity.plant.treeEntity
import com.ovle.rl.content.tile.naturalFloorSand
import com.ovle.rl.content.tile.shallowWater
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val desertTreeEntity = EntityTemplate(
    name = "desert tree",
    shortName = "dst",
    parent = treeEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.2f,
        tileCheck = { t -> t == naturalFloorSand },
        nearTiles = setOf(shallowWater),
        nearRadius = 3
    )
)