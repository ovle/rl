package com.ovle.rl.content.entity.plant.tree

import com.ovle.rl.content.entity.plant.plantEntity
import com.ovle.rl.content.tile.naturalFloorJungleGrass
import com.ovle.rl.content.time.TREE_GROW_CHANCE_PER_HOUR
import com.ovle.rl.content.time.TREE_SPAWN_CHANCE_PER_HOUR
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.game.life.age.TransformConfig
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val jungleTreeSaplingEntity = EntityTemplate(
    name = "jungle tree sap.",
    shortName = "jus",
    parent = plantEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.25f,
        chancePerHour = TREE_SPAWN_CHANCE_PER_HOUR,
        tileCheck = { t -> t == naturalFloorJungleGrass },
    ),
    getState = {
        listOf(
            AgeComponent(
                transform = TransformConfig(
                    entity = jungleTreeEntity,
                    chancePerHour = TREE_GROW_CHANCE_PER_HOUR
                )
            )
        )
    }
)