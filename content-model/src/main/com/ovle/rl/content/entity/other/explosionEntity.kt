package com.ovle.rl.content.entity.other

import com.ovle.rl.content.aoe.circleAoe
import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.trigger.damageObstacleOnCollideTrigger
import com.ovle.rl.model.game.space.aoe.AOEComponent
import com.ovle.rl.model.game.trigger.TriggerComponent
import com.ovle.rl.model.template.entity.EntityTemplate


//todo break on solid object interaction (see circleAoe)
val explosionEntity = EntityTemplate(
    name = "explosion",
    parent = objectEntity,
    getState = {
        listOf(
            AOEComponent(template = circleAoe, duration = 3),
            TriggerComponent(
                triggers = mutableListOf(
                    damageObstacleOnCollideTrigger
                )
            ),
        )
    }
)