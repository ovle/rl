package com.ovle.rl.content.entity.resource.proc

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.ironMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val procIronEntity = EntityTemplate(
    name = "iron (bar)",
    shortName = "irn+",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(ironMaterial)
        )
    }
)