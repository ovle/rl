package com.ovle.rl.content.entity.other

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.game.spawn.portal.PortalComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val portalEntity = EntityTemplate(
    name = "portal",
    shortName = "ptl",
    description = "portal to the unknown places",
    parent = objectEntity,
    getState = {
        listOf(
//            MaterialComponent(MaterialType.AETHER),
            PortalComponent()
        )
    }
)