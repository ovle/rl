package com.ovle.rl.content.entity.plant.tree

import com.ovle.rl.content.entity.plant.treeEntity
import com.ovle.rl.content.tile.naturalFloorJungleGrass
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val jungleTreeEntity = EntityTemplate(
    name = "jungle tree",
    shortName = "jut",
    parent = treeEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.2f,
        tileCheck = { t -> t == naturalFloorJungleGrass },
    )
)