package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.skill.basicAttackSkill
import com.ovle.rl.content.tile.naturalFloorMetaStone
import com.ovle.rl.content.tile.naturalFloorSedStone
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val batEntity = EntityTemplate(
    name = "bat",
    shortName = "bat",
    parent = creatureEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.005f,
        tileCheck = { t -> t in setOf(
            naturalFloorSedStone, naturalFloorMetaStone
        )},
        nearRadius = 4
    ),
    getState = {
        listOf(
            MoveComponent(defaultSpeed = 2.0, type = MoveType.FLYING),
            SkillComponent(skills = setOf(basicAttackSkill)),
        )
    }
)