package com.ovle.rl.content.entity.skill

import com.ovle.rl.content.path.linePath
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val magicMissileEntity = EntityTemplate(
    name = "magic missile",
    parent = projectileEntity,
    getState = {
        listOf(
            MoveComponent(
                defaultSpeed = 15.0, pathTemplate = linePath
            )
        )
    }
)