package com.ovle.rl.content.entity.plant.tree

import com.ovle.rl.content.entity.plant.plantEntity
import com.ovle.rl.content.entity.resource.raw.fruitsEntity
import com.ovle.rl.content.tile.naturalFloorGrass
import com.ovle.rl.content.time.*
import com.ovle.rl.model.game.gather.SourceComponent
import com.ovle.rl.model.game.gather.SourceInfo
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val fruitBushEntity = EntityTemplate(
    name = "fruit bush",
    shortName = "frb",
    parent = plantEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.2f,
        chancePerHour = FRUIT_BUSH_SPAWN_CHANCE_PER_HOUR,
        tileCheck = { t -> t == naturalFloorGrass },
    ),
    getState = {
        listOf(
            AgeComponent(dieChancePerHour = FRUIT_BUSH_WITHER_CHANCE_PER_HOUR),
            SourceComponent(
                SourceInfo(fruitsEntity, 0..1),
            )
        )
    }
)