package com.ovle.rl.content.entity.resource.raw

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.fruitMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val fruitsEntity = EntityTemplate(
    name = "fruits",
    shortName = "fru-",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(fruitMaterial)
        )
    }
)