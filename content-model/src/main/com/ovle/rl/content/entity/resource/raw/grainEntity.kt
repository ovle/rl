package com.ovle.rl.content.entity.resource.raw

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.plantMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val grainEntity = EntityTemplate(
    name = "grain",
    shortName = "grn-",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(plantMaterial)
        )
    }
)