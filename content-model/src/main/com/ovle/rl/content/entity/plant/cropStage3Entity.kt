package com.ovle.rl.content.entity.plant

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.content.entity.resource.raw.grainEntity
import com.ovle.rl.content.material.plantMaterial
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.game.gather.SourceComponent
import com.ovle.rl.model.game.gather.SourceInfo
import com.ovle.rl.model.template.entity.EntityTemplate

val cropStage3Entity = EntityTemplate(
    name = "crop 3",
    shortName = "cr3",
    parent = objectEntity,
    getState = {
        listOf(
            CollisionComponent(hasBody = false),
            MaterialComponent(plantMaterial),
            AgeComponent(maxAgeHours = 100),
            SourceComponent(
                SourceInfo(grainEntity, 1..2)
            )
        )
    }
)