package com.ovle.rl.content.entity.resource.proc

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.cookPlantMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val breadEntity = EntityTemplate(
    name = "bread",
    shortName = "bre+",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(cookPlantMaterial)
        )
    }
)