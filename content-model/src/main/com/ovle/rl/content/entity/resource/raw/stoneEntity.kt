package com.ovle.rl.content.entity.resource.raw

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.stoneMaterial
import com.ovle.rl.content.tile.naturalFloorSedStone
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val stoneEntity = EntityTemplate(
    name = "stone (raw)",
    shortName = "stn-",
    parent = resourceEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.2f,
        tileCheck = { t -> t == naturalFloorSedStone },
        nearTiles = listOf(naturalFloorSedStone),
        nearRadius = 3
    ),
    getState = {
        listOf(
            MaterialComponent(stoneMaterial)
        )
    }
)