package com.ovle.rl.content.entity.resource

import com.ovle.rl.content.entity.objectEntity
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.container.CarriableComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val resourceEntity = EntityTemplate(
    name = "resource",
    parent = objectEntity,
    getState = {
        listOf(
            CollisionComponent(hasBody = false),
            CarriableComponent(),
        )
    }
)