package com.ovle.rl.content.entity.resource.raw

import com.ovle.rl.content.entity.resource.resourceEntity
import com.ovle.rl.content.material.goldMaterial
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.template.entity.EntityTemplate

val goldEntity = EntityTemplate(
    name = "gold (raw)",
    shortName = "gld-",
    parent = resourceEntity,
    getState = {
        listOf(
            MaterialComponent(goldMaterial)
        )
    }
)