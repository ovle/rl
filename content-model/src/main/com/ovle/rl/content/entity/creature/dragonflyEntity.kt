package com.ovle.rl.content.entity.creature

import com.ovle.rl.content.ai.task.CombatAITaskFactory
import com.ovle.rl.content.ai.task.IdleAITaskFactory
import com.ovle.rl.content.skill.basicAttackSkill
import com.ovle.rl.content.tile.naturalFloorJungleGrass
import com.ovle.rl.model.game.ai.AIComponent
import com.ovle.rl.model.game.skill.SkillComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate

val dragonflyEntity = EntityTemplate(
    name = "dragonfly",
    shortName = "drf",
    parent = creatureEntity,
    spawnTemplate = SpawnTemplate(
        chancePerTile = 0.025f,
        tileCheck = { t -> t in setOf(
            naturalFloorJungleGrass
        )},
        nearRadius = 4
    ),
    getState = {
        listOf(
            MoveComponent(defaultSpeed = 4.0, type = MoveType.FLYING),
            SkillComponent(skills = setOf(basicAttackSkill)),
            AIComponent(aiTaskFactories = listOf(
                CombatAITaskFactory(),
                IdleAITaskFactory()
            )),
        )
    }
)