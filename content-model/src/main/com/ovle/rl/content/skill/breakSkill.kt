package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.BreakEffectPayload
import com.ovle.rl.content.effect.breakEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val breakSkill = SkillTemplate(
    name = "break",
    range = (1..1),
    ticks = BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = breakEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Point
                EffectParams(
                    e, BreakEffectPayload(position = (t.point))
                )
            }
        )
    ),
    isTargetValid = { t, c ->
        t is SkillTarget.Point && c.tiles.check(t.point) { it.props.isArtificial }
    },
)