package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.CreateProjectileEffectPayload
import com.ovle.rl.content.effect.createProjectileEffect
import com.ovle.rl.content.path.linePath
import com.ovle.rl.content.trigger.damageObstacleOnCollideTrigger
import com.ovle.rl.model.game.collision.CollisionComponent
import com.ovle.rl.model.game.container.EntityDropCarryItemCommand
import com.ovle.rl.model.game.container.carriedItem
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION
import com.ovle.rl.model.game.trigger.Components.trigger
import com.ovle.rl.model.game.trigger.TriggerComponent
import com.ovle.utils.event.EventBus.send
import ktx.ashley.get
/*

val throwSkill = SkillTemplate(
    name = "throw",
    tags = listOf(SkillTag.ATTACK),
    cost = SkillCost(),
    range = (3..8),
    ticks = 2 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = createProjectileEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                val projectile = e.carriedItem()
                    ?: throw IllegalStateException("can't use 'throw' skill without carried item")

                //todo avoid logic inside getParams
                //todo damage calc
                val collisionDamage = 3
                with(projectile) {
                    remove(MoveComponent::class.java)
                    remove(CollisionComponent::class.java)
                    add(MoveComponent(defaultSpeed = 10.0, pathTemplate = linePath))
                    add(CollisionComponent(active = false, hasBody = false, collisionDamage = collisionDamage))
                    this[trigger]!!.triggers += damageObstacleOnCollideTrigger
                }

                val target = t.entity.position()
                send(EntityDropCarryItemCommand(e, projectile))

                EffectParams(
                    e,
                    CreateProjectileEffectPayload(
                        entityId = projectile.id(),
                        target = target
                    )
                )
            }
        )
    ),
    isOwnerValid = { o, _ -> o.carriedItem() != null },
    isTargetValid = ::isAvailableToAttack,
)*/
