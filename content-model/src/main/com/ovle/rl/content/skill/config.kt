package com.ovle.rl.content.skill

val skillTemplates = listOf(
    basicAttackSkill,
    heavyAttackSkill,
    dragonBreathSkill,
    gatherSkill,
    healSkill,
    magicMissileSkill,
//    throwSkill,
    mineSkill,
    raiseDeadSkill,
    teleportSkill,
    buildSkill,
    breakSkill,
    craftSkill,
    farmSkill
)