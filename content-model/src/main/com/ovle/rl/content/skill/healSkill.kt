package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.HealEffectPayload
import com.ovle.rl.content.effect.healEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION


val healSkill = SkillTemplate(
    name = "heal",
    range = (1..3),
    ticks = BASE_SKILL_DURATION * 2,
    effects = listOf(
        SkillEffect(
            effect = healEffect.effect,
            getParams = { e, t ->
                val target = t as SkillTarget.Entity
                EffectParams(e, HealEffectPayload(target.entity))
            }
        )
    ),
    isTargetValid = ::isAvailableToHeal,
)