package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.CreateProjectileEffectPayload
import com.ovle.rl.content.effect.createProjectileEffect
import com.ovle.rl.content.entity.skill.magicMissileEntity
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.util.randomId
import com.ovle.utils.event.EventBus.send


val magicMissileSkill = SkillTemplate(
    name = "magic missile",
    tags = listOf(SkillTag.ATTACK),
    range = (2..5),
    ticks = 2 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = createProjectileEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                val projectileId = randomId()

                //todo avoid logic inside getParams
                val spawn = EntitySpawn(
                    template = magicMissileEntity,
                    position = e.position(),
                    id = projectileId,
                    source = e
                )
                send(CreateEntityCommand(spawn))

                EffectParams(
                    e,
                    CreateProjectileEffectPayload(
                        entityId = projectileId,
                        target = t.entity.position()
                    )
                )
            }
        )
    ),
    isTargetValid = ::isAvailableToAttack,
)