package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.DamageEffectPayload
import com.ovle.rl.content.effect.ForceMoveEffectPayload
import com.ovle.rl.content.effect.damageEffect
import com.ovle.rl.content.effect.forceMoveEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION
import com.ovle.utils.gdx.math.point.nextAfter

val heavyAttackSkill = SkillTemplate(
    name = "heavy attack",
    tags = listOf(SkillTag.ATTACK),
    range = (1..1),
    ticks = BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = damageEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                EffectParams(e, DamageEffectPayload(t.entity, amount = 2))
            }
        ),
        SkillEffect(
            effect = forceMoveEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                val entity = t.entity
                val targetPosition = entity.position()
                val entityPosition = e.position()
                val to = nextAfter(entityPosition, targetPosition)!!

                EffectParams(e, ForceMoveEffectPayload(entity, to))
            }
        )
    ),
    isTargetValid = ::isAvailableToAttack,
)