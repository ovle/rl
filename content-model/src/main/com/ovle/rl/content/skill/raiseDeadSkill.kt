package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.CreateEntityEffectPayload
import com.ovle.rl.content.effect.DestroyEffectPayload
import com.ovle.rl.content.effect.createEntityEffect
import com.ovle.rl.content.effect.destroyEffect
import com.ovle.rl.content.entity.creature.skeletonEntity
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.life.isDead
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

//todo faction of the created entity?
val raiseDeadSkill = SkillTemplate(
    name = "raise dead",
    range = (1..3),
    ticks = 8 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = destroyEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                EffectParams(e, DestroyEffectPayload(t.entity))
            }
        ),
        SkillEffect(
            effect = createEntityEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                EffectParams(
                    e,
                    CreateEntityEffectPayload(
                        template = skeletonEntity,
                        position = t.entity.position(),
                        source = e
                    )
                )
            }
        )
    ),
    isTargetValid = { t, _ ->
        t is SkillTarget.Entity && t.entity.isDead()
    },
)