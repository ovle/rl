package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.BuildEffectPayload
import com.ovle.rl.content.effect.buildEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.tile.isPlaceToBuildOn
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val buildSkill = SkillTemplate(
    name = "build",
    range = (1..1),
    ticks = 1 * BASE_SKILL_DURATION,
//    ticks = 4 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = buildEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Build
                EffectParams(
                    e, BuildEffectPayload(
                        newTile = t.template.tile,
                        resource = t.resource,
                        position = t.point
                    )
                )
            }
        )
    ),
    isTargetValid = { t, c ->
        t is SkillTarget.Build && c.tiles.isPlaceToBuildOn(t.point, t.template)
    },
)