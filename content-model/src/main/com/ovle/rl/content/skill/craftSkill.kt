package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.CraftEffectPayload
import com.ovle.rl.content.effect.craftEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.tile.isPlaceToCraftOn
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val craftSkill = SkillTemplate(
    name = "craft",
    range = (1..1),
    ticks = 8 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = craftEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Craft
                EffectParams(
                    e, CraftEffectPayload(
                        newEntity = t.template.entity,
                        resource = t.resource,
                        position = t.point,
                        source = e,
                    )
                )
            }
        )
    ),
    isTargetValid = { t, c ->
        t is SkillTarget.Craft && c.tiles.isPlaceToCraftOn(t.point, t.template)
    },
)