package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.ForceMoveEffectPayload
import com.ovle.rl.content.effect.forceMoveEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val teleportSkill = SkillTemplate(
    name = "teleport",
    ticks = BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = forceMoveEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Point
                EffectParams(e, ForceMoveEffectPayload(e, t.point))
            }
        )
    ),
)