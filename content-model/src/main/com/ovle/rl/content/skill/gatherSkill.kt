package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.GatherEffectPayload
import com.ovle.rl.content.effect.gatherEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.gather.isGatherable
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val gatherSkill = SkillTemplate(
    name = "gather",
    range = (1..1),
    ticks = 1 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = gatherEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                EffectParams(e, GatherEffectPayload(t.entity))
            }
        )
    ),
    isTargetValid = { t, _ -> t is SkillTarget.Entity && t.entity.isGatherable() },
)