package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.FarmEffectPayload
import com.ovle.rl.content.effect.farmEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val farmSkill = SkillTemplate(
    name = "farm",
    range = (1..1),
    ticks = 1 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = farmEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Farm

                EffectParams(
                    e, FarmEffectPayload(
                        position = t.point,
                        template = t.template,
                        resource = t.resource,
                    )
                )
            }
        )
    ),
    isTargetValid = { t, c ->
        t is SkillTarget.Farm && t.template.farmTileCheck.check(t.point, c.tiles)
    },
)