package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.CreateEntityEffectPayload
import com.ovle.rl.content.effect.createEntityEffect
import com.ovle.rl.content.entity.creature.chelEntity
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION


val spawnChelSkill = SkillTemplate(
    name = "create chel",
    range = (0..2),
    ticks = 2 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = createEntityEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Point
                EffectParams(
                    e,
                    CreateEntityEffectPayload(
                        template = chelEntity,
                        position = t.position(),
                        source = e
                    )
                )
            }
        )
    ),
    isTargetValid = { t, _ ->
        t is SkillTarget.Point
    },
)