package com.ovle.rl.content.skill

import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.life.isAlive
import com.ovle.rl.model.game.life.isFullHealth
import com.ovle.rl.model.game.skill.SkillTarget


//todo enemy checks are for ai, not skills
fun isAvailableToHeal(t: SkillTarget, c: LocationContent): Boolean {
    if (t !is SkillTarget.Entity) return false
    return t.entity.isAlive() /*&& !target.isEnemy(e)*/ && !t.entity.isFullHealth()
}

//todo enemy checks are for ai, not skills
fun isAvailableToAttack(t: SkillTarget, c: LocationContent): Boolean {
    if (t !is SkillTarget.Entity) return false
    return t.entity.isAlive() /*&& target.isEnemy(e)*/
}