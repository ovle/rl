package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.CreateEntityEffectPayload
import com.ovle.rl.content.effect.createEntityEffect
import com.ovle.rl.content.entity.skill.dragonBreathEntity
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val dragonBreathSkill = SkillTemplate(
    name = "dragon breath",
    tags = listOf(SkillTag.ATTACK),
    range = (1..4),
    ticks = BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = createEntityEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                EffectParams(
                    e,
                    CreateEntityEffectPayload(dragonBreathEntity, t.entity.position(), e)
                )
            }
        )
    ),
)