package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.DamageEffectPayload
import com.ovle.rl.content.effect.damageEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val basicAttackSkill = SkillTemplate(
    name = "basic attack",
    tags = listOf(SkillTag.ATTACK),
    range = (1..1),
    ticks = BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = damageEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Entity
                EffectParams(e, DamageEffectPayload(t.entity)) }
        )
    ),
    isTargetValid = ::isAvailableToAttack,
)