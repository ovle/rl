package com.ovle.rl.content.skill.player

import com.ovle.rl.content.skill.spawnChelSkill
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate

val createChelPlayerSkill = PlayerSkillTemplate(
    name = "+chl",
    skill = spawnChelSkill,
//    cost = listOf(
//        PlayerSkillCost.Resource(copperResource, 1)
//    ),
    getTargets = { p, _ -> setOf(SkillTarget.Point(p)) },
)