package com.ovle.rl.content.skill

import com.ovle.rl.SkillEffect
import com.ovle.rl.content.effect.MineEffectPayload
import com.ovle.rl.content.effect.mineEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.tile.isMineable
import com.ovle.rl.model.game.time.BASE_SKILL_DURATION

val mineSkill = SkillTemplate(
    name = "mine",
    range = (1..1),
    ticks = 1 * BASE_SKILL_DURATION,
    effects = listOf(
        SkillEffect(
            effect = mineEffect.effect,
            getParams = { e, t ->
                t as SkillTarget.Point
                EffectParams(
                    e, MineEffectPayload(position = t.point)
                )
            }
        )
    ),
    isTargetValid = { t, c ->
        t is SkillTarget.Point && c.tiles.isMineable(t.point)
    },
)