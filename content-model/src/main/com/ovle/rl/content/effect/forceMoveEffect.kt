package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.space.ChangeEntityPositionCommand
import com.ovle.utils.event.EventBus.send

val forceMoveEffect = EffectTemplate(
    name = "move",
    effect = { params ->
        val (_, payload) = params
        payload as ForceMoveEffectPayload

        send(ChangeEntityPositionCommand(payload.entity, payload.to))
    }
)

data class ForceMoveEffectPayload(
    val entity: Entity,
    val to: GridPoint2
)