package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.farm.FarmCommand
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.utils.event.EventBus.send

val farmEffect = EffectTemplate(
    name = "farm",
    effect = { params ->
        val (_, payload) = params
        payload as FarmEffectPayload
        send(
            FarmCommand(
                payload.position,
                payload.template,
                payload.resource
            )
        )
    }
)

data class FarmEffectPayload(
    val position: GridPoint2,
    val template: FarmTemplate,
    val resource: Entity?
)