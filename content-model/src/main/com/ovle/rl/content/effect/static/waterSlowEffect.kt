package com.ovle.rl.content.effect.static

import com.ovle.rl.model.game.effect.StaticEffectTemplate
import com.ovle.rl.model.game.space.Components.move
import ktx.ashley.get


val waterSlowEffect = StaticEffectTemplate(
    name = "water-slow",
    apply = {
        val mc = it[move] ?: return@StaticEffectTemplate
        mc.speed /= 2
    },
    revert = {
        val mc = it[move] ?: return@StaticEffectTemplate
        mc.speed *= 2
    },
)