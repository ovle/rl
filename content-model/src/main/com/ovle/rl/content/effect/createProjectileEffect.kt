package com.ovle.rl.content.effect

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.EntityId
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.projectile.CreateProjectileCommand
import com.ovle.rl.model.game.projectile.dto.ProjectileInfo
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.event.EventBus.send

val createProjectileEffect = EffectTemplate(
    name = "createProjectile",
    effect = { params ->
        val (source, payload) = params
        payload as CreateProjectileEffectPayload

        val to = payload.target
        val from = source!!.position()

        val pi = ProjectileInfo(
            entityId = payload.entityId,
            from = from,
            to = to,
            source = source
        )

        send(CreateProjectileCommand(pi))
    }
)

data class CreateProjectileEffectPayload(
    val entityId: EntityId,
    val target: GridPoint2
)