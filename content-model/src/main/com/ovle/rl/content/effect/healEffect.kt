package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.ovle.utils.event.EventBus.send
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.life.EntityHealCommand

val healEffect = EffectTemplate(
    name = "heal",
    effect = { params ->
        val (source, payload) = params
        payload as HealEffectPayload

        send(EntityHealCommand(payload.target, source, payload.amount))
    }
)

data class HealEffectPayload(
    val target: Entity,
    val amount: Int = 1,
)