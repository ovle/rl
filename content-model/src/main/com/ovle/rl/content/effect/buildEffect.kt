package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.event.EventBus.send
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.game.DestroyEntityCommand
import com.ovle.rl.model.game.tile.ChangeTileCommand
import com.ovle.rl.model.game.tile.TileTemplate

val buildEffect = EffectTemplate(
    name = "build",
    effect = { params ->
        val (_, payload) = params
        payload as BuildEffectPayload

        send(ChangeTileCommand(payload.newTile, payload.position))

        payload.resource?.let {
            send(DestroyEntityCommand(it))
        }
    }
)

data class BuildEffectPayload(
    val newTile: TileTemplate,
    val resource: Entity?,
    val position: GridPoint2
)