package com.ovle.rl.content.effect

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.event.EventBus.send
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.gather.MineTileCommand

val mineEffect = EffectTemplate(
    name = "mine",
    effect = { params ->
        val (e, payload) = params
        payload as MineEffectPayload

        send(MineTileCommand(payload.position, e))
    }
)

data class MineEffectPayload(
    val position: GridPoint2
)