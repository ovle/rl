package com.ovle.rl.content.effect

import com.ovle.rl.model.game.effect.EffectTemplate

val testGlobalEffect = EffectTemplate(
    name = "testGlobalEffect",
    effect = { params ->
        val (_, payload) = params
        payload as TestGlobalEffectPayload
    }
)

class TestGlobalEffectPayload