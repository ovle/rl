package com.ovle.rl.content.effect

import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.factory.EntityFactoryCreateEntityCommand
import com.ovle.utils.event.EventBus.send

val spawnEntityEffect = EffectTemplate(
    name = "spawn entity",
    effect = { params ->
        val (e, _) = params

        send(EntityFactoryCreateEntityCommand(e!!))
    }
)