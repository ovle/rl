package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.ovle.utils.event.EventBus.send
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.life.EntityTakeDamageCommand

val damageEffect = EffectTemplate(
    name = "damage",
    effect = { params ->
        val (source, payload) = params
        payload as DamageEffectPayload

        send(EntityTakeDamageCommand(payload.target, source, payload.amount))
    }
)

data class DamageEffectPayload(
    val target: Entity,
    val amount: Int = 1,
)