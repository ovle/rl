package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.ovle.utils.event.EventBus.send
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.gather.GatherEntityCommand

val gatherEffect = EffectTemplate(
    name = "gather",
    effect = { params ->
        val (source, payload) = params
        payload as GatherEffectPayload
        send(GatherEntityCommand(payload.target, source))
    }
)

data class GatherEffectPayload(
    val target: Entity
)