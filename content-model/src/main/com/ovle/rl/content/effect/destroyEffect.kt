package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.ovle.utils.event.EventBus.send
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.game.DestroyEntityCommand

val destroyEffect = EffectTemplate(
    name = "destroy",
    effect = { params ->
        val (_, payload) = params
        payload as DestroyEffectPayload

        send(DestroyEntityCommand(payload.target))
    }
)

data class DestroyEffectPayload(
    val target: Entity
)