package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.game.game.DestroyEntityCommand
import com.ovle.rl.model.game.material.material
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.event.EventBus.send

val craftEffect = EffectTemplate(
    name = "craft",
    effect = { params ->
        val (_, payload) = params
        payload as CraftEffectPayload

        val spawn = EntitySpawn(
            template = payload.newEntity,
            position = payload.position,
            source = payload.source,
            material = payload.resource?.material()
        )

        send(CreateEntityCommand(spawn))

        payload.resource?.let {
            send(DestroyEntityCommand(it))
        }
    }
)

data class CraftEffectPayload(
    val newEntity: EntityTemplate,
    val resource: Entity?,
    val position: GridPoint2,
    val source: Entity
)