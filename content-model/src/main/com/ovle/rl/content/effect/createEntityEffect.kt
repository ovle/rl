package com.ovle.rl.content.effect

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.event.EventBus.send
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.game.spawn.EntitySpawn

val createEntityEffect = EffectTemplate(
    name = "createEntity",
    effect = { params ->
        val (_, payload) = params
        payload as CreateEntityEffectPayload
        val spawn = EntitySpawn(
            template = payload.template,
            position = payload.position,
            source = payload.source
        )

        send(CreateEntityCommand(spawn))
    }
)

data class CreateEntityEffectPayload(
    val template: EntityTemplate,
    val position: GridPoint2,
    val source: Entity? = null
)