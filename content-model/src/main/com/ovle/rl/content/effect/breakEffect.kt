package com.ovle.rl.content.effect

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.effect.EffectTemplate
import com.ovle.rl.model.game.tile.ChangeTileCommand
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.utils.event.EventBus.send

val breakEffect = EffectTemplate(
    name = "break",
    effect = { params ->
        val (_, payload) = params
        payload as BreakEffectPayload

        send(ChangeTileCommand(TileTemplate.NOTHING, payload.position))
    }
)

data class BreakEffectPayload(
    val position: GridPoint2
)