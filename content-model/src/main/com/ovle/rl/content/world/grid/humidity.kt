package com.ovle.rl.content.world.grid

import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.factory.impl.fractal.FractalGridFactory

fun humidityGridFactory(
    random: RandomParams, borderValuesTypes: Array<Array<FloatArray>?>, noises: Array<Float>,
): FractalGridFactory {
    val kRandom = random.kRandom
    val noise = noises.random(kRandom)
    val initialBorderValues = borderValuesTypes.random(kRandom)

    return FractalGridFactory(
        params = FractalGridFactory.Companion.Params(
            size = WORLD_SIZE,
            startIteration = 1..3,
            noise = noise,
            initialBorderValues = initialBorderValues,
            isNormalized = true
        )
    )
}