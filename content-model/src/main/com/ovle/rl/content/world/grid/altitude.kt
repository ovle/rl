package com.ovle.rl.content.world.grid

import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.content.tile.mapper.TileMapChecker.Companion.RIVER_LEVEL_V0
import com.ovle.rl.model.procgen.grid.GridsValue
import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.factory.Combine
import com.ovle.utils.noise4j.grid.factory.GridFactory
import com.ovle.utils.noise4j.grid.factory.impl.fractal.FractalGridFactory
import com.ovle.utils.noise4j.grid.factory.impl.randomPath.RandomPathGridFactory
import com.ovle.utils.noise4j.grid.factory.path.impl.RandomPathFactory


fun altitudeGridFactory(
    random: RandomParams,
    noises: Array<Float>,
    mediumValues: Array<Float>,
    mediumDiffNoises: Array<Float>,
    startIteration: IntRange,
    riverCount: IntRange,
    riverLength: IntRange,
    tileMapChecker: TileMapChecker
): GridFactory<*> {
    val kRandom = random.kRandom
    val noise = noises.random(kRandom)
    val mediumValue = mediumValues.random(kRandom)
    val mediumDiffNoise = mediumDiffNoises.random(kRandom)

    val baseFactory = FractalGridFactory(
        params = FractalGridFactory.Companion.Params(
            size = WORLD_SIZE,
            startIteration = startIteration,
            noise = noise,
            mediumValue = mediumValue,
            mediumDiffNoise = mediumDiffNoise,
            isNormalized = true
        )
    )
    val riverFactory = RandomPathGridFactory(
        params = RandomPathGridFactory.Companion.Params(
            size = WORLD_SIZE,
            emptyValue = 0.0f,
            pathValue = 1.0f,
            countRange = riverCount,
            lengthRange = riverLength,
            isAllowDiag = false,
            isSelfIntersect = false,
        ), RandomPathFactory()
    )

    return Combine(
        params = Combine.Companion.Params(
            size = WORLD_SIZE,
            factories = arrayOf(baseFactory, riverFactory),
            combine = { values ->
                val alt = values[0]
                val isRiver = values[1] == 1.0f && tileMapChecker
                    .isAllowedRiver(GridsValue(values))
                when {
                    isRiver -> RIVER_LEVEL_V0
                    else -> alt
                }
            }
        )
    )
}