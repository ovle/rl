package com.ovle.rl.content.world

import com.ovle.rl.content.tile.mapper.TileMapChecker
import com.ovle.rl.content.tile.mapper.WorldTileMapper
import com.ovle.rl.content.world.grid.*
import com.ovle.rl.content.world.place.townWorldPlace
import com.ovle.rl.model.procgen.config.WorldTemplate
import com.ovle.rl.model.procgen.grid.world.WorldPlacesHelper


val world = WorldTemplate(
    name = "Common",
    getGridFactories = { r ->
        arrayOf(
            altitudeGridFactory(
                random = r,
                noises = arrayOf(2.0f),
                mediumValues = arrayOf(0.0f, 0.5f, 1.0f),
                mediumDiffNoises = arrayOf(0.0f, 2.0f, 4.0f),
                startIteration = 2..4,
                riverCount = 0..200,
                riverLength = 5..30,
                tileMapChecker = TileMapChecker()
            ),
            temperatureGridFactory(
                random = r,
                borderValuesTypes = temperatureBorderValuesTypes,
                noises = arrayOf(1.0f),
            ),
            humidityGridFactory(
                random = r,
                borderValuesTypes = humidityBorderValuesTypes,
                noises = arrayOf(1.0f, 5.0f, 10.0f),
            ),
            mineralTypeGridFactory(r),
            chasmGridFactory(r)
        )
    },
    placesParams = WorldPlacesHelper.Companion.Params(
        countByTemplate = mapOf(
            townWorldPlace to (0..10)
        )
    ),
    tileMapper = WorldTileMapper(TileMapChecker())
)