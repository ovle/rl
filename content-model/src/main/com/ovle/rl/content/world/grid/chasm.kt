package com.ovle.rl.content.world.grid

import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.factory.impl.fractal.FractalGridFactory

fun chasmGridFactory(r: RandomParams) = FractalGridFactory(
    params = FractalGridFactory.Companion.Params(
        size = WORLD_SIZE,
        startIteration = 4..4,
        noise = 0.1f,
        isNormalized = true
    )
)