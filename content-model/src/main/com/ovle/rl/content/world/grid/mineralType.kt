package com.ovle.rl.content.world.grid

import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.factory.impl.fractal.FractalGridFactory

fun mineralTypeGridFactory(r: RandomParams) = FractalGridFactory(
    params = FractalGridFactory.Companion.Params(
        size = WORLD_SIZE,
        startIteration = 3..5,
        noise = 2.0f,
        isNormalized = true
    )
)