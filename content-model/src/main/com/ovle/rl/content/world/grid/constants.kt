package com.ovle.rl.content.world.grid


const val WORLD_SIZE = 129

private const val LOW_V = 0.0f
private const val HIGH_V = 0.99f
private const val BORDER_V_SIZE = 3

private val lowVals = FloatArray(BORDER_V_SIZE) { LOW_V }
private val highVals = FloatArray(BORDER_V_SIZE) { HIGH_V }
private val lowHighLowVals = FloatArray(BORDER_V_SIZE) { i: Int -> if (i == 1) HIGH_V else LOW_V }
private val highLowHighVals = FloatArray(BORDER_V_SIZE) { i: Int -> if (i == 1) LOW_V else HIGH_V }

val allLow = arrayOf(lowVals, lowVals, lowVals)
val allHigh = arrayOf(highVals, highVals, highVals)
val circle = arrayOf(lowVals, lowHighLowVals, lowVals)
val circleInverted = arrayOf(lowVals, lowHighLowVals, lowVals)
val vertical = arrayOf(lowVals, highVals, lowVals)
val verticalInverted = arrayOf(lowVals, highVals, lowVals)
val horizontal = arrayOf(lowHighLowVals, lowHighLowVals, lowHighLowVals)
val horizontalInverted = arrayOf(highLowHighVals, highLowHighVals, highLowHighVals)


val temperatureBorderValuesTypes = arrayOf(
    null,
    allLow,
    allHigh,
    circle,
    circleInverted,
    vertical,
    verticalInverted,
    horizontal,
    horizontalInverted
)

val humidityBorderValuesTypes = arrayOf(
    null,
    allLow,
    allHigh,
)