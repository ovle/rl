package com.ovle.rl.content.world.place

import com.ovle.rl.content.entity.furniture.fenceEntity
import com.ovle.rl.content.entity.other.graveEntity
import com.ovle.rl.content.entity.plant.tree.fruitBushEntity
import com.ovle.rl.content.entity.plant.tree.temperateTreeEntity
import com.ovle.rl.content.entity.plant.tree.temperateTreeSaplingEntity
import com.ovle.rl.content.location.faction.neutralsFaction
import com.ovle.rl.content.location.instance.world.structure.DefaultLocationStructureTileHelper
import com.ovle.rl.content.location.instance.world.structure.area.areaTemplates
import com.ovle.rl.content.location.instance.world.structure.area.processor.*
import com.ovle.rl.content.task.type.craft.craftTemplates
import com.ovle.rl.content.task.type.farm.cropFarmTemplate
import com.ovle.rl.content.tile.naturalFloorGrass
import com.ovle.rl.content.tile.naturalFloorSoil
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.*
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.MergeAreasHelper
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.RoomEntitySpawnHelper
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout.CenterBasedRectAreaLayoutMapFactory
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout.LayoutParamsHelper
import com.ovle.rl.model.procgen.grid.world.WorldPlaceTemplate
import com.ovle.utils.gdx.math.array2d.ArrayValidationHelper
import com.ovle.utils.gdx.math.linear
import com.ovle.utils.gdx.math.rect.RectAreasHelper
import com.ovle.utils.gdx.math.reverseLinear

val townWorldPlace = WorldPlaceTemplate(
    name = "town",
    worldTileCheck = { it in setOf(naturalFloorGrass, naturalFloorSoil) },
    locationProcessor = RectAreasLocationProcessor(
        params = RectAreasLocationProcessor.Companion.Params(
            tileCheck = { it in setOf(naturalFloorGrass, naturalFloorSoil) },
            areaSize = BASE_AREA_SIZE
        ),
        layoutParamsHelper = LayoutParamsHelper(
            layoutMapFactories = listOf(
                CenterBasedRectAreaLayoutMapFactory(
                    params = CenterBasedRectAreaLayoutMapFactory.Companion.Params(
                        minSizeCoef = 0.2f
                    )
                )
            )
        ),
        rectAreasHelper = RectAreasHelper(),
        rectAreaSpawnHelper = RectAreaSpawnHelper(
            emptyChancePerTile = reverseLinear(add = 0.1f, mul = 15.0f),
            mergeAreasHelper = MergeAreasHelper(
                params = MergeAreasHelper.Companion.Params(
                    mergeChancePerLv = linear(mul = 1.0f),
                    largeAreaThresholdLv = LAYOUT_LEVEL_AVG,
                    largeAreaChance = 0.25f
                )
            ),
            areaProcessors = listOf(
                ParkAreaProcessor(
                    params = ParkAreaProcessor.Companion.Params(
                        treeTemplates = setOf(
                            temperateTreeEntity,
                            temperateTreeSaplingEntity,
                            fruitBushEntity
                        ),
                        treeChancePerTile = 0.5f
                    )
                ),
                FarmAreaProcessor(
                    params = FarmAreaProcessor.Companion.Params(
                        cropTemplates = setOf(
                            cropFarmTemplate
                        ),
                        fenceTemplate = fenceEntity,
                        cropChancePerTile = 0.8f
                    )
                ),
                GraveyardAreaProcessor(
                    params = GraveyardAreaProcessor.Companion.Params(
                        graveTemplates = setOf(graveEntity)
                    )
                ),
                TemplatedAreaProcessor(
                    areaTemplates = areaTemplates,
                    roomEntitySpawnHelper = RoomEntitySpawnHelper(
                        craftTemplates = craftTemplates,
                        faction = neutralsFaction
                    )
                )
            )
        ),
        locationStructureTileHelper = DefaultLocationStructureTileHelper(),
        arrayValidationHelper = ArrayValidationHelper(),
    )
)