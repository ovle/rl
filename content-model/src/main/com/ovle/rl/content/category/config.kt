package com.ovle.rl.content.category

import com.ovle.rl.content.entity.resource.proc.*
import com.ovle.rl.content.entity.resource.raw.*
import com.ovle.rl.model.game.category.EntityCategoryTemplate


val stoneCategory = EntityCategoryTemplate(
    name = "stn",
    description = "stone objects",
    entities = listOf(stoneEntity, procStoneEntity)
)

val woodCategory = EntityCategoryTemplate(
    name = "wod",
    description = "wooden objects",
    entities = listOf(woodEntity, procWoodEntity)
)

val metalCategory = EntityCategoryTemplate(
    name = "mtl",
    description = "metal objects",
    entities = listOf(
        copperEntity, procCopperEntity,
        ironEntity, procIronEntity,
        goldEntity, procGoldEntity
    )
)

val plantCategory = EntityCategoryTemplate(
    name = "plt",
    description = "plants",
    entities = listOf(grainEntity)
)


val categoryTemplates = listOf(
    stoneCategory, woodCategory, metalCategory, plantCategory
)