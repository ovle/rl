package com.ovle.rl.content.aoe

val aoeTemplates = listOf(
    circleAoe,
    filledCircleAoe,
    lineAoe,
)