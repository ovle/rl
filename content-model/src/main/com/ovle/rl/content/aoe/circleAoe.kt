package com.ovle.rl.content.aoe

import com.ovle.utils.gdx.math.discretization.bresenham.circle
import com.ovle.rl.model.game.space.aoe.AOETemplate
import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType.*
import com.ovle.utils.gdx.math.point.point

val circleAoe = AOETemplate(
    name = "circle",
    factory = { _, count ->
        circle(center = point(0, 0), radius = count, type = DISTANCE_BASED)
    }
)