package com.ovle.rl.content.aoe

import com.ovle.rl.model.game.core.Components.core
import com.ovle.rl.model.game.space.aoe.AOETemplate
import com.ovle.rl.model.game.space.position
import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType
import com.ovle.utils.gdx.math.discretization.bresenham.filledCircle
import com.ovle.utils.gdx.math.discretization.bresenham.line
import com.ovle.utils.gdx.math.point.nextTo
import com.ovle.utils.gdx.math.point.point
import ktx.ashley.get


val lineAoe = AOETemplate(
    name = "line",
    factory = { e, count ->
        val to = point(0, 0) //relative to e.position
        val aoePosition = e.position()
        val source = e[core]!!.source!!
        val sourcePosition = source.position()

        val nextToClosestBodyPoint = nextTo(sourcePosition, aoePosition)!!
        val from = nextToClosestBodyPoint.cpy().sub(aoePosition)
        val line = line(from, to, isAllowDiagonals = true).take(count)
        val result = line.flatMap {
            filledCircle(it, 1, DiscreetCircleType.DISTANCE_BASED)
        }.toSet() // - sourceBody

        result
    },
)