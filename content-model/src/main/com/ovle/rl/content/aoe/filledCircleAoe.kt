package com.ovle.rl.content.aoe

import com.ovle.rl.model.game.space.aoe.AOETemplate
import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType.*
import com.ovle.utils.gdx.math.discretization.bresenham.filledCircle
import com.ovle.utils.gdx.math.point.point

val filledCircleAoe = AOETemplate(
    name = "filled circle",
    factory = { _, count ->
        filledCircle(center = point(0, 0), radius = count, type = DISTANCE_BASED)
    }
)