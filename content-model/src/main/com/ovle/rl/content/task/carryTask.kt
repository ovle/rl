package com.ovle.rl.content.task

import com.ovle.rl.content.task.type.factory.CarryTaskFactory
import com.ovle.rl.model.game.container.Components.carriable
import com.ovle.rl.model.game.container.Components.carrier
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.space.accessibility.Accessibility.Companion.Key
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.TaskPerformerFilter
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.isTaskPerformer
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.TaskTargetFilter
import com.ovle.rl.model.game.task.target.selection.CarryTargetSelection
import com.ovle.utils.gdx.math.point.adjHV
import ktx.ashley.has


val carryTask = TaskTemplate(
    name = "carry",
    taskFactory = CarryTaskFactory(),
    targetConfig = TaskTargetConfig(
        getTarget = { CarryTargetSelection() },
        targetFilter = taskTargetFilter()
    ),
    performerFilter = taskPerformerFilter()
)

private fun taskPerformerFilter(): TaskPerformerFilter = { e, t, l ->
    t as TaskTarget.Carry
    val te = t.entity
    val p = t.point
    val teAdj = te.position().adjHV().toSet()
    val a = l.content.accessibility
    val position = e.position()

    e.isTaskPerformer() && e.has(carrier)
        && a.isAccessible(Key(e), position, p)
        && teAdj.any { a.isAccessible(Key(e), position, it) }
}

private fun taskTargetFilter(): TaskTargetFilter = { t, _, _ ->
    t as TaskTarget.Carry
    val te = t.entity

    te.isExists() && te.has(carriable)
}