package com.ovle.rl.content.task

import com.ovle.rl.content.task.type.factory.ExploreTaskFactory
import com.ovle.rl.model.game.space.accessibility.Accessibility.Companion.Key
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.TaskPerformerFilter
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.isTaskPerformer
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.selection.ExploreTargetSelection

val exploreTask = TaskTemplate(
    name = "explore",
    taskFactory = ExploreTaskFactory(),
    performerFilter = taskPerformerFilter(),
    targetConfig = TaskTargetConfig(
        getTarget = { ExploreTargetSelection() },
        allowUnknownTargets = true
    )
)


private fun taskPerformerFilter(): TaskPerformerFilter = { e, t, l ->
    val p = (t as TaskTarget.Point).point

    val a = l.content.accessibility

    e.isTaskPerformer() && a.isAccessible(Key(e), e.position(), p)
}