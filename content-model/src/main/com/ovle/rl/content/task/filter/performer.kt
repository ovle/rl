package com.ovle.rl.content.task.filter

import com.ovle.rl.model.game.skill.helper.PositionHelper
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.skill.hasSkill
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.game.task.TaskPerformerFilter
import com.ovle.rl.model.game.task.isTaskPerformer

/**
 *
 */
fun skillTaskPerformerFilter(skill: SkillTemplate): TaskPerformerFilter = { e, t, l ->
    //todo get from DI
    val sph = PositionHelper(PassabilityHelper())
    e.isTaskPerformer() && e.hasSkill(skill)
        && t.positions().all { sph.isTargetAccessible(e, skill.range, it, l.content) }
}