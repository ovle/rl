package com.ovle.rl.content.task.type.factory

import com.badlogic.ashley.core.Entity
import com.ovle.rl.content.skill.buildSkill
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.FAIL
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.SUCCESS
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.Take
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.tile.isPlaceToBuildOn
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2

class BuildTaskFactory : TaskFactory {

    override fun task(
        entity: Entity, target: TaskTarget, resource: Entity?, context: AITaskFactoryContext
    ): RootTask {
        target as TaskTarget.Build
        val point = target.point
        val template = target.template
        val (x, y) = point
        val tiles = context.content.tiles
        val tile = tiles[x, y]
        val newTile = template.tile

        if (tile == newTile) return SUCCESS
        if (!tiles.isPlaceToBuildOn(point, template)) return FAIL

        return RootTask(
            "build","$point $template",
            Composite.Sequence(
                if (resource != null) {
                    Composite.Sequence(
                        Move.ToEntity(resource),
                        Take(resource),
                    )
                } else null,

                Move.ToPosition(point, buildSkill.range!!),
                UseSkill(buildSkill, SkillTarget.Build(point, template, resource), isLoop = true)
            )
        )
    }
}