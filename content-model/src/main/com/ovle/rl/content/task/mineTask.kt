package com.ovle.rl.content.task

import com.ovle.rl.content.skill.mineSkill
import com.ovle.rl.content.task.filter.skillTaskPerformerFilter
import com.ovle.rl.content.task.type.factory.MineTaskFactory
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.TaskTargetFilter
import com.ovle.rl.model.game.task.target.selection.MineTargetSelection
import com.ovle.rl.model.game.tile.isMineable

val mineTask = TaskTemplate(
    name = "mine",
    taskFactory = MineTaskFactory(),
    performerFilter = skillTaskPerformerFilter(mineSkill),
    targetConfig = TaskTargetConfig(
        getTarget = { MineTargetSelection() },
        targetFilter = taskTargetFilter(),
        allowUnknownTargets = true
    )
)

private fun taskTargetFilter(): TaskTargetFilter = { t, l, _ ->
    t as TaskTarget.Point
    l.content.tiles.isMineable(t.point)
}