package com.ovle.rl.content.task.type.factory

import com.badlogic.ashley.core.Entity
import com.ovle.rl.content.skill.breakSkill
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.SUCCESS
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.task.target.TaskTarget

class BreakTaskFactory: TaskFactory {

    override fun task(
        entity: Entity, target: TaskTarget, resource: TaskResource?, context: AITaskFactoryContext
    ): RootTask {
        target as TaskTarget.Point
        val point = target.point
        val tiles = context.content.tiles

        if (!tiles.check(point) { it.props.isArtificial }) return SUCCESS

        return RootTask(
            "break","$point",
            Composite.Sequence(
                Move.ToPosition(point, breakSkill.range!!),
                UseSkill(breakSkill, SkillTarget.Point(point), isLoop = true)
            )
        )
    }
}