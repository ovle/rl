package com.ovle.rl.content.task.type.factory

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.collision.hasBody
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.game.dto.Entities
import com.ovle.rl.model.game.game.dto.location.LocationContent

class FarmPointHelper {

    fun isFarmPointExists(
        template: FarmTemplate, content: LocationContent, farmAreas: Collection<AreaInfo>
    ): Boolean {
        val tiles = content.tiles
        val entities = content.entities
        return farmAreas
            .flatMap { it.area }
            .any { farmPointCheck(template, it, tiles, entities) }
    }

    fun validFarmPoints(
        points: Collection<GridPoint2>, content: LocationContent, template: FarmTemplate
    ) = points
        .filter {
            farmPointCheck(template, it, content.tiles, content.entities)
        }

    private fun farmPointCheck(
        template: FarmTemplate, point: GridPoint2, tiles: TileArray, entities: Entities
    ): Boolean {
        val entitiesOnPoint = entities.on(point)
        val isTileValid = template.farmTileCheck.check(point, tiles)

        return isTileValid && !entitiesOnPoint.any {
            it.template() in template.lifecycle || it.hasBody()
        }
    }
}