package com.ovle.rl.content.task.type.farm

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.content.entity.plant.cropStage1Entity
import com.ovle.rl.content.entity.plant.cropStage3Entity
import com.ovle.rl.content.entity.resource.raw.grainEntity
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.farm.FarmTileCheck
import com.ovle.utils.gdx.math.discretization.bresenham.filledCircle

val cropFarmTemplate = FarmTemplate(
    name = "crop",
    resource = grainEntity,
    gatherResource = grainEntity,
    lifecycle = listOf(
        cropStage1Entity, cropStage3Entity
    ),
    farmTileCheck = FarmTileCheck(
        check = { p, t ->
            t.check(p) { it.props.isSoil } && isHaveWaterInRadius(p, t, 2)
        },
        description = "soil & water"
    )
)

private fun isHaveWaterInRadius(p: GridPoint2, t: TileArray, radius: Int) =
    filledCircle(p, radius)
        .filter { t.isValid(it) }
        .any { np -> t.check(np) { it.props.isWater } }