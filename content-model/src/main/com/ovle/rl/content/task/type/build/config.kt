package com.ovle.rl.content.task.type.build

import com.ovle.rl.content.entity.resource.raw.stoneEntity
import com.ovle.rl.content.entity.resource.raw.woodEntity
import com.ovle.rl.content.tile.*
import com.ovle.rl.model.game.build.BuildCheck
import com.ovle.rl.model.game.build.BuildTemplate

private val floorBuildCheck = BuildCheck(
    check = { p, ts -> ts[p.x, p.y].props.isFloor },
    description = "floor"
)

private val wallBuildCheck = BuildCheck(
    check = { p, ts -> ts[p.x, p.y].props.isWall },
    description = "wall"
)


val stoneFloorBuild = BuildTemplate(
    name = "flr (stn)",
    tile = artFloorStone,
    material = stoneEntity,
    check = floorBuildCheck
) { true }

val stoneWallBuild = BuildTemplate(
    name = "wal (stn)",
    tile = artWallStone,
    material = stoneEntity,
    check = floorBuildCheck
) { true }

val woodFloorBuild = BuildTemplate(
    name = "flr (wod)",
    tile = artFloorWood,
    material = woodEntity,
    check = floorBuildCheck
) { true }

val woodWallBuild = BuildTemplate(
    name = "wal (wod)",
    tile = artWallWood,
    material = woodEntity,
    check = floorBuildCheck
) { true }

/*
val brickFloorBuild = BuildTemplate(
    name = "flr (brk)",
    tile = artFloorBrick,
    material = stoneEntity, //todo
    check = floorBuildCheck
) { true }

val brickWallBuild = BuildTemplate(
    name = "wal (brk)",
    tile = artWallBrick,
    material = stoneEntity, //todo
    check = floorBuildCheck
) { true }
*/

val stoneDoorBuild = BuildTemplate(
    name = "dor (stn)",
    tile = doorStone,
    material = stoneEntity,
    check = floorBuildCheck
) { true }

val stoneWindowBuild = BuildTemplate(
    name = "win (stn)",
    tile = windowStone,
    material = stoneEntity,
    check = wallBuildCheck
) { true }

val woodDoorBuild = BuildTemplate(
    name = "dor (wod)",
    tile = doorWood,
    material = woodEntity,
    check = floorBuildCheck
) { true }

val woodWindowBuild = BuildTemplate(
    name = "win (wod)",
    tile = windowWood,
    material = woodEntity,
    check = wallBuildCheck
) { true }

val stoneRoadBuild = BuildTemplate(
    name = "roa (stn)",
    tile = artFloorStoneRoad,
    material = stoneEntity,
    check = floorBuildCheck
) { true }


val buildTemplates = listOf(
    stoneFloorBuild,
    stoneWallBuild,
    woodFloorBuild,
    woodWallBuild,
    stoneDoorBuild,
    stoneWindowBuild,
    woodDoorBuild,
    woodWindowBuild,
    stoneRoadBuild,
)