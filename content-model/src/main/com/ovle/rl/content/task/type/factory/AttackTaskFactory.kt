package com.ovle.rl.content.task.type.factory

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.FAIL
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.SUCCESS
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.life.isDead
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.task.target.TaskTarget

class AttackTaskFactory: TaskFactory {

    override fun task(
        entity: Entity, target: TaskTarget, resource: TaskResource?, context: AITaskFactoryContext
    ): RootTask {
        target as TaskTarget.Entity
        val targetEntity = target.entity

        if (targetEntity.isDead()) return SUCCESS
        if (!targetEntity.isExists()) return FAIL

        val skillHelper = context.skillHelper
        val skill = skillHelper.bestSkill(entity, targetEntity, context.content, SkillTag.ATTACK)
            ?: return FAIL

        return RootTask("attack",
            "${targetEntity.id()} ${skill.name}",
            Composite.Sequence(
                Move.ToEntity(targetEntity, skill.range!!),
                UseSkill(
                    skill, SkillTarget.Entity(targetEntity), isLoop = true,
                )
            )
        )
    }
}