package com.ovle.rl.content.task

import com.ovle.rl.content.skill.breakSkill
import com.ovle.rl.content.task.filter.skillTaskPerformerFilter
import com.ovle.rl.content.task.type.factory.BreakTaskFactory
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.TaskTargetFilter
import com.ovle.rl.model.game.task.target.selection.BreakTargetSelection


val breakTask = TaskTemplate(
    name = "break",
    taskFactory = BreakTaskFactory(),
    performerFilter = skillTaskPerformerFilter(breakSkill),
    targetConfig = TaskTargetConfig(
        getTarget = { BreakTargetSelection() },
        targetFilter = taskTargetFilter()
    )
)

private fun taskTargetFilter(): TaskTargetFilter = { t, l, _ ->
    t as TaskTarget.Point
    val p = t.point
    l.content.tiles.check(p) { it.props.isArtificial }
}