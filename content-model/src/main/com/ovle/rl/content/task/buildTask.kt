package com.ovle.rl.content.task

import com.ovle.rl.content.skill.buildSkill
import com.ovle.rl.content.task.filter.skillTaskPerformerFilter
import com.ovle.rl.content.task.type.factory.BuildTaskFactory
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.TaskTargetFilter
import com.ovle.rl.model.game.task.target.selection.BuildTargetSelection


val buildTask = TaskTemplate(
    name = "build",
    taskFactory = BuildTaskFactory(),
    performerFilter = skillTaskPerformerFilter(buildSkill),
    targetConfig = TaskTargetConfig(
        getTarget = { BuildTargetSelection() },
        targetFilter = taskTargetFilter()
    )
)


private fun taskTargetFilter(): TaskTargetFilter = { t, l, _ ->
    t as TaskTarget.Build
    val p = t.point
    val bt = t.template
    val tt = bt.tile
    val c = l.content
    val isSameTileAlready = c.tiles[p.x, p.y] == tt
    val isCanBuild = buildSkill.isTargetValid(SkillTarget.Build(p, bt), c)

    !isSameTileAlready && isCanBuild
}