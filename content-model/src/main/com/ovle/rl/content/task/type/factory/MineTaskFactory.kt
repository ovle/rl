package com.ovle.rl.content.task.type.factory

import com.badlogic.ashley.core.Entity
import com.ovle.rl.content.skill.mineSkill
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.FAIL
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.SUCCESS
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.social.player
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.tile.isMineable

class MineTaskFactory : TaskFactory {

    override fun task(
        entity: Entity, target: TaskTarget, resource: TaskResource?, context: AITaskFactoryContext
    ): RootTask {
        target as TaskTarget.Point
        val point = target.point
        val tiles = context.content.tiles
        val locationProjection = entity.player()!!.locationProjection

        if (locationProjection.knownMap.isKnownTile(point)) {
            if (tiles.check(point) { it.props.isFloor }) return SUCCESS
            if (!tiles.isMineable(point)) return FAIL
        }

        return RootTask(
            "mine", "$point",
            Composite.Sequence(
                Move.ToPosition(point, mineSkill.range!!),
                UseSkill(mineSkill, target = SkillTarget.Point(point), isLoop = true)
            )
        )
    }
}