package com.ovle.rl.content.task.type.factory

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.FAIL
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.SUCCESS
import com.ovle.rl.model.game.ai.task.impl.Drop
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.Take
import com.ovle.rl.model.game.container.Components.carriable
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.target.TaskTarget
import ktx.ashley.get

class CarryTaskFactory: TaskFactory {

    override fun task(
        entity: Entity, target: TaskTarget, resource: TaskResource?, context: AITaskFactoryContext
    ): RootTask {
        target as TaskTarget.Carry
        val te = target.entity
        val p = target.point
        val carrier = te[carriable]!!.carrier

        if (!te.isExists()) return FAIL
        if (te.position() == p && carrier == null) return SUCCESS

        return RootTask(
            "carry","${te.id()} to $p",
            Composite.Sequence(
                Move.ToEntity(te),
                Take(te),
                Move.ToPosition(p),
                Drop
            )
        )
    }
}