package com.ovle.rl.content.task.type.factory

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.content.skill.craftSkill
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.FAIL
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.SUCCESS
import com.ovle.rl.model.game.ai.task.impl.Drop
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.Take
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.social.player
import com.ovle.rl.model.game.space.accessibility.Accessibility
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.tile.isPlaceToCraftOn
import com.ovle.rl.model.util.accessiblesTo
import com.ovle.rl.model.util.knownByPlayer
import com.ovle.rl.model.util.templates
import com.ovle.utils.gdx.math.point.adjHV

class CraftTaskFactory: TaskFactory {

    override fun task(
        entity: Entity, target: TaskTarget, resource: TaskResource?, context: AITaskFactoryContext
    ): RootTask {
        target as TaskTarget.Craft
        val template = target.template
        val craftedEntity = template.entity
        val content = context.content
        // needs determination not to stuck
        val station = validStations(content, entity.player()!!, template, entity)
            .firstOrNull() //.closestTo(entity.position())

        if (template.station != null && station == null) {
            return FAIL
        }

        val targetPoint = station?.position()
            ?: targetPoint(resource, entity, content, template)
            ?: return FAIL
        val entities = content.entities.on(targetPoint)
        val isResourceSpent = resource == null || !resource.isExists()
        //todo sometimes stuck and continue craft
        if (craftedEntity in entities.templates() && isResourceSpent) return SUCCESS

        return RootTask(
            "craft","$craftedEntity $station",
            Composite.Sequence(
                if (resource != null) {
                    Composite.Sequence(
                        Move.ToEntity(resource),
                        Take(resource),
                    )
                } else null,

                if (station != null) {
                    Move.ToEntity(station, craftSkill.range!!)
                } else {
                    Move.ToPosition(targetPoint, craftSkill.range!!)
                },

                if (resource != null) {
                    Drop
                } else null,

                UseSkill(craftSkill, SkillTarget.Craft(targetPoint, template, resource), isLoop = true),
            )
        )
    }


    private fun validStations(
        content: LocationContent, player: LocationPlayer, template: CraftTemplate, entity: Entity
    ) = content.entities.all()
            .knownByPlayer(player)
            .filter { it.template() == template.station }
            .accessiblesTo(entity, content)

    private fun targetPoint(
        resource: TaskResource?, entity: Entity, content: LocationContent, template: CraftTemplate
    ): GridPoint2? {
        val tiles = content.tiles
        val entityPosition = entity.position()
        val anchorPoint = resource?.position() ?: entityPosition
        val key = Accessibility.Companion.Key(entity)

        val candidates = anchorPoint.adjHV().filter {
            tiles.isValid(it) && tiles.isPlaceToCraftOn(it, template)
                && content.accessibility.isAccessible(key, entityPosition, it)
        }

        return candidates.firstOrNull()
    }
}