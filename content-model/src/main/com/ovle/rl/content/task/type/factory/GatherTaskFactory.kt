package com.ovle.rl.content.task.type.factory

import com.badlogic.ashley.core.Entity
import com.ovle.rl.content.skill.gatherSkill
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.FAIL
import com.ovle.rl.model.game.ai.task.TaskFactory.Companion.SUCCESS
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.gather.Components.source
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.util.templates
import ktx.ashley.get

class GatherTaskFactory : TaskFactory {

    override fun task(
        entity: Entity, target: TaskTarget, resource: TaskResource?, context: AITaskFactoryContext
    ): RootTask {
        target as TaskTarget.Entity
        val targetEntity = target.entity
        val entities = context.content
            .entities.on(listOf(targetEntity.position()))

        val yields = targetEntity[source]!!.info.map { it.yield }
        if (entities.templates().all { it in yields }) return SUCCESS
        if (!targetEntity.isExists()) return FAIL

        return RootTask("gather", targetEntity.id(),
            Composite.Sequence(
                Move.ToEntity(targetEntity, gatherSkill.range!!),
                UseSkill(gatherSkill, SkillTarget.Entity(targetEntity), isLoop = true)
                //todo take the resource to the storage?
            )
        )
    }
}