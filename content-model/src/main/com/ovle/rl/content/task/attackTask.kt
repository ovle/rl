package com.ovle.rl.content.task

import com.ovle.rl.content.task.type.factory.AttackTaskFactory
import com.ovle.rl.model.game.skill.helper.SkillHelper
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.life.isAlive
import com.ovle.rl.model.game.skill.helper.PositionHelper
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.game.task.TaskPerformerFilter
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.isTaskPerformer
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.TaskTargetFilter
import com.ovle.rl.model.game.task.target.selection.AttackTargetSelection

val attackTask = TaskTemplate(
    name = "attack",
    taskFactory = AttackTaskFactory(),
    targetConfig = TaskTargetConfig(
        getTarget = { AttackTargetSelection() },
        targetFilter = taskTargetFilter()
    ),
    performerFilter = taskPerformerFilter()
)

private fun taskPerformerFilter(): TaskPerformerFilter = { e, t, l ->
    t as TaskTarget.Entity
    val te = t.entity
    //todo get from DI
    val skillHelper = SkillHelper(PositionHelper(PassabilityHelper()))

    e.isTaskPerformer() && e != te
        && skillHelper.bestSkill(e, te, l.content, SkillTag.ATTACK) != null
}

private fun taskTargetFilter(): TaskTargetFilter = { t, _, _ ->
    t as TaskTarget.Entity
    val te = t.entity

    te.isExists() && te.isAlive()
}