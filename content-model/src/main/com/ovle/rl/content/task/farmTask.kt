package com.ovle.rl.content.task

import com.ovle.rl.content.area.farmArea
import com.ovle.rl.content.skill.farmSkill
import com.ovle.rl.content.task.filter.skillTaskPerformerFilter
import com.ovle.rl.content.task.type.factory.FarmPointHelper
import com.ovle.rl.content.task.type.factory.FarmTaskFactory
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.TaskTargetFilter
import com.ovle.rl.model.game.task.target.selection.FarmTargetSelection

val farmTask = TaskTemplate(
    name = "farm",
    isAutoTask = true,
    taskFactory = FarmTaskFactory(),
    performerFilter = skillTaskPerformerFilter(farmSkill),
    targetConfig = TaskTargetConfig(
        getTarget = { FarmTargetSelection() },
        targetFilter = taskTargetFilter()
    )
)

private fun taskTargetFilter(): TaskTargetFilter = { t, l, lp ->
    t as TaskTarget.Farm
    FarmPointHelper().isFarmPointExists(t.template, l.content, lp.areas.filter { it.template == farmArea })
}