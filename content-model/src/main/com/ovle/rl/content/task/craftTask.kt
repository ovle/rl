package com.ovle.rl.content.task

import com.ovle.rl.content.skill.craftSkill
import com.ovle.rl.content.task.filter.skillTaskPerformerFilter
import com.ovle.rl.content.task.type.factory.CraftTaskFactory
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.TaskTargetFilter
import com.ovle.rl.model.game.task.target.selection.CraftTargetSelection
import com.ovle.rl.model.util.templates


val craftTask = TaskTemplate(
    name = "craft",
    taskFactory = CraftTaskFactory(),
    performerFilter = skillTaskPerformerFilter(craftSkill), //todo
    targetConfig = TaskTargetConfig(
        getTarget = { CraftTargetSelection() },
        targetFilter = taskTargetFilter()
    )
)

private fun taskTargetFilter(): TaskTargetFilter = { t, l, _ ->
    t as TaskTarget.Craft
    val expectedStation = t.template.station
    val templates = l.content.entities.all().templates()

    expectedStation == null || expectedStation in templates
}