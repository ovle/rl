package com.ovle.rl.content.task.type.craft

import com.ovle.rl.content.entity.furniture.*
import com.ovle.rl.content.entity.resource.proc.*
import com.ovle.rl.content.entity.resource.raw.*
import com.ovle.rl.model.game.craft.CraftTemplate


private val rawCraftResources = listOf(
    woodEntity, stoneEntity, copperEntity, ironEntity, /*goldEntity*/
)
private val rawCraftBasicResources = listOf(woodEntity, stoneEntity)
private val rawCraftMetalResources = listOf(copperEntity, ironEntity, /*goldEntity*/)

private val procCraftResources = listOf(
    procWoodEntity, procStoneEntity, procCopperEntity, procIronEntity, /*procGoldEntity*/
)
private val procCraftBasicResources = listOf(procWoodEntity, procStoneEntity)
private val procCraftMetalResources = listOf(procCopperEntity, procIronEntity, /*procGoldEntity*/)


val tableCraft = CraftTemplate(
    name = "tbl",
    entity = tableEntity,
    resources = rawCraftResources,
    check = { it.props.isFloor }
)


val carpWrkCraft = CraftTemplate(
    name = "car",
    entity = carpWrkEntity,
    station = tableEntity,
    resources = listOf(woodEntity, procWoodEntity),
    check = { it.props.isFloor }
)

val masonWrkCraft = CraftTemplate(
    name = "mas",
    entity = masonWrkEntity,
    station = tableEntity,
    resources = listOf(stoneEntity, procStoneEntity),
    check = { it.props.isFloor }
)

val ovenCraft = CraftTemplate(
    name = "ovn",
    entity = ovenEntity,
    station = tableEntity,
    resources = listOf(stoneEntity, procStoneEntity),
    check = { it.props.isFloor }
)

val anvilCraft = CraftTemplate(
    name = "anv",
    entity = anvilEntity,
    station = tableEntity,
    resources = listOf(ironEntity, procIronEntity),
    check = { it.props.isFloor }
)

val forgeCraft = CraftTemplate(
    name = "frg",
    entity = forgeEntity,
    station = tableEntity,
    resources = listOf(procStoneEntity),
    check = { it.props.isFloor }
)


val procStoneCraft = CraftTemplate(
    name = "stn+",
    entity = procStoneEntity,
    station = masonWrkEntity,
    resources = listOf(stoneEntity),
    check = { it.props.isFloor }
)

val procWoodCraft = CraftTemplate(
    name = "wod+",
    entity = procWoodEntity,
    station = carpWrkEntity,
    resources = listOf(woodEntity),
    check = { it.props.isFloor }
)

val procCopperCraft = CraftTemplate(
    name = "cop+",
    entity = procCopperEntity,
    station = forgeEntity,
    resources = listOf(copperEntity),
    check = { it.props.isFloor }
)

val procIronCraft = CraftTemplate(
    name = "irn+",
    entity = procIronEntity,
    station = forgeEntity,
    resources = listOf(ironEntity),
    check = { it.props.isFloor }
)

val procGoldCraft = CraftTemplate(
    name = "gld+",
    entity = procGoldEntity,
    station = forgeEntity,
    resources = listOf(goldEntity),
    check = { it.props.isFloor }
)


val chairCraft = CraftTemplate(
    name = "chr",
    entity = chairEntity,
    station = tableEntity,
    resources = listOf(woodEntity, stoneEntity),
    check = { it.props.isFloor }
)

val stashCraft = CraftTemplate(
    name = "sts",
    entity = stashEntity,
    station = tableEntity,
    resources = listOf(procWoodEntity, procStoneEntity),
    check = { it.props.isFloor }
)

val bedCraft = CraftTemplate(
    name = "bed",
    entity = bedEntity,
    station = tableEntity,
    resources = listOf(procWoodEntity),
    check = { it.props.isFloor }
)

val dummyCraft = CraftTemplate(
    name = "dum",
    entity = dummyEntity,
    station = tableEntity,
    resources = listOf(procWoodEntity),
    check = { it.props.isFloor }
)

val barrelCraft = CraftTemplate(
    name = "bar",
    entity = barrelEntity,
    station = tableEntity,
    resources = listOf(procWoodEntity, procStoneEntity),
    check = { it.props.isFloor }
)

val closetCraft = CraftTemplate(
    name = "cls",
    entity = closetEntity,
    station = tableEntity,
    resources = listOf(procWoodEntity, procStoneEntity),
    check = { it.props.isFloor }
)

val fenceCraft = CraftTemplate(
    name = "fnc",
    entity = fenceEntity,
    station = tableEntity,
    resources = listOf(woodEntity),
    check = { it.props.isFloor }
)

val breadCraft = CraftTemplate(
    name = "bre",
    entity = breadEntity,
    station = ovenEntity,
    resources = listOf(grainEntity),
    check = { it.props.isFloor }
)


val craftTemplates = listOf(
    //basic
    tableCraft,
    chairCraft,

    //workshop
    carpWrkCraft,
    masonWrkCraft,
    ovenCraft,
    anvilCraft,
    forgeCraft,

    //proc resource
    procStoneCraft,
    procWoodCraft,
    procCopperCraft,
    procIronCraft,
    procGoldCraft,

    //advanced
    stashCraft,
    bedCraft,
    dummyCraft,
    barrelCraft,
    closetCraft,
    fenceCraft,

    //food
    breadCraft
)