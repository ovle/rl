package com.ovle.rl.content.task.type.factory

import com.badlogic.ashley.core.Entity
import com.ovle.rl.content.skill.farmSkill
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.Take
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.task.target.TaskTarget

class FarmTaskFactory : TaskFactory {

    override fun task(
        entity: Entity, target: TaskTarget, resource: TaskResource?, context: AITaskFactoryContext
    ): RootTask {
        target as TaskTarget.Farm
        val template = target.template
        val cropTemplate = template.crop
        val farmPoint = target.point

        return RootTask(
            "farm","${cropTemplate.name} $farmPoint",
            Composite.Sequence(
                if (resource != null) {
                    Composite.Sequence(
                        Move.ToEntity(resource),
                        Take(resource),
                    )
                } else null,

                Move.ToPosition(farmPoint, farmSkill.range!!),
                UseSkill(farmSkill, target = SkillTarget.Farm(farmPoint, template, resource))
            )
        )
    }
}