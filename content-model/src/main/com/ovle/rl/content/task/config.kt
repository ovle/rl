package com.ovle.rl.content.task


val taskTemplates = listOf(
    attackTask,
    exploreTask,
    carryTask,
    gatherTask,
    buildTask,
    breakTask,
    craftTask,
    mineTask,
    farmTask
)