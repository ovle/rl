package com.ovle.rl.content.task

import com.ovle.rl.content.skill.gatherSkill
import com.ovle.rl.content.task.filter.skillTaskPerformerFilter
import com.ovle.rl.content.task.type.factory.GatherTaskFactory
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.gather.isGatherable
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import com.ovle.rl.model.game.task.target.TaskTargetFilter
import com.ovle.rl.model.game.task.target.selection.GatherTargetSelection

val gatherTask = TaskTemplate(
    name = "gather",
    taskFactory = GatherTaskFactory(),
    targetConfig = TaskTargetConfig(
        getTarget = { GatherTargetSelection() },
        targetFilter = taskTargetFilter()
    ),
    performerFilter = skillTaskPerformerFilter(gatherSkill)
)

private fun taskTargetFilter(): TaskTargetFilter = { t, _, _ ->
    t as TaskTarget.Entity
    val e = t.entity
    e.isExists() && e.isGatherable()
}