package com.ovle.rl.content.story.event

import com.ovle.rl.content.story.event.raid.raidStoryEvent

val storyEventTemplates = listOf(
    raidStoryEvent
)