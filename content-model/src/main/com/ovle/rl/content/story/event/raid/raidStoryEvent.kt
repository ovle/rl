package com.ovle.rl.content.story.event.raid

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.content.entity.creature.skeletonEntity
import com.ovle.rl.content.entity.other.keeperEntity
import com.ovle.rl.content.location.faction.enemyFaction
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.social.Components.social
import com.ovle.rl.model.game.space.accessibility.Accessibility
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.story.dto.StoryEventCondition
import com.ovle.rl.model.game.story.dto.StoryEventTemplate
import com.ovle.rl.model.game.story.dto.raid.RaidConfig
import com.ovle.rl.model.game.story.dto.raid.RaidMemberConfig
import com.ovle.rl.model.game.time.globalTime
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.util.ofTemplate
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.gdx.math.point.allBorders
import ktx.ashley.get

val raidStoryEvent = StoryEventTemplate(
    name = "test raid",
    condition = StoryEventCondition(
        startCheck = { g ->
            g.globalTime().toGameTime().day > 7
        },
        cooldownHours = 24
    ),
    effectConfig = RaidConfig(
        getStartPoint = ::borderRaidStartPoint,
        members = listOf(
            RaidMemberConfig(skeletonEntity, (1..2))
        )
    ),
    effect = { c, l ->
        c as RaidConfig
        val startPoint = c.getStartPoint(l)
            ?: return@StoryEventTemplate

        c.members.forEach { m ->
            val spawn = EntitySpawn(
                template = m.entityTemplate, position = startPoint
            ) { e, l ->
                val sc = e[social]!!
                val faction = enemyFaction
                sc.faction = faction
                sc.player = l.players.byFaction[faction]
            }

            val count = m.countRange.random(l.random.kRandom)
            repeat(count) {
                send(CreateEntityCommand(spawn))
            }
        }
    }
)

private fun borderRaidStartPoint(l: Location): GridPoint2? {
    val tiles = l.content.tiles
    val borders = tiles.points.allBorders()
    val floorBorders = borders.filter {
        tiles[it.x, it.y].props.isFloor
    }
    if (floorBorders.isEmpty()) return null

    val target = l.content.entities.all().ofTemplate(keeperEntity)
        .firstOrNull() ?: return null

    val access = l.content.accessibility
    val accessKey = Accessibility.Companion.Key(MoveType.DEFAULT, null)
    val accessBorders = floorBorders.filter {
        access.isAccessible(accessKey, it, target.position())
    }

    return accessBorders.firstOrNull()
        ?: floorBorders.firstOrNull()
}