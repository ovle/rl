package com.ovle.rl.content.path

import com.ovle.rl.model.game.space.move.impl.path.MovePath
import com.ovle.rl.model.game.space.move.impl.path.PathTemplate
import com.ovle.utils.gdx.math.discretization.bresenham.line


val linePath = PathTemplate(
    name = "line",
    getPath = { from, to ->
        MovePath(line(from, to!!))
    }
)