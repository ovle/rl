package com.ovle.rl.content.time

import com.ovle.rl.content.entity.other.explosionEntity
import com.ovle.rl.content.time.effect.createEntityTimerEffect
import com.ovle.rl.content.time.effect.destroyTimerEffect
import com.ovle.rl.model.game.time.dto.TimerTemplate

val explodeTimer = TimerTemplate(
    name = "explode",
    maxRepeats = 3,
    onFinish = listOf(
        destroyTimerEffect,
        createEntityTimerEffect(explosionEntity)
    )
)