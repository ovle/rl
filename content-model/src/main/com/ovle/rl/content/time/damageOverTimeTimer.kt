package com.ovle.rl.content.time

import com.ovle.rl.content.time.effect.damageTimerEffect
import com.ovle.rl.model.game.time.dto.TimerTemplate

//todo damage amount?
val damageOverTimeTimer = TimerTemplate(
    name = "damage-over-time",
    onFire = listOf(
        damageTimerEffect
    )
)

val deepWaterDamageOverTimeTimer = TimerTemplate(
    name = "deep-water-damage-over-time",
    onFire = listOf(
        damageTimerEffect
    )
)