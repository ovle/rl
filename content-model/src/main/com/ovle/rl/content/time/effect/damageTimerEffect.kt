package com.ovle.rl.content.time.effect

import com.ovle.rl.TimerEffect
import com.ovle.rl.content.effect.DamageEffectPayload
import com.ovle.rl.content.effect.damageEffect
import com.ovle.rl.model.game.effect.EffectParams

val damageTimerEffect = TimerEffect(
    effect = damageEffect.effect,
    getParams = { p ->
        val e = p.entity
        EffectParams(e, DamageEffectPayload(e))
    }
)