package com.ovle.rl.content.time.effect

import com.ovle.rl.TimerEffect
import com.ovle.rl.content.effect.TestGlobalEffectPayload
import com.ovle.rl.content.effect.testGlobalEffect
import com.ovle.rl.model.game.effect.EffectParams

val testGlobalTimerEffect = TimerEffect(
    effect = testGlobalEffect.effect,
    getParams = { p ->
        val e = p.entity
        EffectParams(e, TestGlobalEffectPayload())
    }
)