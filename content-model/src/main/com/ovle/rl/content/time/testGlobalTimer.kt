package com.ovle.rl.content.time

import com.ovle.rl.content.time.effect.testGlobalTimerEffect
import com.ovle.rl.model.game.time.dto.TimerTemplate

val testGlobalTimer = TimerTemplate(
    name = "test-global",
    maxRepeats = 3,
    onFinish = listOf(
        testGlobalTimerEffect
    )
)