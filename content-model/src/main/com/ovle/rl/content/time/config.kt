package com.ovle.rl.content.time

val globalTimers = listOf(
    testGlobalTimer,
)

val timerTemplates = listOf(
//    altarSpawnTimer,
    damageOverTimeTimer,
    deepWaterDamageOverTimeTimer,
    destroyTimer,
    explodeTimer,
    testGlobalTimer,
)