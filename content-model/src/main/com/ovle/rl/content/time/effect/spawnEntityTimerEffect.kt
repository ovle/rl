package com.ovle.rl.content.time.effect

import com.ovle.rl.TimerEffect
import com.ovle.rl.content.effect.spawnEntityEffect
import com.ovle.rl.model.game.effect.EffectParams

val spawnEntityTimerEffect = TimerEffect(
    effect = spawnEntityEffect.effect,
    getParams = { p ->
        EffectParams(p.entity)
    }
)