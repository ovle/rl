package com.ovle.rl.content.time

import com.ovle.rl.content.time.effect.destroyTimerEffect
import com.ovle.rl.model.game.time.dto.TimerTemplate


val destroyTimer = TimerTemplate(
    name = "destroy",
    maxRepeats = 0,
    onFinish = listOf(
        destroyTimerEffect
    )
)