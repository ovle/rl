package com.ovle.rl.content.time.effect

import com.ovle.rl.TimerEffect
import com.ovle.rl.content.effect.DestroyEffectPayload
import com.ovle.rl.content.effect.destroyEffect
import com.ovle.rl.model.game.effect.EffectParams

val destroyTimerEffect = TimerEffect(
    effect = destroyEffect.effect,
    getParams = { p ->
        val e = p.entity
        EffectParams(e, DestroyEffectPayload(target = e))
    }
)