package com.ovle.rl.content.time.effect

import com.ovle.rl.TimerEffect
import com.ovle.rl.content.effect.CreateEntityEffectPayload
import com.ovle.rl.content.effect.createEntityEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.template.entity.EntityTemplate


fun createEntityTimerEffect(entity: EntityTemplate) = TimerEffect(
    effect = createEntityEffect.effect,
    getParams = { p ->
        val e = p.entity
        EffectParams(e, CreateEntityEffectPayload(entity, position = e.position()))
    }
)