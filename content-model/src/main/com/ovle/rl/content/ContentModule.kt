package com.ovle.rl.content

import com.ovle.rl.content.aoe.aoeTemplates
import com.ovle.rl.content.area.areaTemplates
import com.ovle.rl.content.category.categoryTemplates
import com.ovle.rl.content.effect.static.staticEffectTemplates
import com.ovle.rl.content.entity.entityTemplates
import com.ovle.rl.content.location.instance.world.structure.purpose.areaPurposes
import com.ovle.rl.content.name.entity.entityNameSets
import com.ovle.rl.content.name.region.regionNameParts
import com.ovle.rl.content.path.pathTemplates
import com.ovle.rl.content.skill.player.playerSkillTemplates
import com.ovle.rl.content.skill.skillTemplates
import com.ovle.rl.content.story.event.storyEventTemplates
import com.ovle.rl.content.task.taskTemplates
import com.ovle.rl.content.task.type.build.buildTemplates
import com.ovle.rl.content.task.type.craft.craftTemplates
import com.ovle.rl.content.task.type.farm.farmTemplates
import com.ovle.rl.content.tile.effect.tileEffectTemplates
import com.ovle.rl.content.tile.mine.mineTemplates
import com.ovle.rl.content.tile.tileTemplates
import com.ovle.rl.content.time.timerTemplates
import com.ovle.rl.content.trigger.triggerTemplates
import com.ovle.rl.content.location.playgroundLocationTemplates
import com.ovle.rl.content.location.instance.world.worldLocation
import com.ovle.rl.content.material.materialTemplates
import com.ovle.rl.content.world.place.worldPlaceTemplates
import com.ovle.rl.content.world.world
import com.ovle.rl.model.game.area.AreaTemplate
import com.ovle.rl.model.game.build.BuildTemplate
import com.ovle.rl.model.game.category.EntityCategoryTemplate
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.effect.StaticEffectTemplate
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.material.MaterialTemplate
import com.ovle.rl.model.game.mine.MineTemplate
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.space.aoe.AOETemplate
import com.ovle.rl.model.game.space.move.impl.path.PathTemplate
import com.ovle.rl.model.game.story.dto.StoryEventTemplate
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.tile.TileEffectsTemplate
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.game.time.dto.TimerTemplate
import com.ovle.rl.model.game.trigger.dto.TriggerTemplate
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.config.WorldTemplate
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaPartPurpose
import com.ovle.rl.model.procgen.grid.world.WorldPlaceTemplate
import com.ovle.rl.model.procgen.name.entity.NameSetTemplate
import com.ovle.rl.model.procgen.name.region.RegionNamePart
import com.ovle.rl.model.template.entity.EntityTemplate
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.singleton


val contentModule = DI.Module("content") {
    bind<WorldTemplate>() with singleton { world }
    bind<LocationTemplate>() with singleton { worldLocation }
    bind<Collection<LocationTemplate>>() with singleton { playgroundLocationTemplates }

    bind<Collection<TileTemplate>>() with singleton { tileTemplates }
    bind<Collection<EntityTemplate>>() with singleton { entityTemplates }

    bind<Collection<TileEffectsTemplate>>() with singleton { tileEffectTemplates }
    bind<Collection<TaskTemplate>>() with singleton { taskTemplates }
    bind<Collection<SkillTemplate>>() with singleton { skillTemplates }
    bind<Collection<TimerTemplate>>() with singleton { timerTemplates }
    bind<Collection<TriggerTemplate>>() with singleton { triggerTemplates }
    bind<Collection<AOETemplate>>() with singleton { aoeTemplates }
    bind<Collection<PathTemplate>>() with singleton { pathTemplates }
    bind<Collection<StaticEffectTemplate>>() with singleton { staticEffectTemplates }
    bind<Collection<MineTemplate>>() with singleton { mineTemplates }
    bind<Collection<FarmTemplate>>() with singleton { farmTemplates }
    bind<Collection<BuildTemplate>>() with singleton { buildTemplates }
    bind<Collection<CraftTemplate>>() with singleton { craftTemplates }
    bind<Collection<MaterialTemplate>>() with singleton { materialTemplates }
    bind<Collection<EntityCategoryTemplate>>() with singleton { categoryTemplates }
    bind<Collection<AreaTemplate>>() with singleton { areaTemplates }
    bind<Collection<AreaPartPurpose>>() with singleton { areaPurposes }
    bind<Collection<PlayerSkillTemplate>>() with singleton { playerSkillTemplates }
    bind<Collection<StoryEventTemplate>>() with singleton { storyEventTemplates }
    bind<Collection<WorldPlaceTemplate>>() with singleton { worldPlaceTemplates }

    bind<Collection<RegionNamePart>>() with singleton { regionNameParts }
    bind<Collection<NameSetTemplate>>() with singleton { entityNameSets }
}