package com.ovle.rl.content.structure.biome
/*

import com.github.czyzby.noise4j.map.generator.room.RoomType
import com.ovle.rl.model.game.tile.TileBiome
import com.ovle.rl.model.game.tile.TileTemplate.*
import com.ovle.rl.content.tile.mapper.DungeonLocationTileMapper
import com.ovle.rl.model.procgen.grid.processor.location.structure.procgen.BiomeProcgenStructureConfig
import com.ovle.rl.model.procgen.grid.processor.location.structure.procgen.ProcgenStructureConfig
import com.ovle.rl.model.procgen.grid.processor.location.structure.procgen.ProcgenStructureNumberConfig
import com.ovle.utils.noise4j.grid.factory.impl.dungeon.DungeonGridFactory

val desertBiomeStructures = BiomeProcgenStructureConfig(
    biome = TileBiome.Desert,
    numberConfig = listOf(
        ProcgenStructureNumberConfig(0.5f, 0),
        ProcgenStructureNumberConfig(0.5f, 1)
    ),
    structureConfigs = listOf(
        ProcgenStructureConfig(
            factory = DungeonGridFactory(
                params = DungeonGridFactory.Companion.Params(
                    size = 10..15,
                    roomTypes = arrayOf(RoomType.DefaultRoomType.SQUARE),
                    maxRoomSize = 3,
                    minRoomSize = 3,
                    tolerance = 5,
                    windingChance = 0.25f,
                    randomConnectorChance = 0.0f
                )
            ),
            tileMapper = DungeonLocationTileMapper(
                DungeonLocationTileMapper.Companion.Params(
                    WallSandstone, FloorSandstone, Road
                )
            )
        )
    ),
)*/
