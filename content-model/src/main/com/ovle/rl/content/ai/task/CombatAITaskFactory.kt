package com.ovle.rl.content.ai.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.decision.AITaskFactory
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.core.templateName
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.social.player
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.util.knownByPlayer
import ktx.ashley.has

class CombatAITaskFactory: AITaskFactory {

    override fun process(entity: Entity, context: AITaskFactoryContext): RootTask? {
        var enemies = context.enemies[entity] ?: emptyList()
        val player = entity.player()
        if (player != null) {
            enemies = enemies.knownByPlayer(player)
        }

        val content = context.content
        val attackSkillWithTarget = context.skillHelper.bestSkillWithTarget(
            entity, enemies, content, SkillTag.ATTACK
        ) ?: return null

        val skill = attackSkillWithTarget.first
        val target = attackSkillWithTarget.second!!
        val id = "${skill.name} ${target.templateName()} ${target.id()}"

        return when {
            !entity.has(move) -> RootTask(
                "combat", id,
                UseSkill(skill, target = SkillTarget.Entity(target))
            )
            else -> RootTask(
                "combat", id,
                Composite.Sequence(
                    Move.ToEntity(target, skill.range!!),
                    UseSkill(skill, target = SkillTarget.Entity(target))
                )
            )
        }
    }
}