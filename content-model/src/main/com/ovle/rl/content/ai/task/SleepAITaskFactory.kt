package com.ovle.rl.content.ai.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.decision.AITaskFactory
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.Sleep
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.life.sleepPlacePointOrNull
import com.ovle.rl.model.game.space.position

class SleepAITaskFactory: AITaskFactory {

    override fun process(entity: Entity, context: AITaskFactoryContext): RootTask? {
        val position = entity.position()
        val sleepPoint = entity.sleepPlacePointOrNull()

        return when {
            sleepPoint == null -> null
            position == sleepPoint -> RootTask("sleep", "", Sleep())
            else -> RootTask(
                "sleep", "${entity.id()} on $sleepPoint",
                Composite.Sequence(
                    Move.ToPosition(sleepPoint), Sleep()
                )
            )
        }
    }
}