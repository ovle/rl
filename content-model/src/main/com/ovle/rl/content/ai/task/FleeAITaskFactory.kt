package com.ovle.rl.content.ai.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.decision.AITaskFactory
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.life.isLowHealth
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.nearTo


class FleeAITaskFactory(
    private val belowHealth: Float? = null,
    private val enemyRadius: Int? = null,
): AITaskFactory {

    override fun process(entity: Entity, context: AITaskFactoryContext): RootTask? {
        val position = entity.position()
        val enemies = context.enemies[entity] ?: emptyList()
        val nearbyEnemies = if (enemyRadius != null)
            enemies.nearTo(position, enemyRadius) else enemies

        return when {
            nearbyEnemies.isEmpty() -> null
            belowHealth == null -> flee(nearbyEnemies)
            entity.isLowHealth(belowHealth) -> flee(nearbyEnemies)
            else -> null
        }
    }

    private fun flee(enemies: Collection<Entity>) = RootTask(
        "flee", enemies.joinToString { it.id() },
        Move.From(enemies)
    )
}