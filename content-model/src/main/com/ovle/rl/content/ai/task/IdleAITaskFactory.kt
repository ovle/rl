package com.ovle.rl.content.ai.task

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.ai.decision.AITaskFactory
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.Success
import com.ovle.rl.model.game.house.closestHouseOrNull
import com.ovle.rl.model.game.house.closestOwnedHouseOrNull
import com.ovle.rl.model.game.perception.fov
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.accessibility.Accessibility
import com.ovle.rl.model.game.space.movePath
import com.ovle.rl.model.game.space.position
import ktx.ashley.has

class IdleAITaskFactory: AITaskFactory {

    override fun process(entity: Entity, context: AITaskFactoryContext): RootTask {
        if (!entity.has(move)) {
            return RootTask(
                "idle", "nothing",
                Success
            )
        }

        val existingMoveTarget = entity.movePath()?.last
        if (existingMoveTarget != null) {
            return RootTask(
                "idle", "$existingMoveTarget",
                Composite.Sequence(
                    Move.ToPosition(existingMoveTarget)
                )
            )
        }

        val area = idleArea(entity, context)
        val target = area.randomOrNull(context.random.kRandom)

        return if (target == null) RootTask(
            "idle", "nothing",
            Success
        )
        else RootTask(
            "idle", "$target",
            Composite.Sequence(
                Move.ToPosition(target)
            )
        )
    }

    private fun idleArea(
        entity: Entity, context: AITaskFactoryContext
    ): Collection<GridPoint2> {
        val isNight = context.gameTime.isNight
        val house = entity.closestOwnedHouseOrNull()
            ?: entity.closestHouseOrNull()
        if (isNight && house != null) return house.area

        val accessibility = context.content.accessibility
        val key = Accessibility.Companion.Key(entity)
        return entity.fov()
            .filter { accessibility.isAccessible(key, entity.position(), it) }
    }
}