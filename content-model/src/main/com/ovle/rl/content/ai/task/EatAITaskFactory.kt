package com.ovle.rl.content.ai.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.content.skill.gatherSkill
import com.ovle.rl.model.game.ai.decision.AITaskFactory
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.ai.task.impl.Eat
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.life.*
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.skill.hasSkill
import com.ovle.rl.model.game.skill.helper.SkillHelper
import com.ovle.rl.model.game.space.Components
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.accessiblesTo
import com.ovle.rl.model.util.closestTo
import com.ovle.rl.model.util.materials
import com.ovle.rl.model.util.sources
import ktx.ashley.has

class EatAITaskFactory(
    private val skillHelper: SkillHelper,
) : AITaskFactory {

    override fun process(entity: Entity, context: AITaskFactoryContext): RootTask? {
        if (!entity.isHungry()) return null

        val content = context.content
        val position = entity.position()

        val food = context.entities.materials()
            .minus(entity)
            .filter { entity.consumes(it) }
        val accessibleFood = food.accessiblesTo(entity, content)
        val consumableFood = accessibleFood.filter { !it.has(Components.move) || it.isDead() }
        val killableFood = accessibleFood.filter { it.isAlive() }
        val gatherableFood = context.entities.sources().filter {
            entity.consumesGatherResult(it)
        }
        val targetConsumableFood = consumableFood.closestTo(position)
        val targetKillableFood = killableFood.closestTo(position)
        val targetGatherableFood = if (entity.hasSkill(gatherSkill))
            gatherableFood.closestTo(position) else null

        val attackSkill = targetKillableFood?.let {
            skillHelper.bestSkill(entity, it, content)
        }

        return when {
            targetConsumableFood != null -> eat(targetConsumableFood)
            targetGatherableFood != null -> gather(targetGatherableFood)
            attackSkill != null -> hunt(targetKillableFood, attackSkill)
            else -> null
        }
    }


    private fun eat(targetFood: Entity) = RootTask(
        "eat", targetFood.id(), Composite.Sequence(
            Move.ToEntity(targetFood, range = 0..1),
            Eat(targetFood),
        )
    )

    private fun gather(targetSource: Entity) = RootTask(
        "gather", "${targetSource.id()} ", Composite.Sequence(
            Move.ToEntity(targetSource, gatherSkill.range!!),
            UseSkill(gatherSkill, target = SkillTarget.Entity(targetSource))
        )
    )

    private fun hunt(targetFood: Entity, attackSkill: SkillTemplate) = RootTask(
        "hunt", "${targetFood.id()} ${attackSkill.name}", Composite.Sequence(
            Move.ToEntity(targetFood, attackSkill.range!!),
            UseSkill(attackSkill, target = SkillTarget.Entity(targetFood))
        )
    )

}