package com.ovle.rl.content.ai.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.decision.AITaskFactory
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.task.Components.taskPerformer
import ktx.ashley.get

class TaskPerformerAITaskFactory : AITaskFactory {

    override fun process(entity: Entity, context: AITaskFactoryContext): RootTask? {
        val taskPerformerComponent = entity[taskPerformer] ?: return null
        val taskInfo = taskPerformerComponent.current ?: return null

//        val rootTask = entity[ai]!!.rootTask
//        if (rootTask != null) return rootTask

        val taskFactory = taskInfo.template.taskFactory
        val result = taskFactory.task(
            entity, taskInfo.target, taskInfo.lockedResource, context
        )

        return result.apply { this.source = taskInfo }
    }
}