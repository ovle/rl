package com.ovle.rl.content.name.entity

import com.ovle.rl.model.procgen.name.entity.NameSetTemplate
import com.valkryst.VNameGenerator.generator.MarkovGenerator

//todo content?
private val samples = arrayOf(
    "Abr",
    "Am",
    "Apep",
    "Ax",
    "Be",
    "Bta",
    "Bro",
    "Dad",
    "Dep",
    "Dnek",
    "Dopak",
    "Dod",
    "Deb",
    "Dadak",
    "Enod",
    "Eak",
    "Hmo",
    "Hun",
    "Hoh",
    "Ka",
    "Kep",
    "Kort",
    "Komp",
    "Mapnik",
    "Mem",
    "Mho",
    "Nehep",
    "Napop",
    "Oah",
    "Okoa",
    "Opob",
    "Rah",
    "Rem",
    "Romek",
    "Stob",
    "Sehep",
    "Xax",
    "Xem",
    "Xok",
)

val testNameSetTemplate = NameSetTemplate(
    maxLength = 7,
    generator = MarkovGenerator(samples)
)