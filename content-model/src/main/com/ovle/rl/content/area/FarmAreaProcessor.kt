package com.ovle.rl.content.area

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.content.task.farmTask
import com.ovle.rl.content.task.gatherTask
import com.ovle.rl.content.task.type.factory.FarmPointHelper
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.area.AreaProcessor
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.gather.yields
import com.ovle.rl.model.game.task.CheckTaskCommand
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.selection.FarmTargetSelection
import com.ovle.rl.model.game.task.target.selection.GatherTargetSelection
import com.ovle.rl.model.util.sources
import com.ovle.utils.event.EventBus.send


class FarmAreaProcessor(
    private val farmPointHelper: FarmPointHelper
): AreaProcessor {

    override fun process(
        areas: Collection<AreaInfo>, player: LocationPlayer, location: Location
    ) {
        val gatherTargets = gatherTargets(player)
        val farmTargets = farmTargets(player)

        areas.forEach {
            processArea(it, player, gatherTargets, farmTargets, location)
        }
    }

    private fun processArea(
        area: AreaInfo,
        player: LocationPlayer,
        gatherTargets: Set<Entity>,
        farmTargets: Set<GridPoint2>,
        location: Location
    ) {
        val points = area.area
        val farmTemplate = (area.params as AreaParams.Farm).template
            ?: return

        val gatherEntities = validGatherEntities(points, location, farmTemplate) - gatherTargets
        val gatherTargetSelection = GatherTargetSelection(
            entities = gatherEntities.toMutableSet()
        )

        send(CheckTaskCommand(gatherTargetSelection, gatherTask, player))

        val farmPoints = farmPointHelper.validFarmPoints(
            points, location.content, farmTemplate
        ) - farmTargets
        val farmTargetSelection = FarmTargetSelection(
            template = farmTemplate,
            points = farmPoints.toMutableSet()
        )

        send(CheckTaskCommand(farmTargetSelection, farmTask, player))
    }


    private fun gatherTargets(player: LocationPlayer) = player.tasks
        .filter { it.template == gatherTask }
        .map { (it.target as TaskTarget.Entity).entity }
        .toSet()

    private fun farmTargets(player: LocationPlayer) = player.tasks
        .filter { it.template == farmTask }
        .map { (it.target as TaskTarget.Farm).point }
        .toSet()


    private fun validGatherEntities(
        points: Collection<GridPoint2>, location: Location, farmTemplate: FarmTemplate
    ) = location.content.entities
        .on(points)
        .sources()
        .filter { it.yields(farmTemplate.gatherResource) }

}