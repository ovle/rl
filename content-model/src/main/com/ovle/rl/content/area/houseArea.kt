package com.ovle.rl.content.area

import com.ovle.rl.content.area.check.closedAreaCheck
import com.ovle.rl.content.area.check.hasEntityCheck
import com.ovle.rl.content.area.check.sizeAreaCheck
import com.ovle.rl.content.entity.furniture.bedEntity
import com.ovle.rl.model.game.area.AreaTemplate
import com.ovle.rl.model.game.tile.TileCheck

val houseArea = AreaTemplate(
    name = "house",
    processor = HouseAreaProcessor(),
    tileCheck = TileCheck(
        check = { it.props.isArtificial },
        description = "floor"
    ),
    checks = listOf(
        closedAreaCheck(maxInvalidBorders = 2),
        hasEntityCheck(entity = bedEntity),
        sizeAreaCheck(size = 4)
    )
)