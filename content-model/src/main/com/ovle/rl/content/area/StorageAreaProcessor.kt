package com.ovle.rl.content.area

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.content.task.carryTask
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.area.AreaProcessor
import com.ovle.rl.model.game.category.EntityCategoryTemplate
import com.ovle.rl.model.game.container.carrier
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.CheckTaskCommand
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.selection.CarryTargetSelection
import com.ovle.rl.model.util.knownByPlayer
import com.ovle.rl.model.util.positions
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.gdx.math.closestPoints

class StorageAreaProcessor: AreaProcessor {

    override fun process(areas: Collection<AreaInfo>, player: LocationPlayer, location: Location) {
        val areasByTemplate = areas.groupBy {
            (it.params as AreaParams.Storage).categoryTemplate
        }

        for (template in areasByTemplate.keys) {
            val templateAreas = areasByTemplate[template] ?: continue

            val storagePoints = templateAreas.flatMap { ta -> ta.area }.toSet()
            val storagePointsEntities = location.content.entities.on(storagePoints)
            val freeStoragePoints = (storagePoints - storagePointsEntities.positions())
                .toMutableSet()

            val entities = entitiesToCarry(template, location, storagePoints, player)

            for (entity in entities) {
                val points = freeStoragePoints.ifEmpty { storagePoints }
                val closestPoints = closestPoints(setOf(entity.position()), points)
                    ?: continue

                val targetPoint = closestPoints.second
                val targetSelection = CarryTargetSelection(
                    point = targetPoint,
                    entity = entity
                )

                send(CheckTaskCommand(targetSelection, carryTask, player))

                freeStoragePoints -= targetPoint
            }
        }
    }


    private fun entitiesToCarry(
        template: EntityCategoryTemplate?, location: Location, carryTargets: Collection<GridPoint2>, player: LocationPlayer
    ): Collection<Entity> {
        val content = location.content
        val entityTemplates = template?.entities ?: emptySet()
        val entities = content.entities.all()
            .knownByPlayer(player)
            .filter { it.template() in entityTemplates }
            .toSet()

        val onTargets = content.entities.on(carryTargets).toSet()
        val inCarry = entities.filter { it.carrier() != null }.toSet()
        val tasks = player.tasks
        val taskTargets = tasks.filter { it.template == carryTask }
            .map { (it.target as TaskTarget.Carry).entity }
        val excludeEntities = onTargets + inCarry + taskTargets

        return entities - excludeEntities
    }
}