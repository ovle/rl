package com.ovle.rl.content.area

import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.area.AreaProcessor
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.house.Components.houseAware
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.accessibleTo
import com.ovle.rl.model.util.houseAwares
import com.ovle.rl.model.util.ofPlayer
import com.ovle.utils.gdx.math.distance
import ktx.ashley.get

class HouseAreaProcessor: AreaProcessor {

    override fun process(areas: Collection<AreaInfo>, player: LocationPlayer, location: Location) {
        val areasByHost = areas.groupBy { (it.params as AreaParams.House).host }
        val houseAwares = location.content.entities.all()
            .ofPlayer(player)
            .houseAwares()

        for (entity in houseAwares) {
            val ha = entity[houseAware]!!
            val position = entity.position()

            ha.closestOwnedHouse = (areasByHost[entity] ?: emptySet())
                .accessibleTo(entity, location.content)
                .minByOrNull { distance(position, it.area.first()) }

            //todo may be improved
            ha.closestHouse = areas
                .accessibleTo(entity, location.content)
                .minByOrNull { distance(position, it.area.first()) }
        }
    }
}