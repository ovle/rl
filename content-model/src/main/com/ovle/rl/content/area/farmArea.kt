package com.ovle.rl.content.area

import com.ovle.rl.content.task.type.factory.FarmPointHelper
import com.ovle.rl.model.game.area.AreaTemplate
import com.ovle.rl.model.game.tile.TileCheck

val farmArea = AreaTemplate(
    name = "farm",
    processor = FarmAreaProcessor(FarmPointHelper()),
    tileCheck = TileCheck(
        check = { it.props.isSoil },
        description = "soil"
    )
)