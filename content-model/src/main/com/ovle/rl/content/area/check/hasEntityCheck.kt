package com.ovle.rl.content.area.check

import com.ovle.rl.model.game.area.AreaCheck
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.ofTemplate

fun hasEntityCheck(entity: EntityTemplate, amount: Int = 1) = AreaCheck(
    check = { a, l ->
        val entities = l.content.entities.on(a.area)
            .ofTemplate(entity)

        entities.size >= amount
    },
    description = "has ${entity.name} x $amount"
)