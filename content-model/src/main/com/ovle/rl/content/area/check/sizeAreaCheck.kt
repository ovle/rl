package com.ovle.rl.content.area.check

import com.ovle.rl.model.game.area.AreaCheck


fun sizeAreaCheck(size: Int) = AreaCheck(
    check = { a, _ ->
        a.area.size >= size
    },
    description = "min size: $size"
)