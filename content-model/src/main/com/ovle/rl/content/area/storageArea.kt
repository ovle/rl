package com.ovle.rl.content.area

import com.ovle.rl.model.game.area.AreaTemplate
import com.ovle.rl.model.game.tile.TileCheck

val storageArea = AreaTemplate(
    name = "storage",
    processor = StorageAreaProcessor(),
    tileCheck = TileCheck(
        check = { it.props.isArtificial },
        description = "floor"
    )
)