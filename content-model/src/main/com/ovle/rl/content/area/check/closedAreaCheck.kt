package com.ovle.rl.content.area.check

import com.ovle.rl.model.game.area.AreaCheck
import com.ovle.utils.gdx.math.point.allOuterBorders


fun closedAreaCheck(maxInvalidBorders: Int) = AreaCheck(
    check = { a, l ->
        val borders = a.area.allOuterBorders()
        val tiles = l.content.tiles
        val invalidBorders = borders.filter { p ->
            val props = tiles[p.x, p.y].props
            props.isPassable || !props.isArtificial
        }

        invalidBorders.size <= maxInvalidBorders
    },
    description = "closed area"
)