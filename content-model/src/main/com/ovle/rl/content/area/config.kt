package com.ovle.rl.content.area

val areaTemplates = listOf(
    houseArea,
    farmArea,
    storageArea,
)