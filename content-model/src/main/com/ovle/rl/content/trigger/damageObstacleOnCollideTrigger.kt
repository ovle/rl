package com.ovle.rl.content.trigger

import com.ovle.rl.TriggerEffect
import com.ovle.rl.content.effect.DamageEffectPayload
import com.ovle.rl.content.effect.damageEffect
import com.ovle.rl.model.game.collision.Components.collision
import com.ovle.rl.model.game.collision.EntityCollidesEntityEvent
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.trigger.dto.TriggerTemplate
import ktx.ashley.get

val damageObstacleOnCollideTrigger = TriggerTemplate(
    name = "damageObstacleOnCollide",
    key = EntityCollidesEntityEvent::class.java,
    effects = listOf(
        TriggerEffect(
            effect = damageEffect.effect,
            getParams = { event ->
                event as EntityCollidesEntityEvent
                val collisionDamage = event.entity[collision]!!.collisionDamage

                EffectParams(event.entity, DamageEffectPayload(event.obstacle, collisionDamage))
            },
        )
    )
)