package com.ovle.rl.content.trigger

import com.ovle.rl.TriggerEffect
import com.ovle.rl.content.effect.DamageEffectPayload
import com.ovle.rl.content.effect.DestroyEffectPayload
import com.ovle.rl.content.effect.damageEffect
import com.ovle.rl.content.effect.destroyEffect
import com.ovle.rl.model.game.collision.EntityCollidesEntityEvent
import com.ovle.rl.model.game.collision.EntityCollidesTilesEvent
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.trigger.dto.TriggerTemplate

val destroyEntityOnEntityCollide = TriggerTemplate(
    name = "destroyEntityOnEntityCollide",
    key = EntityCollidesEntityEvent::class.java,
    effects = listOf(
        TriggerEffect(
            effect = destroyEffect.effect,
            getParams = { event ->
                event as EntityCollidesEntityEvent
                val source = event.entity
                EffectParams(source, DestroyEffectPayload(source))
            }
        )
    )
)