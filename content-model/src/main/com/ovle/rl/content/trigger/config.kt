package com.ovle.rl.content.trigger

val triggerTemplates = listOf(
    damageObstacleOnCollideTrigger,
    destroyEntityOnTileCollideTrigger,
    destroyEntityOnEntityCollide,
    destroyEntityOnStopMovementTrigger,
)