package com.ovle.rl.content.trigger

import com.ovle.rl.TriggerEffect
import com.ovle.rl.content.effect.DestroyEffectPayload
import com.ovle.rl.content.effect.destroyEffect
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.space.EntityStoppedMovementEvent
import com.ovle.rl.model.game.trigger.dto.TriggerTemplate

val destroyEntityOnStopMovementTrigger = TriggerTemplate(
    name = "destroyEntityOnStopMovement",
    key = EntityStoppedMovementEvent::class.java,
    effects = listOf(
        TriggerEffect(
            effect = destroyEffect.effect,
            getParams = { event ->
                event as EntityStoppedMovementEvent
                val source = event.entity
                EffectParams(source, DestroyEffectPayload(source))
            }
        )
    )
)