package com.ovle.rl.content.game.condition

import com.ovle.rl.model.game.game.dto.FinishGameCondition
import com.ovle.rl.model.game.game.dto.GameResult
import com.ovle.rl.model.game.time.GameTimeChangedEvent
import com.ovle.rl.model.game.time.globalTime

fun timeoutLoseCondition(hours: Int) = FinishGameCondition(
    description = "Lose game due to timeout ($hours hours)",
    result = GameResult.LOSE,
    check = { e, g ->
        if (e !is GameTimeChangedEvent) return@FinishGameCondition false

        val time = g.globalTime().toGameTime()
        (time.totalHours >= hours)
    }
)