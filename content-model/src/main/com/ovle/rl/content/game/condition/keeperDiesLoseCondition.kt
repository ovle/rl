package com.ovle.rl.content.game.condition

import com.ovle.rl.content.entity.other.keeperEntity
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.game.EntityDestroyedEvent
import com.ovle.rl.model.game.game.dto.FinishGameCondition
import com.ovle.rl.model.game.game.dto.GameResult
import com.ovle.rl.model.game.social.player

val keeperDiesLoseCondition = FinishGameCondition(
    description = "lose game if the keeper is destroyed",
    result = GameResult.LOSE,
    check = { e, g ->
        if (e !is EntityDestroyedEvent) return@FinishGameCondition false

        val entity = e.entity
        entity.template()== keeperEntity
            && entity.player() != null
    }
)