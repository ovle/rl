package com.ovle.rl.content.game.condition

import com.ovle.rl.model.game.game.dto.FinishGameCondition
import com.ovle.rl.model.game.game.dto.GameResult
import com.ovle.rl.model.game.life.isAlive
import com.ovle.rl.model.game.time.GameTimeChangedEvent
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.ofPlayer


fun manyMenWinCondition(threshold: Int) = FinishGameCondition(
    description = "Win game if having $threshold men",
    result = GameResult.WIN,
    check = { e, g ->
        if (e !is GameTimeChangedEvent) return@FinishGameCondition false

        val location = g.location()
        val player = location.players.human
        val pfe = location.content.entities.all().ofPlayer(player)
        pfe.count { it.isAlive() } >= threshold
    }
)