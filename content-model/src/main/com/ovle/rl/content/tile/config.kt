package com.ovle.rl.content.tile

import com.ovle.rl.model.game.tile.TileProps
import com.ovle.rl.model.game.tile.TileTemplate

//soil = limestone
//sand soil = sandstone
//soft rock = dolomite
//? = shale
//hard rock = granite
//bedrock = ?

val deepLava = TileTemplate(
    'L', "lava (deep)",
    TileProps(isLava = true, isDeep = true))
val shallowLava = TileTemplate(
    'l', "lava (shallow)",
    TileProps(isLava = true))
val deepWater = TileTemplate(
    'W', "water (deep)",
    TileProps(isWater = true, isDeep = true))
val shallowWater = TileTemplate(
    'w', "water (shallow)",
    TileProps(isWater = true))
val pitFloor = TileTemplate(
    'V', "pit",
    TileProps(isDeep = true))
val iceFloor = TileTemplate(
    '~', "ice",
    TileProps(isIce = true))
val arcticDeepWater = TileTemplate(
    '≈', "water (arctic, deep)",
    TileProps(isWater = true, isDeep = true))

val naturalWallMineralCopper = TileTemplate(
    'c', "wall +copper",
    TileProps(isWall = true, isNatural = true, isDeposit = true))
val naturalWallMineralIron = TileTemplate(
    'i', "wall +iron",
    TileProps(isWall = true, isNatural = true, isDeposit = true))
val naturalWallMineralGold = TileTemplate(
    'g', "wall +gold",
    TileProps(isWall = true, isNatural = true, isDeposit = true))
val naturalWallMineralDiamond = TileTemplate(
    'd', "wall +diamond",
    TileProps(isWall = true, isNatural = true, isDeposit = true))

val naturalFloorGrass = TileTemplate(
    '/', "floor (grass)",
    TileProps(isFloor = true, isNatural = true, isSoil = true))
val naturalFloorSand = TileTemplate(
    's', "floor (sand)",
    TileProps(isFloor = true, isNatural = true))
val naturalFloorSnow = TileTemplate(
    'z', "floor (snow)",
    TileProps(isFloor = true, isNatural = true))
val naturalFloorTundraSoil = TileTemplate(
    't', "floor (tundra)",
    TileProps(isFloor = true, isNatural = true, isSoil = true))
val naturalFloorJungleGrass = TileTemplate(
    'j', "floor (jungle)",
    TileProps(isFloor = true, isNatural = true, isSoil = true))

val naturalFloorSoil = TileTemplate(
    '.', "floor (soil)",
    TileProps(isFloor = true, isNatural = true, isSoil = true))

val naturalFloorSedStone = TileTemplate(
    ',', "floor (sed stone)",
    TileProps(isFloor = true, isNatural = true))
val naturalFloorMetaStone = TileTemplate(
    'o', "floor (meta stone)",
    TileProps(isFloor = true, isNatural = true))
val naturalFloorIgnStone = TileTemplate(
    'q', "floor (ign stone)",
    TileProps(isFloor = true, isNatural = true))

val naturalWallSoil = TileTemplate(
    ':', "wall (soil)",
    TileProps(isWall = true, isNatural = true))
val naturalWallSand = TileTemplate(
    'S', "wall (sand)",
    TileProps(isWall = true, isNatural = true))
val naturalWallSnow = TileTemplate(
    'Z', "wall (snow)",
    TileProps(isWall = true, isNatural = true))

val naturalWallSedStone = TileTemplate(
    '=', "wall (sed stone)",
    TileProps(isWall = true, isNatural = true))
val naturalWallMetaStone = TileTemplate(
    'O', "wall (meta stone)",
    TileProps(isWall = true, isNatural = true))
val naturalWallIgnStone = TileTemplate(
    'Q', "wall (ign stone)",
    TileProps(isWall = true, isNatural = true))

val artFloorStoneRoad = TileTemplate(
    'R', "road [stone]",
    TileProps(isFloor = true, isArtificial = true))


val artWallStone = TileTemplate(
    'x', "wall [stone]",
    TileProps(isWall = true, isArtificial = true))
val artFloorStone = TileTemplate(
    'X', "floor [stone]",
    TileProps(isFloor = true, isArtificial = true))
val doorStone = TileTemplate(
    '{', "door [stone]",
    TileProps(isDoor = true, isArtificial = true))
val windowStone = TileTemplate(
    '}', "window [stone]",
    TileProps(isWindow = true, isArtificial = true))

val artWallWood = TileTemplate(
    'n', "wall [wood]",
    TileProps(isWall = true, isArtificial = true))
val artFloorWood = TileTemplate(
    'N', "floor [wood]",
    TileProps(isFloor = true, isArtificial = true))
val doorWood = TileTemplate(
    '[', "door [wood]",
    TileProps(isDoor = true, isArtificial = true))
val windowWood = TileTemplate(
    ']', "window [wood]",
    TileProps(isWindow = true, isArtificial = true))

val artWallBrick = TileTemplate(
    'b', "wall [brick]",
    TileProps(isWall = true, isArtificial = true))
val artFloorBrick = TileTemplate(
    'B', "floor [brick]",
    TileProps(isFloor = true, isArtificial = true))


val tileTemplates = listOf(
    deepLava, shallowLava, deepWater, shallowWater, pitFloor, iceFloor,
    naturalWallMineralCopper, naturalWallMineralIron, naturalWallMineralGold, naturalWallMineralDiamond,
    naturalFloorSand, naturalFloorSnow, naturalFloorTundraSoil, naturalFloorJungleGrass,
    naturalFloorSoil, naturalFloorSedStone, naturalFloorMetaStone, naturalFloorIgnStone,
    naturalWallSoil, naturalWallSedStone, naturalWallMetaStone, naturalWallIgnStone,
    artFloorStoneRoad, artFloorStone, artWallStone, artFloorWood, artWallWood, artFloorBrick, artWallBrick,
    doorStone, doorWood, windowStone, windowWood
)