package com.ovle.rl.content.tile.mapper

import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.config.TileMapper
import com.ovle.rl.model.procgen.grid.GridsValue
import com.ovle.utils.noise4j.grid.factory.impl.dungeon.DungeonGridFactory


class DungeonLocationTileMapper(
    val params: Params
): TileMapper {

    companion object {
        data class Params(
            val wallTile: TileTemplate,
            val floorTile: TileTemplate,
            val corridorTile: TileTemplate,
        )
    }

    override fun map(v: GridsValue): TileTemplate {
        val v0 = v.value[0]
        return when {
            v0 >= DungeonGridFactory.WALL_THRESHOLD -> params.wallTile
            v0 >= DungeonGridFactory.FLOOR_THRESHOLD -> params.floorTile
            v0 >= DungeonGridFactory.CORRIDOR_THRESHOLD -> params.corridorTile
            else -> throw IllegalArgumentException("illegal tile value: $v0")
        }
    }
}