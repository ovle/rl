package com.ovle.rl.content.tile.mapper

import com.ovle.rl.content.tile.*
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.config.TileMapper
import com.ovle.rl.model.procgen.grid.GridsValue


class WorldTileMapper(
    private val checker: TileMapChecker
): TileMapper {

    override fun map(v: GridsValue): TileTemplate {
        val isDeepWater = checker.isDeepWater(v)
        val isShallowWater = checker.isShallowWater(v)
        val isGround = checker.isGround(v)
        val isSoilLayer = checker.isSoilLayer(v)
        val isSedimentaryLayer = checker.isSedimentaryLayer(v)
        val isMetaLayer = checker.isMetaLayer(v)
        val isMagmaLayer = checker.isMagmaLayer(v)
        val isDeepMagmaLayer = checker.isDeepMagmaLayer(v)

        val isIgneousLayer = checker.isIgneousLayer(v)

        val isChasmShallow = checker.isChasmShallow(v)
        val isChasmDeep = checker.isChasmDeep(v)

        val isFreezing = checker.isFreezing(v)
        val isCold = checker.isCold(v)
        val isScorching = checker.isScorching(v)
        val isHot = checker.isHot(v)

        val isHumid = checker.isHumid(v)
        val isDry = checker.isDry(v)

        return when {
            isDeepWater -> when {
                isFreezing -> arcticDeepWater
                else -> deepWater
            }
            isShallowWater -> when {
                isFreezing -> iceFloor
                //isScorching -> naturalFloorSoil
                else -> shallowWater
            }
            isChasmDeep -> pitFloor
            isChasmShallow -> when {
                isIgneousLayer -> naturalFloorIgnStone
                isSedimentaryLayer -> naturalFloorSedStone
                isMetaLayer -> naturalFloorMetaStone
                else -> naturalFloorSoil
            }
            isGround -> {
                when {
                    isFreezing -> naturalFloorSnow
                    isScorching -> naturalFloorSand
                    isCold -> naturalFloorTundraSoil
                    isHot -> when {
                        isHumid -> naturalFloorJungleGrass
                        isDry -> naturalFloorSand
                        else -> naturalFloorSoil
                    }
                    else -> when {
                        isDry -> naturalFloorSoil
                        else -> naturalFloorGrass
                    }
                }
            }
            isSoilLayer -> when {
                isFreezing -> naturalWallSnow
                isScorching -> naturalWallSand
                else -> naturalWallSoil
            }
            isIgneousLayer -> naturalWallIgnStone

            isSedimentaryLayer -> naturalWallSedStone
            isMetaLayer -> naturalWallMetaStone
            isMagmaLayer -> shallowLava
            isDeepMagmaLayer -> deepLava

            else -> throw IllegalStateException("unknown tile for values: " +
                "alt: ${v.alt}, temp: ${v.temp}, hum: ${v.hum}, min: ${v.min}")
        }
    }
}