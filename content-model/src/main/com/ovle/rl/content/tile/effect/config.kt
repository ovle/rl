package com.ovle.rl.content.tile.effect

import com.badlogic.ashley.core.Entity
import com.ovle.rl.content.effect.static.waterSlowEffect
import com.ovle.rl.content.tile.deepLava
import com.ovle.rl.content.tile.deepWater
import com.ovle.rl.content.tile.shallowLava
import com.ovle.rl.content.tile.shallowWater
import com.ovle.rl.content.time.deepWaterDamageOverTimeTimer
import com.ovle.rl.content.time.destroyTimer
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.game.tile.TileEffectsTemplate
import ktx.ashley.get


val terrestrialEntityCheck: (Entity) -> Boolean = {
    it[move]?.type == MoveType.DEFAULT
}

val tileEffectTemplates = listOf(
    TileEffectsTemplate(
        tiles = listOf(deepLava, shallowLava),
        entityCheck = { it[move]?.type != MoveType.FLYING },
        timerEffects = listOf(destroyTimer)
    ),
    TileEffectsTemplate(
        tiles = setOf(shallowWater),
        entityCheck = terrestrialEntityCheck,
        staticEffects = listOf(
            waterSlowEffect
        )
    ),
    TileEffectsTemplate(
        tiles = setOf(deepWater),
        entityCheck = terrestrialEntityCheck,
        timerEffects = listOf(
            deepWaterDamageOverTimeTimer
        )
    )
)