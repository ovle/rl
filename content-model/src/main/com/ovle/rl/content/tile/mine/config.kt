package com.ovle.rl.content.tile.mine

import com.ovle.rl.content.entity.resource.raw.*
import com.ovle.rl.content.tile.*
import com.ovle.rl.model.game.mine.MineTemplate


val mineTemplates = listOf(
    MineTemplate(
        name = "sed stone",
        tileTemplate = naturalWallSedStone,
        minedTileTemplate = naturalFloorSedStone,
        yield = stoneEntity,
        durability = 5
    ),
    MineTemplate(
        name = "meta stone",
        tileTemplate = naturalWallMetaStone,
        minedTileTemplate = naturalFloorMetaStone,
        yield = stoneEntity,
        durability = 10
    ),
    MineTemplate(
        name = "ign stone",
        tileTemplate = naturalWallIgnStone,
        minedTileTemplate = naturalFloorIgnStone,
        yield = stoneEntity,
        durability = 20
    ),

    MineTemplate(
        name = "soil",
        tileTemplate = naturalWallSoil,
        minedTileTemplate = naturalFloorSoil,
        durability = 2
    ),
    MineTemplate(
        name = "snow",
        tileTemplate = naturalWallSnow,
        minedTileTemplate = naturalFloorSnow,
        durability = 2
    ),
    MineTemplate(
        name = "sand",
        tileTemplate = naturalWallSand,
        minedTileTemplate = naturalFloorSand,
        durability = 2
    ),

    MineTemplate(
        name = "copper",
        tileTemplate = naturalWallMineralCopper,
        minedTileTemplate = naturalFloorSedStone,
        yield = copperEntity,
        durability = 5
    ),
    MineTemplate(
        name = "iron",
        tileTemplate = naturalWallMineralIron,
        minedTileTemplate = naturalFloorMetaStone,
        yield = ironEntity,
        durability = 10
    ),
    MineTemplate(
        name = "gold",
        tileTemplate = naturalWallMineralGold,
        minedTileTemplate = naturalFloorMetaStone,
        yield = goldEntity,
        durability = 5
    ),
    MineTemplate(
        name = "diamond",
        tileTemplate = naturalWallMineralDiamond,
        minedTileTemplate = naturalFloorIgnStone,
        yield = diamondEntity,
        durability = 50
    )
)