package com.ovle.rl.content.tile.mapper

import com.ovle.rl.model.procgen.grid.GridsValue
import com.ovle.utils.gdx.math.between


class TileMapChecker {

    companion object {
        const val DEEP_MAGMA_ROCK_LEVEL_V0 = 0.92f
        const val MAGMA_ROCK_LEVEL_V0 = 0.9f
        const val META_ROCK_LEVEL_V0 = 0.8f
        const val SEDIMENTARY_ROCK_LEVEL_V0 = 0.7f
        const val SOIL_ROCK_LEVEL_V0 = 0.6f
        //soil ground
        const val WATER_LEVEL_V0 = 0.4f
        const val DEEP_WATER_LEVEL_V0 = 0.3f

        const val RIVER_LEVEL_V0 = (WATER_LEVEL_V0 + DEEP_WATER_LEVEL_V0) / 2
        const val SAMPLE_GROUND_LEVEL_V0 = (SOIL_ROCK_LEVEL_V0 + WATER_LEVEL_V0) / 2

        const val FREEZING_LEVEL_V1 = 0.2f
        const val COLD_LEVEL_V1 = 0.3f
        const val HOT_LEVEL_V1 = 0.6f
        const val SCORCHING_LEVEL_V1 = 0.8f

        const val DRY_LEVEL_V2 = 0.3f
        const val HUMID_LEVEL_V2 = 0.7f

        const val IGNEOUS_ROCK_LEVEL_V3 = 0.6f

        const val CHASM_SHALLOW_FROM_V4 = 0.3f
        const val CHASM_DEEP_FROM_V4 = 0.32f
        const val CHASM_DEEP_TO_V4 = 0.33f
        const val CHASM_SHALLOW_TO_V4 = 0.35f
    }


    fun isDeepWater(v: GridsValue) = v.alt <= DEEP_WATER_LEVEL_V0
    fun isShallowWater(v: GridsValue) = v.alt.between(DEEP_WATER_LEVEL_V0, WATER_LEVEL_V0)
    fun isGround(v: GridsValue) = v.alt.between(WATER_LEVEL_V0, SOIL_ROCK_LEVEL_V0)
    fun isSoilLayer(v: GridsValue) = v.alt.between(SOIL_ROCK_LEVEL_V0, SEDIMENTARY_ROCK_LEVEL_V0)
    fun isSedimentaryLayer(v: GridsValue) = v.alt.between(SEDIMENTARY_ROCK_LEVEL_V0, META_ROCK_LEVEL_V0)
    fun isMetaLayer(v: GridsValue) = v.alt.between(META_ROCK_LEVEL_V0, MAGMA_ROCK_LEVEL_V0)
    fun isMagmaLayer(v: GridsValue) = v.alt.between(MAGMA_ROCK_LEVEL_V0, DEEP_MAGMA_ROCK_LEVEL_V0)
    fun isDeepMagmaLayer(v: GridsValue) = v.alt >= DEEP_MAGMA_ROCK_LEVEL_V0

    fun isIgneousLayer(v: GridsValue) = v.min >= IGNEOUS_ROCK_LEVEL_V3
        && (isSedimentaryLayer(v) || isMetaLayer(v))

    fun isAllowedRiver(v: GridsValue) = v.alt.between(WATER_LEVEL_V0, SEDIMENTARY_ROCK_LEVEL_V0)
    fun isAllowedChasm(v: GridsValue) = v.alt.between(SOIL_ROCK_LEVEL_V0, MAGMA_ROCK_LEVEL_V0)

    fun isChasmShallow(v: GridsValue) = isAllowedChasm(v)
        && v.cha.between(CHASM_SHALLOW_FROM_V4, CHASM_SHALLOW_TO_V4)
    fun isChasmDeep(v: GridsValue) = isAllowedChasm(v)
        && v.cha.between(CHASM_DEEP_FROM_V4, CHASM_DEEP_TO_V4)

    fun isFreezing(v: GridsValue) = v.temp < FREEZING_LEVEL_V1
    fun isCold(v: GridsValue) = v.temp < COLD_LEVEL_V1
    fun isScorching(v: GridsValue) = v.temp > SCORCHING_LEVEL_V1
    fun isHot(v: GridsValue) = v.temp > HOT_LEVEL_V1

    fun isHumid(v: GridsValue) = v.hum > HUMID_LEVEL_V2
    fun isDry(v: GridsValue) = v.hum < DRY_LEVEL_V2

    fun isAllowedDeposit(v: GridsValue) = !isChasmShallow(v) && !isChasmDeep(v)
    fun isSedimentaryLayerDeposit(v: GridsValue) = isAllowedDeposit(v) && isSedimentaryLayer(v)
    fun isMetaLayerDeposit(v: GridsValue) = isAllowedDeposit(v) && isIgneousLayer(v)
    fun isIgneousLayerDeposit(v: GridsValue) = isAllowedDeposit(v) && isIgneousLayer(v)

    fun isAllowedWaterStreams(v: GridsValue) = isHumid(v) && v.temp.between(COLD_LEVEL_V1, SCORCHING_LEVEL_V1)
}
