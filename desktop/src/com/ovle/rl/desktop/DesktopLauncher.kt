package com.ovle.rl.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.ovle.rl.RlGame

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        LwjglApplication(RlGame(), appConfig)
    }
}
