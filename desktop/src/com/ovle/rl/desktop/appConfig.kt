package com.ovle.rl.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.ovle.rl.view.defaultPalette
import com.ovle.rl.view.SCREEN_HEIGHT
import com.ovle.rl.view.SCREEN_WIDTH
import kotlin.math.roundToInt

val appConfig = LwjglApplicationConfiguration().apply {
    fullscreen = false
    width = SCREEN_WIDTH.roundToInt()
    height = SCREEN_HEIGHT.roundToInt()

    initialBackgroundColor = defaultPalette.first()
}
