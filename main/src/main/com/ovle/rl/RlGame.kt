package com.ovle.rl

import com.ovle.rl.screen.BaseScreen
import com.ovle.rl.screen.LoadingScreen
import com.ovle.rl.screen.ScreenManager
import ktx.app.KtxGame
import ktx.async.KtxAsync
import org.kodein.di.direct
import org.kodein.di.factory
import java.util.*

class RlGame : KtxGame<BaseScreen>(), ScreenManager {

    private val screensStack = Stack<Class<out BaseScreen>>()
    private var baseScreen: Class<out BaseScreen>? = null

    override fun create() {
        KtxAsync.initiate()
        initScreens()

        super.create()
    }

    override fun dispose() {
        screensStack.clear()
        super.dispose()
    }

    override fun <Type : BaseScreen> setBaseScreen(type: Class<Type>, payload: Any?) {
        screensStack.clear()
        screensStack.push(type)

        setScreenIntr(type, payload)

        baseScreen = type
    }

    override fun <Type : BaseScreen> pushScreen(type: Class<Type>, payload: Any?) {
        screensStack.push(type)
        setScreenIntr(type, payload)
    }

    override fun popScreen() {
        if (screensStack.size == 1) return

        screensStack.pop()
        val type = screensStack.peek()
        setScreen(type)
    }

    override fun popToBaseScreen() {
        while (true) {
            if (screensStack.peek().javaClass == baseScreen) break
            if (screensStack.size == 1) break //?

            screensStack.pop()
        }
        val type = screensStack.peek()
        setScreen(type)
    }


    private fun <Type : BaseScreen> setScreenIntr(type: Class<Type>, payload: Any?) {
        val nextScreen = getScreen(type)
        (nextScreen as BaseScreen).payload = payload
        setScreen(type)
    }

    private fun initScreens() {
        val screens = di.direct.factory<ScreenManager, Collection<BaseScreen>>().invoke(this)
        screens.forEach { addScreen(it.javaClass, it) }
        setScreen(LoadingScreen::class.java)
    }
}