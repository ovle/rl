package com.ovle.rl

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.content.contentModule
import com.ovle.rl.contentView.contentViewModule
import com.ovle.rl.controls.controlsModule
import com.ovle.rl.interaction.interactionModule
import com.ovle.rl.model.game.ai.aiModule
import com.ovle.rl.model.game.ai.impl.behaviorTree.aiBtModule
import com.ovle.rl.model.game.area.areaModule
import com.ovle.rl.model.game.collision.collisionModule
import com.ovle.rl.model.game.container.containerModule
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.factory.factoryModule
import com.ovle.rl.model.game.farm.farmModule
import com.ovle.rl.model.game.game.gameModule
import com.ovle.rl.model.game.gather.gatherModule
import com.ovle.rl.model.game.life.lifeModule
import com.ovle.rl.model.game.light.lightModule
import com.ovle.rl.model.game.material.materialModule
import com.ovle.rl.model.game.mine.mineModule
import com.ovle.rl.model.game.perception.perceptionModule
import com.ovle.rl.model.game.spawn.spawnModule
import com.ovle.rl.persistence.persistenceModule
import com.ovle.rl.model.game.projectile.projectileModule
import com.ovle.rl.model.game.schedule.dayScheduleModule
import com.ovle.rl.view.game.render.renderModule
import com.ovle.rl.model.game.skill.skillModule
import com.ovle.rl.model.game.social.socialModule
import com.ovle.rl.model.game.space.spaceModule
import com.ovle.rl.model.game.story.storyModule
import com.ovle.rl.model.game.task.taskModule
import com.ovle.rl.model.game.tile.tileModule
import com.ovle.rl.model.game.time.timeModule
import com.ovle.rl.model.game.transform.transformModule
import com.ovle.rl.model.game.trigger.triggerModule
import com.ovle.rl.model.procgen.procgenModule
import com.ovle.rl.screen.screensModule
import com.ovle.rl.view.viewModule
import org.kodein.di.DI
import org.kodein.di.bindSet

val di = DI {
    //todo disable in test environment
    import(viewModule)
    import(contentViewModule)
    import(screensModule)

    bindSet<EntitySystem>()
    bindSet<EntityComponent>()
    bindSet<GlobalComponent>()

    import(gameModule)

    import(aiModule)
    import(aiBtModule)
    import(areaModule)
    import(collisionModule)
    import(containerModule)
    import(controlsModule)
    import(dayScheduleModule)
    import(factoryModule)
    import(farmModule)
//    import(influenceModule)
    import(interactionModule)
    import(lifeModule)
    import(lightModule)
    import(materialModule)
    import(mineModule)
    import(gatherModule)
    import(perceptionModule)
    import(spawnModule)
    import(projectileModule)
    import(persistenceModule)
    import(renderModule)
    import(skillModule)
    import(spaceModule)
    import(socialModule)
    import(storyModule)
    import(taskModule)
    import(tileModule)
    import(timeModule)
    import(triggerModule)
    import(transformModule)

    import(procgenModule)

    import(contentModule)
}
