package com.ovle.rl.view.palette.full

import com.badlogic.gdx.graphics.Color

object Oil {
    val blackOil = Color.valueOf("272744")
    val darkOil = Color.valueOf("494D7E")
    val grayOil = Color.valueOf("8B6D9C")
    val lightGrayOil = Color.valueOf("C69FA5")
    val lightOil = Color.valueOf("F2D3AB")
    val whiteOil = Color.valueOf("FBF5EF")
}