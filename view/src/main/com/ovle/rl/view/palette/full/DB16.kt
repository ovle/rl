package com.ovle.rl.view.palette.full

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Colors

object DB16 {
    val black = Color.valueOf("140C1Cff")
    val darkPurple = Color.valueOf("442434ff")
    val darkBlue = Color.valueOf("30346Dff")
    val darkGray = Color.valueOf("4E4A4Eff")
    val brown = Color.valueOf("854C30ff")
    val darkGreen = Color.valueOf("346524ff")
    val red = Color.valueOf("D04648ff")
    val grayBrown = Color.valueOf("757161ff")
    val blue = Color.valueOf("597DCEff")
    val orange = Color.valueOf("D27D2Cff")
    val gray = Color.valueOf("8595A1ff")
    val lightGreen = Color.valueOf("6DAA2Cff")
    val lightBrown = Color.valueOf("D2AA99ff")
    val aquamarine = Color.valueOf("6DC2CAff")
    val yellow = Color.valueOf("DAD45Eff")
    val white = Color.valueOf("DEEED6ff")

    init {
        val colorsMap = Colors.getColors()
        colorsMap.put("db16-black", black)
        colorsMap.put("db16-darkPurple", darkPurple)
        colorsMap.put("db16-darkGray", darkGray)
        colorsMap.put("db16-grayBrown", grayBrown)
        colorsMap.put("db16-brown", brown)
        colorsMap.put("db16-lightBrown", lightBrown)
        colorsMap.put("db16-white", white)
        colorsMap.put("db16-darkBlue", darkBlue)
        colorsMap.put("db16-blue", blue)
        colorsMap.put("db16-gray", gray)

        colorsMap.put("db16-lightGreen", lightGreen)
        colorsMap.put("db16-darkGreen", darkGreen)
        colorsMap.put("db16-yellow", yellow)
        colorsMap.put("db16-orange", orange)
        colorsMap.put("db16-red", red)
    }
}