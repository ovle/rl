package com.ovle.rl.view.palette.full

import com.badlogic.gdx.graphics.Color

object En {
    val blackEn = Color.valueOf("20283D")
    val greenEn = Color.valueOf("426E5D")
    val tealEn = Color.valueOf("E5B083")
    val whiteEn = Color.valueOf("FBF7F3")
}