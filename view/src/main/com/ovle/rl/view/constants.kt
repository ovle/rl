package com.ovle.rl.view

import com.ovle.rl.view.palette.paletteDay

const val SCALE_SCROLL_COEFF = 0.05f
const val CAMERA_MOVE_COEFF = 0.5f

const val SCREEN_WIDTH = 1024.0f
const val SCREEN_HEIGHT = 768.0f

const val TILE_SIZE = 8
const val TEXTURE_TILE_SIZE = 8
const val SPRITE_SIZE = TILE_SIZE
const val SPRITE_SIZE_BIG = 12

const val DEFAULT_TILE_ANIMATION_INTERVAL = 0.125f

const val TILE_LAYER_NAME = "tile-layer"
const val MASK_LAYER_NAME = "mask-layer"
const val DECOR_LAYER_NAME = "decor-layer"

const val SKIN_PATH = "skins/c64/uiskin.json"
const val FONT_NAME = "commodore-64"
const val FONT_SIZE = 16.0f

const val FONT_NAME_SMALL = "commodore-64-small"
const val FONT_SIZE_SMALL = 4.0f

val defaultPalette = paletteDay
