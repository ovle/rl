package com.ovle.rl.view

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.ovle.rl.assets.RlAssetsManager
import com.ovle.rl.screen.ScreenData
import com.ovle.rl.view.game.render.STAGE_VIEWPORT_TAG
import com.ovle.rl.view.game.render.VIEWPORT_TAG
import com.ovle.rl.view.util.camera
import com.ovle.rl.view.util.skin
import com.ovle.utils.gdx.view.PaletteManager
import ktx.graphics.LetterboxingViewport
import ktx.scene2d.Scene2DSkin
import org.kodein.di.*

val viewModule = DI.Module("view") {
    bind<RlAssetsManager>() with singleton { RlAssetsManager(AssetManager()) } //todo dispose
    bind<PaletteManager>() with singleton { PaletteManager(defaultPalette, defaultPalette) }

    bind<OrthographicCamera>() with singleton { camera() }

    bind<Viewport>(STAGE_VIEWPORT_TAG) with singleton { LetterboxingViewport(aspectRatio = SCREEN_WIDTH / SCREEN_HEIGHT) }
    bind<Viewport>(VIEWPORT_TAG) with singleton { FitViewport(SCREEN_WIDTH, SCREEN_HEIGHT, instance()) }

    bind<SpriteBatch>() with singleton { SpriteBatch() }  //todo dispose

    bind<ScreenData>() with singleton {
        ScreenData(SCREEN_WIDTH, SCREEN_HEIGHT, instance(), instance(STAGE_VIEWPORT_TAG), instance(VIEWPORT_TAG), instance())
    }

    val skin = skin()
    Scene2DSkin.defaultSkin = skin //todo how to make it better?
    bind<Skin>() with singleton { skin }
}