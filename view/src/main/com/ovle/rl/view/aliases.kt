package com.ovle.rl.view

import com.ovle.rl.model.game.skill.dto.SkillUsage
import com.ovle.rl.view.game.render.effect.SpriteEffect

typealias GetSkillSpriteEffect = (SkillUsage.Entity) -> SpriteEffect