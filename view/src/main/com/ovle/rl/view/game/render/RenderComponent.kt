package com.ovle.rl.view.game.render

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.ovle.rl.model.game.core.component.EntityComponent

class RenderComponent(
    var sprite: BaseSprite? = null,
    var portrait: TextureRegion? = null,

    var visible: Boolean = true,
    //var zLevel: Int = 0,
) : EntityComponent() {

    override val isModel: Boolean
        get() = false
}