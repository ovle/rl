package com.ovle.rl.view.game.scene2d.tabs

import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.ovle.rl.GetGame
import com.ovle.rl.interaction.PlayerControlMode
import com.ovle.rl.interaction.PlayerControlMode.*
import com.ovle.rl.interaction.SwitchPlayerControlModeCommand
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.time.AbsoluteTimeChangedEvent
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.rl.view.game.scene2d.BIG_BUTTON_HEIGHT
import com.ovle.rl.view.game.scene2d.BaseWidget
import com.ovle.rl.view.game.scene2d.stack
import com.ovle.rl.view.game.scene2d.tabs.areas.AreaInfoHelper
import com.ovle.rl.view.game.scene2d.tabs.areas.AreasWidget
import com.ovle.rl.view.game.scene2d.tabs.player.PlayerWidget
import com.ovle.rl.view.game.scene2d.tabs.tasks.TaskInfoHelper
import com.ovle.rl.view.game.scene2d.tabs.tasks.TasksWidget
import com.ovle.rl.view.game.scene2d.tabs.view.ViewWidget
import com.ovle.rl.view.game.scene2d.tabs.view.EntityInfoHelper
import com.ovle.rl.view.game.scene2d.tabs.view.EntityTemplateInfoHelper
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.*


class GameTabsWidget(
    playerSkillTemplates: Collection<PlayerSkillTemplate>,
    taskTemplates: Collection<TaskTemplate>,
    entityViewTemplates: Collection<EntityViewTemplate>,
    areaViewTemplates: Collection<AreaViewTemplate>,
    taskViewTemplates: Collection<TaskViewTemplate>,
    private val getGame: GetGame
) : BaseWidget() {

    companion object {
        const val ACTIONS_ROW_SIZE = 1
    }

    private lateinit var controlGroup: KStack
    private var currentMode: PlayerControlMode? = null

    private val playerWidget = PlayerWidget(getGame, playerSkillTemplates)
    private val viewWidget = ViewWidget(getGame, EntityTemplateInfoHelper(), EntityInfoHelper(), entityViewTemplates)
    private val tasksWidget = TasksWidget(getGame, TaskInfoHelper(taskViewTemplates), taskTemplates, taskViewTemplates)
    private val areasWidget = AreasWidget(getGame, AreaInfoHelper(), areaViewTemplates)
    private val modeButtons: MutableCollection<Button> = mutableListOf()

    private val widgets: Map<PlayerControlMode, BaseWidget> = mapOf(
        PLAYER to playerWidget,
        TASKS to tasksWidget,
        VIEW to viewWidget,
        AREAS to areasWidget
    )


    override fun subscribe() {
        currentMode = null

        subscribe<AbsoluteTimeChangedEvent>(this) { onTimeChangedEvent() }
        subscribe<SwitchPlayerControlModeCommand>(this) { onSwitchControlModeEvent(it.controlMode) }
    }


    override fun children() = widgets.values

    override fun <S> addActorIntr(container: KWidget<S>): KTableWidget {
        return container.table {
            modeButtons.clear()

            row()
            modeButtons()

            row().growX().growY()
            controlGroup = stack {
                it.fill()
            }
        }
    }

    private fun KTableWidget.modeButtons() {
        buttonGroup(1, 1) {
            it.fill()
            controlModes().forEach { btnRow ->
                btnRow.forEach { cm ->
                    textButton(text = cm.displayName, "toggle") { tb ->
                        tb.growX().height(BIG_BUTTON_HEIGHT)

                        onClick { onSwitchCtrlModeBtnClick(cm) }
                        this.userObject = cm
                        modeButtons += this
                    }
                }
            }
        }
    }

    private fun onSwitchCtrlModeBtnClick(controlMode: PlayerControlMode) {
        val oldControlMode = interaction().controlMode
        if (controlMode == oldControlMode) return

        send(SwitchPlayerControlModeCommand(controlMode))
    }

    private fun onSwitchControlModeEvent(controlMode: PlayerControlMode) {
        updateButtons(controlMode, currentMode)
        updateControlModeGUI(controlMode)
    }

    private fun onTimeChangedEvent() {
        val interaction = interaction()
        val newControlMode = interaction.controlMode
        if (newControlMode == currentMode) return

        updateButtons(newControlMode, currentMode)
        updateControlModeGUI(newControlMode)
    }


    private fun controlModes() = values().toList().chunked(ACTIONS_ROW_SIZE)

    private fun interaction() = getGame().interaction()

    private fun updateButtons(newControlMode: PlayerControlMode, oldControlMode: PlayerControlMode?) {
        if (modeButtons.isEmpty()) return

        modeButtons.single { it.userObject == newControlMode }.isChecked = true
        if (oldControlMode != null) {
            val button = modeButtons.single { it.userObject == oldControlMode }
            button.isChecked = false
        }
    }

    private fun updateControlModeGUI(controlMode: PlayerControlMode) {
        widgets.values.forEach {
            it.removeActor()
        }

        val widget = widgets[controlMode]!!
        widget.addActor(controlGroup)

        controlGroup.layout()

        currentMode = controlMode
    }
}