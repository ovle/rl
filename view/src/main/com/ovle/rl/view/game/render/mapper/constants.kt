package com.ovle.rl.view.game.render.mapper

const val LOCATION_TILE_MAPPER_TAG = "locationTileMapper"
const val WORLD_TILE_MAPPER_TAG = "worldTileMapper"

const val LOCATION_CELL_FACTORY_TAG = "locationCellFactory"
const val WORLD_CELL_FACTORY_TAG = "worldCellFactory"

const val LOCATION_MAP_FACTORY_TAG = "locationMapFactory"
const val WORLD_MAP_FACTORY_TAG = "worldMapFactory"