package com.ovle.rl.view.game.render.gui

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.GameEntity
import com.ovle.rl.interaction.PlayerControlMode.PLAYER
import com.ovle.rl.interaction.PlayerControlMode.TASKS
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.interaction.type.task.TaskInteraction
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.skill.helper.ValidPlayerSkillTargetsHelper
import com.ovle.rl.model.game.task.ValidTaskTargetsHelper
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.util.location
import com.ovle.rl.view.game.render.draw

class RenderCursorHelper(
    private val batch: Batch,
    private val guiSprites: GUISprites,
    private val validTaskTargetsHelper: ValidTaskTargetsHelper,
    private val validPlayerSkillTargetsHelper: ValidPlayerSkillTargetsHelper
) {

    fun drawCursor(interaction: PlayerInteractionComponent, game: GameEntity) {
        val location = game.location()
        val cursorPosition = interaction.cursorPosition
        val hoveredEntity = interaction.entityInteraction.hoveredEntity

        fun drawDefaultCursor() {
            batch.draw(cursorPosition, guiSprites.cursorSprite, isBig = true)
            if (hoveredEntity != null) {
                batch.draw(cursorPosition, guiSprites.entityHoverSprite, isBig = true)
            }
        }

        when (interaction.controlMode) {
            TASKS -> {
                val taskInteraction = interaction.taskInteraction
                val taskTemplate = taskInteraction.selectedTaskTemplate
                if (taskTemplate == null) {
                    drawDefaultCursor()
                    return
                }

                val isValidTarget = isValidTaskTarget(taskInteraction, location, cursorPosition)
                val sprite = if (isValidTarget) guiSprites.validTaskTargetSprite else guiSprites.invalidTaskTargetSprite

                batch.draw(cursorPosition, sprite)
            }
            PLAYER -> {
                val playerSkillInteraction = interaction.playerSkillInteraction
                val skill = playerSkillInteraction.selectedSkill
                if (skill == null) {
                    drawDefaultCursor()
                    return
                }

                val validTarget = validPlayerSkillTargetsHelper.validTarget(skill, cursorPosition, location)
                val sprite = if (validTarget != null) guiSprites.validPlayerSkillTargetSprite
                    else guiSprites.invalidPlayerSkillTargetSprite

                batch.draw(cursorPosition, sprite)
            }

            else -> drawDefaultCursor()
        }
    }

    private fun isValidTaskTarget(
        interaction: TaskInteraction, location: Location, cursorPosition: GridPoint2
    ): Boolean {
        val content = location.content
        if (!content.tiles.isValid(cursorPosition)) return false

        val targetConfig = interaction.selectedTaskTemplate!!.targetConfig
        val entities = content.entities.on(listOf(cursorPosition))
        val targetSelection = interaction.targetSelection!!.copy().apply {
            updateEntities(entities)
            updatePoints(setOf(cursorPosition))
        }

        val potentialTargets = if (targetSelection.check() is TaskTargetCheck.Error) emptySet()
            else targetSelection.taskTargets()
        val player = location.players.human
        val validTargets = validTaskTargetsHelper.validTargets(
            potentialTargets, targetConfig, location, player
        )

        return validTargets.isNotEmpty()
    }
}