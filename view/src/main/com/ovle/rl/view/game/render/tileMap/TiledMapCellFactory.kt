package com.ovle.rl.view.game.render.tileMap

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.view.DEFAULT_TILE_ANIMATION_INTERVAL
import com.ovle.rl.view.game.render.mapper.TileToTextureMapper
import com.ovle.utils.gdx.view.TextureRegions


class TiledMapCellFactory(
    private val tileToTextureMapper: TileToTextureMapper
) {

    fun wallCellMask(
        regions: TextureRegions,
        isFloorDown: Boolean = false,
        isFloorRight: Boolean = false,
        isFloorDiag: Boolean = false
    ): TiledMapTileLayer.Cell {
        val params = TileToTextureParams(
            null,
            regions,
            isFloorDown = isFloorDown,
            isFloorRight = isFloorRight,
            isFloorDiag = isFloorDiag,
        )
        return cell(arrayOf(tileToTextureMapper.map(params)))
    }

    fun unknownCellMask(regions: TextureRegions): TiledMapTileLayer.Cell {
        val params = TileToTextureParams(null, regions, isKnown = false)
        return cell(arrayOf(tileToTextureMapper.map(params)))
    }

    fun invisibleCellMask(regions: TextureRegions): TiledMapTileLayer.Cell {
        val params = TileToTextureParams(null, regions, isVisible = false)
        return cell(arrayOf(tileToTextureMapper.map(params)))
    }

    fun cell(tile: TileTemplate, point: GridPoint2, regions: TextureRegions): TiledMapTileLayer.Cell {
        val params = TileToTextureParams(tile, regions, point)
        return cell(arrayOf(tileToTextureMapper.map(params)))
    }

    fun cell(regions: Array<TextureRegion>): TiledMapTileLayer.Cell {
        val result = TiledMapTileLayer.Cell()
        if (regions.isNotEmpty()) {
            val staticTiles = regions.map { StaticTiledMapTile(it) }.toTypedArray()
            result.tile = tile(staticTiles)
        }
        return result
    }


    private fun tile(staticTiles: Array<StaticTiledMapTile>) =
        if (staticTiles.size == 1) staticTiles.single()
        else AnimatedTiledMapTile(DEFAULT_TILE_ANIMATION_INTERVAL, com.badlogic.gdx.utils.Array(staticTiles))
}