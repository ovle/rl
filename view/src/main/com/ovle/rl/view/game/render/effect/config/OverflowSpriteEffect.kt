package com.ovle.rl.view.game.render.effect.config

import com.ovle.rl.view.game.render.BaseSprite
import com.ovle.rl.view.game.render.effect.SpriteEffect
import com.ovle.rl.view.game.render.effect.SpriteEffectKey
import com.ovle.rl.view.SPRITE_SIZE
import com.ovle.utils.gdx.view.textureRegion


class OverflowSpriteEffect(
    private val effect: SpriteEffect,
): SpriteEffect() {

    override val totalLength = effect.totalLength

    private var effectSprite: BaseSprite? = null


    override fun process(sprite: BaseSprite, time: Double) {
        if (effectSprite == null) {
            effectSprite = effectSprite(sprite)
            sprite.addChild(effectSprite!!)
        } else {
            effect.process(effectSprite!!, time)
        }
    }

    override fun clear(sprite: BaseSprite) {
        effectSprite?.let {
            sprite.removeChild(it)
        }
    }


    private fun effectSprite(sprite: BaseSprite): BaseSprite {
        val childSprite = BaseSprite(textureRegion(sprite.texture, 0, 0, SPRITE_SIZE))
        childSprite.addEffect(SpriteEffectKey.DEFAULT, effect)
        return childSprite
    }
}