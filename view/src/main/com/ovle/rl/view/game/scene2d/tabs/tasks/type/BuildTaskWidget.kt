package com.ovle.rl.view.game.scene2d.tabs.tasks.type

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.ovle.rl.GetGame
import com.ovle.rl.model.game.build.BuildTemplate
import com.ovle.rl.model.game.task.target.selection.BuildTargetSelection
import com.ovle.rl.model.game.tile.isPlaceToBuildOn
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.markError
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import ktx.actors.onClick
import ktx.actors.onEnter
import ktx.actors.onExit
import ktx.scene2d.*


class BuildTaskWidget(
    getGame: GetGame,
    private val templates: Collection<BuildTemplate>
): BaseListWidget(getGame) {

    private lateinit var tileLabel: Label
    private lateinit var resourceLabel: Label
    private lateinit var buildCheckLabel: Label

    private val templateButtons = mutableMapOf<BuildTemplate, KTextButton>()


    override fun subscribe() {
        super.subscribe()

        onSelectBuildTemplateClick(null)
    }

    override fun onTimeChange() {
        super.onTimeChange()

        updateGUI()
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        return container.table {
            row().growX()
            buildTemplates()
            row()
            tileLabel = label("") { it.padTop(5.0f) }
            row()
            resourceLabel = label("") { }
            row()
            buildCheckLabel = label("") { }
            row()
            verticalGroup { it.growY() }
        }
    }

    private fun KTableWidget.buildTemplates() {
        buttonGroup(0, 1) {
            it.fill().padTop(5.0f)
            templates.chunked(2).forEach { btnRow ->
                row()
                btnRow.forEach { bt ->
                    val name = bt.name
                    textButton(name, style = "toggle") { bc ->
                        bc.growX()
                        onClick {
                            if (!isDisabled) {
                                val newTemplate = if (isChecked) bt else null
                                onSelectBuildTemplateClick(newTemplate)
                            }
                        }
                        onEnter { updateTemplateInfo(bt) }
                        onExit { updateTemplateInfo(targetSelection().template) }

                        userObject = bt
                        templateButtons[bt] = this
                    }
                }
            }
        }
    }

    private fun onSelectBuildTemplateClick(template: BuildTemplate?) {
        targetSelection().template = template
    }


    private fun updateGUI() {
        val location = getGame().location()
        val availableTemplates = templates.filter { it.isAvailable(location) }
        val unavailableTemplates = templates - availableTemplates.toSet()

        availableTemplates.forEach { templateButtons[it]!!.isDisabled = false }
        unavailableTemplates.forEach { templateButtons[it]!!.isDisabled = true }
    }

    private fun updateTemplateInfo(template: BuildTemplate?) {
        val tileText = template?.tile?.displayName?.let { "tile: $it" }
        tileLabel.setText(tileText)

        val resourceText = template?.material?.name?.let { "resource: $it" }
        resourceLabel.setText(resourceText)

        val check = template?.check
        val game = getGame()
        val tiles = game.location().content.tiles
        val cursorPosition = game.interaction().cursorPosition

        var buildCheckText = check?.description?.let { "require: $it" }
        if (check != null && !tiles.isPlaceToBuildOn(cursorPosition, template)) {
            buildCheckText = buildCheckText?.markError()
        }

        buildCheckLabel.setText(buildCheckText)
    }

    private fun taskInteraction() = getGame().interaction().taskInteraction

    //todo may get ClassCastException
    private fun targetSelection() = taskInteraction().targetSelection as BuildTargetSelection

}