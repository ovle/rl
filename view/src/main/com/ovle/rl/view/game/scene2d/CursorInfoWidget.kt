package com.ovle.rl.view.game.scene2d

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.ovle.rl.GetGame
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.game.perception.isVisibleWhileKnown
import com.ovle.rl.model.util.info
import com.ovle.rl.model.util.location
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import ktx.scene2d.KWidget
import ktx.scene2d.label
import ktx.scene2d.table

class CursorInfoWidget(getGame: GetGame): BaseListWidget(getGame) {

    private lateinit var pointInfo: Label
    private lateinit var tileInfo: Label
    private lateinit var entitiesInfo: Label

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        return container.table {
            row().growX().padTop(5.0f).padBottom(5.0f).height(35.0f)
            pointInfo = label("") {
                it.padLeft(10.0f).align(Align.left).width(200.0f)
            }
            tileInfo = label("") {
                it.align(Align.center).growX()
            }
            entitiesInfo = label("") {
                it.padRight(10.0f).align(Align.right).width(200.0f)
            }
        }
    }

    override fun onTimeChange() {
        updateTileDetails()
    }


    private fun updateTileDetails() {
        val game = getGame()
        val location = game.location()
        val interaction = game.interaction()
        val player = location.players.human

        val isViewMaskEnabled = interaction.viewConfig.isViewMaskEnabled
        val locationProjection = player.locationProjection
        val point = interaction.cursorPosition
        val pointInfo = point.info()
//        val influence = player.influence
//        val pointInfo = "${point.info()} ${if (influence.get(point)) "*${player.player.id}*" else ""}"

        val isKnownTile = !isViewMaskEnabled || locationProjection.knownMap.isKnownTile(point)
        val isVisibleTile = !isViewMaskEnabled || locationProjection.visibleMap.isVisibleTile(point)
        val tiles = location.content.tiles
        val isSelectedPointValid = tiles.isValid(point)
        val hoveredTile = if (isKnownTile && isSelectedPointValid) tiles[point.x, point.y] else null
        val tileInfo = if (hoveredTile == null) "unknown" else "${hoveredTile.displayName}"
        val entities = location.content.entities.on(point)

        val entitiesInfo = when {
            hoveredTile == null -> "unknown"
            !isVisibleTile -> entitiesInfo(entities.filter { it.isVisibleWhileKnown() })
            else -> entitiesInfo(entities)
        }

        this.pointInfo.setText(pointInfo)
        this.tileInfo.setText(tileInfo)
        this.entitiesInfo.setText(entitiesInfo)
    }

    private fun entitiesInfo(entities: Collection<Entity>) = when (entities.size) {
        0 -> "no entities"
        1 -> entities.single().name()
        else -> "${entities.size} entities"
    }
}
