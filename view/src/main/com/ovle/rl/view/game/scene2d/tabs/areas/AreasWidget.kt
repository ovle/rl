package com.ovle.rl.view.game.scene2d.tabs.areas

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.ovle.rl.GetGame
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.interaction.type.area.PlayerAreaAction
import com.ovle.rl.model.game.area.AreaCheck
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.markError
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import com.ovle.rl.view.game.scene2d.list.ListActionsWidget
import com.ovle.rl.view.game.scene2d.list.ListGroupWidget
import com.ovle.rl.view.game.scene2d.list.ListItemsWidget
import com.ovle.utils.event.EventBus.subscribe
import ktx.scene2d.KWidget
import ktx.scene2d.label
import ktx.scene2d.table


class AreasWidget(
    getGame: GetGame,
    private val areaInfoHelper: AreaInfoHelper,
    private val areaViewTemplates: Collection<AreaViewTemplate>,
): BaseListWidget(getGame) {

    private lateinit var groupsWidget: ListGroupWidget<AreaViewTemplate>
    private lateinit var actionsWidget: ListActionsWidget<PlayerAreaAction>
    private lateinit var itemsWidget: ListItemsWidget<AreaInfo>
    private lateinit var areaLabel: Label
    private lateinit var areaTileCheckLabel: Label
    private lateinit var areaChecksLabel: Label

    private var customWidgetContainer: Group? = null


    override fun subscribe() {
        super.subscribe()

        subscribe<KeyPressedEvent>(this) { onKeyPressedEvent(it.code) }

        onAreaViewTemplateSelect(null)
    }

    private fun onKeyPressedEvent(code: Int) {
        val areaViewTemplate = areaViewTemplates.singleOrNull { it.keyCode == code }
            ?: return

        groupsWidget.selectedGroup = areaViewTemplate
        onAreaViewTemplateSelect(areaViewTemplate)
    }

    override fun onTimeChange() {
        updateGUI()
    }

    private fun onAreaViewTemplateSelect(areaViewTemplate: AreaViewTemplate?) {
        val template = areaViewTemplate?.template
        interaction().selectedAreaTemplate = template
        interaction().selectedArea = null
        interaction().params = null

        updateTemplateInfo(areaViewTemplate)
    }

    private fun onActionSelect(action: PlayerAreaAction) {
        val game = getGame()
        val player = game.location().players.human
        action.action.invoke(itemsWidget.selectedListItem!!, player, game)
    }


    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        val actions = PlayerAreaAction.values().toList()
        actionsWidget = ListActionsWidget(
            actions, actions.map { it.displayName }, ::onActionSelect
        )

        val groups = areaViewTemplates
        val groupNames = groups.map { it.displayName }
        groupsWidget = ListGroupWidget(
            groups, groupNames, ::onAreaViewTemplateSelect,
            groupRowSize = 3
        )

        itemsWidget = ListItemsWidget(
            areaInfo(), ::externalSelectedItem, ::externalSelectItem, { }, 3.5f
        )

        return container.table {
            groupsWidget.addActor(this)
            row()
            areaLabel = label("") {
                it.top().growX().padLeft(10.0f).padTop(5.0f)
                setAlignment(Align.center)
            }
            row()
            areaTileCheckLabel = label("") {
                it.top().growX().padLeft(10.0f)
                setAlignment(Align.center)
            }
            row()
            areaChecksLabel = label("") {
                it.top().growX().padLeft(10.0f)
                setAlignment(Align.center)
            }
            row()
            customWidgetContainer = table {  }
            row()
            actionsWidget.addActor(this)
            itemsWidget.addActor(this)
        }
    }

    private fun areaInfo(): (AreaInfo) -> String = {
        val content = getGame().location().content
        areaInfoHelper.areaInfo(it, content)
    }


    private fun updateGUI() {
        actionsWidget.setActionButtonsDisabled(itemsWidget.selectedListItem == null)
        val areaViewTemplate = groupsWidget.selectedGroup
        itemsWidget.setItems(items(areaViewTemplate))
        itemsWidget.syncSelections()

        val template = areaViewTemplate?.template
        val areaText = template?.name?.lowercase() ?: "all"
        areaLabel.setText(areaText)

        val areaTileCheckText = if (template == null) ""
            else "tile: ${template.tileCheck.description}"
        areaTileCheckLabel.setText(areaTileCheckText)

        val areaChecksText = template?.checks
            ?.joinToString("\n") { areaCheckText(it) } ?: ""
        areaChecksLabel.setText(areaChecksText)
    }

    private fun updateTemplateInfo(viewTemplate: AreaViewTemplate?) {
        val container = customWidgetContainer ?: return
        container.clearChildren()

        viewTemplate ?: return
        val widget = viewTemplate.getWidget.invoke(getGame)
        widget?.let {
            widget.addActor(container as KWidget<Group>)
        }
    }


    private fun items(areaViewTemplate: AreaViewTemplate?): Array<AreaInfo> {
        val player = getGame().location().players.human
        val areas = player.areas
        if (areaViewTemplate == null) return areas.toTypedArray()

        val template = areaViewTemplate.template
        return areas.filter { it.template == template }.toTypedArray()
    }

    private fun externalSelectedItem() = interaction().selectedArea

    private fun externalSelectItem(item: AreaInfo?) {
        val interaction = interaction()

        interaction.selectedArea = item
    }

    private fun areaCheckText(check: AreaCheck): String {
        val areaCheckDescription = check.description
        val selectedArea = interaction().selectedArea
        val selectedAreaTemplate = interaction().selectedAreaTemplate
        if (selectedArea == null || selectedArea.template != selectedAreaTemplate) {
            return areaCheckDescription
        }

        val isCheckValid = check !in selectedArea.invalidChecks

        return if (isCheckValid) areaCheckDescription
            else areaCheckDescription.markError()
    }

    private fun interaction() = getGame().interaction().areaInteraction

}