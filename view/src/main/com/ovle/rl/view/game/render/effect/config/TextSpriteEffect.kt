package com.ovle.rl.view.game.render.effect.config

import com.badlogic.gdx.math.Vector2
import com.ovle.rl.view.game.render.BaseSprite
import com.ovle.rl.view.game.render.SpriteText
import com.ovle.rl.view.game.render.effect.SpriteEffect


class TextSpriteEffect(
    val text: String,
    val points: Array<Vector2>,
    override val totalLength: Double,
    override val timed: Boolean = true
): SpriteEffect() {

    override fun process(sprite: BaseSprite, time: Double) {
        val step = totalLength / points.size
        val index = (time / step).toInt()
        val point = points[index]
        val text = SpriteText(text, point, this)

        sprite.setText(text)
    }

    override fun clear(sprite: BaseSprite) {
        sprite.clearText(this)
    }
}