package com.ovle.rl.view.game.scene2d.tabs.view

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.template.entity.EntityTemplate

class EntityTemplateInfoHelper {

    companion object {
        const val SEPARATOR = "------------------"
    }

    fun entityTemplateInfo(entityTemplate: EntityTemplate, entities: Collection<Entity>): String {
        val count = entities.count()
        val mainInfo = "${entityTemplate.shortName} x $count"
        val secondaryInfo = entityTemplate.name

        return "$mainInfo \n" +
            "$secondaryInfo \n" +
            SEPARATOR
    }
}