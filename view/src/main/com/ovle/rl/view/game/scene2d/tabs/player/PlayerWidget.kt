package com.ovle.rl.view.game.scene2d.tabs.player

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.ovle.rl.GetGame
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.life.hp
import com.ovle.rl.model.game.life.isAlive
import com.ovle.rl.model.game.life.maxHp
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate
import com.ovle.rl.model.game.social.faction
import com.ovle.rl.model.util.keepers
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.ofPlayer
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.scene2d.EntityPortraitWidget
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import com.ovle.rl.view.game.scene2d.stack
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.*


class PlayerWidget(
    getGame: GetGame,
    private val playerSkillTemplates: Collection<PlayerSkillTemplate>,
): BaseListWidget(getGame) {

    private val entityPortrait = EntityPortraitWidget(48.0f)
    private lateinit var nameLabel: Label
    private lateinit var infoLabel: Label

    private val skillButtons = mutableMapOf<PlayerSkillTemplate, KTextButton>()

    private lateinit var skillInfoLabel: Label

    override fun subscribe() {
        super.subscribe()

        subscribe<KeyPressedEvent>(this) { onKeyPressedEvent(it.code) }

        entityPortrait.setPortrait(playerKeeper())
    }

    private fun onKeyPressedEvent(code: Int) {
        //todo hotkeys
    }

    override fun onTimeChange() {
        updateGUI()
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        val keeper = playerKeeper()
        val title = title(keeper)

        return container.table {
            row().height(48.0f).expandX().pad(10.0f)
            entityPortrait.addActor(this)

            nameLabel = label(title) {
                it.top().growX()
                setAlignment(Align.center)
            }

            row().growX().colspan(2)
            infoLabel = label("") {
                it.top().growX().padLeft(10.0f).padBottom(10.0f)
                setAlignment(Align.left)
            }

            row().growX().colspan(2)
            skillTemplates()

            row().growX().colspan(2)
            skillInfoLabel = label("") {
                it.top().growX()
                setAlignment(Align.center)
            }

            row().expandX().expandY()
            stack {
                it.fill()
            }
        }
    }

    private fun KTableWidget.skillTemplates() {
        val location = getGame().location()
        val availableSkillTemplates = skillTemplates(location)

        buttonGroup(0, 1) {
            it.fill().padTop(5.0f)
            availableSkillTemplates.chunked(3).forEach { btnRow ->
                row()
                btnRow.forEach { pst ->
                    val name = pst.name.take(2)
                    textButton(name, style = "toggle") {
                        onClick {
                            if (!isDisabled) {
                                val newTemplate = if (isChecked) pst else null
                                onSkillSelect(newTemplate)
                            }
                        }
//                        onEnter { updateTemplateInfo(pst) }
//                        onExit {
//                            val targetSelection = taskInteraction().targetSelection
//                            updateTemplateInfo(targetSelection.template)
//                        }

                        userObject = pst
                        skillButtons[pst] = this
                    }
                }
            }
        }
    }

    private fun onSkillSelect(skill: PlayerSkillTemplate?) {
        val interaction = interaction()
        interaction.selectedSkill = skill
    }

    private fun updateGUI() {
        val keeper = playerKeeper() ?: return
        val health = "${keeper.hp()}/${keeper.maxHp()}"
//        val influence = player().influence.influencedSize

        val info = arrayOf(
            "hp: $health",
//            "inf: $influence"
        ).joinToString("\n")

        infoLabel.setText(info)

        val skillName = interaction().selectedSkill?.skill?.name ?: ""
        skillInfoLabel.setText(skillName)

        val location = getGame().location()
        val player = getGame().location().players.human
        val templates = skillTemplates(location)
        val availableTemplates = templates.filter {
            keeper.isAlive() &&
            it.cost.all { c -> c.isAbleToPay(player, location) }
        }
        val unavailableTemplates = templates - availableTemplates.toSet()

        availableTemplates.forEach { skillButtons[it]!!.isDisabled = false }
        unavailableTemplates.forEach { skillButtons[it]!!.isDisabled = true }
    }


    private fun title(keeper: Entity?): String {
        keeper ?: return ""
        val faction = keeper.faction()?.name ?: "nothing"
        return "${keeper.name()} \n of $faction"
    }

    private fun player() = getGame().location().players.human

    private fun playerKeeper(): Entity? {
        val location = getGame().location()
        val player = location.players.human
        return location.content.entities.all().ofPlayer(player)
            .keepers()
            .singleOrNull()
    }

    private fun interaction() = getGame().interaction().playerSkillInteraction

    private fun skillTemplates(location: Location) = playerSkillTemplates
        .filter { it.isAvailable(location) }

}