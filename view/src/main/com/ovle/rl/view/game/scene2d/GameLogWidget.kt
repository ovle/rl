package com.ovle.rl.view.game.scene2d

import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.ovle.rl.model.game.time.AbsoluteTimeChangedEvent
import com.ovle.rl.model.game.game.GameLogItem
import com.ovle.utils.event.EventBus.subscribe
import ktx.scene2d.KWidget
import ktx.scene2d.listWidget
import ktx.scene2d.scrollPane
import kotlin.collections.List
import com.badlogic.gdx.scenes.scene2d.ui.List as GdxList


class GameLogWidget(
    private val gameLog: List<GameLogItem>,
) : BaseWidget() {

    private lateinit var listActor: GdxList<GameLogItem>

    override fun subscribe() {
        subscribe<AbsoluteTimeChangedEvent>(this) { onTimeChangedEvent() }
    }

    private fun onTimeChangedEvent() {
        val firstLogItem = gameLog.firstOrNull()
        val lastLogItem = gameLog.lastOrNull()
        val items = listActor.items
        val firstItem = items.firstOrNull()
        val lastItem = items.lastOrNull()
        if (firstItem == firstLogItem && lastItem == lastLogItem) return

        val childrenToDelete = if (firstLogItem == null) items
            else items.takeWhile { it != firstLogItem }
        val deleteCount = childrenToDelete.count()

        val itemsToAdd = if (items.isEmpty) gameLog
            else gameLog.takeLastWhile { lastItem != it }

        val newItems = items.drop(deleteCount) + itemsToAdd

        listActor.setItems(*newItems.toTypedArray())
    }


    override fun <S> addActorIntr(container: KWidget<S>) =
        container.scrollPane {c ->
            (c as Cell<ScrollPane>).grow()
            listWidget<GameLogItem> {}.also {
                listActor = it

                with (listActor.selection) {
                    required = false
                    isDisabled = true
                    toggle = true
                }
            }
        }
}