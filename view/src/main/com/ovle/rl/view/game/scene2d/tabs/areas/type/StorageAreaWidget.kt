package com.ovle.rl.view.game.scene2d.tabs.areas.type

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.ovle.rl.GetGame
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.category.EntityCategoryTemplate
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import ktx.actors.onClick
import ktx.actors.onEnter
import ktx.actors.onExit
import ktx.scene2d.*


class StorageAreaWidget(
    getGame: GetGame,
    private val templates: Collection<EntityCategoryTemplate>
): BaseListWidget(getGame) {

    companion object {
        private const val BTN_GROUP_SIZE = 3
    }

    private lateinit var storageTemplateLabel: Label

    private val templateButtons = mutableMapOf<EntityCategoryTemplate, KTextButton>()


    override fun subscribe() {
        super.subscribe()

        onSelectTemplateClick(null)
    }

    override fun onTimeChange() {
        super.onTimeChange()

//        val templates = templates()

        syncSelections()
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        return container.table {
            row().expandX()
            storageTemplates()
            row()
            storageTemplateLabel = label("") {}
            row()
            verticalGroup { it.growY() }
        }
    }

    private fun KTableWidget.storageTemplates() {
        buttonGroup(0, 1) {
            it.fill().padTop(5.0f)
            templates.chunked(BTN_GROUP_SIZE).forEach { btnRow ->
                row()
                btnRow.forEach { ct ->
                    val name = ct.name
                    textButton(name, style = "toggle") { bc ->
                        bc.growX()
                        onClick {
                            if (!isDisabled) {
                                val newTemplate = if (isChecked) ct else null
                                onSelectTemplateClick(newTemplate)
                            }
                        }
                        onEnter { updateTemplateInfo(ct) }
                        onExit {
                            val p = interaction().params
                            if (p !is AreaParams.Storage) return@onExit
                            updateTemplateInfo(p.categoryTemplate)
                        }

                        userObject = ct
                        templateButtons[ct] = this
                    }
                }
            }
        }
    }

    private fun onSelectTemplateClick(template: EntityCategoryTemplate?) {
        storageTemplateLabel.setText(template?.description ?: "")

        val interaction = interaction()
        val areaParams = AreaParams.Storage(template)
        val selectedArea = interaction.selectedArea

        selectedArea?.params = areaParams
        interaction.params = areaParams
    }

    private fun updateTemplateInfo(template: EntityCategoryTemplate?) {
        storageTemplateLabel.setText(template?.description)
    }

    private fun syncSelections() {
        val selectedTemplate = externalSelectedItem()
        val selectedTemplateBtn = templateButtons[selectedTemplate]

        templateButtons.values.forEach { it.isChecked = false }
        if (selectedTemplate != null) {
            selectedTemplateBtn!!.isChecked = true
        }
    }

    private fun externalSelectedItem(): EntityCategoryTemplate? {
        val interaction = interaction()
        val params = (interaction.selectedArea?.params ?: interaction.params)
        if (params !is AreaParams.Storage?) return null

        params?.let { interaction.params = it }
        return params?.categoryTemplate
    }


    private fun interaction() = getGame().interaction().areaInteraction

}