package com.ovle.rl.view.game.render

import com.ovle.rl.GetGame
import com.ovle.rl.model.game.area.AreaTemplate
import com.ovle.rl.view.game.scene2d.BaseWidget

data class AreaViewTemplate(
    val template: AreaTemplate,
    val displayName: String,
    val areaTileView: AreaTileView? = null,
    val getWidget: (GetGame) -> BaseWidget? = { _ -> null },
    val keyCode: Int
)