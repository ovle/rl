package com.ovle.rl.view.game.scene2d.tabs.view

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.ovle.rl.GameEntity
import com.ovle.rl.GetGame
import com.ovle.rl.interaction.DeselectEntityCommand
import com.ovle.rl.interaction.SelectEntityCommand
import com.ovle.rl.interaction.type.entity.PlayerEntityAction
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.*
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import com.ovle.rl.view.game.scene2d.list.ListActionsWidget
import com.ovle.rl.view.game.scene2d.list.ListGroupWidget
import com.ovle.rl.view.game.scene2d.list.ListItemsWidget
import com.ovle.utils.event.EventBus.send
import ktx.scene2d.KWidget
import ktx.scene2d.label
import ktx.scene2d.table


class EntitiesWidget(
    private val entityInfoHelper: EntityInfoHelper,
    private val entityViewTemplates: Collection<EntityViewTemplate>,
    getGame: GetGame
): BaseListWidget(getGame) {

    companion object {
        private const val ALL_CREATURES_TEXT = "all"
    }

    private lateinit var groupsWidget: ListGroupWidget<EntityTemplate>
    private lateinit var actionsWidget: ListActionsWidget<PlayerEntityAction>
    private lateinit var itemsWidget: ListItemsWidget<Entity>
    private lateinit var creatureNameLabel: Label
    private lateinit var creatureRequiresLabel: Label


    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        val actions = PlayerEntityAction.values().toList()
        actionsWidget = ListActionsWidget(
            actions, actions.map { it.displayName }, ::onActionSelect
        )

        val groups = groups()
        groupsWidget = ListGroupWidget(
            groups, groups.map { groupTitle(it) }, { }, 3, optionalSelection = true
        )

        itemsWidget = ListItemsWidget(
            { entityInfoHelper.entityInfo(it) }, ::externalSelectedItem, ::externalSelectItem, { }, 3.5f
        )

        return container.table {
            groupsWidget.addActor(this)
            row()
            creatureNameLabel = label(ALL_CREATURES_TEXT) {
                it.top().growX().padLeft(10.0f).padTop(5.0f)
                setAlignment(Align.center)
            }
            row()
            creatureRequiresLabel = label("") {
                it.top().growX().padLeft(10.0f)
                setAlignment(Align.center)
            }
            row()
            actionsWidget.addActor(this)
            itemsWidget.addActor(this)
        }
    }

    private fun onActionSelect(action: PlayerEntityAction) {
        action.action.invoke(itemsWidget.selectedListItem!!, getGame())
    }

    override fun onTimeChange() {
        actionsWidget.setActionButtonsDisabled(itemsWidget.selectedListItem == null)

        val entityTemplate = groupsWidget.selectedGroup
        itemsWidget.setItems(items(entityTemplate))
        itemsWidget.syncSelections()

        if (entityTemplate != null) {
            creatureNameLabel.setText(entityTemplate.name)
            val evt = entityViewTemplates.find { evt -> evt.entity == entityTemplate }
            creatureRequiresLabel.setText("require: ${evt?.requireText}" ?: "")
        } else {
            creatureNameLabel.setText(ALL_CREATURES_TEXT)
            creatureRequiresLabel.setText("")
        }
    }

    private fun groups(): Set<EntityTemplate> {
        val location = getGame().location()
        val content = location.content
        return content.entities.all()
            .ofPlayer(location.players.human)
            .excludeKeeper()
            .map { it.template() }.toSet()
    }

    private fun items(template: EntityTemplate?): Array<Entity> {
        val game = getGame()
        var entities = entitiesOfTemplate(game, template)

        val vc = game.interaction().viewConfig
        if (vc.isViewMaskEnabled) {
            val player = game.location().players.human
            entities = entities.knownByPlayer(player)
        }

        return entities.toTypedArray()
    }


    private fun groupTitle(entityTemplate: EntityTemplate): String {
        return entityTemplate.shortName
    }

    //todo not works after destroy
    private fun externalSelectedItem() = getGame().interaction().entityInteraction.selectedEntity

    private fun externalSelectItem(item: Entity?) {
        val command = if (item == null) DeselectEntityCommand() else SelectEntityCommand(item)
        send(command)
    }

    private fun entitiesOfTemplate(
        game: GameEntity, template: EntityTemplate?
    ): Collection<Entity> {
        val location = game.location()
        return location.content.entities.all()
            .ofPlayer(location.players.human)
            .excludeKeeper()
            .ofTemplate(template)
    }
}
