package com.ovle.rl.view.game.render.effect

import com.ovle.rl.view.GetSkillSpriteEffect
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.view.game.render.effect.config.defaultGetSkillObjectSpriteEffect
import com.ovle.rl.view.game.render.effect.config.defaultGetSkillSubjectSpriteEffect

data class SkillViewTemplate (
    val skillTemplate: SkillTemplate?,
    val getSubjectSpriteEffect: GetSkillSpriteEffect = ::defaultGetSkillSubjectSpriteEffect,
    val getObjectSpriteEffect: GetSkillSpriteEffect = ::defaultGetSkillObjectSpriteEffect,
)