package com.ovle.rl.view.game.scene2d.list

import com.badlogic.gdx.scenes.scene2d.Actor
import com.ovle.rl.view.game.scene2d.BaseWidget
import ktx.actors.onClick
import ktx.scene2d.*

/**
 *
 */
class ListActionsWidget<T>(
    private val listActions: List<T>,
    private val actionNames: List<String>,
    private val onActionSelect: (T) -> Unit,
): BaseWidget() {

    private val actionButtons = mutableListOf<KTextButton>()

    fun setActionButtonsDisabled(disabled: Boolean, actions: Collection<T> = listActions) {
        actionButtons.filter { it.userObject in actions }
            .forEach { it.isDisabled = disabled }
    }


    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        container as KTableWidget
        container.row().expandX()

        return container.buttonGroup(0, 1) {
            it.fill().padTop(5.0f).padBottom(5.0f)
            listActions.forEachIndexed { i, a ->
                textButton(actionNames[i]) {
                    onClick {
                        if (!isDisabled) {
                            onActionSelect(a)
                        }
                    }
                    actionButtons += this
                    this.userObject = a
                }
            }
        }
    }
}