package com.ovle.rl.view.game.scene2d

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Scaling
import com.ovle.rl.view.game.render.Components.render
import ktx.ashley.get
import ktx.scene2d.KWidget
import ktx.scene2d.image

class EntityPortraitWidget(
    private val size: Float
): BaseWidget() {

    private lateinit var image: Image

    override fun <S> addActorIntr(container: KWidget<S>) =
        container.image(TextureRegionDrawable() /*hack*/) {
            drawable = null
            it as Cell<*> //todo

            it.padRight(5.0f).left().width(size)
            setScaling(Scaling.fit)

            image = this
        }

    fun setPortrait(entity: Entity?) {
        if (entity == null) {
            image.drawable = null
            return
        }

        val textureRegion = entity[render]!!.portrait
        image.drawable = TextureRegionDrawable(textureRegion)
    }
}