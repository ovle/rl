package com.ovle.rl.view.game.render.effect.config

import com.ovle.rl.view.game.render.BaseSprite
import com.ovle.rl.view.game.render.effect.SpriteEffect


class BlinkSpriteEffect(
    private val frameLength: Double
): SpriteEffect() {

    override val totalLength = frameLength * 2

    override fun process(sprite: BaseSprite, time: Double) {
        val frameIndex = (time / frameLength).toInt()
        val isOdd = frameIndex % 2 == 0

        if (isOdd) {
            sprite.setAlpha(0.0f)
        } else {
            sprite.setAlpha(1.0f)
        }
    }

    override fun clear(sprite: BaseSprite) {
        sprite.setAlpha(1.0f)
    }
}