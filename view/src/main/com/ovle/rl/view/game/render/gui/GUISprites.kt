package com.ovle.rl.view.game.render.gui

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.ovle.rl.assets.RlAssetsManager
import com.ovle.rl.view.SPRITE_SIZE
import com.ovle.rl.view.SPRITE_SIZE_BIG
import com.ovle.rl.view.TEXTURE_TILE_SIZE
import com.ovle.utils.gdx.view.PaletteManager
import com.ovle.utils.gdx.view.textureRegion
import com.ovle.utils.gdx.view.textureRegions

class GUISprites(
    assetsManager: RlAssetsManager,
    paletteManager: PaletteManager
) {
    val guiRegions: Array<Array<TextureRegion>> = textureRegions(
        assetsManager.guiTexture, paletteManager, SPRITE_SIZE
    )
    private val bigRegions: Array<Array<TextureRegion>> = textureRegions(
        assetsManager.guiTexture, paletteManager, SPRITE_SIZE_BIG
    )
    val tileTextureRegions: Array<Array<TextureRegion>> = textureRegions(
        assetsManager.gameTexture, paletteManager, TEXTURE_TILE_SIZE
    )

    val cursorSprite = textureRegion(bigRegions, 0, 0)
    val entitySelectionSprite = textureRegion(bigRegions, 1, 0)
    val entityHoverSprite = textureRegion(bigRegions, 2, 0)
    val entityTaskPerformerSprite = textureRegion(bigRegions, 3, 0)

    val recLeftSprite = textureRegion(guiRegions, 0, 6)
    val recRightSprite = textureRegion(guiRegions, 3, 6)
    val recTopSprite = textureRegion(guiRegions, 1, 6)
    val recBottomSprite = textureRegion(guiRegions, 2, 6)

    val validTaskTargetSprite = textureRegion(guiRegions, 6, 6)
    val invalidTaskTargetSprite = textureRegion(guiRegions, 7, 6)

    val validPlayerSkillTargetSprite = textureRegion(guiRegions, 6, 6)
    val invalidPlayerSkillTargetSprite = textureRegion(guiRegions, 7, 6)

    val selectedAreaSprite = textureRegion(guiRegions, 4, 6)
    val highlightedAreaSprite = textureRegion(guiRegions, 4, 7)
    val areaLeftSprite = textureRegion(guiRegions, 0, 7)
    val areaRightSprite = textureRegion(guiRegions, 3, 7)
    val areaTopSprite = textureRegion(guiRegions, 1, 7)
    val areaBottomSprite = textureRegion(guiRegions, 2, 7)
    val invalidAreaTileSprite = textureRegion(guiRegions, 6, 7)

    val influencedTileSprite = textureRegion(guiRegions, 5, 6)
    val moveGoalTileSprite = textureRegion(guiRegions, 5, 7)
    val unavailableTileSprite = textureRegion(guiRegions, 6, 7)
}