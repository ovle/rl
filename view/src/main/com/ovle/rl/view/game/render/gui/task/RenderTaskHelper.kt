package com.ovle.rl.view.game.render.gui.task

import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.model.game.task.dto.TaskInfo

interface RenderTaskHelper {
    fun drawTasks(tasks: Collection<TaskInfo>)
    fun drawSelection(interaction: PlayerInteractionComponent)
}