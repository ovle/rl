package com.ovle.rl.view.game.render.mapper

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.ovle.rl.view.game.render.tileMap.TileToTextureParams

interface TileToTextureMapper {
    fun map(params: TileToTextureParams): TextureRegion
}