package com.ovle.rl.view.game.render.gui.task

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.selection.BuildTargetSelection
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.view.game.render.draw
import com.ovle.rl.view.game.render.gui.GUISprites
import com.ovle.rl.view.game.render.mapper.TileToTextureMapper
import com.ovle.rl.view.game.render.tileMap.TileToTextureParams
import com.ovle.utils.gdx.math.Direction
import com.ovle.utils.gdx.math.point.borders
import com.ovle.utils.gdx.math.points

class BuildRenderTaskHelper(
    private val batch: Batch,
    private val guiSprites: GUISprites,
    private val tileToTextureMapper: TileToTextureMapper
): RenderTaskHelper {

    override fun drawTasks(tasks: Collection<TaskInfo>) {
        val tiles = tasks.map { it.target as TaskTarget.Build }
            .associate { it.point to it.template.tile }
        drawBuild(tiles)
    }

    override fun drawSelection(interaction: PlayerInteractionComponent) {
        val selection = interaction.selectionRectangle
        val targetSelection = interaction.taskInteraction.targetSelection
        if (targetSelection !is BuildTargetSelection) return

        val tileTemplate = targetSelection.template?.tile

        if (selection != null && tileTemplate != null) {
            val points = selection.points()
            val tiles = points.associateWith { tileTemplate }
            drawBuild(tiles)
        }
    }

    private fun drawBuild(tiles: Map<GridPoint2, TileTemplate>) {
        val points = tiles.keys.toSet()
        points.forEach {
            val tileTemplate = tiles[it]
            tileTemplate?.let { tt ->
                val region = textureRegion(tt)
                batch.draw(it, region)
            }
        }
        drawBorders(points)
    }

    private fun textureRegion(tile: TileTemplate) = tileToTextureMapper.map(
        TileToTextureParams(tile, guiSprites.tileTextureRegions)
    )

    private fun drawBorders(points: Collection<GridPoint2>) {
        with(points) {
            borders(Direction.H, -1).forEach {
                batch.draw(it, guiSprites.recLeftSprite)
            }
            borders(Direction.H, 1).forEach {
                batch.draw(it, guiSprites.recRightSprite)
            }
            borders(Direction.V, 1).forEach {
                batch.draw(it, guiSprites.recTopSprite)
            }
            borders(Direction.V, -1).forEach {
                batch.draw(it, guiSprites.recBottomSprite)
            }
        }
    }
}