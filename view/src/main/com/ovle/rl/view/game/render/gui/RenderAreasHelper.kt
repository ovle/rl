package com.ovle.rl.view.game.render.gui

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.interaction.PlayerControlMode
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.interaction.playerView.PlayerViewMode
import com.ovle.rl.interaction.type.area.AreaInteraction
import com.ovle.rl.model.game.area.ValidAreasHelper
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.view.game.render.draw
import com.ovle.utils.gdx.math.Direction
import com.ovle.utils.gdx.math.point.borders
import com.ovle.utils.gdx.math.points

class RenderAreasHelper(
    private val validAreasHelper: ValidAreasHelper,
    private val batch: Batch,
    private val guiSprites: GUISprites
) {

    fun drawAreas(interaction: PlayerInteractionComponent, location: Location) {
        val isAreaInteractionMode = interaction.controlMode == PlayerControlMode.AREAS
        val isAreaViewMode = PlayerViewMode.AREAS in interaction.viewConfig.viewModes
        if (!isAreaInteractionMode && !isAreaViewMode) return

        //todo other players areas?
        val player = location.players.human
        val areas = player.areas
        areas.forEach {
            drawBorders(it.area)
        }

        val areaInteraction = interaction.areaInteraction
        val selectedArea = areaInteraction.selectedArea
        selectedArea?.let {
            draw(it.area, guiSprites.selectedAreaSprite)
        }

        val template = selectedArea?.template ?: areaInteraction.selectedAreaTemplate
        template?.let {
            val sameTypeAreas = areas.filter { it.template == template }
            sameTypeAreas.forEach { a ->
                draw(a.area, guiSprites.highlightedAreaSprite)
            }
        }

        drawInvalidAreaPoints(areaInteraction, location, interaction)
    }

    private fun drawInvalidAreaPoints(
        areaInteraction: AreaInteraction, location: Location, interaction: PlayerInteractionComponent,
    ) {
        val selectedTemplate = areaInteraction.selectedAreaTemplate ?: return

        val tiles = location.content.tiles
        val player = location.players.human
        val locationProjection = player.locationProjection
        val color = batch.color.cpy()
        val drawColor = Color(color.r, color.g, color.b, 0.5f)
        batch.color = drawColor

        val selection = interaction.selectionRectangle
        val invalidPoints = selection?.points()
            ?.filter { p ->
                !tiles.isValid(p)
                    || !locationProjection.knownMap.isKnownTile(p)
                    || !validAreasHelper.isTileValid(selectedTemplate, p, location)
            } ?: emptyList()

        draw(invalidPoints, guiSprites.invalidAreaTileSprite)

        batch.color = color
    }


    private fun draw(points: Collection<GridPoint2>, tr: TextureRegion) {
        points.forEach { batch.draw(it, tr) }
    }

    private fun drawBorders(points: Collection<GridPoint2>) {
        with(points) {
            borders(Direction.H, -1).forEach {
                batch.draw(it, guiSprites.areaLeftSprite)
            }
            borders(Direction.H, 1).forEach {
                batch.draw(it, guiSprites.areaRightSprite)
            }
            borders(Direction.V, 1).forEach {
                batch.draw(it, guiSprites.areaTopSprite)
            }
            borders(Direction.V, -1).forEach {
                batch.draw(it, guiSprites.areaBottomSprite)
            }
        }
    }
}