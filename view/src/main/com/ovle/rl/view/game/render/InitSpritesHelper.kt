package com.ovle.rl.view.game.render

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.Colors
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.assets.RlAssetsManager
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.core.templateName
import com.ovle.rl.model.game.material.Components.material
import com.ovle.rl.model.game.material.material
import com.ovle.rl.view.SPRITE_SIZE
import com.ovle.rl.view.SPRITE_SIZE_BIG
import com.ovle.rl.view.game.render.Components.render
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.view.textureRegion
import ktx.ashley.get
import ktx.ashley.has
import ktx.log.info

class InitSpritesHelper(
    private val assetsManager: RlAssetsManager,
    private val entityViewTemplates: Collection<EntityViewTemplate>,
) {

    companion object {
        private val DEFAULT_SPRITE = point(6, 7)
        private val BG_SPRITE = point(5, 7)
    }

    private val spriteTexture: Texture
        get() = assetsManager.gameTexture

    private val defaultSprite: TextureRegion by lazy {
        textureRegion(spriteTexture, DEFAULT_SPRITE.x, DEFAULT_SPRITE.y, SPRITE_SIZE.toInt())
    }


    fun initSprites(entity: Entity) {
        val renderComponent = entity[render]!!
        if (renderComponent.sprite != null) return

        val viewTemplate = entityViewTemplate(entity)
        val spritesConfig = (viewTemplate?.sprite)?.toMutableMap()
        if (spritesConfig == null) {
            renderComponent.sprite = BaseSprite(defaultSprite)
            return
        }

        val isBig = viewTemplate.isBig
        val size = if (isBig) SPRITE_SIZE_BIG else SPRITE_SIZE
        val regions = textureRegions(spritesConfig, size)
        viewTemplate.regionsByState = regions

        val region = regions[DEFAULT_SPRITE_KEY]!!
        val mainSprite = BaseSprite(region, isBig)
        val isTinted = viewTemplate.isTinted

        with (renderComponent) {
            sprite = if (isTinted) tintedSprite(entity, viewTemplate, mainSprite) else mainSprite
            portrait = region   //todo separate portraits
        }
    }

    fun switchSprite(entity: Entity, key: String) {
        val viewTemplate = entityViewTemplate(entity) ?: return
        val sprite = entity.spriteOrNull() ?: return

        val textureRegion = viewTemplate.regionsByState!![key] ?: defaultSprite
        sprite.setRegion(textureRegion)
    }


    private fun tintedSprite(
        entity: Entity, viewTemplate: EntityViewTemplate, mainSprite: BaseSprite
    ): BaseSprite {
        val result = BaseSprite(bgSpriteRegion(), viewTemplate.isBig)
        result.addChild(mainSprite)

        val materialColorName = if (entity.has(material)) entity.material()?.color else null
        if (materialColorName != null) {
            mainSprite.tintColor = Colors.get(materialColorName)
        } else {
            info { "no material for tinted sprite: ${entity.templateName()}" }
        }

        return result
    }

    private fun textureRegions(spritesConfig: Map<String, GridPoint2>, size: Int) =
        spritesConfig.mapValues { (_, texturePoint) ->
            textureRegion(spriteTexture, texturePoint.x, texturePoint.y, size)
        }.toMutableMap()

    private fun bgSpriteRegion() = textureRegion(
        spriteTexture, BG_SPRITE.x, BG_SPRITE.y, SPRITE_SIZE,
    )

    private fun entityViewTemplate(entity: Entity) =
        entityViewTemplates.firstOrNull { it.entity == entity.template() }
}