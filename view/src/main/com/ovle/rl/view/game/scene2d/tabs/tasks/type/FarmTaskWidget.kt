package com.ovle.rl.view.game.scene2d.tabs.tasks.type

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.ovle.rl.GetGame
import com.ovle.rl.controls.TaskApprovedEvent
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.game.task.target.selection.FarmTargetSelection
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.markError
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import com.ovle.utils.event.EventBus.send
import ktx.actors.onClick
import ktx.actors.onEnter
import ktx.actors.onExit
import ktx.scene2d.*

/*
class FarmTaskWidget(
    getGame: GetGame,
    private val templates: Collection<FarmTemplate>
): BaseListWidget(getGame) {

    private lateinit var resourceLabel: Label
    private lateinit var areaLabel: Label
    private lateinit var cropLabel: Label
    private lateinit var gatherLabel: Label
    private lateinit var approveBtn: Button

    private val templateButtons = mutableMapOf<FarmTemplate, KTextButton>()


    override fun subscribe() {
        super.subscribe()

        onSelectFarmTemplateClick(null)
    }

    override fun onTimeChange() {
        super.onTimeChange()

        updateGUI()
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        return container.table {
            row().expandX()
            farmTemplates()
            row()
            cropLabel = label("") { }
            row()
            resourceLabel = label("") { }
            row()
            areaLabel = label("") { }
            row()
            gatherLabel = label("") { }
            row()
            approveBtn = textButton("farm") {
                it.padTop(5.0f)
                onClick { if (!isDisabled) onApproveBtnClick() }
            }
            row()
            verticalGroup { it.growY().padTop(10.0f) }
        }
    }


    private fun KTableWidget.farmTemplates() {
        buttonGroup(0, 1) {
            it.fill().padTop(5.0f)
            templates.chunked(4).forEach { btnRow ->
                row()
                btnRow.forEach { ct ->
                    val name = ct.name.take(2)
                    textButton(name, style = "toggle") { bc ->
                        bc.growX()
                        onClick {
                            if (!isDisabled) {
                                val newTemplate = if (isChecked) ct else null
                                onSelectFarmTemplateClick(newTemplate)
                            }
                        }
                        onEnter { updateTemplateInfo(ct) }
                        onExit {
                            val targetSelection = taskInteraction().targetSelection
                            if (targetSelection !is FarmTargetSelection) return@onExit
                            updateTemplateInfo(targetSelection.template)
                        }

                        userObject = ct
                        templateButtons[ct] = this
                    }
                }
            }
        }
    }

    private fun onSelectFarmTemplateClick(template: FarmTemplate?) {
        val targetSelection = taskInteraction().targetSelection
        if (targetSelection is FarmTargetSelection) {
            targetSelection.updateFarmTemplate(template)
        }
    }

    private fun onApproveBtnClick() {
        val targetSelection = taskInteraction().targetSelection
        if (targetSelection is FarmTargetSelection) {
            targetSelection.approved = true
        }

        send(TaskApprovedEvent())
    }


    private fun updateGUI() {
        val location = getGame().location()
        val humanPlayer = location.players.human
        val availableTemplates = templates.filter {
            it.isResourceAvailable(location) && it.isAreaAvailable(humanPlayer)
        }
        val unavailableTemplates = templates - availableTemplates.toSet()

        availableTemplates.forEach { templateButtons[it]!!.isDisabled = false }
        unavailableTemplates.forEach { templateButtons[it]!!.isDisabled = true }

        val selection = taskInteraction().targetSelection
        if (selection is FarmTargetSelection) {
            val isCanBeApproved = selection.check() == TaskTargetCheck.NotApproved
            approveBtn.isDisabled = !isCanBeApproved
        }
    }

    private fun updateTemplateInfo(template: FarmTemplate?) {
        cropLabel.setText(template?.crop?.name)

        val location = getGame().location()
        val humanPlayer = location.players.human
        val isResourceAvailable = template?.isResourceAvailable(location) != false
        val isAreaAvailable = template?.isAreaAvailable(humanPlayer) != false

        var resourceText = template?.resource?.name?.let { "resource: $it" }
        if (!isResourceAvailable) { resourceText = resourceText?.markError() }
        resourceLabel.setText(resourceText)

        var areaText = template?.let { "require: farm area" }
        if (!isAreaAvailable) { areaText = areaText?.markError() }
        areaLabel.setText(areaText)

        val gatherText = template?.gatherResource?.name?.let { "produce: $it" }
        gatherLabel.setText(gatherText)
    }

    private fun taskInteraction() = getGame().interaction().taskInteraction

}
*/
