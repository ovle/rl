package com.ovle.rl.view.game.render

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.Batch
import com.ovle.rl.model.game.core.Components.core
import com.ovle.rl.model.game.core.entity.with
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.EntityLoadedEvent
import com.ovle.rl.model.game.life.EntityAwakenEvent
import com.ovle.rl.model.game.life.EntityDiedEvent
import com.ovle.rl.model.game.life.EntityResurrectedEvent
import com.ovle.rl.model.game.life.EntityStartSleepEvent
import com.ovle.rl.model.game.perception.isVisible
import com.ovle.rl.model.game.perception.isVisibleForInteraction
import com.ovle.rl.model.game.space.Components.aoe
import com.ovle.rl.model.game.space.Components.body
import com.ovle.rl.model.game.space.EntityChangedPositionEvent
import com.ovle.rl.model.game.space.aoeOrNull
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.globalTime
import com.ovle.rl.model.game.transform.EntityTransformedEvent

import com.ovle.rl.model.util.location
import com.ovle.rl.util.interaction
import com.ovle.rl.view.TILE_SIZE
import com.ovle.rl.view.game.render.Components.render
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.ashley.has


class RenderEntitiesSystem(
    private val batch: Batch,
    private val initSpritesHelper: InitSpritesHelper,
) : BaseSystem() {

    companion object {
        private const val VISIBLE_ENTITY_ALPHA = 1.0f
        private const val KNOWN_ENTITY_ALPHA = 0.3f
    }


    override fun update(deltaTime: Float) {
        val content = game().location().content
        val entities = content.entities.all().with(RenderComponent::class)

        val entitiesToDraw = mutableListOf<Entity>()
        entities.forEach { processEntity(it, entitiesToDraw) }

        val entityGroups = entitiesToDraw.groupBy { it.position() }
            .entries
            .sortedBy { e1 -> -e1.key.y }
            .map { it.value }

        drawEntities(entityGroups)
    }

    override fun subscribe() {
        subscribe<EntityLoadedEvent>(this) { onEntityLoadedEvent(it.entity) }
        subscribe<EntityTransformedEvent>(this) { onEntityLoadedEvent(it.entity) }
        subscribe<EntityDiedEvent>(this) { onEntityDiedEvent(it.entity) }
        subscribe<EntityResurrectedEvent>(this) { onEntityResurrectedEvent(it.entity) }
        subscribe<EntityStartSleepEvent>(this) { onEntityStartSleepEvent(it.entity) }
        subscribe<EntityAwakenEvent>(this) { onEntityAwakenEvent(it.entity) }

        subscribe<EntityChangedPositionEvent>(this) { onEntityChangedPositionEvent(it.entity) }
    }


    private fun onEntityLoadedEvent(entity: Entity) {
        if (entity[render] == null) {
            entity.add(RenderComponent())
        }

        initSpritesHelper.initSprites(entity)
        updateSprite(entity)
    }

    private fun onEntityDiedEvent(entity: Entity) {
        val sprite = entity[render]?.sprite ?: return
        sprite.rotation = 270.0f
//        sprite.tintColor = Colors.get("db16-darkPurple")
    }

    private fun onEntityResurrectedEvent(entity: Entity) {
        val sprite = entity[render]?.sprite ?: return
        sprite.rotation = 0.0f
//        sprite.tintColor = null
    }

    private fun onEntityStartSleepEvent(entity: Entity) {
        val sprite = entity[render]?.sprite ?: return
        sprite.rotation = 270.0f
    }

    private fun onEntityAwakenEvent(entity: Entity) {
        val sprite = entity[render]?.sprite ?: return
        sprite.rotation = 0.0f
    }

    private fun onEntityChangedPositionEvent(entity: Entity) {
        updateSprite(entity)
    }


    private fun updateSprite(entity: Entity) {
        val position = entity.position()
        val sprite = entity.spriteOrNull() ?: return

        val screenX = position.x * TILE_SIZE
        val screenY = position.y * TILE_SIZE

        val offsetX = if (sprite.isBig) -TILE_SIZE.toFloat() / 4 else 0.0f

        sprite.setPosition(screenX + offsetX, screenY.toFloat())
    }


    private fun processEntity(entity: Entity, toRender: MutableCollection<Entity>) {
        if (entity[core]!!.isSkipped) return
        if (!entity[render]!!.visible) return

        val game = game()
        val interaction = game.interaction()

        if (!interaction.viewConfig.isViewMaskEnabled) {
            toRender.add(entity)
            entity.spriteOrNull()!!.setAlpha(VISIBLE_ENTITY_ALPHA)
            return
        }

        val player = game.location().players.human
        val locationProjection = player.locationProjection
        if (entity.isVisibleForInteraction(locationProjection)) {
            toRender.add(entity)

            val isVisible = entity.isVisible(locationProjection)
            val alpha = if (isVisible) VISIBLE_ENTITY_ALPHA else KNOWN_ENTITY_ALPHA
            entity.spriteOrNull()!!.setAlpha(alpha)
        }
    }


    private fun drawEntities(entityGroups: List<List<Entity>>) {
        batch.begin()

        val switchSpriteTimeSec = game().globalTime().tick / 60
        for (entities in entityGroups) {
            val entityToDraw = entityToDraw(entities, switchSpriteTimeSec)

            draw(entityToDraw)
        }

        batch.end()
    }


    private fun entityToDraw(entities: List<Entity>, switchSpriteTimeSec: Long) =
        if (entities.size == 1) entities.single()
        else {
            val i = (switchSpriteTimeSec % entities.size).toInt()
            entities[i]
        }

    private fun draw(entity: Entity) {
        checkNotNull(entity[render])

        val renderComponent = entity[render]!!
        when {
            entity.has(aoe) -> {
                val positions = entity.aoeOrNull()!!
                positions.forEach { p ->
                    val region = renderComponent.sprite ?: return
                    batch.draw(p, region)
                }
            }

            entity.has(body) -> {
                val sprite = renderComponent.sprite ?: return
                sprite.draw(batch)
                sprite.drawText(batch)
            }

            else -> throw IllegalStateException("render entity must have body/aoe")
        }
    }
}