package com.ovle.rl.view.game.render.effect

enum class SpriteEffectKey {
    DEFAULT,

    IDLE,
    MOVE,
    SKILL_SUBJECT,
    SKILL_OBJECT,
    TAKE_DAMAGE,
    HEAL,
    SLEEP,
}