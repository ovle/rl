package com.ovle.rl.view.game.render

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.view.SPRITE_SIZE
import com.ovle.rl.view.SPRITE_SIZE_BIG
import com.ovle.rl.view.TILE_SIZE


fun Batch.draw(position: GridPoint2, region: TextureRegion, isBig: Boolean = false) {
    val offset = if (isBig) -TILE_SIZE.toFloat() / 4 else 0.0f
    val screenX = position.x * TILE_SIZE + offset
    val screenY = position.y * TILE_SIZE + offset
    val size = (if (isBig) SPRITE_SIZE_BIG else SPRITE_SIZE).toFloat()

    this.draw(
        region, screenX, screenY, size, size
    )
}