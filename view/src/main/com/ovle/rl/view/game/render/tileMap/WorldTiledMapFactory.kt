package com.ovle.rl.view.game.render.tileMap

import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.ovle.rl.TileArray
import com.ovle.rl.model.procgen.grid.Grids
import com.ovle.rl.view.TILE_LAYER_NAME
import com.ovle.rl.view.TILE_SIZE
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.view.TextureRegions

class WorldTiledMapFactory(
    private val cellFactory: TiledMapCellFactory,
) {

    fun tiledMap(
        tiles: TileArray, grids: Grids, textureRegions: TextureRegions, tileSize: Int = TILE_SIZE
    ): TiledMap {
        val result = TiledMap()

        with (result.layers) {
            add(mainMapLayer(tiles, grids, textureRegions, tileSize))
        }

        return result
    }


    private fun mainMapLayer(
        tiles: TileArray, grids: Grids, textureRegions: TextureRegions, tileSize: Int
    ): MapLayer {
        val result = TiledMapTileLayer(tiles.size, tiles.size, tileSize, tileSize)

        for (x in 0 until tiles.size) {
            for (y in 0 until tiles.size) {
                val tile = tiles[x, y]
                val point = point(x, y)
                val cell = cellFactory.cell(tile, point, textureRegions)

                result.setCell(x, y, cell)
            }
        }

        result.name = TILE_LAYER_NAME
        return result
    }
}