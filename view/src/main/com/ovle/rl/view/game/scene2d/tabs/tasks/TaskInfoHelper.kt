package com.ovle.rl.view.game.scene2d.tabs.tasks

import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.view.game.render.TaskViewTemplate

class TaskInfoHelper(
    private val taskViewTemplates: Collection<TaskViewTemplate>,
) {
    companion object {
        const val SEPARATOR = "------------------"
    }

    fun taskInfo(task: TaskInfo): String {
        val status = "(${task.status.displayName})"
        val name = task.template.name
        val info = taskViewTemplates
            .single { it.taskTemplate == task.template }
            .getInfo(task)

        return "$status $name\n" +
            "$info\n" +
            SEPARATOR
    }
}