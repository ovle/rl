package com.ovle.rl.view.game.render.tileMap

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.utils.gdx.math.point.*


class DecorMaskTileLayer(
    private val tiles: TileArray,
    tileWidth: Int, tileHeight: Int,
    private val downMaskCell: Cell,
    private val rightMaskCell: Cell,
    private val downRightMaskCell: Cell,
    private val downRightDiagMaskCell: Cell,
) : TiledMapTileLayer(tiles.size, tiles.size, tileWidth, tileHeight) {

    override fun getCell(x: Int, y: Int): Cell? {
        val point = point(x, y)
        if (!tiles.isValid(point)) return null

        val tile = tiles[x, y]
        val isWall = tile.props.isWall
        if (isWall) {
            return wallCell(point)
        }
        val isFloor = tile.props.isFloor
        if (isFloor) {
            return floorCell(point)
        }

        return null
    }


    private fun floorCell(point: GridPoint2): Cell? {
        val isDeepDown = isDeep(tiles, point.bottom())
        val isDeepRight = isDeep(tiles, point.right())
        val isDeepDiag = isDeep(tiles, point(point.x + 1, point.y - 1))

        return when {
            isDeepDown && isDeepRight -> downRightMaskCell
            isDeepDown -> downMaskCell
            isDeepRight -> rightMaskCell
            isDeepDiag -> downRightDiagMaskCell
            else -> null
        }
    }

    private fun wallCell(point: GridPoint2): Cell? {
        val isNotWallDown = isNotWall(tiles, point.bottom())
        val isNotWallRight = isNotWall(tiles, point.right())
        val isNotWallDiag = isNotWall(tiles, point(point.x + 1, point.y - 1))

        return when {
            isNotWallDown && isNotWallRight -> downRightMaskCell
            isNotWallDown -> downMaskCell
            isNotWallRight -> rightMaskCell
            isNotWallDiag -> downRightDiagMaskCell
            else -> null
        }
    }

    private fun isNotWall(tiles: TileArray, point: GridPoint2): Boolean {
        if (!tiles.isValid(point)) return false

        val props = tiles[point.x, point.y].props
        return !props.isWall
    }

    private fun isDeep(tiles: TileArray, point: GridPoint2): Boolean {
        if (!tiles.isValid(point)) return false

        val props = tiles[point.x, point.y].props
        return props.isDeep || props.isLiquid
    }
}