package com.ovle.rl.view.game.scene2d.tabs.tasks.type

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.ovle.rl.GetGame
import com.ovle.rl.controls.TaskApprovedEvent
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.game.task.target.selection.CraftTargetSelection
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.knownByPlayer
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.markFail
import com.ovle.rl.model.util.templates
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import com.ovle.utils.event.EventBus.send
import ktx.actors.onClick
import ktx.actors.onEnter
import ktx.actors.onExit
import ktx.scene2d.*


class CraftTaskWidget(
    getGame: GetGame,
    private val craftTemplates: Collection<CraftTemplate>
): BaseListWidget(getGame) {

    private lateinit var tileLabel: Label
    private lateinit var stationLabel: Label
    private lateinit var resourceLabel: Label

    private lateinit var approveBtn: Button

    private val templateButtons = mutableMapOf<CraftTemplate, KTextButton>()
    private val resourceButtons = mutableMapOf<EntityTemplate, KTextButton>()

    private val resources: Collection<EntityTemplate> = craftTemplates
        .flatMap { it.resources ?: emptyList() }.distinct()


    override fun subscribe() {
        super.subscribe()

        onSelectCraftTemplateClick(null)
    }

    override fun onTimeChange() {
        super.onTimeChange()

        updateGUI()
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        return container.table {
            row().growX()
            craftTemplates()
            row()
            tileLabel = label("") { it.padTop(5.0f) }
            row()
            stationLabel = label("") { }
            row().growX()
            resources()
            row()
            resourceLabel = label("") { }
            row()
            approveBtn = textButton("craft") {
                it.padTop(5.0f)
                onClick { if (!isDisabled) onApproveBtnClick() }
            }
            row()
            verticalGroup { it.growY().padTop(5.0f) }
        }
    }


    private fun KTableWidget.craftTemplates() {
        buttonGroup(0, 1) {
            it.fill().padTop(5.0f)
            craftTemplates.chunked(3).forEach { btnRow ->
                row()
                btnRow.forEach { ct ->
                    val name = ct.entity.shortName
                    textButton(name, style = "toggle") { bc ->
                        bc.growX()
                        onClick {
                            if (!isDisabled) {
                                val newTemplate = if (isChecked) ct else null
                                onSelectCraftTemplateClick(newTemplate)
                            }
                        }
                        onEnter { updateTemplateInfo(ct) }
                        onExit { updateTemplateInfo(targetSelection().template) }

                        userObject = ct
                        templateButtons[ct] = this
                    }
                }
            }
        }
    }

    private fun KTableWidget.resources() {
        buttonGroup(0, 1) {
            it.fill().padTop(5.0f)
            resources.chunked(3).forEach { btnRow ->
                row()
                btnRow.forEach { mt ->
                    val name = mt.shortName
                    textButton(name, style = "toggle") { bc ->
                        bc.growX()
                        onClick {
                            if (!isDisabled) {
                                val newTemplate = if (isChecked) mt else null
                                onSelectResourceTemplateClick(newTemplate)
                            }
                        }
                        onEnter { updateResourceInfo(mt) }
                        onExit { updateResourceInfo(targetSelection().resource) }

                        userObject = mt
                        resourceButtons[mt] = this
                    }
                }
            }
        }
    }

    private fun onSelectCraftTemplateClick(template: CraftTemplate?) {
        targetSelection().updateCraftTemplate(template)
    }

    private fun onSelectResourceTemplateClick(template: EntityTemplate?) {
        targetSelection().updateResource(template)
    }

    private fun onApproveBtnClick() {
        targetSelection().approved = true

        send(TaskApprovedEvent())
    }


    private fun updateGUI() {
        val selection = taskInteraction().targetSelection
        if (selection !is CraftTargetSelection) return

        val location = getGame().location()
        val availableTemplates = craftTemplates.filter {
            isStationAvailable(it, location) && isAnyResourceAvailable(it, location)
        }
        val unavailableTemplates = craftTemplates - availableTemplates.toSet()

        availableTemplates.forEach { templateButtons[it]!!.isDisabled = false }
        unavailableTemplates.forEach { templateButtons[it]!!.isDisabled = true }

        val allowedResources = selection.template?.resources
            ?: emptyList()
        val templates = knownEntities(location).templates()
        val availableResources = allowedResources.filter {
            it in templates
        }
        val unavailableResources = resources - availableResources.toSet()

        availableResources.forEach { resourceButtons[it]!!.isDisabled = false }
        unavailableResources.forEach { resourceButtons[it]!!.isDisabled = true }

        val isCanBeApproved = selection.check() == TaskTargetCheck.NotApproved
        approveBtn.isDisabled = !isCanBeApproved
    }


    private fun updateTemplateInfo(template: CraftTemplate?) {
        tileLabel.setText(template?.entity?.name)

        val location = getGame().location()
        val isStationAvailable = isStationAvailable(template, location)

        var stationText = template?.station?.name?.let { "on: $it" }
        if (!isStationAvailable) { stationText = stationText?.markFail() }
        stationLabel.setText(stationText)
    }

    private fun updateResourceInfo(template: EntityTemplate?) {
        val location = getGame().location()
        val isResourceAvailable = isResourceAvailable(template, location)

        var resourceText = template?.name
        if (!isResourceAvailable) { resourceText = resourceText?.markFail() }
        resourceLabel.setText(resourceText)
    }


    private fun isStationAvailable(craftTemplate: CraftTemplate?, l: Location): Boolean {
        val station = craftTemplate?.station ?: return true

        val templates = knownEntities(l).templates()
        return station in templates
    }

    private fun isAnyResourceAvailable(craftTemplate: CraftTemplate?, l: Location): Boolean {
        val ms = craftTemplate?.resources ?: return true

        val templates = knownEntities(l).templates()
        return ms.intersect(templates).isNotEmpty()
    }

    private fun isResourceAvailable(entityTemplate: EntityTemplate?, l: Location): Boolean {
        entityTemplate ?: return true

        val templates = knownEntities(l).templates()
        return entityTemplate in templates
    }


    private fun knownEntities(l: Location) = l.content.entities.all()
        .knownByPlayer(l.players.human)

    private fun taskInteraction() = getGame().interaction().taskInteraction

    private fun targetSelection() = taskInteraction().targetSelection as CraftTargetSelection

}