package com.ovle.rl.view.game.render.effect

import com.ovle.rl.view.game.render.BaseSprite


abstract class SpriteEffect {
    var effectsTime: Double = 0.0

    open val timed: Boolean = false

    abstract val totalLength: Double

    abstract fun process(sprite: BaseSprite, time: Double)
    abstract fun clear(sprite: BaseSprite)
    open fun onPositionChanged(sprite: BaseSprite) {}
}