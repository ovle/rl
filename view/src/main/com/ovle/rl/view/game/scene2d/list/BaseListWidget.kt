package com.ovle.rl.view.game.scene2d.list

import com.ovle.rl.GetGame
import com.ovle.rl.model.game.time.AbsoluteTimeChangedEvent
import com.ovle.rl.view.game.scene2d.BaseWidget
import com.ovle.utils.event.EventBus.subscribe

/**
 *
 */
abstract class BaseListWidget(protected val getGame: GetGame): BaseWidget() {

    override fun subscribe() {
        subscribe<AbsoluteTimeChangedEvent>(subscriber = this) { onTimeChange() }
    }

    open fun onTimeChange() { }
}
