package com.ovle.rl.view.game.scene2d

//import com.badlogic.gdx.scenes.scene2d.ui.Label
//import com.badlogic.gdx.utils.Align
//import com.ovle.rl.GetGame
//import com.ovle.rl.model.game.resource.ResourceInfo
//import com.ovle.rl.model.util.location
//import com.ovle.rl.view.game.scene2d.list.BaseListWidget
//import ktx.scene2d.KWidget
//import ktx.scene2d.label
//import ktx.scene2d.table
//
//class ResourcesWidget(getGame: GetGame): BaseListWidget(getGame) {
//
//    private val amountLabels = mutableMapOf<ResourceType, Label>()
//
//
//    override fun <S> addActorIntr(container: KWidget<S>) = container.table {
//        groups().forEach { rtg ->
//            row().growX()
//            rtg.forEach { rt ->
//                label(rt.shortcut) { _ ->
//                    setAlignment(Align.center)
//                }
//
//                label("0") {
//                    setAlignment(Align.center)
//                }.also { l -> amountLabels[rt] = l }
//            }
//        }
//    }
//
//    override fun onTimeChange() {
//        updateItems()
//    }
//
//
//    private fun updateItems() {
//        val items = items()
//        items.forEach {
//            amountLabels[it.type]!!.setText(it.quantity)
//        }
//    }
//
//    private fun items(): Array<ResourceInfo> {
//        val location = getGame().location()
//        val resources = location.player.resources
//
//        return ResourceType.values()
//            .map {
//                resources.singleOrNull { r -> r.type == it }
//                    ?: ResourceInfo(it, 0)
//            }
//            .toTypedArray()
//    }
//
//    private fun groups() = ResourceType.values().toList().chunked(2)
//}