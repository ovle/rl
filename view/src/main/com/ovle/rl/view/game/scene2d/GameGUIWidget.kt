package com.ovle.rl.view.game.scene2d

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Value
import com.badlogic.gdx.utils.Align
import com.ovle.rl.GetGame
import com.ovle.rl.model.game.game.GameLogItem
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.time.*
import com.ovle.rl.model.util.location
import com.ovle.rl.screen.ScreenManager
import com.ovle.rl.screen.game.EntityDetailsPayload
import com.ovle.rl.screen.game.EntityDetailsScreen
import com.ovle.rl.screen.game.GameMenuScreen
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.rl.view.game.scene2d.tabs.GameTabsWidget
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.*


class GameGUIWidget(
    private val screenManager: ScreenManager,
    private val getGame: GetGame,
    playerSkillTemplates: Collection<PlayerSkillTemplate>,
    taskTemplates: Collection<TaskTemplate>,
    entityViewTemplates: Collection<EntityViewTemplate>,
    areaViewTemplates: Collection<AreaViewTemplate>,
    taskViewTemplates: Collection<TaskViewTemplate>,
    gameLog: List<GameLogItem>,
): BaseWidget() {

    private lateinit var locationInfoLbl: Label
    private lateinit var pauseBtn: Button
    private lateinit var timeInfoLbl: Label

    private val cursorInfoWidget = CursorInfoWidget(getGame)
    private val gameTabsWidget = GameTabsWidget(
        playerSkillTemplates, taskTemplates, entityViewTemplates, areaViewTemplates, taskViewTemplates, getGame
    )
    private val gameLogWidget = GameLogWidget(gameLog)
    private val headerButtonsWidget = HeaderButtonsWidget(getGame)

    private val columnsSizes = arrayOf(0.75f, 0.25f)
    private val timeInfoHelper = TimeInfoHelper()

    override fun subscribe() {
        subscribe<AbsoluteTimeChangedEvent>(this) { onTimeChangedEvent() }

        subscribe<ToggleGameMainMenuCommand>(this) { onToggleGameMainMenuCommand() }
        subscribe<ShowEntityDetailsCommand>(this) { onShowEntityDetailsCommand(it.entity) }
    }


    override fun children() = listOf(
        cursorInfoWidget, gameTabsWidget, gameLogWidget, headerButtonsWidget
    )

    private fun onTimeChangedEvent() {
        locationInfoLbl.setText(locationInfo())
        timeInfoLbl.setText(timeInfo())

        val paused = isPaused()
        if (pauseBtn.isChecked != paused) {
            pauseBtn.toggle()
        }
    }

    private fun onToggleGameMainMenuCommand() {
        showGameMenu()
    }

    private fun onShowEntityDetailsCommand(entity: Entity) {
        showEntityDetails(entity)
    }


    override fun <S> addActorIntr(container: KWidget<S>) = container.table container@ {
        setFillParent(true)
        row().growY()

        table main@{ mc ->
            mc.width(Value.percentWidth(columnsSizes[0], this@container)).growX()

            row()
            table header@{ hc ->
                hc.growX()
                textButton(text = "Menu") { c ->
                    c.width(150.0f).height(BIG_BUTTON_HEIGHT)
                    onClick { onMenuBtnClick() }
                }
                stack {
                    it.grow()
                    bgImageDark()
                    locationInfoLbl = label("") {
                        setAlignment(Align.center)
                    }
                }

                row()
                pauseBtn = textButton(text = "Pause", "toggle") { c ->
                    c.width(150.0f).height(BIG_BUTTON_HEIGHT)
                    onClick { onPauseBtnClick() }
                }
                stack {
                    it.grow()
                    bgImageDark()
                    timeInfoLbl = label("") {
                        setAlignment(Align.center)
                    }
                }
            }

            rowOfHeight(0.025f, this@main)
            headerButtonsWidget.addActor(this@main)

            row()
            verticalGroup { c -> c.growY() }

            row().growX()
            stack cursorInfo@{
                bgImageDark()
                cursorInfoWidget.addActor(this@cursorInfo)
            }
        }

        stack tabs@ {
            it.width(Value.percentWidth(columnsSizes[1], this@container)).growX()

            bgImage()
            gameTabsWidget.addActor(this@tabs)
        }
    }

    private fun onMenuBtnClick() {
        showGameMenu()
    }

    private fun onPauseBtnClick() {
        togglePause()
    }

    private fun showGameMenu() {
        screenManager.pushScreen(GameMenuScreen::class.java)
    }

    private fun showEntityDetails(entity: Entity) {
        val payload = EntityDetailsPayload(entity)
        screenManager.pushScreen(EntityDetailsScreen::class.java, payload)
    }

    private fun locationInfo(): String {
        val location = getGame().location()
        return location.name
    }

    private fun timeInfo(): String {
        val globalTime = getGame().globalTime()
        return timeInfoHelper.timeInfo(globalTime)
    }

    private fun isPaused() = getGame().globalTime().paused

    private fun togglePause() {
        send(ToggleGamePauseCommand())
        pauseBtn.toggle()
    }
}