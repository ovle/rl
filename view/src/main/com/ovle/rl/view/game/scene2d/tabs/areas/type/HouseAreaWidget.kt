package com.ovle.rl.view.game.scene2d.tabs.areas.type

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.scenes.scene2d.Actor
import com.ovle.rl.GetGame
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.social.personalNameOrNull
import com.ovle.rl.model.util.excludeKeeper
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.movables
import com.ovle.rl.model.util.ofPlayer
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import ktx.actors.onClick
import ktx.scene2d.*


class HouseAreaWidget(getGame: GetGame): BaseListWidget(getGame) {

    companion object {
        private const val BTN_GROUP_SIZE = 3
    }

    private val templateButtons = mutableMapOf<Entity, KTextButton>()


    override fun subscribe() {
        super.subscribe()

        onSelectHostClick(null)
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        return container.table {
            row().expandX()
            playerCreatures()
            row()
            verticalGroup { it.growY().padTop(10.0f) }
        }
    }


    private fun KTableWidget.playerCreatures() {
        buttonGroup(0, 1) {
            it.fill().padTop(5.0f)
            playerEntities().chunked(BTN_GROUP_SIZE).forEach { btnRow ->
                row()
                btnRow.forEach { e ->
                    val name = e.personalNameOrNull()?.take(3) ?: "-"
                    textButton(name, style = "toggle") {
                        onClick {
                            if (!isDisabled) {
                                val newTemplate = if (isChecked) e else null
                                onSelectHostClick(newTemplate)
                            }
                        }
//                        onEnter { updateTemplateInfo(e) }
//                        onExit {
//                            val p = interaction().params
//                            if (p !is AreaParams.Rest) return@onExit
//                            updateTemplateInfo(p.template)
//                        }

                        userObject = e
                        templateButtons[e] = this
                    }
                }
            }
        }
    }

    private fun onSelectHostClick(host: Entity?) {
        val interaction = interaction()
        val areaParams = AreaParams.House(host)
        val selectedArea = interaction.selectedArea

        selectedArea?.params = areaParams
        interaction.params = areaParams
    }

//    private fun updateTemplateInfo(template: Entity?) {
//        cropHintLabel.setText(template?.crop?.name)
//    }

    private fun playerEntities(): Collection<Entity> {
        val location = getGame().location()
        val player = location.players.human

        return location
            .content.entities.all()
            .ofPlayer(player)
            .movables()
            .excludeKeeper()
    }

    private fun interaction() = getGame().interaction().areaInteraction

}