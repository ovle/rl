package com.ovle.rl.view.game.render

import com.badlogic.gdx.math.Vector2
import com.ovle.rl.view.game.render.effect.SpriteEffect

data class SpriteText(
    val text: String,
    val point: Vector2,
    val source: SpriteEffect
)