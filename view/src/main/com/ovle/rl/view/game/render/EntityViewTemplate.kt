package com.ovle.rl.view.game.render

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.StateToRegionMap
import com.ovle.rl.model.template.entity.EntityTemplate

data class EntityViewTemplate(
    val entity: EntityTemplate? = null,
    val requireText: String? = null,
    val isBig: Boolean = false,
    val isTinted: Boolean = false,
    val sprite: Map<String, GridPoint2>? = null,
    val animation: Map<String, List<GridPoint2>>? = null,
    val portrait: Collection<GridPoint2>? = null,

    var regionsByState: StateToRegionMap? = null
)
