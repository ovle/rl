package com.ovle.rl.view.game.render.tileMap

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.ovle.rl.TileArray
import com.ovle.rl.interaction.playerView.PlayerViewConfig
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.view.DECOR_LAYER_NAME
import com.ovle.rl.view.MASK_LAYER_NAME
import com.ovle.rl.view.TILE_LAYER_NAME
import com.ovle.rl.view.TILE_SIZE
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.view.TextureRegions

class LocationTiledMapFactory(
    private val cellFactory: TiledMapCellFactory,
    private val areaViewTemplates: Collection<AreaViewTemplate>,
) {

    fun tiledMap(
        location: Location,
        textureRegions: TextureRegions,
        tileSize: Int = TILE_SIZE,
        viewConfig: PlayerViewConfig? = null
    ): TiledMap {
        val content = location.content
        val player = location.players.human
        val tiles = content.tiles
        val result = TiledMap()

        with (result.layers) {
            add(mainMapLayer(tiles, textureRegions, tileSize))
            add(areasTileLayer(tiles, textureRegions, tileSize, location))
            add(decorMapLayer(tiles, textureRegions, tileSize))

            val isNeedAddVisibilityMask = viewConfig != null
            if (isNeedAddVisibilityMask) {
                add(visibilityMapLayer(tiles, textureRegions, tileSize, location, viewConfig!!))
            }
        }

        return result
    }

    private fun areasTileLayer(
        tiles: Array2d<TileTemplate>,
        textureRegions: Array<Array<TextureRegion>>,
        tileSize: Int,
        location: Location
    ): MapLayer {
        return AreasTileLayer(
            tiles.size, tiles.size, tileSize, tileSize,
            areaViewTemplates, textureRegions, cellFactory, location
        )
    }

    private fun mainMapLayer(
        tiles: TileArray, textureRegions: TextureRegions, tileSize: Int
    ): MapLayer {
        val result = TiledMapTileLayer(tiles.size, tiles.size, tileSize, tileSize)

        for (x in 0 until tiles.size) {
            for (y in 0 until tiles.size) {
                val tile = tiles[x, y]
                val point = point(x, y)
                val cell = cellFactory.cell(tile, point, textureRegions)

                result.setCell(x, y, cell)
            }
        }

        result.name = TILE_LAYER_NAME
        return result
    }

    private fun visibilityMapLayer(
        tiles: TileArray,
        textureRegions: TextureRegions,
        tileSize: Int,
        location: Location,
        viewConfig: PlayerViewConfig
    ): PlayerViewMaskTileLayer {
        val unknownCellMask = cellFactory.unknownCellMask(textureRegions)
        val invisibleCellMask = cellFactory.invisibleCellMask(textureRegions)

        val result = PlayerViewMaskTileLayer(
            tiles.size, tiles.size, tileSize, tileSize,
            location, viewConfig,
            unknownCellMask, invisibleCellMask
        )

        result.name = MASK_LAYER_NAME
        return result
    }

    private fun decorMapLayer(
        tiles: TileArray, textureRegions: TextureRegions, tileSize: Int
    ): MapLayer {
        val downMask = cellFactory.wallCellMask(textureRegions, isFloorDown = true)
        val rightMask = cellFactory.wallCellMask(textureRegions, isFloorRight = true)
        val downRightMask = cellFactory.wallCellMask(textureRegions, isFloorDown = true, isFloorRight = true)
        val downRightDiagMask = cellFactory.wallCellMask(textureRegions, isFloorDiag = true)

        val result = DecorMaskTileLayer(
            tiles, tileSize, tileSize,
            downMask, rightMask, downRightMask, downRightDiagMask
        )

        result.name = DECOR_LAYER_NAME
        return result
    }
}