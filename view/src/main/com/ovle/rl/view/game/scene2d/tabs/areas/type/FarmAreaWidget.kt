package com.ovle.rl.view.game.scene2d.tabs.areas.type

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.ovle.rl.GetGame
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.markError
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import ktx.actors.onClick
import ktx.actors.onEnter
import ktx.actors.onExit
import ktx.scene2d.*


class FarmAreaWidget(
    getGame: GetGame,
    private val templates: Collection<FarmTemplate>
): BaseListWidget(getGame) {

    companion object {
        private const val BTN_GROUP_SIZE = 3
    }

    private lateinit var cropLabel: Label
    private lateinit var resourceLabel: Label
    private lateinit var gatherLabel: Label

    private val templateButtons = mutableMapOf<FarmTemplate, KTextButton>()


    override fun subscribe() {
        super.subscribe()

        onSelectFarmTemplateClick(null)
    }

    override fun onTimeChange() {
        super.onTimeChange()

        val availableTemplates = templates //todo
        val unavailableTemplates = templates - availableTemplates.toSet()

        availableTemplates.forEach { templateButtons[it]!!.isDisabled = false }
        unavailableTemplates.forEach { templateButtons[it]!!.isDisabled = true }
        //todo use crop conditions

        syncSelections()
    }


    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        return container.table {
            row().expandX()
            farmTemplates()
            row()
            cropLabel = label("") { }
            row()
            resourceLabel = label("") { }
            row()
            gatherLabel = label("") { }
            row()
            verticalGroup { it.growY().padTop(10.0f) }
        }
    }


    private fun KTableWidget.farmTemplates() {
        buttonGroup(0, 1) {
            it.fill().padTop(5.0f)
            templates.chunked(BTN_GROUP_SIZE).forEach { btnRow ->
                row()
                btnRow.forEach { ft ->
                    val name = ft.crop.shortName
                    textButton(name, style = "toggle") { bc ->
                        bc.growX()
                        onClick {
                            if (!isDisabled) {
                                val newTemplate = if (isChecked) ft else null
                                onSelectFarmTemplateClick(newTemplate)
                            }
                        }
                        onEnter { updateTemplateInfo(ft) }
                        onExit {
                            val p = interaction().params
                            if (p !is AreaParams.Farm) return@onExit
                            updateTemplateInfo(p.template)
                        }

                        userObject = ft
                        templateButtons[ft] = this
                    }
                }
            }
        }
    }

    private fun onSelectFarmTemplateClick(template: FarmTemplate?) {
        val interaction = interaction()
        val areaParams = AreaParams.Farm(template)
        val selectedArea = interaction.selectedArea

        selectedArea?.params = areaParams
        interaction.params = areaParams
    }


    private fun updateTemplateInfo(template: FarmTemplate?) {
        val location = getGame().location()

        cropLabel.setText(template?.crop?.name)

        var resourceText = template?.resource?.name?.let { "resource: $it" }
        val isResourceAvailable = template?.isResourceAvailable(location) != false
        if (!isResourceAvailable) { resourceText = resourceText?.markError() }
        resourceLabel.setText(resourceText)

        val gatherText = template?.gatherResource?.name?.let { "produce: $it" }
        gatherLabel.setText(gatherText)
    }

    private fun syncSelections() {
        val selectedTemplate = externalSelectedItem()
        val selectedTemplateBtn = templateButtons[selectedTemplate]

        templateButtons.values.forEach { it.isChecked = false }
        if (selectedTemplate != null) {
            selectedTemplateBtn!!.isChecked = true
        }
    }

    private fun externalSelectedItem(): FarmTemplate? {
        val interaction = interaction()
        val params = (interaction.selectedArea?.params ?: interaction.params)
        if (params !is AreaParams.Farm?) return null

        params?.let { interaction.params = it }
        return params?.template
    }

    private fun interaction() = getGame().interaction().areaInteraction

}