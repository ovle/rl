package com.ovle.rl.view.game.scene2d.tabs.areas

import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.game.dto.location.LocationContent

class AreaInfoHelper {
    companion object {
        const val SEPARATOR = "------------------"
    }

    fun areaInfo(area: AreaInfo, content: LocationContent): String {
        val status = if (area.isValid) "V" else "X"
        val name = area.name
        val info = area.params.info(area, content)

        return "$status $name\n" +
            "$info\n" +
            SEPARATOR
    }
}