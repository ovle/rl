package com.ovle.rl.view.game.render

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2
import com.ovle.rl.controls.FocusEntityCommand
import com.ovle.rl.controls.FocusTileCommand
import com.ovle.rl.controls.gdx.*
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.GameStartedEvent
import com.ovle.rl.model.game.space.EntityChangedPositionEvent
import com.ovle.rl.model.game.space.position

import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.ofPlayer
import com.ovle.rl.util.focusedEntity
import com.ovle.rl.util.interaction
import com.ovle.rl.view.*
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.math.point.point
import ktx.math.vec3


class CameraSystem(
    private val camera: OrthographicCamera
): BaseSystem() {

    private val screenCenter = vec3(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)
    private val focusZoom = 0.25f

    override fun subscribe() {
        subscribe<GameStartedEvent>(this) { onGameStartedEvent() }
        subscribe<CameraScaleIncCommand>(this) { onScaleChange(0.1f) }
        subscribe<CameraScaleDecCommand>(this) { onScaleChange(-0.1f) }
        subscribe<CameraMoveCommand>(this) { onMove(it.diff) }
        subscribe<CameraScrollCommand>(this) { onScaleChange(-it.amount.toFloat() * SCALE_SCROLL_COEFF) }

        subscribe<EntityChangedPositionEvent>(this) { onEntityChangedPositionEvent(it.entity) }

        subscribe<FocusEntityCommand>(this) { onEntityFocusCommand(it.entity) }
        subscribe<FocusTileCommand>(this) { onFocusTileCommand(it.point) }
    }

    private fun onGameStartedEvent() {
        val game = game()
        val location = game.location()
        val interaction = game.interaction()
        val content = location.content
        val size = content.tiles.size
        val tilesCenter = point(size / 2, size / 2)
        val playerEntities = content.entities.all()
            .ofPlayer(location.players.human)

        val focusedEntity = interaction.entityInteraction.focusedEntity
            ?: playerEntities.firstOrNull()
        val focusPoint = focusedEntity?.position() ?: tilesCenter

        focusCamera(focusPoint)
    }

    private fun onEntityFocusCommand(entity: Entity) {
        val interaction = game().interaction()
        with (interaction.entityInteraction) {
            focusedEntity = entity
            focusedEntity?.let { focusCamera(it) }
        }
    }

    private fun onFocusTileCommand(point: GridPoint2) {
        focusCamera(point)
    }

    private fun onEntityChangedPositionEvent(entity: Entity) {
        if (entity != game().focusedEntity()) return

        focusCamera(entity)
    }

    private fun onScaleChange(diff: Float) {
        val c = camera
        c.zoom -= diff
        c.update()
    }

    private fun onMove(amount: Vector2) {
        val game = game()
        val focusedEntity = game.focusedEntity()
        if (focusedEntity != null) return

        camera.position.add(amount.x * CAMERA_MOVE_COEFF, amount.y * CAMERA_MOVE_COEFF, 0.0f)
        camera.update()
    }

    private fun focusCamera(entity: Entity) {
        val position = entity.position()
        focusCamera(position)
    }

    private fun focusCamera(point: GridPoint2) {
        val focusedWorldPoint = vec3(
            point.x * TILE_SIZE.toFloat(),
            point.y * TILE_SIZE.toFloat()
        )

        if (focusedWorldPoint.epsilonEquals(camera.position)) return
        val c = camera
        c.position.set(focusedWorldPoint.x, focusedWorldPoint.y, 0.0f)
        c.zoom = focusZoom
        camera.update()
    }
}