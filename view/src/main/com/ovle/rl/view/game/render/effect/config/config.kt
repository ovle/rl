package com.ovle.rl.view.game.render.effect.config

import com.ovle.rl.model.game.skill.dto.SkillUsage
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.MIN_ANIMATION_FRAME_LENGTH
import com.ovle.rl.model.util.markDamage
import com.ovle.rl.model.util.markHeal
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.effect.SpriteEffect
import com.ovle.rl.view.game.render.effect.SpriteEffectKey
import com.ovle.utils.gdx.math.Direction
import ktx.math.vec2


fun animationSpriteEffect(viewTemplate: EntityViewTemplate, key: SpriteEffectKey): SpriteEffect? {
    val animationConfig = viewTemplate.animation ?: return null
    val keyName = key.name.lowercase()
    val idleAnimation = animationConfig[keyName] ?: return null

    return FrameAnimationSpriteEffect(
        frameLength = 0.25,
        frames = idleAnimation.toTypedArray()
    )
}


fun defaultGetSkillObjectSpriteEffect(skillUsage: SkillUsage): SpriteEffect {
    return BlinkSpriteEffect(
        frameLength = MIN_ANIMATION_FRAME_LENGTH
    )
}

fun defaultGetSkillSubjectSpriteEffect(skillUsage: SkillUsage.Entity): SpriteEffect {
    val target = skillUsage.target
    val source = skillUsage.source

    val sourcePosition = source.position()
    val targetPosition = target!!.position()

    val direction = if (sourcePosition.x != targetPosition.x) Direction.H else Direction.V  //todo both?
    val isPositive = if (direction == Direction.V) sourcePosition.y < targetPosition.y
        else sourcePosition.x < targetPosition.x

    val frames = arrayOf(0, 1, 2, 3, 4, 2)
        .map { if (isPositive) it else -it }
        .toTypedArray()

    return OffsetSpriteEffect(
        direction = direction,
        frames = frames,
        frameLength = 0.1
    )
}

fun moveSpriteEffect() = OffsetSpriteEffect(
    direction = Direction.V,
    frames = arrayOf(0, 1, 2, 1),
    frameLength = MIN_ANIMATION_FRAME_LENGTH
)

fun actionSpriteEffect() = OffsetSpriteEffect(
    direction = Direction.H,
    frames = arrayOf(0, 1, 0, -1, 0, 1, 0, -1),
    frameLength = MIN_ANIMATION_FRAME_LENGTH
)

fun damageSpriteEffect(amount: Int) = TextSpriteEffect(
    text = "-$amount".markDamage(),
    points = arrayOf(
        vec2(0.0f, 10.0f),
        vec2(0.0f, 12.0f),
        vec2(0.0f, 14.0f),
        vec2(0.0f, 14.0f),
    ),
    totalLength = MIN_ANIMATION_FRAME_LENGTH * 4
)

fun healSpriteEffect(amount: Int) = TextSpriteEffect(
    text = "+$amount".markHeal(),
    points = arrayOf(
        vec2(0.0f, 10.0f),
        vec2(0.0f, 12.0f),
        vec2(0.0f, 14.0f),
        vec2(0.0f, 14.0f),
    ),
    totalLength = MIN_ANIMATION_FRAME_LENGTH * 4
)

fun sleepSpriteEffect() = TextSpriteEffect(
    text = "Z",
    points = arrayOf(
        vec2(0.0f, 10.0f),
        vec2(0.0f, 12.0f),
        vec2(0.0f, 14.0f),
        vec2(0.0f, 14.0f),
    ),
    timed = false,
    totalLength = MIN_ANIMATION_FRAME_LENGTH * 2
)