package com.ovle.rl.view.game.render

const val DEFAULT_SPRITE_KEY = "default"

const val STAGE_VIEWPORT_TAG = "stageViewportTag"
const val VIEWPORT_TAG = "viewportTag"
const val WORLD_MAP_FACTORY_TAG = "worldMapFactory"