package com.ovle.rl.view.game.scene2d.list

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener


class DelegatingList<T>(
    skin: Skin, styleName: String,
    private val getItemInfo: (T) -> String,
    private val onSelectionChange: (T?) -> Unit
): CustomScene2dList<T>(skin, styleName) {

    private val changeListener = object : ChangeListener() {
        override fun changed(event: ChangeEvent, actor: Actor) {
            onSelectionChange(this@DelegatingList.selected)
        }
    }

    init {
        addListener(changeListener)
    }


    override fun toString(item: T): String {
        return getItemInfo(item)
    }
}