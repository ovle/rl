package com.ovle.rl.view.game.scene2d.tabs.view

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.Components.ai
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.core.templateName
import com.ovle.rl.model.game.life.Components.health
import com.ovle.rl.model.game.life.Components.hunger
import com.ovle.rl.model.game.life.hp
import com.ovle.rl.model.game.life.isDead
import com.ovle.rl.model.game.life.maxHp
import com.ovle.rl.model.game.social.personalNameOrNull
import ktx.ashley.get
import ktx.ashley.has

class EntityInfoHelper {

    companion object {
        const val SEPARATOR = "------------------"
    }

    fun entityInfo(entity: Entity): String {
        if (!entity.isExists()) return ""

        val mainInfo = mainInfo(entity)
        val secondaryInfo = secondaryInfo(entity)

        return "$mainInfo \n" +
            "$secondaryInfo \n" +
            SEPARATOR
    }

    private fun mainInfo(entity: Entity): String {
        val name = entity.personalNameOrNull() ?: entity.templateName()
        val aiState = when {
            entity.isDead() -> "dead"
            entity.has(ai) -> entity[ai]!!.rootTask?.name ?: "idle"
            else -> "----"
        }

        return "$name ($aiState)"
    }

    private fun secondaryInfo(entity: Entity): String {
        //todo morale
        val healthInfo = if (entity.has(health)) {
            "hp:${entity.hp()}/${entity.maxHp()}"
        } else null
        val hungerInfo = if (entity.has(hunger)) {
            val hu = entity[hunger]!!
            "hu:${hu.hunger}/${hu.maxHunger}"
        } else null

        val result = listOfNotNull(
            healthInfo,
            hungerInfo,
        )

        return if (result.isEmpty()) "" else result.joinToString(" ")
    }
}