package com.ovle.rl.view.game.render.gui

/*

class RenderInfluenceHelper(
    private val batch: Batch,
    private val guiSprites: GUISprites
) {

    fun drawInfluenceArea(location: Location) {
        val tiles = location.content.tiles
        val influence = location.player.influence
        val influencedPoints = tiles.points.filter { influence.get(it) }

        influencedPoints.forEach {
            batch.draw(it, guiSprites.influencedTileSprite)
        }

        drawBorders(influencedPoints)
    }

    private fun drawBorders(points: Collection<GridPoint2>) {
        with(points) {
            innerBorders(Direction.H, -1).forEach {
                batch.draw(it, guiSprites.areaLeftSprite)
            }
            innerBorders(Direction.H, 1).forEach {
                batch.draw(it, guiSprites.areaRightSprite)
            }
            innerBorders(Direction.V, 1).forEach {
                batch.draw(it, guiSprites.areaTopSprite)
            }
            innerBorders(Direction.V, -1).forEach {
                batch.draw(it, guiSprites.areaBottomSprite)
            }
        }
    }
}*/
