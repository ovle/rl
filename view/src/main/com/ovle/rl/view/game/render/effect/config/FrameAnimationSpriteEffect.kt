package com.ovle.rl.view.game.render.effect.config

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.view.game.render.BaseSprite
import com.ovle.rl.view.game.render.effect.SpriteEffect
import com.ovle.rl.view.SPRITE_SIZE
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import com.ovle.utils.gdx.view.textureRegion


class FrameAnimationSpriteEffect(
    private val frameLength: Double,
    private val frames: Array<GridPoint2>,
): SpriteEffect() {

    override val totalLength = frameLength * frames.size

    private var initialRegion: TextureRegion? = null


    override fun process(sprite: BaseSprite, time: Double) {
        val frameIndex = (time / frameLength).toInt()

        if (initialRegion == null) {
            initialRegion = TextureRegion(sprite)
        }

        sprite.setRegion(region(frameIndex, sprite.texture))
    }

    override fun clear(sprite: BaseSprite) {
        initialRegion?.let {
            sprite.setRegion(it)
        }
    }

    private fun region(frameIndex: Int, texture: Texture): TextureRegion {
        val (rx, ry) = frames[frameIndex]
        val size = 1 //todo
        return textureRegion(texture, rx, ry, SPRITE_SIZE.toInt(), size)
    }
}