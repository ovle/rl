package com.ovle.rl.view.game.scene2d

import com.badlogic.gdx.graphics.g2d.NinePatch
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Value
import ktx.scene2d.*
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

fun <S> KWidget<S>.bgImage(style: String = "window"): Image {
    val tr = Scene2DSkin.defaultSkin.getRegion(style)
    val np = NinePatch(tr, 3, 3, 3, 3)
    return this.image(np)
}

fun <S> KWidget<S>.bgImageDark(style: String = "black-bg"): Image {
    val tr = Scene2DSkin.defaultSkin.getRegion(style)
    val np = NinePatch(tr, 0, 0, 0, 0)
    return this.image(np)
}

@Scene2dDsl
@OptIn(ExperimentalContracts::class)
inline fun <S> KWidget<S>.stack(
    init: KStack.(S) -> Unit = {}
): KStack {
    contract { callsInPlace(init, InvocationKind.EXACTLY_ONCE) }
    return actor(KStack(), init)
}

fun KTableWidget.rowOfHeight(height: Float, parent: KTableWidget) {
    row().height(Value.percentHeight(height, parent)).growX()
}