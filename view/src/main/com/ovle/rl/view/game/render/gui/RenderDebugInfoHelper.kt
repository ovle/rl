package com.ovle.rl.view.game.render.gui

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.ovle.rl.GameEntity
import com.ovle.rl.interaction.playerView.PlayerViewMode
import com.ovle.rl.interaction.playerView.PlayerViewMode.*
import com.ovle.rl.model.game.perception.Components.perception
import com.ovle.rl.model.game.space.accessibility.Accessibility.Companion.Key
import com.ovle.rl.model.game.space.movePath
import com.ovle.rl.model.game.space.positionOrNull
import com.ovle.rl.model.util.location
import com.ovle.rl.view.game.render.draw
import ktx.ashley.get

class RenderDebugInfoHelper(
    private val batch: Batch,
    private val guiSprites: GUISprites
) {

    fun drawDebugInfo(entity: Entity?, game: GameEntity, viewModes: Collection<PlayerViewMode>) {
        val color = batch.color.cpy()
        val drawColor = Color(color.r, color.g, color.b, 0.5f)
        batch.color = drawColor

        draw(viewModes, entity, game)

        batch.color = color
    }

    private fun draw(
        viewModes: Collection<PlayerViewMode>, entity: Entity?, game: GameEntity
    ) {
        val location = game.location()
        val content = location.content
        val tiles = content.tiles

        if (MOVE_MAP in viewModes) {
            val movePath = entity?.movePath()
            movePath?.path?.forEach {
                batch.draw(it, guiSprites.moveGoalTileSprite)
            }
        }

        if (ACCESSIBILITY_MAP in viewModes) {
            val check = content.accessibility::isAccessible
            tiles.points.forEach {
                val position = entity?.positionOrNull()
                if (position != null && !check(Key(entity), position, it)) {
                    batch.draw(it, guiSprites.unavailableTileSprite)
                }
            }
        }

        if (FOV in viewModes) {
            val fov = entity?.get(perception)?.fov
            fov?.let { f ->
                f.filter { tiles.isValid(it) }
                    .forEach {
                        batch.draw(it, guiSprites.unavailableTileSprite)
                    }
            }
        }
    }

//    private fun drawMap(dijkstraMap: DijkstraMap?) {
//        if (dijkstraMap == null) return
//
//        val points = dijkstraMap.data.inaccessiblePoints()
//        points.forEach {
//            batch.draw(it, guiSprites.unavailableTileSprite)
//        }
//
//        val goals = dijkstraMap.goals
//        goals.forEach {
//            batch.draw(it, guiSprites.moveGoalTileSprite)
//        }
//    }
}