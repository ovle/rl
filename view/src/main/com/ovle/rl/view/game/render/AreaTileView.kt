package com.ovle.rl.view.game.render

import com.badlogic.gdx.math.GridPoint2

data class AreaTileView(
    val floor: GridPoint2? = null,
    val wall: GridPoint2? = null,
)