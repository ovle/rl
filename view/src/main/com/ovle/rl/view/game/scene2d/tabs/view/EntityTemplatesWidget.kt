package com.ovle.rl.view.game.scene2d.tabs.view

import com.badlogic.gdx.scenes.scene2d.Actor
import com.ovle.rl.GetGame
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.knownByPlayer
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.templates
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import com.ovle.rl.view.game.scene2d.list.ListItemsWidget
import ktx.scene2d.KWidget
import ktx.scene2d.table


class EntityTemplatesWidget(
    private val entityTemplateInfoHelper: EntityTemplateInfoHelper,
    getGame: GetGame
): BaseListWidget(getGame) {

    private lateinit var itemsWidget: ListItemsWidget<EntityTemplate>


    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        itemsWidget = ListItemsWidget(
            getItemInfo = {
                entityTemplateInfoHelper.entityTemplateInfo(it, emptyList()) //todo
            },
            onSelectionChange = { },
            itemHeightCoef = 3.5f
        )

        return container.table {
            itemsWidget.addActor(this)
        }
    }

    override fun onTimeChange() {
        //todo not changed?
        val location = getGame().location()
        val entities = location.content.entities
        //todo filter / group
        val resources = entities.all()
            .knownByPlayer(location.players.human)
            .templates()

        itemsWidget.setItems(resources.toTypedArray())
    }
}
