package com.ovle.rl.view.game.render

import com.badlogic.ashley.core.Entity
import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.core.entity.with
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.game.EntityLoadedEvent
import com.ovle.rl.model.game.life.*
import com.ovle.rl.view.game.render.Components.render
import com.ovle.rl.view.game.render.effect.SpriteEffectKey.*
import com.ovle.rl.model.game.skill.EntityFinishUseSkillEvent
import com.ovle.rl.model.game.skill.EntityStartUseSkillEvent
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillUsage
import com.ovle.rl.model.game.space.EntityStartedMovementEvent
import com.ovle.rl.model.game.space.EntityStoppedMovementEvent
import com.ovle.rl.model.game.time.ticksToTime

import com.ovle.rl.model.util.location
import com.ovle.rl.view.game.render.effect.SkillViewTemplate
import com.ovle.rl.view.game.render.effect.config.*
import com.ovle.rl.view.game.render.effect.config.moveSpriteEffect
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get

class AnimationSystem(
    private val skillViews: Collection<SkillViewTemplate>,
    private val entityViewTemplates: Collection<EntityViewTemplate>,
): TimedEventSystem() {

    override val updateInterval = SystemUpdateInterval.RENDER

    override fun subscribeIntr() {
        subscribe<EntityLoadedEvent>(this) { onEntityLoadedEvent(it.entity) }

        subscribe<EntityStartedMovementEvent>(this) { onEntityStartedMoveEvent(it.entity) }
        subscribe<EntityStoppedMovementEvent>(this) { onEntityFinishedMoveEvent(it.entity) }

        subscribe<EntityStartUseSkillEvent>(this) { onEntityStartUseSkillEvent(it.skillUsage) }
        subscribe<EntityFinishUseSkillEvent>(this) { onEntityFinishUseSkillEvent(it.skillUsage) }

        subscribe<EntityTakenDamageEvent>(this) { onEntityTakenDamageEvent(it.entity, it.amount) }
        subscribe<EntityHealedEvent>(this) { onEntityHealedEvent(it.entity, it.amount) }
        subscribe<EntityDiedEvent>(this) { onEntityDiedEvent(it.entity) }
        subscribe<EntityStartSleepEvent>(this) { onEntityStartSleepEvent(it.entity) }
        subscribe<EntityAwakenEvent>(this) { onEntityAwakenEvent(it.entity) }
    }


    override fun updateTime() {
        val content = game().location().content
        val entities = content.entities.all().with(RenderComponent::class)
        entities.forEach { processEntity(it, updateInterval) }
    }

    private fun processEntity(entity: Entity, deltaTicks: Tick) {
        val renderComponent = entity[render]!!
        val sprite = renderComponent.sprite ?: return

        val deltaTime = ticksToTime(deltaTicks)
        sprite.processEffects(deltaTime)
    }

    private fun onEntityStartUseSkillEvent(skillUsage: SkillUsage.Entity) {
        val skill = skillUsage.skill
        val target = skillUsage.target
        val source = skillUsage.source
        val skillViewTemplate = skillViews.find { it.skillTemplate == skill }
        val subjectEffect = skillViewTemplate?.getSubjectSpriteEffect ?: ::defaultGetSkillSubjectSpriteEffect
        val objectEffect = skillViewTemplate?.getObjectSpriteEffect ?: ::defaultGetSkillObjectSpriteEffect

        source.spriteOrNull()?.addEffect(SKILL_SUBJECT, subjectEffect.invoke(skillUsage))
        if (target is SkillTarget.Entity) {
            target.entity.spriteOrNull()
                ?.addEffect(SKILL_OBJECT, objectEffect.invoke(skillUsage))
        }
    }

    private fun onEntityLoadedEvent(entity: Entity) {
        val viewTemplate = entityViewTemplates.firstOrNull { it.entity == entity.template() } ?: return
        val animationEffect = animationSpriteEffect(viewTemplate, IDLE) ?: return

        entity.spriteOrNull()!!.addEffect(IDLE, animationEffect)
    }

    private fun onEntityFinishUseSkillEvent(skillUsage: SkillUsage.Entity) {
        val target = skillUsage.target
        val source = skillUsage.source

        source.spriteOrNull()?.removeEffect(SKILL_SUBJECT)
        if (target is SkillTarget.Entity) {
            target.entity.spriteOrNull()
                ?.removeEffect(SKILL_OBJECT)
        }
    }

    private fun onEntityStartedMoveEvent(entity: Entity) {
        entity.spriteOrNull()?.addEffect(MOVE, moveSpriteEffect())
    }

    private fun onEntityFinishedMoveEvent(entity: Entity) {
        entity.spriteOrNull()?.removeEffect(MOVE)
    }

    private fun onEntityDiedEvent(entity: Entity) {
        entity.spriteOrNull()?.clearEffects()
    }

    private fun onEntityTakenDamageEvent(entity: Entity, amount: Int) {
        entity.spriteOrNull()?.addEffect(TAKE_DAMAGE, damageSpriteEffect(amount))
    }

    private fun onEntityHealedEvent(entity: Entity, amount: Int) {
        entity.spriteOrNull()?.addEffect(HEAL, healSpriteEffect(amount))
    }

    private fun onEntityStartSleepEvent(entity: Entity) {
        entity.spriteOrNull()?.addEffect(SLEEP, sleepSpriteEffect())
    }

    private fun onEntityAwakenEvent(entity: Entity) {
        entity.spriteOrNull()?.removeEffect(SLEEP)
    }
}