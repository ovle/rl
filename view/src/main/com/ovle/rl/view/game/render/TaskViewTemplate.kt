package com.ovle.rl.view.game.render

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.GetGame
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.view.game.render.gui.GUISprites
import com.ovle.rl.view.game.render.gui.task.RenderTaskHelper
import com.ovle.rl.view.game.render.mapper.TileToTextureMapper
import com.ovle.rl.view.game.scene2d.BaseWidget

data class TaskViewTemplate(
    val taskTemplate: TaskTemplate,
    val displayName: String,
    val icon: GridPoint2,
    val selectedIcon: GridPoint2,
    val getWidget: (GetGame) -> BaseWidget? = { _ -> null },
    val getRenderHelper: (Batch, GUISprites, TileToTextureMapper) -> RenderTaskHelper? = { _, _, _ -> null },
    val getInfo: (TaskInfo) -> String = { ti -> ti.target.positions().firstOrNull()?.toString() ?: "" },
    val keyCode: Int
)
