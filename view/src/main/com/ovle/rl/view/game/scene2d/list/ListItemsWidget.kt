package com.ovle.rl.view.game.scene2d.list

import com.badlogic.gdx.scenes.scene2d.Actor
import com.ovle.rl.view.game.scene2d.BaseWidget
import ktx.scene2d.*

/**
 *
 */
class ListItemsWidget<T>(
    getItemInfo: (T) -> String,
    private val getExternalSelectedItem: () -> T? = { null },
    private val externalSelectItem: (T?) -> Unit = { },
    onSelectionChange: (T?) -> Unit = { },
    itemHeightCoef: Float = 1.5f
): BaseWidget() {

    private var list = DelegatingList(
        Scene2DSkin.defaultSkin, "default", getItemInfo, onSelectionChange
    ).apply { this.itemHeightCoef = itemHeightCoef }

    val selectedListItem: T?
        get() = list.selected

    val items: com.badlogic.gdx.utils.Array<T>
        get() = list.items

    private var lastSelectedItem: T? = null


    override fun subscribe() {
        super.subscribe()

        with(list.selection) {
            required = false
            toggle = true
        }
    }


    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        container as KTableWidget
        container.row().expandX().expandY()

        return container.scrollPane {
            it.fill()
            actor(list) { }

            list.layout()
        }
    }


    fun setItems(items: Array<T>) {
        list.setItems(*items)
    }

    fun syncSelections() {
        val extSelectedItem = getExternalSelectedItem()
        val isExtSelectionEq = extSelectedItem == lastSelectedItem
        val isListSelectionEq = selectedListItem == lastSelectedItem
        //val isSelectedItemInList = list.items.contains(lastSelectedItem, false)

        when {
            isExtSelectionEq && isListSelectionEq -> {}
            //lastSelectedItem != null && !isSelectedItemInList -> { }
            isListSelectionEq -> {
                lastSelectedItem = extSelectedItem
                list.selected = lastSelectedItem
            }
            else -> {
                lastSelectedItem = selectedListItem
                externalSelectItem(lastSelectedItem)
            }
        }
    }
}