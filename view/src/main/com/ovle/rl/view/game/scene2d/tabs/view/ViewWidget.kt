package com.ovle.rl.view.game.scene2d.tabs.view

import com.badlogic.gdx.scenes.scene2d.Actor
import com.ovle.rl.GetGame
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.scene2d.BaseWidget
import com.ovle.rl.view.game.scene2d.list.ListGroupWidget
import com.ovle.rl.view.game.scene2d.stack
import ktx.scene2d.KStack
import ktx.scene2d.KWidget
import ktx.scene2d.table


class ViewWidget(
    getGame: GetGame,
    entityTemplateInfoHelper: EntityTemplateInfoHelper,
    entityInfoHelper: EntityInfoHelper,
    entityViewTemplates: Collection<EntityViewTemplate>,
): BaseWidget() {

    companion object {
        enum class ViewWidgetMode(val displayName: String) {
            UNITS("Units"),
            OBJECTS("Objects")
        }
    }


    private val unitsWidget = EntitiesWidget(entityInfoHelper, entityViewTemplates, getGame)
    private val entityTemplatesWidget = EntityTemplatesWidget(entityTemplateInfoHelper, getGame)

    private val widgets: Map<ViewWidgetMode, BaseWidget> = mapOf(
        ViewWidgetMode.UNITS to unitsWidget,
        ViewWidgetMode.OBJECTS to entityTemplatesWidget
    )

    private lateinit var groupsWidget: ListGroupWidget<ViewWidgetMode>
    private var controlGroup: KStack? = null


    override fun subscribe() {
        super.subscribe()

        onViewModeSelect(ViewWidgetMode.values().first())
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        val groups = ViewWidgetMode.values().toList()
        groupsWidget = ListGroupWidget(
            groups, groups.map { it.displayName }, ::onViewModeSelect, 2, false
        )

        return container.table {
            groupsWidget.addActor(this)
            row().expandX().expandY()

            controlGroup = stack {
                it.fill()
            }
        }
    }

    private fun onViewModeSelect(viewWidgetMode: ViewWidgetMode?) {
        viewWidgetMode ?: return
        val container = controlGroup ?: return

        widgets.values.forEach {
            it.removeActor()
        }

        val widget = widgets[viewWidgetMode]!!
        widget.addActor(container)

        container.layout()
    }
}