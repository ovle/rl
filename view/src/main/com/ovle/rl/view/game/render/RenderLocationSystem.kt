package com.ovle.rl.view.game.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapRenderer
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.assets.RlAssetsManager
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.LocationLoadedEvent
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.tile.TileChangedEvent
import com.ovle.rl.model.game.tile.TileTemplate

import com.ovle.rl.model.util.location
import com.ovle.rl.util.interaction
import com.ovle.rl.view.TEXTURE_TILE_SIZE
import com.ovle.rl.view.TILE_LAYER_NAME
import com.ovle.rl.view.TILE_SIZE
import com.ovle.rl.view.game.render.tileMap.TiledMapCellFactory
import com.ovle.rl.view.game.render.tileMap.LocationTiledMapFactory
import com.ovle.rl.view.game.render.tileMap.layer
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.view.PaletteManager
import com.ovle.utils.gdx.view.textureRegions


class RenderLocationSystem(
    assetsManager: RlAssetsManager,
    private val camera: OrthographicCamera,
    private val paletteManager: PaletteManager,
    private val tiledMapFactory: LocationTiledMapFactory,
    private val tiledMapCellFactory: TiledMapCellFactory,
) : BaseSystem() {

    private val textureRegions = lazy {
        textureRegions(assetsManager.gameTexture, paletteManager, TEXTURE_TILE_SIZE)
    }

    private var mapRenderer: TiledMapRenderer? = null
    private var tiledMap: TiledMap? = null


    override fun subscribe() {
        subscribe<LocationLoadedEvent>(this) { onLocationLoaded(it.location) }
        subscribe<TileChangedEvent>(this) { onTileChangedEvent(it.tile, it.position) }
    }

    override fun update(deltaTime: Float) {
        super.update(deltaTime)

        if (mapRenderer != null) {
            draw()
        }
    }

    private fun onLocationLoaded(location: Location) {
        val game = game()
        val viewConfig = game.interaction().viewConfig

        tiledMap = tiledMapFactory.tiledMap(
            location, textureRegions.value, TILE_SIZE, viewConfig
        )

        mapRenderer = OrthogonalTiledMapRenderer(tiledMap)
    }

    private fun onTileChangedEvent(tile: TileTemplate, position: GridPoint2) {
        val location = game().location()
        val grids = location.content.grids
//        val biome = TileBiome.biome(grids.value(position))

        val layer = tiledMap!!.layer(TILE_LAYER_NAME)
        val cell = tiledMapCellFactory.cell(tile, position, textureRegions.value)

        layer.setCell(position.x, position.y, cell)
    }


    private fun draw() {
        val bgColor = paletteManager.bgColor
        Gdx.gl.glClearColor(bgColor.r, bgColor.g, bgColor.b, bgColor.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        mapRenderer!!.setView(camera)
        mapRenderer!!.render()
    }
}