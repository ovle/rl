package com.ovle.rl.view.game.render

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.view.game.render.gui.*
import com.ovle.rl.view.game.render.gui.task.RenderTasksHelper
import com.ovle.rl.view.game.render.mapper.*
import com.ovle.rl.view.game.render.tileMap.TiledMapCellFactory
import com.ovle.rl.view.game.render.tileMap.LocationTiledMapFactory
import com.ovle.rl.view.game.render.tileMap.WorldTiledMapFactory
import org.kodein.di.*


val renderModule = DI.Module("render") {
    bind<TiledMapCellFactory>(LOCATION_CELL_FACTORY_TAG) with singleton {
        TiledMapCellFactory(instance(LOCATION_TILE_MAPPER_TAG))
    }
    bind<TiledMapCellFactory>(WORLD_CELL_FACTORY_TAG) with singleton {
        TiledMapCellFactory(instance(WORLD_TILE_MAPPER_TAG))
    }

    bind<LocationTiledMapFactory>(LOCATION_MAP_FACTORY_TAG) with singleton {
        LocationTiledMapFactory(instance(LOCATION_CELL_FACTORY_TAG), instance())
    }
    bind<WorldTiledMapFactory>(WORLD_MAP_FACTORY_TAG) with singleton {
        WorldTiledMapFactory(instance(WORLD_CELL_FACTORY_TAG))
    }

    bind<GUISprites>() with singleton {
        GUISprites(instance(), instance())
    }
    bind<RenderCursorHelper>() with singleton {
        RenderCursorHelper(instance(), instance(), instance(), instance())
    }
    bind<RenderTasksHelper>() with singleton {
        RenderTasksHelper(instance(), instance(), instance(), instance(LOCATION_TILE_MAPPER_TAG))
    }
    bind<RenderDebugInfoHelper>() with singleton {
        RenderDebugInfoHelper(instance(), instance())
    }
    bind<RenderAreasHelper>() with singleton {
        RenderAreasHelper(instance(), instance(), instance())
    }
//    bind<RenderInfluenceHelper>() with singleton {
//        RenderInfluenceHelper(instance(), instance())
//    }
    bind<InitSpritesHelper>() with singleton {
        InitSpritesHelper(instance(), instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        CameraSystem(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        RenderLocationSystem(instance(), instance(), instance(), instance(LOCATION_MAP_FACTORY_TAG), instance(LOCATION_CELL_FACTORY_TAG))
    }
    bind<EntitySystem>().inSet() with singleton {
        RenderEntitiesSystem(instance(), instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        RenderGUISystem(instance(), instance(), instance(),
            instance(), instance(), instance(), instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        AnimationSystem(instance(), instance())
    }
}