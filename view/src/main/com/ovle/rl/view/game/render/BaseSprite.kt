package com.ovle.rl.view.game.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.ovle.rl.view.FONT_NAME_SMALL
import com.ovle.rl.view.game.render.effect.SpriteEffect
import com.ovle.rl.view.game.render.effect.SpriteEffectKey
import com.ovle.rl.view.palette.full.DB16
import ktx.scene2d.Scene2DSkin
import kotlin.collections.set

class BaseSprite(
    region: TextureRegion,
    val isBig: Boolean = false
): Sprite(region) {

    private val effects: MutableMap<SpriteEffectKey, SpriteEffect> = mutableMapOf()
    private val children: MutableCollection<BaseSprite> = mutableListOf()
    private val strings: MutableMap<SpriteEffect, SpriteText> = mutableMapOf()

    var tintColor: Color? = null


    override fun draw(batch: Batch?) {
        if (tintColor == null) {
            drawIntr(batch)
        } else {
            val oldColor = this.color
            color = tintColor
            drawIntr(batch)
            color = oldColor
        }
    }

    private fun drawIntr(batch: Batch?) {
        super.draw(batch)

        children.forEach { it.draw(batch) }
    }

    fun drawText(batch: Batch?) {
        val font = Scene2DSkin.defaultSkin.getFont(FONT_NAME_SMALL)
        strings.values.forEach {
            val tx = it.point.x + x
            val ty = it.point.y + y
            font.draw(batch, it.text, tx, ty)
        }
    }


    fun addChild(child: BaseSprite) {
        children += child
        child.setPosition(x, y)
    }

    fun removeChild(child: BaseSprite) {
        children -= child
    }

    fun addEffect(key: SpriteEffectKey, effect: SpriteEffect) {
        effects[key] = effect
    }

    fun removeEffect(key: SpriteEffectKey) {
        val spriteEffect = effects[key] ?: return
        spriteEffect.clear(this)

        effects.remove(key)
    }

    fun removeEffect(effect: SpriteEffect) {
        effect.clear(this)

        val effectToKey = effects.entries.singleOrNull { it.value == effect } ?: return
        effects.remove(effectToKey.key)
    }

    fun clearEffects() {
        effects.values.forEach {
            it.clear(this)
        }

        effects.clear()
    }

    fun processEffects(deltaTime: Double) {
        val effects = effects.values.toList()

        for (effect in effects) {
            effect.effectsTime += deltaTime
            val totalLength = effect.totalLength

            if (effect.effectsTime >= totalLength) {
                if (effect.timed) {
                    removeEffect(effect)
                    continue
                }

                effect.effectsTime = effect.effectsTime % totalLength
            }

            effect.process(this, effect.effectsTime)
        }
    }

    fun setText(text: SpriteText) {
        strings[text.source] = text
    }

    fun clearText(source: SpriteEffect) {
        strings.remove(source)
    }

    override fun setPosition(x: Float, y: Float) {
        setPositionIntr(x, y)

        effects.values.forEach { it.onPositionChanged(this) }
    }

    fun setPositionIntr(x: Float, y: Float) {
        super.setPosition(x, y)

        children.forEach { it.setPosition(x, y) }
    }
}