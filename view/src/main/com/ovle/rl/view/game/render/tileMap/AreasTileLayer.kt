package com.ovle.rl.view.game.render.tileMap

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.utils.gdx.math.point.point


class AreasTileLayer(
    width: Int, height: Int, tileWidth: Int, tileHeight: Int,
    private val areaViewTemplates: Collection<AreaViewTemplate>,
    private val textureRegions: Array<Array<TextureRegion>>,
    private val cellFactory: TiledMapCellFactory,
    private val location: Location,
) : TiledMapTileLayer(width, height, tileWidth, tileHeight) {

    override fun getCell(x: Int, y: Int): Cell? {
        val point = point(x, y)
        val areas = location.players.all().flatMap { it.areas }
        val area = areas.singleOrNull { point in it.area }
            ?: return null
        val areaView = areaViewTemplates.singleOrNull { it.template == area.template }
            ?: return null
        val areaTile  = areaView.areaTileView ?: return null
        val tile = location.content.tiles[x, y]

        return when {
            tile.props.isFloor -> cell(areaTile.floor)
            tile.props.isWall -> cell(areaTile.wall)
            else -> null
        }
    }

    private fun cell(point: GridPoint2?): Cell? {
        if (point == null) return null

        val region = textureRegions[point.y][point.x]
        return cellFactory.cell(arrayOf(region))
    }
}