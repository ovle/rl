package com.ovle.rl.view.game.render.effect.config

import com.ovle.rl.view.game.render.BaseSprite
import com.ovle.rl.view.game.render.effect.SpriteEffect
import com.ovle.utils.gdx.math.Direction
import com.ovle.utils.gdx.math.point.point

class OffsetSpriteEffect(
    private val direction: Direction,
    private val frames: Array<Int>,
    private val frameLength: Double
): SpriteEffect() {

    companion object {
        private const val FRAME_VALUE_COEFF = 2
    }

    private var lastFrameValue = 0

    override val totalLength = frameLength * frames.size


    override fun process(sprite: BaseSprite, time: Double) {
        val frameIndex = (time / frameLength).toInt()
        val frameValue = frames[frameIndex] * FRAME_VALUE_COEFF
        val valueDiff = frameValue - lastFrameValue

        val newPosition = direction.plus(point(sprite.x, sprite.y), valueDiff)
        sprite.setPositionIntr(newPosition.x.toFloat(), newPosition.y.toFloat())

        lastFrameValue = frameValue
    }

    override fun clear(sprite: BaseSprite) {
        val newPosition = direction.plus(point(sprite.x, sprite.y), -lastFrameValue)
        sprite.setPositionIntr(newPosition.x.toFloat(), newPosition.y.toFloat())
    }

    override fun onPositionChanged(sprite: BaseSprite) {
        lastFrameValue = 0
    }
}