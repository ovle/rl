package com.ovle.rl.view.game.render.tileMap

import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer


fun TiledMap.layer(name: String) = this.layers.get(name) as TiledMapTileLayer