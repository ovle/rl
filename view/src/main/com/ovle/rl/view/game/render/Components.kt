package com.ovle.rl.view.game.render

import com.badlogic.ashley.core.Entity
import com.ovle.rl.view.game.render.Components.render
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get

object Components {
    val render = mapper<RenderComponent>()
}

fun Entity.spriteOrNull(): BaseSprite? {
    return this[render]?.sprite
}