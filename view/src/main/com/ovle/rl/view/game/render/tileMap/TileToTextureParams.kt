package com.ovle.rl.view.game.render.tileMap

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.utils.gdx.view.TextureRegions


class TileToTextureParams(
    val tile: TileTemplate?,
    val textureRegions: TextureRegions,
    val point: GridPoint2? = null,
    val isVisible: Boolean = true,
    val isKnown: Boolean = true,

    val isFloorDown: Boolean = false,
    val isFloorRight: Boolean = false,
    val isFloorDiag: Boolean = false,
)