package com.ovle.rl.view.game.scene2d

import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.ovle.rl.GetGame
import com.ovle.rl.interaction.PlayerControlMode
import com.ovle.rl.interaction.SwitchViewModeCommand
import com.ovle.rl.interaction.playerView.PlayerViewMode
import com.ovle.rl.model.game.time.AbsoluteTimeChangedEvent
import com.ovle.rl.util.interaction
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.KTableWidget
import ktx.scene2d.KWidget
import ktx.scene2d.table
import ktx.scene2d.textButton

enum class UtilAction(
    val displayName: String, val action: () -> Unit
) {
    TASKS("tsk", {
        send(SwitchViewModeCommand(PlayerViewMode.TASKS))
    }),
    AREAS("are", {
        send(SwitchViewModeCommand(PlayerViewMode.AREAS))
    }),
    MOVE_MAP("mov", {
        send(SwitchViewModeCommand(PlayerViewMode.MOVE_MAP))
    }),
    ACCESSIBILITY_MAP("acs", {
        send(SwitchViewModeCommand(PlayerViewMode.ACCESSIBILITY_MAP))
    }),
    FOV("fov", {
        send(SwitchViewModeCommand(PlayerViewMode.FOV))
    }),
    NO_FOW("-fow", {
        send(SwitchViewModeCommand(PlayerViewMode.NO_FOW))
    }),
    FPS("fps", {
        send(SwitchViewModeCommand(PlayerViewMode.FPS))
    }),
}


class HeaderButtonsWidget(
    private val getGame: GetGame
) : BaseWidget() {

    private var currentMode: PlayerControlMode? = null

    private val buttons: MutableCollection<Button> = mutableListOf()

    override fun subscribe() {
        currentMode = null

        subscribe<AbsoluteTimeChangedEvent>(this) { onTimeChangedEvent() }
    }


    override fun <S> addActorIntr(container: KWidget<S>): KTableWidget {
        return container.table {
            buttons.clear()

            row().expandX().expandY()
            UtilAction.values().forEach { ua ->
                textButton(text = ua.displayName, "toggle") { c ->
                    c.fill()
                    onClick { onUtilActionBtnClick(ua) }
                    this.userObject = ua
                    buttons += this
                }
            }
        }
    }


    private fun onUtilActionBtnClick(controlMode: UtilAction) {
        controlMode.action()
    }

    private fun onTimeChangedEvent() {
        val interaction = interaction()
//        val newControlMode = interaction.controlMode
//        if (newControlMode == currentMode) return

//        updateButtons(newControlMode, currentMode)
//        updateControlModeGUI(newControlMode)
    }

    private fun interaction() = getGame().interaction()

}