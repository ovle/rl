package com.ovle.rl.view.game.render.tileMap

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.ovle.rl.interaction.playerView.PlayerViewConfig
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.location.projection.LocationProjection
import com.ovle.utils.gdx.math.point.point


class PlayerViewMaskTileLayer(
    width: Int, height: Int, tileWidth: Int, tileHeight: Int,
    private val location: Location,
    private val viewConfig: PlayerViewConfig,
    private val unknownCell: Cell,
    private val invisibleCell: Cell,
) : TiledMapTileLayer(width, height, tileWidth, tileHeight) {

    override fun getCell(x: Int, y: Int): Cell? {
        if (!viewConfig.isViewMaskEnabled) return null

        val p = point(x, y)
        val player = location.players.human
        val locationProjection = player.locationProjection
        if (!locationProjection.knownMap.isKnownTile(p)) return unknownCell
        if (!locationProjection.visibleMap.isVisibleTile(p)) return invisibleCell

        return null
    }
}