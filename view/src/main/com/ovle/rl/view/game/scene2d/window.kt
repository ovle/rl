package com.ovle.rl.view.game.scene2d

import com.badlogic.gdx.utils.Align
import ktx.scene2d.KWindow
import ktx.scene2d.scene2d
import ktx.scene2d.window

fun modalWindow(title: String, init: KWindow.() -> Unit = {}) =
    scene2d.window(title, "dialog-modal", init = init)
        .also {
            //it.isModal = true //disables keys
            it.isMovable = false
            it.setFillParent(true)
            it.align(Align.center)
        }