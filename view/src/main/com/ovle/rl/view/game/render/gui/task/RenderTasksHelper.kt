package com.ovle.rl.view.game.render.gui.task

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.interaction.PlayerControlMode.TASKS
import com.ovle.rl.interaction.playerView.PlayerViewMode
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.rl.view.game.render.draw
import com.ovle.rl.view.game.render.gui.GUISprites
import com.ovle.rl.view.game.render.mapper.TileToTextureMapper
import com.ovle.utils.gdx.view.textureRegion

class RenderTasksHelper (
    private val batch: Batch,
    private val guiSprites: GUISprites,
    private val taskViews: Collection<TaskViewTemplate>,
    private val tileToTextureMapper: TileToTextureMapper,
) {

    fun drawTasksInfo(interaction: PlayerInteractionComponent, location: Location) {
        val isTaskInteractionMode = interaction.controlMode == TASKS
        val isTaskViewMode = PlayerViewMode.TASKS in interaction.viewConfig.viewModes
        if (!isTaskInteractionMode && !isTaskViewMode) return

        val tasks = location.players.human.tasks
        val taskInteraction = interaction.taskInteraction
        val selectedTask = taskInteraction.selectedTask
        val isNeedDisplayTaskTargets = isNeedDisplayTaskTargets()
        if (isNeedDisplayTaskTargets) {
            tasks.forEach {
                val isSelected = it == selectedTask
                drawTaskInfo(it, isSelected)
            }
        }
        selectedTask?.let {
            drawTaskPerformer(it)
        }

        val tasksByTemplate = tasks.groupBy { it.template }
        tasksByTemplate.forEach { (template, tasks) ->
            val renderTaskHelper = renderTaskHelper(template) ?: return
            if (!isNeedDisplayTaskTargets) {
                renderTaskHelper.drawTasks(tasks)
            }
        }

        val template = taskInteraction.selectedTaskTemplate
        template?.let {
            val renderTaskHelper = renderTaskHelper(template)
            renderTaskHelper?.drawSelection(interaction)
        }
    }


    private fun renderTaskHelper(taskTemplate: TaskTemplate): RenderTaskHelper? {
        val getRenderHelper = taskViewTemplate(taskTemplate).getRenderHelper
        return getRenderHelper
            .invoke(batch, guiSprites, tileToTextureMapper)
    }

    private fun drawTaskInfo(task: TaskInfo, isSelected: Boolean) {
        val taskView = taskViewTemplate(task.template)
        val spritePoint = if (isSelected) taskView.selectedIcon else taskView.icon
        val sprite = textureRegion(guiSprites.guiRegions, spritePoint.x, spritePoint.y)
        val positions = task.target.positions()

        draw(positions, sprite)
    }

    private fun drawTaskPerformer(taskInfo: TaskInfo) {
        val performer = taskInfo.performer ?: return
        val positions = setOf(performer.position())

        draw(positions, guiSprites.entityTaskPerformerSprite)
    }

    private fun draw(positions: Collection<GridPoint2>, sprite: TextureRegion) {
        positions.forEach {
            batch.draw(it, sprite)
        }
    }

    private fun taskViewTemplate(taskTemplate: TaskTemplate) =
        taskViews.singleOrNull { it.taskTemplate == taskTemplate }!!

    private fun isNeedDisplayTaskTargets() = (System.currentTimeMillis() / 500) % 2 == 0L
}