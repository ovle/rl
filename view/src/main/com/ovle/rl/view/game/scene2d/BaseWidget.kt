package com.ovle.rl.view.game.scene2d

import com.badlogic.gdx.scenes.scene2d.Actor
import com.ovle.utils.event.EventBus
import ktx.scene2d.KWidget


abstract class BaseWidget {

    private var rootActor: Actor? = null

    fun <S> addActor(container: KWidget<S>) {
        rootActor = addActorIntr(container)
        subscribe()
    }

    fun removeActor() {
        rootActor?.parent?.removeActor(rootActor)
        children().forEach { it.removeActor() }

        clearSubscriptions()
    }


    protected abstract fun <S> addActorIntr(container: KWidget<S>): Actor

    protected open fun children(): Collection<BaseWidget> = emptyList()


    protected open fun subscribe() { }

    private fun clearSubscriptions() {
        EventBus.clearSubscriptions(this)
    }
}