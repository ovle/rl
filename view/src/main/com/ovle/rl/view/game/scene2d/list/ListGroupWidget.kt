package com.ovle.rl.view.game.scene2d.list

import com.badlogic.gdx.scenes.scene2d.Actor
import com.ovle.rl.view.game.scene2d.BaseWidget
import ktx.actors.onClick
import ktx.scene2d.*

/**
 *
 */
class ListGroupWidget<T>(
    private val groups: Collection<T>,
    private val groupNames: List<String>,
    private val onGroupSelect: (T?) -> Unit,
    private val groupRowSize: Int = 2,
    private val optionalSelection: Boolean = true
): BaseWidget() {

    var selectedGroup: T? = null
        set(value) {
            field = value

            buttons.singleOrNull {
                it.userObject == value
            }?.isChecked = true
        }

    private val buttons = mutableListOf<KTextButton>()


    override fun subscribe() {
        super.subscribe()

        selectedGroup = (if (!optionalSelection) groups().first().first() else null)
        onGroupSelect(selectedGroup)
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        container as KTableWidget
        container.row().expandX()

        val minCheckedCount = if (optionalSelection) 0 else 1

        return container.buttonGroup(minCheckedCount, 1) {
            it.fill()
            groups().forEachIndexed { gi, groupRow ->
                row().expandX()

                groupRow.forEachIndexed { i, g ->
                    textButton(groupNames[gi * groupRowSize + i], style = "toggle") { tb ->
                        tb.fill()

                        onClick {
                            val newGroup = if (isChecked) g else null
                            selectedGroup = newGroup
                            onGroupSelect(newGroup)
                        }

                        this.userObject = g
                        buttons += this
                    }
                }
            }
        }
    }

    private fun groups() = groups.toList().chunked(groupRowSize)
}