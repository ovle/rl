package com.ovle.rl.view.game.render.gui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.interaction.playerView.PlayerViewMode
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.space.positionOrNull

import com.ovle.rl.model.util.location
import com.ovle.rl.screen.ScreenData
import com.ovle.rl.util.interaction
import com.ovle.rl.view.FONT_NAME
import com.ovle.rl.view.game.render.draw
import com.ovle.rl.view.game.render.gui.task.RenderTasksHelper
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.math.rangeX
import com.ovle.utils.gdx.math.rangeY
import ktx.scene2d.Scene2DSkin


class RenderGUISystem(
    private val batch: Batch,
    private val guiSprites: GUISprites,
    private val screenData: ScreenData,

    private val renderCursorHelper: RenderCursorHelper,
    private val renderTasksHelper: RenderTasksHelper,
    private val renderAreasHelper: RenderAreasHelper,
//    private val renderInfluenceHelper: RenderInfluenceHelper,
    private val renderDebugInfoHelper: RenderDebugInfoHelper,
): BaseSystem() {

    override fun subscribe() {}

    override fun update(delta: Float) {
        batch.begin()
        renderGUI()
        batch.end()

        val stageBatch = screenData.gameStage.batch
        stageBatch.begin()
        renderStageGUI()
        stageBatch.end()
    }


    private fun renderGUI() {
        val game = game()
        val interaction = game.interaction()
        renderCursorHelper.drawCursor(interaction, game)

        drawDebugInfo(interaction)
        drawSelection(interaction)

        val location = game.location()
        renderTasksHelper.drawTasksInfo(interaction, location)
        renderAreasHelper.drawAreas(interaction, location)

//        val viewModes = interaction.viewConfig.viewModes
//        if (PlayerViewMode.INFLUENCE_MAP in viewModes) {
//            renderInfluenceHelper.drawInfluenceArea(location)
//        }
    }

    private fun renderStageGUI() {
        val interaction = game().interaction()
        if (PlayerViewMode.FPS in interaction.viewConfig.viewModes) {
            drawProfileInfo()
        }
    }

    private fun drawDebugInfo(interaction: PlayerInteractionComponent) {
        val hoveredEntity = interaction.entityInteraction.hoveredEntity
        val viewModes = interaction.viewConfig.viewModes

        renderDebugInfoHelper.drawDebugInfo(hoveredEntity, game(), viewModes)
    }

    private fun drawSelection(interaction: PlayerInteractionComponent) {
        val selectedEntity = interaction.entityInteraction.selectedEntity

        if (selectedEntity != null) {
            val sprite = guiSprites.entitySelectionSprite
            val position = selectedEntity.positionOrNull()
            if (position != null) {
                batch.draw(position, sprite, isBig = true)
            }
        }

        interaction.selectionRectangle?.let {
            drawRectangle(it)
        }
    }

    //todo calc in separate component
    private fun drawProfileInfo() {
        val fps = Gdx.graphics.framesPerSecond
        val javaHeap = Gdx.app.javaHeap / 1000000
        val screenHeight = screenData.screenHeight
        val y = screenHeight - 40.0f //todo flex

        drawInStage(Vector2(0.0f, y), "FPS: $fps, heap: $javaHeap Mb")
    }


    private fun drawRectangle(rectangle: Rectangle) {
        val rangeX = rectangle.rangeX()
        val rangeY = rectangle.rangeY()
        rangeX.forEach {
            batch.draw(point(it, rangeY.first), guiSprites.recBottomSprite)
            batch.draw(point(it, rangeY.last), guiSprites.recTopSprite)
        }
        rangeY.forEach {
            batch.draw(point(rangeX.first, it), guiSprites.recLeftSprite)
            batch.draw(point(rangeX.last, it), guiSprites.recRightSprite)
        }
    }

    private fun drawInWorld(point: GridPoint2, value: String) {
        val font = Scene2DSkin.defaultSkin.getFont(FONT_NAME)
        font.draw(batch, value, point.x.toFloat(), point.y.toFloat())
    }

    private fun drawInStage(point: Vector2, value: String) {
        val font = Scene2DSkin.defaultSkin.getFont(FONT_NAME)
        val stageBatch = screenData.gameStage.batch

        font.draw(stageBatch, value, point.x, point.y)
    }
}