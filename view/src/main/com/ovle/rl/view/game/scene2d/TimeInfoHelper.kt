package com.ovle.rl.view.game.scene2d

import com.ovle.rl.model.game.time.dto.GlobalTimeInfo

class TimeInfoHelper {

    fun timeInfo(globalTime: GlobalTimeInfo): String {
        val gameSpeed = if (globalTime.paused) "(pause)" else "(x${globalTime.gameSpeed})"
        val gameTime = globalTime.toGameTime()
        val (day, hour, minute) = gameTime
        val dayNightStr = if (gameTime.isDay) "day" else "night"

        return "%s %d, %02d:%02d %s"
            .format(dayNightStr, day, hour, minute, gameSpeed)
    }
}