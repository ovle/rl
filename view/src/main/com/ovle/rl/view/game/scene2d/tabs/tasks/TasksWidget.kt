package com.ovle.rl.view.game.scene2d.tabs.tasks

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.ovle.rl.GetGame
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.interaction.type.task.PlayerTaskAction
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.util.location
import com.ovle.rl.util.interaction
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.rl.view.game.scene2d.list.BaseListWidget
import com.ovle.rl.view.game.scene2d.list.ListActionsWidget
import com.ovle.rl.view.game.scene2d.list.ListGroupWidget
import com.ovle.rl.view.game.scene2d.list.ListItemsWidget
import com.ovle.utils.event.EventBus.subscribe
import ktx.scene2d.*

class TasksWidget(
    getGame: GetGame,
    private val taskInfoHelper: TaskInfoHelper,
    private val taskTemplates: Collection<TaskTemplate>,
    private val taskViewTemplates: Collection<TaskViewTemplate>,
): BaseListWidget(getGame) {

    private lateinit var taskTemplatesWidget: ListGroupWidget<TaskTemplate>
    private lateinit var taskActionsWidget: ListActionsWidget<PlayerTaskAction>
    private lateinit var tasksWidget: ListItemsWidget<TaskInfo>
    private lateinit var taskInfoLabel: Label
    private lateinit var targetInfoLabel: Label

    private var customWidgetContainer: Group? = null


    override fun subscribe() {
        super.subscribe()

        subscribe<KeyPressedEvent>(this) { onKeyPressedEvent(it.code) }
    }

    private fun onKeyPressedEvent(code: Int) {
        val taskViewTemplate = taskViewTemplates.singleOrNull { it.keyCode == code }
            ?: return

        val taskTemplate = taskViewTemplate.taskTemplate

        taskTemplatesWidget.selectedGroup = taskTemplate
        onTaskTemplateSelect(taskTemplate)
    }

    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        val manualTaskTemplates = taskTemplates.filter { !it.isAutoTask }

        val groupNames = manualTaskTemplates.map { t ->
            taskViewTemplates.single { it.taskTemplate == t }.displayName
        }
        taskTemplatesWidget = ListGroupWidget(
            manualTaskTemplates, groupNames, ::onTaskTemplateSelect,
            groupRowSize = 3
        )

        val actions = PlayerTaskAction.values().toList()
        taskActionsWidget = ListActionsWidget(
            actions, actions.map { it.displayName }, ::onTaskActionSelect
        )

        tasksWidget = ListItemsWidget(
            taskInfoHelper::taskInfo, ::externalSelectedItem, ::externalSelectItem, { }, 3.5f
        )

        return container.table {
            taskTemplatesWidget.addActor(this)
            row()
            taskInfoLabel = label("") {
                it.top().growX().padLeft(10.0f).padTop(5.0f)
                setAlignment(Align.center)
            }
            row()
            targetInfoLabel = label("") {
                it.top().growX().padLeft(10.0f)
                setAlignment(Align.center)
            }
            row()
            customWidgetContainer = table {  }
            row()
            taskActionsWidget.addActor(this)
            tasksWidget.addActor(this)
        }
    }


    private fun items(taskTemplate: TaskTemplate?): Array<TaskInfo> {
        val location = getGame().location()
        val player = location.players.human
        return player.tasks
            .filter { !it.template.isAutoTask }
            .toTypedArray()

//        val tasks = getGame().location().player.tasks
//        return tasks.filter { taskTemplate == null || it.template == taskTemplate }.toTypedArray()
    }

    private fun externalSelectItem(item: TaskInfo?) {
        taskInteraction().selectedTask = item
    }

    private fun externalSelectedItem() = taskInteraction().selectedTask

    private fun onTaskActionSelect(action: PlayerTaskAction) {
        val game = getGame()
        val player = game.location().players.human
        action.action.invoke(tasksWidget.selectedListItem!!, player, game)
    }

    private fun onTaskTemplateSelect(taskTemplate: TaskTemplate?) {
        val interaction = taskInteraction()
        interaction.selectedTaskTemplate = taskTemplate
        interaction.targetSelection = taskTemplate?.targetConfig?.getTarget?.invoke()

        updateGUI(taskTemplate)
    }

    override fun onTimeChange() {
        val isActionsDisabled = tasksWidget.selectedListItem == null
        taskActionsWidget.setActionButtonsDisabled(isActionsDisabled)

        val taskTemplate = taskTemplatesWidget.selectedGroup
        tasksWidget.setItems(items(taskTemplate))
        tasksWidget.syncSelections()

        taskTemplate?.let {
            taskInfoLabel.setText(it.name.lowercase())
        }

        val interaction = taskInteraction()
        val check = interaction.targetSelection?.check()
        val message = if (check is TaskTargetCheck.Error) check.message else ""
        targetInfoLabel.setText(message)
    }


    private fun updateGUI(taskTemplate: TaskTemplate?) {
        val container = customWidgetContainer ?: return

        container.clearChildren()
        val taskViewTemplate = taskViewTemplates
            .singleOrNull { it.taskTemplate == taskTemplate } ?: return

        val widget = taskViewTemplate.getWidget.invoke(getGame)
        widget?.let {
            widget.addActor(container as KWidget<Group>)
        }
    }

    private fun taskInteraction() = getGame().interaction().taskInteraction
}