package com.ovle.rl.view.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.ovle.rl.view.*
import ktx.scene2d.Scene2DSkin


fun skin() = Skin(Gdx.files.internal(SKIN_PATH)).apply {
    getFont(FONT_NAME).apply {
        data.markupEnabled = true

        setLineHeight(this, FONT_SIZE)
    }

    getFont(FONT_NAME_SMALL).apply {
        data.markupEnabled = true

        setLineHeight(this, FONT_SIZE_SMALL)
    }
}

private fun setLineHeight(font: BitmapFont, height: Float) {
    font.data.setScale(height * font.scaleY / font.lineHeight)
}

fun labelStyle(styleName: String): Label.LabelStyle? =
    Scene2DSkin.defaultSkin.get(styleName, Label.LabelStyle::class.java)

fun textFieldStyle(styleName: String): TextField.TextFieldStyle? =
    Scene2DSkin.defaultSkin.get(styleName, TextField.TextFieldStyle::class.java)