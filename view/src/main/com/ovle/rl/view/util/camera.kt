package com.ovle.rl.view.util

import com.badlogic.gdx.graphics.OrthographicCamera
import com.ovle.rl.view.SCREEN_HEIGHT
import com.ovle.rl.view.SCREEN_WIDTH

fun camera() =
    OrthographicCamera().apply {
        setToOrtho(false, SCREEN_WIDTH, SCREEN_HEIGHT);
        update()
    }