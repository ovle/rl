package com.ovle.rl.assets

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.TextureLoader
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.utils.Disposable


class RlAssetsManager(private val assets: AssetManager) : Disposable {

    companion object {
        private const val PALETTE = "db16"
        private const val GAME_TEXTURE_PATH = "images/game_${PALETTE}.png"
        private const val GUI_TEXTURE_PATH = "images/gui_$PALETTE.png"
        private const val LOGO_PATH = "images/logo.png"
    }

    lateinit var gameTexture: Texture
    lateinit var guiTexture: Texture
    lateinit var logo: Texture

    init {
        val fileHandleResolver = InternalFileHandleResolver()

        with(assets) {
            setLoader(Texture::class.java, TextureLoader(fileHandleResolver))
        }
    }

    fun load() {
        with(assets) {
            load(GAME_TEXTURE_PATH, Texture::class.java)
            load(GUI_TEXTURE_PATH, Texture::class.java)
            load(LOGO_PATH, Texture::class.java)
        }

        gameTexture = assets.finishLoadingAsset(GAME_TEXTURE_PATH)
        guiTexture = assets.finishLoadingAsset(GUI_TEXTURE_PATH)
        logo = assets.finishLoadingAsset(LOGO_PATH)
    }

    override fun dispose() {
        gameTexture.dispose()
        guiTexture.dispose()
        logo.dispose()
    }
}