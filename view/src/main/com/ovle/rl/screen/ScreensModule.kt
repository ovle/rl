package com.ovle.rl.screen

import com.ovle.rl.screen.game.EntityDetailsScreen
import com.ovle.rl.screen.game.result.GameResultScreen
import com.ovle.rl.screen.game.GameMenuScreen
import com.ovle.rl.screen.game.GameScreen
import com.ovle.rl.screen.game.save.SavedGamesScreen
import com.ovle.rl.view.game.render.mapper.WORLD_MAP_FACTORY_TAG
import org.kodein.di.*

val screensModule = DI.Module("screens") {
    bindArgSet<ScreenManager, BaseScreen>()

    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        LoadingScreen(sm, instance(), instance())
    }
    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        SavedGamesScreen(sm, instance(), instance())
    }
    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        GameMenuScreen(sm, instance())
    }
    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        EntityDetailsScreen(sm, instance())
    }
    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        MainMenuScreen(sm, instance(), instance())
    }
    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        GameResultScreen(sm, instance())
    }
    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        WorldScreen(
            sm, instance(), instance(), instance(), instance(), instance(), instance(),
            instance(WORLD_MAP_FACTORY_TAG)
        )
    }
    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        GameScreen(instance(), sm, instance(), instance(), instance(),
            instance(), instance(), instance()
        )
    }
    bind<BaseScreen>().inSet() with factory { sm: ScreenManager ->
        SelectPlaygroundScreen(sm, instance(), instance())
    }
}