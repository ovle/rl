package com.ovle.rl.screen

import com.badlogic.gdx.Input
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.ui.Value
import com.badlogic.gdx.utils.Align
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.controls.gdx.MouseClickEvent
import com.ovle.rl.controls.gdx.MouseMovedEvent
import com.ovle.rl.model.procgen.grid.world.World
import com.ovle.rl.model.procgen.grid.world.WorldPlace
import com.ovle.rl.model.procgen.grid.world.WorldRegion
import com.ovle.rl.view.game.scene2d.BaseWidget
import com.ovle.rl.view.game.scene2d.bgImage
import com.ovle.rl.view.game.scene2d.list.ListItemsWidget
import com.ovle.rl.view.game.scene2d.stack
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.math.point.point
import ktx.actors.onClick
import ktx.scene2d.*
import kotlin.random.Random

class WorldGUIWidget(
    private val screenManager: ScreenManager,
    private val getWorld: () -> World,
    private val generateWorld: (Long, String) -> Unit,
    private val embark: (GridPoint2) -> Unit
): BaseWidget() {

    companion object {
        private const val VIEWPORT_TO_WORLD_SIZE = 8
    }

    var cursorPoint: GridPoint2? = null
    var selectionPoint: GridPoint2 = point(1, 1)  //todo //(31, 63) (89, 62), (50, 113)
    var selectedRegion: WorldRegion? = null

    private lateinit var nameTextField: TextField
    private lateinit var seedTextField: TextField

    private lateinit var cursorCoordsLabel: Label
    private lateinit var selectionCoordsLabel: Label

    private lateinit var cursorRegionLabel: Label
    private lateinit var selectionRegionLabel: Label

    private lateinit var itemsWidget: ListItemsWidget<WorldPlace>


    override fun subscribe() {
        subscribe<MouseMovedEvent>(this) { onMouseMoved(it.viewportPoint) }
        subscribe<MouseClickEvent>(this) { onMouseClick(it.viewportPoint) }
        subscribe<KeyPressedEvent>(this) { onKeyPressed(it.code) }

        initPlacesInfo()
        updateSelectionInfo(selectionPoint)
    }


    override fun <S> addActorIntr(container: KWidget<S>): Actor {
        itemsWidget = ListItemsWidget(
            getItemInfo = { "${it.name} (${it.point})" },
            onSelectionChange = {
                selectionPoint = it?.point ?: point(0, 0)
                updateSelectionInfo(selectionPoint)
            },
            itemHeightCoef = 2.5f
        )

        return container.table table@ {
            row()
            label(text = "World map", "title") {
                setAlignment(Align.center)
                it.growX()
                it.padTop(10.0f).padBottom(10.0f)
            }

            row()
            table {
                it.padBottom(10.0f)

                row()
                label(text = "World name: ") {}
                nameTextField = textField(style = "light", text = WorldScreen.DEFAULT_WORLD_NAME) { tfc ->
                    tfc.width(300.0f).height(25.0f)
                }

                row().padTop(10.0f)
                label(text = "Seed: ") {}
                seedTextField = textField(style = "light", text = "${WorldScreen.DEFAULT_WORLD_SEED}") { tfc ->
                    tfc.width(300.0f).height(25.0f)
                    textFieldFilter = TextField.TextFieldFilter { _, c -> c.isDigit() }
                }
                textButton(text = "[r]andom") {bc ->
                    bc.padLeft(10.0f)
                    onClick { onRandomSeedBtnClick() }
                }
            }

            row()
            stack { sc ->
                sc.growX().growY()
                bgImage()

                table mainTable@{
                    row()
                    stack { sc ->
                        sc.width(Value.percentWidth(0.75f, this@mainTable))
                            .growY()
                        bgImage()
                    }

                    table { tc ->
                        tc.width(Value.percentWidth(0.25f, this@mainTable))
                            .growY()

                        row()
                        label(text = "Selection: ") { lc ->
                            lc.padTop(10.0f).padBottom(10.0f)
                        }
                        row()
                        selectionCoordsLabel = label(text = "") { }
                        row()
                        selectionRegionLabel = label(text = "") {
                            wrap = true
                            it.width(250.0f).height(50.0f)
                        }

                        row()
                        label(text = "Cursor: ") { lc ->
                            lc.padTop(10.0f).padBottom(10.0f)
                        }
                        row()
                        cursorCoordsLabel = label(text = "") { }
                        row()
                        cursorRegionLabel = label(text = "") {
                            wrap = true
                            it.width(250.0f).height(50.0f)
                        }
                        row()

                        itemsWidget.addActor(this)
                        /*
                        stack { sc ->
                            sc.growY()
                        }*/
                    }
                }
            }

            row()
            horizontalGroup {
                it.padBottom(10.0f)

                textButton(text = "[g]enerate") {
                    onClick { onGenerateWorldClick() }
                }
                textButton(text = "[e]mbark!") {
                    onClick { onEmbarkClick() }
                }
            }

            setFillParent(true)
            align(Align.top)
            pack()
        }
    }


    private fun onKeyPressed(code: Int) {
        when (code) {
            Input.Keys.ESCAPE -> screenManager.popScreen()
            Input.Keys.ENTER -> onEmbarkClick()
            Input.Keys.R -> onRandomSeedBtnClick()
            Input.Keys.G -> onGenerateWorldClick()
            Input.Keys.E -> onEmbarkClick()
        }
    }

    private fun onRandomSeedBtnClick() {
        seedTextField.text = "${Random.nextInt()}"
    }

    private fun onGenerateWorldClick() {
        val seed = seedTextField.text.toLong()
        val name = nameTextField.text
        generateWorld(seed, name)

        updateCursorInfo(cursorPoint)

        initPlacesInfo()
        updateSelectionInfo(selectionPoint)
    }

    private fun onEmbarkClick() {
        embark(selectionPoint)
    }

    private fun onMouseMoved(viewportPoint: Vector2) {
        val x = viewportPoint.x / VIEWPORT_TO_WORLD_SIZE
        val y = viewportPoint.y / VIEWPORT_TO_WORLD_SIZE
        cursorPoint = point(x, y)

        updateCursorInfo(cursorPoint)
    }

    private fun onMouseClick(viewportPoint: Vector2) {
        val point = point(viewportPoint.x / VIEWPORT_TO_WORLD_SIZE, viewportPoint.y / VIEWPORT_TO_WORLD_SIZE)
        if (!getWorld().tiles!!.isValid(point)) return

        selectionPoint = point
        updateSelectionInfo(point)
    }


    private fun updateCursorInfo(point: GridPoint2?) {
        cursorCoordsLabel.setText(pointText(point))
        cursorRegionLabel.setText(regionPointText(point))
    }

    private fun updateSelectionInfo(point: GridPoint2) {
        val world = getWorld()

        selectedRegion = world.regions.single { point in it.points }

        selectionCoordsLabel.setText(pointText(point))
        selectionRegionLabel.setText(selectedRegion?.name)
    }

    private fun initPlacesInfo() {
        val places = getWorld().places
        itemsWidget.setItems(places.toTypedArray())

        selectionPoint = places.firstOrNull()?.point ?: point(0, 0)
    }


    private fun pointText(point: GridPoint2?) = point?.let { "$it" } ?: ""

    private fun regionPointText(point: GridPoint2?) =
        getWorld().region(point)?.name ?: ""

    private fun placePointText(point: GridPoint2?): String {
        val place = getWorld().place(point)
        return place?.name ?: ""
    }
}