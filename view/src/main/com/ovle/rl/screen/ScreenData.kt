package com.ovle.rl.screen

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.Viewport
import com.ovle.utils.gdx.view.PaletteManager

data class ScreenData(
    val screenWidth: Float,
    val screenHeight: Float,
    val paletteManager: PaletteManager,

    val stageViewport: Viewport,
    val batchViewport: Viewport,
    val batch: SpriteBatch
) {
    //todo hack
    lateinit var gameStage: Stage
}