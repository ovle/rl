package com.ovle.rl.screen.game.save

import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.utils.Align
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.model.util.markName
import com.ovle.rl.persistence.LoadGameCommand
import com.ovle.rl.screen.game.payload.LoadGameInfo
import com.ovle.rl.persistence.SaveGameCommand
import com.ovle.rl.persistence.SavedGameMetadata
import com.ovle.rl.persistence.SavedGamesManager
import com.ovle.rl.screen.BaseScreen
import com.ovle.rl.screen.ScreenData
import com.ovle.rl.screen.ScreenManager
import com.ovle.rl.screen.game.ExitGameCommand
import com.ovle.rl.screen.game.GameScreen
import com.ovle.rl.screen.game.save.SavedGamesScreenMode.LOAD
import com.ovle.rl.screen.game.save.SavedGamesScreenMode.SAVE
import com.ovle.rl.view.game.scene2d.list.ListItemsWidget
import com.ovle.rl.view.util.labelStyle
import com.ovle.rl.view.util.textFieldStyle
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.*
import java.text.SimpleDateFormat


class SavedGamesScreen(
    screenManager: ScreenManager,
    screenData: ScreenData,
    private val savedGamesManager: SavedGamesManager
) : BaseScreen(screenManager, screenData) {

    companion object {
        private const val SAVED_GAME_DATE_FORMAT = "dd/MMMM/yyyy hh:mm"
        private const val DEFAULT_SLOT_NAME = "game"
    }

    private lateinit var itemsWidget: ListItemsWidget<SavedGameSlot>
    private lateinit var actionButton: Button
    private lateinit var deleteButton: Button
    private lateinit var nameTextLabel: Label
    private lateinit var infoTextLabel: Label
    private lateinit var nameTextField: TextField

    private val newGameSlot = SavedGameSlot()

    private val mode: SavedGamesScreenMode
        get() = (payload as SavedGamesScreenPayload).mode


    override fun show() {
        var savedGames = savedGamesManager.savedGamesMeta()
            .map { SavedGameSlot(it) }
            .sortedBy { it.meta!!.date }

        val isSaveMode = mode == SAVE
        if (isSaveMode) {
            savedGames = listOf(newGameSlot) + savedGames
        }

        itemsWidget = ListItemsWidget(
            getItemInfo = ::itemInfo,
            onSelectionChange = ::onSelectionChange
        )
        itemsWidget.setItems(savedGames.toTypedArray())

        subscribe<KeyPressedEvent>(subscriber = this) { onKeyPressed(it.code) }

        super.show()

        val defaultSelectedSlot = savedGames.firstOrNull()
        updateGUI(defaultSelectedSlot)
    }

    override fun rootActor() =
        scene2d.table {
            row()
            label(text = mode.screenTitle, "title") {
                this.setAlignment(Align.center)
                it.growX()
                it.padTop(10.0f).padBottom(10.0f)
            }

            if (mode == SAVE) {
                row()
                table {
                    it.padBottom(5.0f).left()
                    row()
                    nameTextLabel = label(text = "Game name: ") {}
                    nameTextField = textField(text = DEFAULT_SLOT_NAME, "light") { tfc ->
                        tfc.width(500.0f)
                    }
                    infoTextLabel = label(text = "") {}
                }
            }

            itemsWidget.addActor(this)

            row()
            horizontalGroup {
                it.padBottom(10.0f)

                actionButton = textButton(text = mode.actionBtnTitle) {
                    onClick { onActionBtnClick(itemsWidget.selectedListItem) }
                }
                deleteButton = textButton(text = "Delete") {
                    onClick { onDeleteBtnClick(itemsWidget.selectedListItem) }
                }
                textButton(text = "Back") {
                    onClick { onBackBtnClick() }
                }
            }

            setFillParent(true)
            align(Align.center)
            pack()
        }


    private fun onKeyPressed(code: Int) {
        when (code) {
            Input.Keys.ESCAPE -> screenManager.popScreen()
            Input.Keys.ENTER -> onActionBtnClick(itemsWidget.selectedListItem)
        }
    }


    private fun onActionBtnClick(slot: SavedGameSlot?) {
        slot ?: return

        val meta = slot.meta
        when (mode) {
            SAVE -> saveGame(meta)
            LOAD -> loadGame(meta)
        }
    }

    private fun onDeleteBtnClick(slot: SavedGameSlot?) {
        slot ?: return
        val meta = slot.meta ?: return

        savedGamesManager.delete(meta)

        val items = itemsWidget.items
        items.removeValue(slot, true)
        itemsWidget.setItems(items.toArray())
    }

    private fun onBackBtnClick() {
        screenManager.popScreen()
    }

    private fun onSelectionChange(slot: SavedGameSlot?) {
        updateGUI(slot)
    }


    private fun saveGame(meta: SavedGameMetadata?) {
        send(SaveGameCommand(meta, nameTextField.text))
        screenManager.popScreen()
    }

    private fun loadGame(meta: SavedGameMetadata?) {
        meta ?: return

        send(ExitGameCommand())

        val payload = LoadGameInfo(meta)
        screenManager.pushScreen(GameScreen::class.java, payload)
    }

    private fun itemInfo(slot: SavedGameSlot): String {
        val meta = slot.meta ?: return slot.slotName

        val dateStr = SimpleDateFormat(SAVED_GAME_DATE_FORMAT).format(meta.date)
        return "${meta.name} (${meta.locationName}), $dateStr"
    }

    private fun updateGUI(selectedSlot: SavedGameSlot?) {
        rootActor ?: return

        val isHaveSelection = selectedSlot != null
        val isRewriteSlot = selectedSlot != null && selectedSlot != newGameSlot

        actionButton.isDisabled = !isHaveSelection
        deleteButton.isDisabled = !isHaveSelection || !isRewriteSlot

        if (mode == SAVE) {
            val infoText = when {
                selectedSlot == null -> ""
                isRewriteSlot -> " (rewrite game: ${selectedSlot.slotName})"
                else -> " (new game)"
            }
            infoTextLabel.setText(infoText)

            nameTextField.text = selectedSlot?.slotName
            nameTextField.isDisabled = !isHaveSelection
            nameTextField.style = textFieldStyle(if (isHaveSelection) "light" else "dark")
            nameTextLabel.style = labelStyle(if (isHaveSelection) "default" else "dark")
        }
    }
}