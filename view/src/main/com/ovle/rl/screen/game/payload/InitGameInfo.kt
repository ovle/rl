package com.ovle.rl.screen.game.payload

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.grid.world.World

data class InitGameInfo(val world: World, val locationPoint: GridPoint2, val locationTemplate: LocationTemplate)