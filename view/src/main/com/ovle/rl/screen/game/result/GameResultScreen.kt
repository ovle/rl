package com.ovle.rl.screen.game.result

import com.badlogic.gdx.Input
import com.badlogic.gdx.utils.Align
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.model.game.game.dto.FinishGameCondition
import com.ovle.rl.model.game.game.dto.GameResult
import com.ovle.rl.model.game.time.globalTime
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.markFail
import com.ovle.rl.model.util.markSuccess
import com.ovle.rl.screen.BaseScreen
import com.ovle.rl.screen.ScreenData
import com.ovle.rl.screen.ScreenManager
import com.ovle.rl.view.game.scene2d.TimeInfoHelper
import com.ovle.rl.view.game.scene2d.bgImage
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.*


class GameResultScreen(
    screenManager: ScreenManager,
    screenData: ScreenData,
) : BaseScreen(screenManager, screenData) {

    private val timeInfoHelper: TimeInfoHelper = TimeInfoHelper()


    override fun show() {
        super.show()

        subscribe<KeyPressedEvent>(this) { onKeyPressed(it.code) }
    }

    override fun rootActor() =
        scene2d.table {
            row()
            label(text = "Game finished", "title") {
                setAlignment(Align.center)
                it.growX()
                it.padTop(10.0f).padBottom(10.0f)
            }

            row()
            stack {
                it.growX().growY()
                bgImage()

                table {
                    val game = resultPayload().game
                    row()
                    label("Location: ${game.location().name}") { setAlignment(Align.center) }
                    row()
                    label("Time: ${timeInfoHelper.timeInfo(game.globalTime())}") { setAlignment(Align.center) }

                    row()
                    label("Results: ") {c ->
                        c.padTop(10.0f)
                        setAlignment(Align.center)
                    }

                    val results = resultPayload().results
                    results.forEach { (condition, isHaveResult) ->
                        row()
                        val text = gameConditionText(condition, isHaveResult)
                        label(text) { setAlignment(Align.center) }
                    }
                }
            }

            row()
            horizontalGroup {
                it.padBottom(10.0f)

                textButton(text = "Exit") {
                    onClick { onBackBtnClick() }
                }
            }

            setFillParent(true)
            align(Align.center)
            pack()
        }

    private fun onKeyPressed(code: Int) {
        when (code) {
            Input.Keys.ESCAPE -> screenManager.popToBaseScreen()
        }
    }

    private fun onBackBtnClick() {
        screenManager.popToBaseScreen()
    }


    private fun gameConditionText(condition: FinishGameCondition, isHaveResult: Boolean): String {
        var text = "${condition.result}: ${condition.description}"
        if (isHaveResult) {
            val markFn = if (condition.result == GameResult.LOSE) String::markFail
            else String::markSuccess
            text = markFn.invoke(text)
        }
        return text
    }

    private fun resultPayload() = (payload as GameResultPayload)
}