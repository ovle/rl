package com.ovle.rl.screen.game

import com.ovle.rl.model.game.game.*
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.persistence.LoadGameCommand
import com.ovle.rl.screen.BaseScreen
import com.ovle.rl.screen.ScreenData
import com.ovle.rl.screen.ScreenManager
import com.ovle.rl.screen.game.payload.InitGameInfo
import com.ovle.rl.screen.game.payload.InitPlaygroundInfo
import com.ovle.rl.screen.game.payload.LoadGameInfo
import com.ovle.rl.screen.game.result.GameResultPayload
import com.ovle.rl.screen.game.result.GameResultScreen
import com.ovle.rl.view.game.render.AreaViewTemplate
import com.ovle.rl.view.game.render.EntityViewTemplate
import com.ovle.rl.view.game.render.TaskViewTemplate
import com.ovle.rl.view.game.scene2d.GameGUIWidget
import com.ovle.rl.view.game.scene2d.stack
import com.ovle.utils.event.EventBus
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.scene2d.scene2d


open class GameScreen(
    private val gameContext: GameContext,
    screenManager: ScreenManager,
    screenData: ScreenData,
    playerSkillTemplates: Collection<PlayerSkillTemplate>,
    taskTemplates: Collection<TaskTemplate>,
    entityViewTemplates: Collection<EntityViewTemplate>,
    areaViewTemplates: Collection<AreaViewTemplate>,
    taskViewTemplates: Collection<TaskViewTemplate>,
) : BaseScreen(screenManager, screenData) {

    private var currentPayload: Any? = null
    private var resultPayload: GameResultPayload? = null
    private var isNeedFinishGame: Boolean = false

    private val gameGUIWidget = GameGUIWidget(
        screenManager,
        { gameContext.game() }, playerSkillTemplates, taskTemplates,
        entityViewTemplates, areaViewTemplates, taskViewTemplates,
        gameContext.log
    )

    override val unsubscribeOnHide: Boolean
        get() = false


    override fun show() {
        super.show()
        if (currentPayload != payload) {
            startGame()
        }
    }

    override fun hide() {
        gameGUIWidget.removeActor()
        super.hide()
    }

    override fun renderBeforeStage(delta: Float) {
        if (isNeedFinishGame) {
            finishGame()
        }

        gameContext.update(delta)
    }

    override fun rootActor() = scene2d.stack {
        setFillParent(true)
        gameGUIWidget.addActor(this)
    }

    private fun startGame() {
        screenData.gameStage = stage

        subscribe<ExitGameCommand>(this) { onExitGameCommand() }
        subscribe<GameFinishedEvent>(this) { onGameFinishedEvent(it.results) }

        gameContext.start()

        currentPayload = payload
        when (val p = payload) {
            is LoadGameInfo -> send(LoadGameCommand(p.meta))
            is InitGameInfo -> send(StartGameCommand(p.world, p.locationPoint, p.locationTemplate))
            is InitPlaygroundInfo -> send(StartPlaygroundCommand(p.locationTemplate))
        }
    }


    private fun onExitGameCommand() {
        finishGame()
    }

    private fun onGameFinishedEvent(results: FinishGameResults) {
        //in-universe game finishing is async, as we need to finish any current processing
        // and finish on next update
        resultPayload = GameResultPayload(gameContext.game(), results)
        isNeedFinishGame = true
    }


    private fun finishGame() {
        EventBus.clearSubscriptions(this)

        currentPayload = null
        isNeedFinishGame = false

        screenManager.popToBaseScreen()

        when {
            resultPayload != null -> {
                screenManager.pushScreen(GameResultScreen::class.java, resultPayload)
                resultPayload = null
            }
        }

        gameContext.finish()
    }
}