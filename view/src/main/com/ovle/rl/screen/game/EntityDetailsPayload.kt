package com.ovle.rl.screen.game

import com.badlogic.ashley.core.Entity

data class EntityDetailsPayload(val entity: Entity)