package com.ovle.rl.screen.game.payload

import com.ovle.rl.persistence.SavedGameMetadata

data class LoadGameInfo(val meta: SavedGameMetadata)