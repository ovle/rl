package com.ovle.rl.screen.game

import com.badlogic.gdx.Input
import com.badlogic.gdx.utils.Align
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.screen.BaseScreen
import com.ovle.rl.screen.ScreenData
import com.ovle.rl.screen.ScreenManager
import com.ovle.rl.screen.game.save.SavedGamesScreen
import com.ovle.rl.screen.game.save.SavedGamesScreenMode.*
import com.ovle.rl.screen.game.save.SavedGamesScreenPayload
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.scene2d
import ktx.scene2d.table
import ktx.scene2d.textButton


class GameMenuScreen(
    screenManager: ScreenManager,
    screenData: ScreenData,
) : BaseScreen(screenManager, screenData) {


    override fun show() {
        super.show()

        subscribe<KeyPressedEvent>(this) { onKeyPressed(it.code) }
    }

    private fun onKeyPressed(code: Int) {
        when (code) {
            Input.Keys.ESCAPE -> back()
        }
    }

    override fun rootActor() =
        scene2d.table {
            defaults().growX().width(150.0f)

            row()
            textButton(text = "Resume") {
                onClick { back() }
            }
            row()
            textButton(text = "Save") {
                onClick {
                    val payload = SavedGamesScreenPayload(SAVE)
                    screenManager.pushScreen(SavedGamesScreen::class.java, payload)
                }
            }
            row()
            textButton(text = "Load") {
                onClick {
                    val payload = SavedGamesScreenPayload(LOAD)
                    screenManager.pushScreen(SavedGamesScreen::class.java, payload)
                }
            }
            row()
            textButton(text = "Exit") {
                onClick {
                    send(ExitGameCommand())
                }
            }

            setFillParent(true)
            align(Align.center)
            pack()
        }

    private fun back() {
        screenManager.popScreen()
    }
}