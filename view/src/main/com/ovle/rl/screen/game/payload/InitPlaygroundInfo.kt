package com.ovle.rl.screen.game.payload

import com.ovle.rl.model.procgen.config.LocationTemplate

class InitPlaygroundInfo(val locationTemplate: LocationTemplate)