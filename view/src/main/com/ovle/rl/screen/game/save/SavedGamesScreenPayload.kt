package com.ovle.rl.screen.game.save

data class SavedGamesScreenPayload(
    val mode: SavedGamesScreenMode
)