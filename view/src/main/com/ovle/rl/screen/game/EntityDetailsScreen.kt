package com.ovle.rl.screen.game

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.util.markEntity
import com.ovle.rl.screen.BaseScreen
import com.ovle.rl.screen.ScreenData
import com.ovle.rl.screen.ScreenManager
import com.ovle.rl.view.game.scene2d.EntityPortraitWidget
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.*

class EntityDetailsScreen(
    screenManager: ScreenManager,
    screenData: ScreenData,
) : BaseScreen(screenManager, screenData) {

    private lateinit var mainEntityInfo: Label
    private lateinit var detailEntityInfo: Label

    private val entityPortrait = EntityPortraitWidget(32.0f)


    override fun show() {
        super.show()

        subscribe<KeyPressedEvent>(this) { onKeyPressed(it.code) }

        entityPortrait.setPortrait(entity())
        mainEntityInfo.setText(mainInfo(entity()))
        detailEntityInfo.setText(detailInfo(entity()))
    }

    private fun onKeyPressed(code: Int) {
        when (code) {
            Input.Keys.ESCAPE -> back()
        }
    }

    override fun rootActor() =
        scene2d.table {
            row().growX().padTop(5.0f).padBottom(5.0f)

            scrollPane {
                table {
                    row().height(32.0f).expandX().padBottom(5.0f)
                    entityPortrait.addActor(this)

                    row().expandX().padBottom(5.0f)
                    mainEntityInfo = label("", "title") {
                        it.center().fillX()
                    }

                    row().expandX().colspan(2).expandY()
                    detailEntityInfo = label("") {
                        it.fillX().top()
                    }
                }
            }

            row()
            textButton(text = "Back") {
                onClick { back() }
            }

            setFillParent(true)
            align(Align.center)
            pack()
        }

    private fun back() {
        screenManager.popScreen()
    }


    private fun entity() = (payload as EntityDetailsPayload).entity

    private fun mainInfo(entity: Entity) = entity.name()

    private fun detailInfo(entity: Entity): String {
        val result = StringBuilder()
        val nodes = entity.components.mapNotNull { (it as EntityComponent).gameInfo() }
        fillDetailInfo(nodes, result, "")

        return result.toString()
    }

    private fun fillDetailInfo(nodes: Collection<GameInfoNode>, temp: StringBuilder, prefix: String) {
        nodes.forEach {
            temp.append(prefix).append(it.text).append("\n")
            fillDetailInfo(it.children, temp, "$prefix  ")
        }
    }
}