package com.ovle.rl.screen.game.save

enum class SavedGamesScreenMode(
    val screenTitle: String,
    val actionBtnTitle: String,
) {
    SAVE("Save game", "Save"),
    LOAD("Load game", "Load"),
}