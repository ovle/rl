package com.ovle.rl.screen.game.result

import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.game.FinishGameResults

data class GameResultPayload(
    val game: GameEntity,
    val results: FinishGameResults
)