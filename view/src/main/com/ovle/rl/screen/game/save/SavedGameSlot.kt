package com.ovle.rl.screen.game.save

import com.ovle.rl.persistence.SavedGameMetadata

data class SavedGameSlot(
    val meta: SavedGameMetadata? = null
) {
    val slotName: String
        get() = meta?.name ?: "new game"
}