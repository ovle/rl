package com.ovle.rl.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.utils.Layout
import com.ovle.rl.controls.gdx.EventInputAdapter
import com.ovle.utils.event.EventBus
import ktx.app.KtxScreen


abstract class BaseScreen(
    protected val screenManager: ScreenManager,
    protected val screenData: ScreenData
): KtxScreen {

    var payload: Any? = null

    protected val stage = Stage(screenData.stageViewport)
    protected var rootActor: Actor? = null
    private val controls = EventInputAdapter(screenData.batchViewport)

    protected open val unsubscribeOnHide: Boolean
        get() = true

    override fun show() {
        super.show()

        if (rootActor == null) {
            rootActor = rootActor()
            stage.addActor(rootActor)
        }

        val inputMultiplexer = InputMultiplexer(stage)
        controls.let { inputMultiplexer.addProcessor(it) }
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun hide() {
        super.hide()

        stage.clear()
        rootActor = null

        if (unsubscribeOnHide) {
            EventBus.clearSubscriptions(this)
        }
    }

    abstract fun rootActor(): Actor

    override fun render(delta: Float) {
        super.render(delta)

        clear()
        updateSpriteBatch()

        renderBeforeStage(delta)
        drawStage(delta)
        renderAfterStage(delta)
    }

    open fun renderBeforeStage(delta: Float) {}
    open fun renderAfterStage(delta: Float) {}

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)

        screenData.batchViewport.update(width, height)
        stage.viewport.update(width, height, true)

        if (rootActor is Layout) {
            (rootActor as Layout).invalidate()
        }
    }

    override fun dispose() {
        stage.dispose() //todo?

        super.dispose()
    }


    private fun clear() {
        val bgColor = screenData.paletteManager.bgColor
        Gdx.gl.glClearColor(bgColor.r, bgColor.g, bgColor.b, bgColor.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    }

    private fun updateSpriteBatch() {
        val batchViewport = screenData.batchViewport
        batchViewport.apply()
        screenData.batch.projectionMatrix = batchViewport.camera.combined
    }

    private fun drawStage(delta: Float) {
        with(stage) {
            viewport.apply()
            act(delta)
            draw()
        }
    }
}
