package com.ovle.rl.screen

import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.utils.Align
import com.ovle.rl.controls.gdx.KeyPressedEvent
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.screen.game.GameScreen
import com.ovle.rl.screen.game.payload.InitPlaygroundInfo
import com.ovle.rl.view.game.scene2d.list.ListItemsWidget
import com.ovle.utils.event.EventBus.subscribe
import ktx.actors.onClick
import ktx.scene2d.*


class SelectPlaygroundScreen(
    screenManager: ScreenManager,
    screenData: ScreenData,
    private val playgroundTemplates: Collection<LocationTemplate>
) : BaseScreen(screenManager, screenData) {

    private lateinit var itemsWidget: ListItemsWidget<LocationTemplate>
    private lateinit var loadButton: Button


    override fun show() {
        itemsWidget = ListItemsWidget(
            getItemInfo = ::itemInfo,
            onSelectionChange = ::onSelectionChange
        )
        itemsWidget.setItems(playgroundTemplates.toTypedArray())

        super.show()

        subscribe<KeyPressedEvent>(this) { onKeyPressed(it.code) }
    }

    override fun rootActor() =
        scene2d.table {
            row()
            label(text = "Select playground", "title") {
                this.setAlignment(Align.center)
                it.growX()
                it.padTop(10.0f).padBottom(10.0f)
            }

            itemsWidget.addActor(this)

            row()
            horizontalGroup {
                it.padBottom(10.0f)

                loadButton = textButton(text = "Play") {
                    onClick { onPlayBtnClick(itemsWidget.selectedListItem) }
                }
                textButton(text = "Back") {
                    onClick { onBackBtnClick() }
                }
            }

            setFillParent(true)
            align(Align.center)
            pack()
        }


    private fun onKeyPressed(code: Int) {
        when (code) {
            Input.Keys.ESCAPE -> screenManager.popScreen()
            Input.Keys.ENTER -> onPlayBtnClick(itemsWidget.selectedListItem)
        }
    }

    private fun onPlayBtnClick(locationTemplate: LocationTemplate?) {
        locationTemplate ?: return

        val payload = InitPlaygroundInfo(locationTemplate)
        screenManager.pushScreen(GameScreen::class.java, payload)
    }

    private fun onSelectionChange(locationTemplate: LocationTemplate?) {
        updateGUI(locationTemplate)
    }

    private fun onBackBtnClick() {
        screenManager.popScreen()
    }

    private fun updateGUI(locationTemplate: LocationTemplate?) {
        rootActor ?: return

        val isHaveSelection = locationTemplate != null
        loadButton.isDisabled = !isHaveSelection
    }

    private fun itemInfo(locationTemplate: LocationTemplate) = locationTemplate.name
}