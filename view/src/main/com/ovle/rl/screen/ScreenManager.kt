package com.ovle.rl.screen

interface ScreenManager {
    fun <Type : BaseScreen> pushScreen(type: Class<Type>, payload: Any? = null)
    fun popScreen()
    fun popToBaseScreen()
    fun <Type : BaseScreen> setBaseScreen(type: Class<Type>, payload: Any? = null)
}