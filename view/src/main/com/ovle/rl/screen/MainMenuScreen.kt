package com.ovle.rl.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Scaling
import com.ovle.rl.assets.RlAssetsManager
import com.ovle.rl.screen.game.save.SavedGamesScreen
import com.ovle.rl.screen.game.save.SavedGamesScreenMode.LOAD
import com.ovle.rl.screen.game.save.SavedGamesScreenPayload
import ktx.actors.onClick
import ktx.math.vec2
import ktx.scene2d.*


class MainMenuScreen(
    screenManager: ScreenManager,
    private val assetsManager: RlAssetsManager,
    screenData: ScreenData
) : BaseScreen(screenManager, screenData) {

    companion object {
        private const val LOGO_SCALE_COEF = 4.0f
        private val LOGO_SIZE = vec2(84.0f, 40.0f)
    }

    override fun rootActor() =
        scene2d.table {
            row()
            image(assetsManager.logo) {
                it.width(LOGO_SIZE.x * LOGO_SCALE_COEF)
                    .height(LOGO_SIZE.y * LOGO_SCALE_COEF)
                    .padBottom(24.0f)

                setScaling(Scaling.fit)
            }
            row()
            textButton(text = "New game") {
                it.width(200.0f)
                onClick { screenManager.pushScreen(WorldScreen::class.java) }
            }
            row()
            textButton(text = "Playground") {
                it.width(200.0f)
                onClick { screenManager.pushScreen(SelectPlaygroundScreen::class.java) }
            }
            row()
            textButton(text = "Load game") {
                it.width(200.0f)
                onClick {
                    val payload = SavedGamesScreenPayload(LOAD)
                    screenManager.pushScreen(SavedGamesScreen::class.java, payload)
                }
            }
            row()
            textButton(text = "Options") {
                this.isDisabled = true
                it.width(200.0f)
            }
            row()
            textButton(text = "Exit") {
                it.width(200.0f)
                onClick { Gdx.app.exit() }
            }

            setFillParent(true)
            align(Align.center)
            pack()
        }
}