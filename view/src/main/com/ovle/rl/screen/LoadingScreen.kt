package com.ovle.rl.screen

import com.badlogic.gdx.graphics.g2d.Batch
import com.ovle.rl.assets.RlAssetsManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ktx.async.KTX
import ktx.async.KtxAsync
import ktx.async.newSingleThreadAsyncContext
import ktx.scene2d.label
import ktx.scene2d.scene2d
import ktx.scene2d.verticalGroup


class LoadingScreen(
    screenManager: ScreenManager,
    private val assetsManager: RlAssetsManager,
    screenData: ScreenData
): BaseScreen(screenManager, screenData) {

    override fun show() {
        super.show()

        val executor = newSingleThreadAsyncContext()
        KtxAsync.launch(executor) {
            withContext(Dispatchers.KTX) {
                assetsManager.load()
                screenManager.setBaseScreen(MainMenuScreen::class.java)
            }
        }
    }

    override fun rootActor() = scene2d
        .verticalGroup {
            label(text = "Loading...") {}
            setPosition(
                screenData.batchViewport.screenWidth / 2.0f,
                screenData.batchViewport.screenHeight / 2.0f
            )
            center()
            pack()
        }
}