package com.ovle.rl.screen

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapRenderer
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.assets.RlAssetsManager
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.config.WorldTemplate
import com.ovle.rl.model.procgen.grid.world.World
import com.ovle.rl.model.procgen.grid.world.WorldFactory
import com.ovle.rl.screen.game.GameScreen
import com.ovle.rl.screen.game.payload.InitGameInfo
import com.ovle.rl.view.TEXTURE_TILE_SIZE
import com.ovle.rl.view.TILE_SIZE
import com.ovle.rl.view.game.render.draw
import com.ovle.rl.view.game.render.tileMap.WorldTiledMapFactory
import com.ovle.rl.view.game.scene2d.stack
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.adj
import com.ovle.utils.gdx.math.point.adjD
import com.ovle.utils.gdx.math.point.adjHV
import com.ovle.utils.gdx.math.point.allBorders
import com.ovle.utils.gdx.view.PaletteManager
import com.ovle.utils.gdx.view.textureRegion
import com.ovle.utils.gdx.view.textureRegions
import ktx.math.vec2
import ktx.scene2d.scene2d


class WorldScreen(
    screenManager: ScreenManager,
    private val assetsManager: RlAssetsManager,
    paletteManager: PaletteManager,
    screenData: ScreenData,
    private val worldTemplate: WorldTemplate,
    private val worldFactory: WorldFactory,
    private val worldLocationTemplate: LocationTemplate,
    private val tiledMapFactory: WorldTiledMapFactory,
) : BaseScreen(screenManager, screenData) {

    companion object {
        const val DEFAULT_WORLD_SEED = 1704581439L
        const val DEFAULT_WORLD_NAME = "new world"
        const val CURSOR_AREA_SIZE = 3

        private const val CAMERA_ZOOM = 1.75f
        private val CAMERA_POSITION = vec2(750.0f, 560.0f)
    }

    private lateinit var world: World

    private val regions by lazy { textureRegions(
        assetsManager.gameTexture,
        paletteManager,
        TEXTURE_TILE_SIZE
    ) }
    private val cursorSpriteLight by lazy { textureRegion(regions, 15, 22) }
    private val cursorSpriteDark by lazy { textureRegion(regions, 8, 21) }
    private val selectedRegionSprite by lazy { textureRegion(regions, 15, 23) }

    private val placeSprite by lazy { textureRegion(regions, 15, 22) }
    private val placeBorderSprite by lazy { textureRegion(regions, 14, 23) }

    private var mapRenderer: TiledMapRenderer? = null
    private var tiledMap: TiledMap? = null

    private val worldGUIWidget = WorldGUIWidget(
        screenManager,
        { world }, ::generateWorld, ::embark
    )


    override fun show() {
        world = world(DEFAULT_WORLD_SEED)

        super.show()

        val camera = orthographicCamera()
        camera.zoom = CAMERA_ZOOM
        camera.position.set(CAMERA_POSITION.x, CAMERA_POSITION.y, 0.0f)

        initMap()
    }

    override fun hide() {
        worldGUIWidget.removeActor()
        super.hide()
    }

    override fun rootActor() = scene2d.stack {
        setFillParent(true)
        worldGUIWidget.addActor(this)
    }

    override fun renderAfterStage(delta: Float) {
        mapRenderer?.let {
            it.setView(orthographicCamera())
            it.render()
        }

        renderWorldPlaces()
        renderInteractionInfo()
    }


    private fun renderWorldPlaces() {
        val batch = screenData.batch
        batch.begin()
        world.places.forEach {
            val point = it.point
            batch.draw(point, placeSprite)

            point.adj().forEach { a ->
                batch.draw(a, placeBorderSprite)
            }
        }
        batch.end()
    }

    private fun renderInteractionInfo() {
        val batch = screenData.batch
        batch.begin()

        worldGUIWidget.cursorPoint?.let {
            it.adjHV().forEach { p -> batch.draw(p, cursorSpriteDark) }
            it.adjD().forEach { p -> batch.draw(p, cursorSpriteLight) }
        }

        worldGUIWidget.selectionPoint.let {
            it.adjHV().forEach { p -> batch.draw(p, cursorSpriteLight) }
            it.adjD().forEach { p -> batch.draw(p, cursorSpriteDark) }
        }

        worldGUIWidget.selectedRegion?.let {
            it.points.allBorders()
                .forEach { p -> batch.draw(p, selectedRegionSprite) }
        }

        batch.end()
    }

    private fun generateWorld(seed: Long, name: String) {
        world = world(seed).apply { this.name = name }

        initMap()
    }

    private fun embark(selectionPoint: GridPoint2) {
        val payload = InitGameInfo(world, selectionPoint, worldLocationTemplate)
        screenManager.pushScreen(GameScreen::class.java, payload)
    }

    private fun initMap() {
        tiledMap = tiledMapFactory.tiledMap(world.tiles!!, world.grids, regions, TILE_SIZE)
        mapRenderer = OrthogonalTiledMapRenderer(tiledMap)
    }


    private fun world(seed: Long): World {
        return worldFactory.get(worldTemplate, RandomParams(seed))
    }

    private fun orthographicCamera() = screenData.batchViewport.camera as OrthographicCamera
}
