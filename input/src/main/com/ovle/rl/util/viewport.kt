package com.ovle.rl.util

import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2
import com.ovle.utils.gdx.math.point.point

private const val VIEWPORT_TO_GAME_SIZE = 8


fun Vector2.viewportToGame(): GridPoint2 = point(x / VIEWPORT_TO_GAME_SIZE, y / VIEWPORT_TO_GAME_SIZE)

fun GridPoint2.gameToViewport(): Vector2 = ktx.math.vec2(
    x * VIEWPORT_TO_GAME_SIZE.toFloat(),
    y * VIEWPORT_TO_GAME_SIZE.toFloat()
)
