package com.ovle.rl.util

import com.ovle.rl.GameEntity
import com.ovle.rl.interaction.Components.interaction
import ktx.ashley.get

fun GameEntity.interaction() = this[interaction]!!
fun GameEntity.focusedEntity() = this.interaction().entityInteraction.focusedEntity
fun GameEntity.hoveredEntity() = this.interaction().entityInteraction.hoveredEntity
fun GameEntity.selectedEntity() = this.interaction().entityInteraction.selectedEntity