package com.ovle.rl.interaction.type.area

import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.area.AreaTemplate

class AreaInteraction(
    var selectedAreaTemplate: AreaTemplate? = null,
    var selectedArea: AreaInfo? = null,
    var params: AreaParams? = null
)