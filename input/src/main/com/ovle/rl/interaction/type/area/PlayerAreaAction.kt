package com.ovle.rl.interaction.type.area

import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.area.DeleteAreaCommand
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.utils.event.EventBus.send

enum class PlayerAreaAction(
    val displayName: String, val action: (AreaInfo, LocationPlayer, GameEntity) -> Unit
) {

    DELETE("delete", { a, lp, _ ->
        send(DeleteAreaCommand(a, lp))
    }),
}