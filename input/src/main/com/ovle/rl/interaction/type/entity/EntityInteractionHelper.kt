package com.ovle.rl.interaction.type.entity

import com.badlogic.ashley.core.Entity
import com.ovle.rl.interaction.EntityDeselectedEvent
import com.ovle.rl.interaction.EntitySelectedEvent
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.utils.event.EventBus.send

class EntityInteractionHelper {

    fun selectEntity(entity: Entity, interaction: PlayerInteractionComponent) {
        interaction.entityInteraction.selectedEntity = entity

        send(EntitySelectedEvent(entity))
    }

    fun deselectEntity(interaction: PlayerInteractionComponent) {
        val entity = interaction.entityInteraction.selectedEntity
        interaction.entityInteraction.selectedEntity = null

        entity?.let {
            send(EntityDeselectedEvent(entity))
        }
    }
}