package com.ovle.rl.interaction.type.keeper

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.model.game.skill.UseSkillCommand
import com.ovle.rl.model.game.skill.helper.ValidPlayerSkillTargetsHelper
import com.ovle.rl.model.game.skill.dto.SkillUsage
import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send


class PlayerSkillInteractionHelper(
    private val validPlayerSkillTargetsHelper: ValidPlayerSkillTargetsHelper
) {

    fun checkSkill(point: GridPoint2, interaction: PlayerInteractionComponent, game: Entity) {
        val keeperInteraction = interaction.playerSkillInteraction
        val skill = keeperInteraction.selectedSkill ?: return
        //todo if can be used at all

        val location = game.location()
        val validTarget = validPlayerSkillTargetsHelper.validTarget(skill, point, location)

        if (validTarget != null) {
            val player = location.players.human
            val skillUsage = SkillUsage.Player(skill, validTarget, player)
            send(UseSkillCommand(skillUsage))
        }
    }
}