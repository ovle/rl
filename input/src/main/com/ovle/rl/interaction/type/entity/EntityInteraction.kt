package com.ovle.rl.interaction.type.entity

import com.badlogic.ashley.core.Entity

/**
 * @property focusedEntity      entity, which has the camera focus
 * @property hoveredEntity      entity under the cursor
 * @property selectedEntity     entity, which is currently selected by left-click, to have some interaction with
 */
class EntityInteraction(
    var focusedEntity: Entity? = null,
    var hoveredEntity: Entity? = null,
    var selectedEntity: Entity? = null,
)