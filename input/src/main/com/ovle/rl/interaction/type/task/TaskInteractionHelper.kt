package com.ovle.rl.interaction.type.task

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.GameEntity
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.task.CheckTaskCommand
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTargetCheck.Success
import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.gdx.math.points


class TaskInteractionHelper {

    fun checkTask(interaction: PlayerInteractionComponent, player: LocationPlayer) {
        val taskInteraction = interaction.taskInteraction
        val targetSelection = taskInteraction.targetSelection!!
        val template = taskInteraction.selectedTaskTemplate!!

        send(CheckTaskCommand(targetSelection, template, player))
        targetSelection.reset()
    }

    fun checkTask(point: GridPoint2, interaction: PlayerInteractionComponent, game: GameEntity) {
        val player = game.location().players.human
        val taskInteraction = interaction.taskInteraction
        val selectedTemplate = taskInteraction.selectedTaskTemplate

        val existingTasks = player.tasks
        val selectedTask = existingTasks.find {
            (selectedTemplate == null || it.template == selectedTemplate)
                && point in it.target.positions()
        }

        when {
            selectedTask != null -> processSelectedTask(selectedTask, taskInteraction)
            selectedTemplate != null -> processSelectedTaskTemplate(selectedTemplate, interaction, point, game)
            else -> { }
        }
    }


    private fun processSelectedTask(
        task: TaskInfo?, taskInteraction: TaskInteraction
    ) {
        taskInteraction.selectedTask =
            if (taskInteraction.selectedTask == task) null
            else task
    }

    private fun processSelectedTaskTemplate(
        template: TaskTemplate, interaction: PlayerInteractionComponent, point: GridPoint2, game: GameEntity
    ) {
        val taskInteraction = interaction.taskInteraction
        val targetSelection = taskInteraction.targetSelection!!

        val points = interaction.selectionRectangle?.points() ?: setOf(point)
        val entities = game.location().content.entities.on(points)
        targetSelection.updatePoints(points)
        targetSelection.updateEntities(entities)

        if (targetSelection.check() == Success) {
            val player = game.location().players.human
            send(CheckTaskCommand(targetSelection, template, player))

            targetSelection.reset()
        }
    }
}