package com.ovle.rl.interaction.type.task

import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.task.CancelTaskCommand
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.utils.event.EventBus.send

enum class PlayerTaskAction(
    val displayName: String, val action: (TaskInfo, LocationPlayer, GameEntity) -> Unit
) {
    CANCEL("cancel", { t, lp, _ ->
        send(CancelTaskCommand(t, lp))
    }),
}