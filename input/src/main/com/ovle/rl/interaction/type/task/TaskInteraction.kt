package com.ovle.rl.interaction.type.task

import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.selection.TaskTargetSelection

class TaskInteraction(
    var selectedTask: TaskInfo? = null,
    var selectedTaskTemplate: TaskTemplate? = null,
    var targetSelection: TaskTargetSelection? = null
)