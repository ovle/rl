package com.ovle.rl.interaction.type.entity

import com.badlogic.ashley.core.Entity
import com.ovle.rl.GameEntity
import com.ovle.rl.controls.FocusEntityCommand
import com.ovle.rl.model.game.time.ShowEntityDetailsCommand
import com.ovle.utils.event.EventBus.send

enum class PlayerEntityAction(val displayName: String, val action: (Entity, GameEntity) -> Unit) {

    FOCUS("focus", { e, _ ->
        send(FocusEntityCommand(e))
    }),
    DETAILS("details", { e, _ ->
        send(ShowEntityDetailsCommand(e))
    }),
}