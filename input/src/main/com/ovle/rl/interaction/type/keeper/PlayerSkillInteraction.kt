package com.ovle.rl.interaction.type.keeper

import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate

class PlayerSkillInteraction(
    var selectedSkill: PlayerSkillTemplate? = null
)