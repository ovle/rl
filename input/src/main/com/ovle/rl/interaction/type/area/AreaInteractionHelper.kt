package com.ovle.rl.interaction.type.area

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.GameEntity
import com.ovle.rl.interaction.PlayerInteractionComponent
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.area.CreateAreaCommand
import com.ovle.rl.model.game.area.MergeAreasCommand
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.gdx.math.points


class AreaInteractionHelper {

    fun checkArea(point: GridPoint2, interaction: PlayerInteractionComponent, game: GameEntity) {
        val areaInteraction = interaction.areaInteraction
        val selectedAreaTemplate = areaInteraction.selectedAreaTemplate
        val selection = interaction.selectionRectangle
        val selectionPoints = selection?.points()
        val params = areaInteraction.params
        val player = game.location().players.human
        val existingAreas = player.areas

        val sameSelectionAreas = existingAreas
            .filter {
                it.template == selectedAreaTemplate
                    && it.params.equalsByParams(params)
                    && (selectionPoints?.intersect(it.area.toSet())?.isNotEmpty() ?: false)
            }
        val pointAreas = existingAreas.filter { point in it.area }

        when {
            sameSelectionAreas.isNotEmpty() ->
                processMergeAreas(sameSelectionAreas, interaction, player)
            selection != null && selectedAreaTemplate != null ->
                processNewArea(interaction, player)
            pointAreas.isNotEmpty() ->
                processSelectedArea(pointAreas, areaInteraction)
            else -> { }
        }
    }


    private fun processMergeAreas(
        areas: List<AreaInfo>, interaction: PlayerInteractionComponent, player: LocationPlayer.Human
    ) {
        val areaInteraction = interaction.areaInteraction
        val selectedAreaTemplate = areaInteraction.selectedAreaTemplate ?: return
        val selection = interaction.selectionRectangle ?: return
        val params = areaInteraction.params ?: return

        send(MergeAreasCommand(selection, selectedAreaTemplate, params, areas, player))
    }

    private fun processNewArea(
        interaction: PlayerInteractionComponent,
        player: LocationPlayer.Human
    ) {
        val areaInteraction = interaction.areaInteraction
        val selectedAreaTemplate = areaInteraction.selectedAreaTemplate ?: return
        val selection = interaction.selectionRectangle ?: return
        val params = areaInteraction.params ?: return

        send(CreateAreaCommand(selection, selectedAreaTemplate, params, player))
    }

    private fun processSelectedArea(areas: List<AreaInfo>, areaInteraction: AreaInteraction) {
        val selectedArea = areaInteraction.selectedArea
        val selectedAreaIndex = areas.indexOf(selectedArea)
        var nextIndex = selectedAreaIndex + 1
        if (nextIndex == areas.size) {
            nextIndex = 0
        }
        val newSelectedArea = areas[nextIndex]

        areaInteraction.selectedArea =
            if (areaInteraction.selectedArea == newSelectedArea) null
            else newSelectedArea
    }
}