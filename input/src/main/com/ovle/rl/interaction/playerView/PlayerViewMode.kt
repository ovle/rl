package com.ovle.rl.interaction.playerView

enum class PlayerViewMode {
    TASKS,
    AREAS,
    MOVE_MAP,
    ACCESSIBILITY_MAP,
    FOV,
    FPS,
    NO_FOW,
    //todo all areas
}