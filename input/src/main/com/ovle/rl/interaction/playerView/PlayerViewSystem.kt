package com.ovle.rl.interaction.playerView

import com.ovle.rl.interaction.SwitchViewModeCommand
import com.ovle.rl.model.game.core.system.BaseSystem

import com.ovle.rl.util.interaction
import com.ovle.utils.event.EventBus.subscribe


class PlayerViewSystem : BaseSystem() {

    override fun subscribe() {
        subscribe<SwitchViewModeCommand>(this) { onSwitchViewModeEvent(it.viewMode) }
    }

    private fun onSwitchViewModeEvent(viewMode: PlayerViewMode) {
        val viewConfig = viewConfig()
        val viewModes = viewConfig.viewModes
        if (viewMode in viewModes) {
            viewModes -= viewMode
        } else {
            viewModes += viewMode
        }
    }

    private fun viewConfig() = game().interaction().viewConfig
}
