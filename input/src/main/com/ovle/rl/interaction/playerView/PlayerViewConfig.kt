package com.ovle.rl.interaction.playerView

import com.ovle.rl.interaction.playerView.PlayerViewMode.*

class PlayerViewConfig(
    val viewModes: MutableCollection<PlayerViewMode> = mutableSetOf(NO_FOW)
) {
    val isViewMaskEnabled: Boolean
        get() = NO_FOW !in viewModes
}