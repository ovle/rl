package com.ovle.rl.interaction

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.interaction.type.entity.EntityInteractionHelper
import com.ovle.rl.interaction.playerView.PlayerViewSystem
import com.ovle.rl.interaction.type.area.AreaInteractionHelper
import com.ovle.rl.interaction.type.keeper.PlayerSkillInteractionHelper
import com.ovle.rl.interaction.type.task.TaskInteractionHelper
import com.ovle.rl.model.game.core.component.GlobalComponent
import org.kodein.di.*


val interactionModule = DI.Module("interaction") {
    bind<PlayerSkillInteractionHelper>() with singleton {
        PlayerSkillInteractionHelper(instance())
    }
    bind<EntityInteractionHelper>() with singleton {
        EntityInteractionHelper()
    }
    bind<TaskInteractionHelper>() with singleton {
        TaskInteractionHelper()
    }
    bind<AreaInteractionHelper>() with singleton {
        AreaInteractionHelper()
    }

    bind<EntitySystem>().inSet() with singleton {
        PlayerInteractionSystem(instance(), instance(), instance(), instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        PlayerViewSystem()
    }

    bind<GlobalComponent>().inSet() with provider {
        PlayerInteractionComponent()
    }
}