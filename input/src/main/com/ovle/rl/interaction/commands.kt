package com.ovle.rl.interaction

import com.badlogic.ashley.core.Entity
import com.ovle.rl.interaction.playerView.PlayerViewMode
import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.utils.event.Event

class SwitchPlayerControlModeCommand(val controlMode: PlayerControlMode) : GameCommand()
class SwitchViewModeCommand(val viewMode: PlayerViewMode) : Event()

class SelectEntityCommand(val entity: Entity) : GameEvent()
class DeselectEntityCommand : GameEvent()