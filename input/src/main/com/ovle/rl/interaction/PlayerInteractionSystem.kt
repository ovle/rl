package com.ovle.rl.interaction

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.ovle.rl.GameEntity
import com.ovle.rl.controls.*
import com.ovle.rl.controls.gdx.CameraMoveCommand
import com.ovle.rl.interaction.Components.interaction
import com.ovle.rl.interaction.PlayerControlMode.*
import com.ovle.rl.interaction.type.area.AreaInteractionHelper
import com.ovle.rl.interaction.type.entity.EntityInteractionHelper
import com.ovle.rl.interaction.type.keeper.PlayerSkillInteractionHelper
import com.ovle.rl.interaction.type.task.TaskInteractionHelper
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.EntityDestroyedEvent
import com.ovle.rl.model.game.game.GameLoadedEvent
import com.ovle.rl.model.util.location

import com.ovle.rl.util.interaction
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get


class PlayerInteractionSystem(
    private val playerSkillInteractionHelper: PlayerSkillInteractionHelper,
    private val entityInteractionHelper: EntityInteractionHelper,
    private val taskInteractionHelper: TaskInteractionHelper,
    private val areaInteractionHelper: AreaInteractionHelper,
) : BaseSystem() {

    override fun subscribe() {
        subscribe<GameLoadedEvent>(this) { onGameLoadedEvent(it.game) }
        subscribe<SwitchPlayerControlModeCommand>(this) { onSwitchControlModeEvent(it.controlMode) }

        subscribe<SelectionChangedEvent>(this) { onSelectionChangedEvent(it.selection, it.diff) }
        subscribe<SelectionResetEvent>(this) { onSelectionResetEvent() }

        subscribe<SelectEntityCommand>(this) { onSelectEntityCommand(it.entity) }
        subscribe<DeselectEntityCommand>(this) { onDeselectEntityCommand() }
        subscribe<TileClickEvent>(this) { onClickEvent(it.point) }
        subscribe<EntityClickEvent>(this) { onEntityClickEvent(it.entity) }
        subscribe<NonEntityClickEvent>(this) { onNonEntityClickEvent() }
        subscribe<EntityHoverEvent>(this) { onEntityHoverEvent(it.entity) }
        subscribe<EntityUnhoverEvent>(this) { onEntityUnhoverEvent() }

        subscribe<EntityDestroyedEvent>(this) { onEntityDestroyedEvent(it.entity) }

        subscribe<TaskApprovedEvent>(this) { onTaskApprovedEvent() }
    }


    private fun onGameLoadedEvent(game: GameEntity) {
        if (game[interaction] == null) {
            game.add(PlayerInteractionComponent())
        }
    }

    private fun onSwitchControlModeEvent(controlMode: PlayerControlMode) {
        val interaction = game().interaction()
        interaction.controlMode = controlMode
        interaction.selectionRectangle = null
    }

    private fun onSelectionChangedEvent(selection: Rectangle, diff: Vector2) {
        val interaction = game().interaction()
        fun fallback() = send(CameraMoveCommand(diff))

        when (interaction.controlMode) {
            PLAYER -> fallback()
            VIEW -> fallback()
            TASKS -> {
                val taskInteraction = interaction.taskInteraction
                val selectedTask = taskInteraction.selectedTaskTemplate
                if (selectedTask == null) {
                    fallback()
                    return
                }

                val isMultiselect = taskInteraction.targetSelection?.isMultiselect ?: false
                if (!isMultiselect) return

                interaction.selectionRectangle = selection
            }
            AREAS -> {
                interaction.selectionRectangle = selection
            }
        }
    }

    private fun onSelectionResetEvent() {
        with(game().interaction()) {
            selectionRectangle = null
        }
    }

    private fun onSelectEntityCommand(entity: Entity) {
        val interaction = game().interaction()
        entityInteractionHelper.selectEntity(entity, interaction)
    }

    private fun onDeselectEntityCommand() {
        val interaction = game().interaction()
        entityInteractionHelper.deselectEntity(interaction)
    }

    private fun onEntityHoverEvent(entity: Entity) {
        val entityInteraction = game().interaction().entityInteraction
        entityInteraction.hoveredEntity = entity
    }

    private fun onEntityUnhoverEvent() {
        val entityInteraction = game().interaction().entityInteraction
        entityInteraction.hoveredEntity = null
    }

    private fun onTaskApprovedEvent() {
        val game = game()
        val interaction = game.interaction()
        val player = game.location().players.human
        when (interaction.controlMode) {
            TASKS -> {
                taskInteractionHelper.checkTask(interaction, player)
            }
            else -> {}
        }
    }

    private fun onClickEvent(point: GridPoint2) {
        val game = game()
        val interaction = game.interaction()
        when (interaction.controlMode) {
            PLAYER -> playerSkillInteractionHelper.checkSkill(point, interaction, game)
            VIEW -> {}
            TASKS -> taskInteractionHelper.checkTask(point, interaction, game)
            AREAS -> areaInteractionHelper.checkArea(point, interaction, game)
        }
    }

    private fun onEntityClickEvent(entity: Entity) {
        val interaction = game().interaction()
        when (interaction.controlMode) {
            PLAYER, AREAS -> {}
            VIEW, TASKS -> {
                entityInteractionHelper.selectEntity(entity, interaction)
            }
        }
    }

    private fun onNonEntityClickEvent() {
        val interaction = game().interaction()
        interaction.entityInteraction.focusedEntity = null

        when (interaction.controlMode) {
            PLAYER, AREAS -> {}
            VIEW, TASKS -> {
                entityInteractionHelper.deselectEntity(interaction)
            }
        }
    }

    private fun onEntityDestroyedEvent(entity: Entity) {
        val interaction = game().interaction()

        with(interaction.entityInteraction) {
            if (selectedEntity == entity) selectedEntity = null
            if (focusedEntity == entity) focusedEntity = null
            if (hoveredEntity == entity) hoveredEntity = null
        }
    }
}