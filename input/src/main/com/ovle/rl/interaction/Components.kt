package com.ovle.rl.interaction

import com.ovle.utils.gdx.ashley.component.mapper

object Components {
    val interaction = mapper<PlayerInteractionComponent>()
}