package com.ovle.rl.interaction

enum class PlayerControlMode(val displayName: String) {
    PLAYER("Me"),
    VIEW("View"),
    TASKS("Task"),
    AREAS("Area")
}
