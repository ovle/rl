package com.ovle.rl.interaction

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.GameEvent


//class EntityInteractionCommand(val source: Entity, val target: Entity, val interaction: EntityInteraction) : GameEvent()

class EntitySelectedEvent(val entity: Entity) : GameEvent()

class EntityDeselectedEvent(val entity: Entity) : GameEvent()