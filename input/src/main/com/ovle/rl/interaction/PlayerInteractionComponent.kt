package com.ovle.rl.interaction

import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle
import com.ovle.rl.interaction.playerView.PlayerViewConfig
import com.ovle.rl.interaction.type.area.AreaInteraction
import com.ovle.rl.interaction.type.entity.EntityInteraction
import com.ovle.rl.interaction.type.keeper.PlayerSkillInteraction
import com.ovle.rl.interaction.type.task.TaskInteraction
import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.utils.gdx.math.point.point

class PlayerInteractionComponent(
    var cursorPosition: GridPoint2 = point(0, 0),
    var controlMode: PlayerControlMode = PlayerControlMode.PLAYER,
    var selectionRectangle: Rectangle? = null,

    val viewConfig: PlayerViewConfig = PlayerViewConfig(),
    val entityInteraction: EntityInteraction = EntityInteraction(),
    val taskInteraction: TaskInteraction = TaskInteraction(),
    val areaInteraction: AreaInteraction = AreaInteraction(),
    val playerSkillInteraction: PlayerSkillInteraction = PlayerSkillInteraction(),
) : GlobalComponent() {

    override val isModel: Boolean
        get() = false
}