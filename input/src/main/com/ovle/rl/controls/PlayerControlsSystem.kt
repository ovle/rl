package com.ovle.rl.controls

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.ovle.rl.AppOptions
import com.ovle.rl.GameEntity
import com.ovle.rl.controls.gdx.*
import com.ovle.rl.controls.keys.KeyBinding
import com.ovle.rl.model.game.core.Components.core
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.perception.isVisibleForInteraction

import com.ovle.rl.model.util.location
import com.ovle.rl.util.interaction
import com.ovle.rl.util.viewportToGame
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import com.ovle.utils.gdx.math.rectangle
import ktx.ashley.get
import java.lang.Integer.max
import java.lang.Integer.min


class PlayerControlsSystem(
    private val keyBindings: Collection<KeyBinding>
) : BaseSystem() {

    override fun subscribe() {
        subscribe<MouseClickEvent>(this) { onMouseClick(it.viewportPoint.viewportToGame(), it.button) }
        subscribe<MouseLongClickEvent>(this) { onMouseLongClickEvent(it.viewportPoint.viewportToGame(), it.button) }
        subscribe<MouseMovedEvent>(this) { onMouseMoved(it.viewportPoint.viewportToGame()) }
        subscribe<DragEvent>(this) { onDragEvent(it.start, it.current, it.lastDiff) }
        subscribe<KeyPressedEvent>(this) { onKeyPressed(it.code) }
    }

    private fun onMouseClick(position: GridPoint2, button: Int) {
        val game = game()
        send(TileClickEvent(button, position))

        val entities = entitiesToInteract(game, position)
        when {
            entities.isNotEmpty() -> send(EntityClickEvent(button, entities.first()))
            else -> send(NonEntityClickEvent(button, position))
        }

        send(SelectionResetEvent())
    }

    private fun onMouseLongClickEvent(position: GridPoint2, button: Int) {
        val game = game()
        val entities = entitiesToInteract(game, position)
        if (entities.isNotEmpty()) {
            send(EntityLongClickEvent(button, entities.first()))
        }
    }

    private fun onMouseMoved(position: GridPoint2) {
        val game = game()
        val interaction = game.interaction()
        if (interaction.cursorPosition == position) return

        interaction.cursorPosition = position

        send(EntityUnhoverEvent())

        val entities = entitiesToInteract(game, position)
        entities.forEach {
            send(EntityHoverEvent(it))
        }
    }

    private fun onDragEvent(start: Vector2, current: Vector2, diff: Vector2) {
        val selection = selectionRectangle(start, current)
        val currentPoint = current.viewportToGame()
        send(SelectionChangedEvent(selection, currentPoint, diff))
    }

    private fun entitiesToInteract(game: GameEntity, position: GridPoint2): List<Entity> {
        val location = game.location()
        val locationProjection = location.players.human.locationProjection
        val isViewMaskEnabled = game.interaction().viewConfig.isViewMaskEnabled

        return location.content.entities.on(listOf(position))
            .filter { it.isExists()
                && !it[core]!!.isSkipped
                && (!isViewMaskEnabled || it.isVisibleForInteraction(locationProjection))
            }
    }

    private fun selectionRectangle(start: Vector2, current: Vector2): Rectangle {
        val (sx, sy) = start.viewportToGame()
        val (cx, cy) = current.viewportToGame()

        val xRange = (min(sx, cx)..max(sx, cx))
        val yRange = (min(sy, cy)..max(sy, cy))

        return rectangle(xRange.first, yRange.first, xRange.last, yRange.last)
    }

    private fun onKeyPressed(code: Int) {
        val keyBinding = keyBindings.filter { !it.isDebug || AppOptions.IS_DEBUG_MODE }
            .singleOrNull { it.key == code } ?: return

        val game = game()
        keyBinding.action.invoke(game)
    }
}