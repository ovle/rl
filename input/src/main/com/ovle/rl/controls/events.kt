package com.ovle.rl.controls

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.ovle.rl.controls.gdx.PlayerInputEvent
import com.ovle.rl.model.game.build.BuildTemplate

open class GamePlayerInputEvent : PlayerInputEvent()

class TileClickEvent(val button: Int, val point: GridPoint2) : GamePlayerInputEvent()
class NonEntityClickEvent(val button: Int, val point: GridPoint2) : GamePlayerInputEvent()
class EntityClickEvent(val button: Int, val entity: Entity) : GamePlayerInputEvent()
class EntityLongClickEvent(val button: Int, val entity: Entity) : GamePlayerInputEvent()
class EntityUnhoverEvent : GamePlayerInputEvent()
class EntityHoverEvent(val entity: Entity) : GamePlayerInputEvent()

class SelectionChangedEvent(val selection: Rectangle, val currentPoint: GridPoint2, val diff: Vector2) : GamePlayerInputEvent()
class SelectionResetEvent : GamePlayerInputEvent()

class TaskApprovedEvent : GamePlayerInputEvent()