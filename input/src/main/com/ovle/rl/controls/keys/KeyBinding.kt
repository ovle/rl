package com.ovle.rl.controls.keys

import com.ovle.rl.GameEntity

typealias Key = Int

class KeyBinding(
    val key: Key, val description: String, val isDebug: Boolean = false,
    val action: (GameEntity) -> Unit
)