package com.ovle.rl.controls.keys

import com.badlogic.gdx.Input.Keys.*
import com.ovle.rl.controls.CAMERA_MOVE_AMOUNT
import com.ovle.rl.controls.gdx.CameraMoveCommand
import com.ovle.rl.controls.gdx.CameraScaleDecCommand
import com.ovle.rl.controls.gdx.CameraScaleIncCommand
import com.ovle.rl.interaction.SwitchPlayerControlModeCommand
import com.ovle.rl.model.game.game.DestroyEntityCommand
import com.ovle.rl.model.game.life.EntityTakeDamageCommand
import com.ovle.rl.model.game.life.KillEntityCommand
import com.ovle.rl.model.game.life.ResurrectEntityCommand
import com.ovle.rl.model.game.space.ChangeEntityPositionCommand
import com.ovle.rl.model.game.time.ChangeGameSpeedCommand
import com.ovle.rl.model.game.time.ShowEntityDetailsCommand
import com.ovle.rl.model.game.time.ToggleGameMainMenuCommand
import com.ovle.rl.model.game.time.ToggleGamePauseCommand
import com.ovle.rl.util.hoveredEntity
import com.ovle.rl.util.interaction
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.next
import ktx.math.vec2

val keyBindings = listOf(
    KeyBinding(MINUS, "") { send(CameraScaleDecCommand()) },
    KeyBinding(EQUALS, "") { send(CameraScaleIncCommand()) },

    KeyBinding(LEFT, "") { send(CameraMoveCommand(vec2(-CAMERA_MOVE_AMOUNT, 0.0f))) },
    KeyBinding(RIGHT, "") { send(CameraMoveCommand(vec2(CAMERA_MOVE_AMOUNT, 0.0f))) },
    KeyBinding(UP, "") { send(CameraMoveCommand(vec2(0.0f, CAMERA_MOVE_AMOUNT))) },
    KeyBinding(DOWN, "") { send(CameraMoveCommand(vec2(0.0f, -CAMERA_MOVE_AMOUNT))) },

    KeyBinding(ESCAPE, "") { send(ToggleGameMainMenuCommand()) },
    KeyBinding(TAB, "") { g ->
        val i = g.interaction()
        val cm = i.controlMode.next()
        send(SwitchPlayerControlModeCommand(cm))
    },
    KeyBinding(I, "") { g ->
        val i = g.interaction()
        val se = i.entityInteraction.hoveredEntity ?: return@KeyBinding
        send(ShowEntityDetailsCommand(se))
    },

    KeyBinding(SPACE, "") { send(ToggleGamePauseCommand()) },
    KeyBinding(LEFT_BRACKET, "", isDebug = true) { send(ChangeGameSpeedCommand(0.5)) },
    KeyBinding(RIGHT_BRACKET, "", isDebug = true) { send(ChangeGameSpeedCommand(2.0)) },

    KeyBinding(T, "", isDebug = true) { g ->
        val e = g.hoveredEntity()
        val i = g.interaction()
        e?.let { send(ChangeEntityPositionCommand(it, i.cursorPosition)) }
    },
    KeyBinding(K, "", isDebug = true) { g ->
        val e = g.hoveredEntity()
        e?.let { send(KillEntityCommand(e)) }
    },
    KeyBinding(R, "", isDebug = true) { g ->
        val e = g.hoveredEntity()
        e?.let { send(ResurrectEntityCommand(e)) }
    },
    KeyBinding(D, "", isDebug = true) { g ->
        val e = g.hoveredEntity()
        e?.let { send(DestroyEntityCommand(e)) }
    },
    KeyBinding(S, "", isDebug = true) { g ->
        val e = g.hoveredEntity()
        e?.let { send(EntityTakeDamageCommand(e, null, 1)) }
    }
)