package com.ovle.rl.controls

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.controls.keys.KeyBinding
import com.ovle.rl.controls.keys.keyBindings
import org.kodein.di.*


val controlsModule = DI.Module("controls") {
    bind<EntitySystem>().inSet() with singleton {
        PlayerControlsSystem(instance())
    }

    bind<Collection<KeyBinding>>() with singleton { keyBindings }
}