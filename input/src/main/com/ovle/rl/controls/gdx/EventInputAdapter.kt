package com.ovle.rl.controls.gdx

import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.Viewport
import com.ovle.utils.event.EventBus.send
import ktx.math.vec2
import ktx.math.vec3

/**
 * low-level controls adapter (tied to view)
 *
 * screen -> viewport
 * InputAdapter methods -> events
 */
class EventInputAdapter(batchViewport: Viewport) : InputAdapter() {

    companion object {
        private const val LONG_TOUCH_TIME_MS = 1000
    }

    private val unproject: ((Vector2) -> Vector2) = batchViewport::unproject

    private var startDragPoint: Vector2? = null
    private var lastDragPoint: Vector2? = null
    private var dragId: Int? = null

    private var startTouchTimeMs: Long? = null


    override fun keyUp(keycode: Int): Boolean {
        send(KeyPressedEvent(keycode));
        return true
    }

    override fun scrolled(amountX: Float, amountY: Float) = send(CameraScrollCommand(amountY.toInt())).run { true }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        val viewportPoint = viewportPoint(screenX, screenY)
        send(MouseMovedEvent(viewportPoint))
        return true
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        startDragPoint = null
        lastDragPoint = null
        dragId = null
        val viewportPoint = viewportPoint(screenX, screenY)
        send(MouseClickEvent(viewportPoint, button))

        val startTime = startTouchTimeMs
        val time = System.currentTimeMillis()
        val isLongClick = startTime != null && time - startTime > LONG_TOUCH_TIME_MS
        if (isLongClick) {
            send(MouseLongClickEvent(viewportPoint, button))
        }

        startTouchTimeMs = null

        return true
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        val viewportPoint = viewportPoint(screenX, screenY)
        startDragPoint = viewportPoint.cpy()
        lastDragPoint = startDragPoint?.cpy()
        dragId = pointer

        startTouchTimeMs = System.currentTimeMillis()

        return true
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        val viewportPoint = viewportPoint(screenX, screenY)

        val sdp = startDragPoint
        val ldp = lastDragPoint
        if (sdp != null && ldp != null) {
            val diff = lastDragPoint!!.sub(viewportPoint)

            send(DragEvent(startDragPoint!!, viewportPoint, diff))
        }

        lastDragPoint = viewportPoint.cpy()

        return true
    }

    private fun viewportPoint(screenX: Int, screenY: Int): Vector2 {
        val x = screenX.toFloat()
        val y = screenY.toFloat()
        return Vector2(x, y).screenToViewport()
    }

    private fun Vector2.screenToViewport(): Vector2 = unproject(this)
}