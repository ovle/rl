package com.ovle.rl.controls.gdx

import com.badlogic.gdx.math.Vector2
import com.ovle.utils.event.Event


open class PlayerInputEvent : Event()

class MouseLongClickEvent(val viewportPoint: Vector2, val button: Int) : PlayerInputEvent()
class MouseMovedEvent(val viewportPoint: Vector2) : PlayerInputEvent()
class MouseClickEvent(val viewportPoint: Vector2, val button: Int) : PlayerInputEvent()
class KeyPressedEvent(val code: Int) : PlayerInputEvent()
class DragEvent(val start: Vector2, val current: Vector2, val lastDiff: Vector2) : PlayerInputEvent()