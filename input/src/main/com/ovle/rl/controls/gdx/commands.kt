package com.ovle.rl.controls.gdx

import com.badlogic.gdx.math.Vector2
import com.ovle.utils.event.Event


open class PlayerInputCommand : Event()

class CameraScaleIncCommand : PlayerInputCommand()
class CameraScaleDecCommand : PlayerInputCommand()
class CameraMoveCommand(val diff: Vector2) : PlayerInputCommand()
class CameraScrollCommand(val amount: Int) : PlayerInputCommand()
