package com.ovle.rl.controls

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.GameCommand


class FocusEntityCommand(val entity: Entity) : GameCommand()

class FocusTileCommand(val point: GridPoint2) : GameCommand()