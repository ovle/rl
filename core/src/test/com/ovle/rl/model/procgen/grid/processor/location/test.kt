package com.ovle.rl.model.procgen.grid.processor.location

import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.utils.gdx.math.array2d.ArrayValidationHelper
import com.ovle.utils.gdx.math.rect.RectArea
import com.ovle.utils.gdx.math.array2d.Array2d.Companion.array
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SpawnTest  {

    companion object {
        val tt1 = TileTemplate('1')
        val tt2 = TileTemplate('2')

        val tf1: (TileTemplate) -> Boolean = { true }
        val tf2: (TileTemplate) -> Boolean = { tt -> tt == tt1 }

        val tiles1 = listOf(
            listOf(tt1)
        )
        val tiles2 = listOf(
            listOf(tt1, tt1),
            listOf(tt1, tt1),
        )
    }

    @Test
    fun `tiles 1x1, struct 1x1, result (0, 0)`() {
        val ssh = ArrayValidationHelper()

        val result = ssh.validAreas(
            1, 1, array(tiles1), tf1
        )

        assertEquals(1, result.size)

        val expected = RectArea(0, 0, 0, 0)
        assertEquals(expected, result.single())
    }

    @Test
    fun `tiles 2x2, struct 2x2, result (0, 1)`() {
        val ssh = ArrayValidationHelper()

        val result = ssh.validAreas(
            2, 2, array(tiles2), tf1
        )

        assertEquals(1, result.size)

        val expected = RectArea(0, 0, 1, 1)
        assertEquals(expected, result.single())
    }

    @Test
    fun `tiles 2x2, struct 1x1 4 results`() {
        val ssh = ArrayValidationHelper()

        val result = ssh.validAreas(
            1, 1, array(tiles2), tf1
        )

        assertEquals(4, result.size)
    }
}