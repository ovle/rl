package com.ovle.rl.model.lightning.other

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.space.body.BodyComponent
import com.ovle.utils.gdx.math.point.point

class EntitiesData(val entities: Array<Entity>, val note: String = "")

class TileTemplate(val size: Int, val data: Array<Int>, val note: String = "")

data class ExpectedResult(
    val areaSize: Int,
    val croppedAreaSize: Int,
    val obstaclesCount: Int
)

fun lightSource(x: Int, y: Int, radius: Int): Entity {
    return Entity().apply {
        add(BodyComponent()
            .apply { position = point(x, y) })
//        add(AOEData(
//            radius = radius,
//            aoePositions = listOf()
//        ))
    }
}