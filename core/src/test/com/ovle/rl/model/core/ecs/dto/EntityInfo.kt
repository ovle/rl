package com.ovle.rl.model.core.ecs.dto

import com.badlogic.ashley.core.Component
import com.ovle.rl.EntityId

data class EntityInfo(
    val id: EntityId,
    val components: Array<Component>
)