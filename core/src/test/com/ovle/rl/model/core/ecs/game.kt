package com.ovle.rl.model.core.ecs

import com.github.czyzby.noise4j.map.Grid
import com.ovle.rl.TileArray
import com.ovle.rl.model.ai.bt.dto.TestGame
import com.ovle.rl.model.game.game.dto.Entities
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.game.dto.location.projection.LocationProjection
import com.ovle.rl.model.game.game.dto.location.projection.PlayerKnownTileMap
import com.ovle.rl.model.game.game.dto.location.projection.PlayerVisibleTileMap
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.game.dto.player.Player
import com.ovle.rl.model.game.game.dto.player.Players
import com.ovle.rl.model.game.social.dto.FactionTemplate
import com.ovle.rl.model.game.social.dto.FactionsConfig
import com.ovle.rl.model.game.space.accessibility.Accessibility
import com.ovle.rl.model.game.spawn.LocationEntitySpawnConfig
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.config.TileMapper
import com.ovle.rl.model.procgen.grid.Grids
import com.ovle.rl.model.procgen.grid.GridsValue
import com.ovle.rl.model.procgen.grid.world.World
import com.ovle.rl.model.util.randomId
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.point
import kotlin.math.sqrt

private val random = RandomParams(0L)

private val world = World(
    randomId(),
    "",
    random,
    Grids(arrayOf(Grid(0), Grid(0), Grid(0))),
    TileArray(arrayOf(), 0),
)

private val empty = TileTemplate.NOTHING
private val testTiles = arrayOf(
    empty, empty, empty,
    empty, empty, empty,
    empty, empty, empty
)

private val tilesArray = TileArray(testTiles, sqrt(testTiles.size.toDouble()).toInt())

val content = LocationContent(
    tilesArray,
    Grids(arrayOf(Grid(3), Grid(3), Grid(3))),
    Entities(), Accessibility()
)

val testFaction = FactionTemplate("")

private val location = Location(
    id = randomId(),
    template = LocationTemplate(
        name = "",
        getGridFactories = { emptyArray() },
        tileMapper = object : TileMapper {
            override fun map(v: GridsValue) = TileTemplate.NOTHING
        },
        factions = FactionsConfig(testFaction, testFaction, listOf()) { testFaction },
        entitySpawnConfig = LocationEntitySpawnConfig(listOf(), listOf())
    ),
    name = "test location",
    worldPoint = point(0, 0),
    worldPlace = null,
    random = RandomParams(123),
    content = content,
    players = Players(
        human = LocationPlayer.Human(
            player = Player.DEFAULT,
            locationProjection = LocationProjection(
                PlayerKnownTileMap(content),
                PlayerVisibleTileMap(content),
            ),
        ),
        byFaction = emptyMap()
    )
)

val testGame = TestGame(location, world, world.random)