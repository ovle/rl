package com.ovle.rl.model.core.ecs

import com.badlogic.ashley.core.Component
import com.ovle.rl.EntityId
import com.ovle.rl.model.core.ecs.dto.EntityInfo
import com.ovle.rl.model.game.core.component.CoreComponent

fun entity(id: EntityId, vararg components: Component) =
    EntityInfo(id, (components as Array<Component>) + CoreComponent(id = id))