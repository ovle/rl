package com.ovle.rl.model.core

import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.backends.headless.HeadlessApplication
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration
import com.ovle.rl.Tick
import com.ovle.rl.model.core.ecs.dto.TestEnvironment
import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.game.game.factory.GameFactory
import com.ovle.rl.model.game.time.ChangeGameSpeedCommand
import com.ovle.rl.model.game.time.ticksToTime
import com.ovle.utils.event.EventBus.send


abstract class GameTest {

    protected lateinit var ecsEngine: PooledEngine

    protected var stopFlag: Boolean = false


    private fun createApp() {
        val config = HeadlessApplicationConfiguration()
        val application = object : ApplicationAdapter() {}
        HeadlessApplication(application, config)
    }

    protected fun createGame(environment: TestEnvironment) {
        createApp()

        ecsEngine = PooledEngine()

        val g = environment.game
        val getGlobalComponents: () -> Set<GlobalComponent> = { environment.globalComponents.toSet() }
        val gf = GameFactory(getGlobalComponents)

        gf.newGame(g.location, g.world, ecsEngine)
    }

    protected fun runGame(step: Tick, gameSpeed: Double = 1.0) {
        send(ChangeGameSpeedCommand(gameSpeed))

        while (!stopFlag) {
            val delta = ticksToTime(step).toFloat()
            ecsEngine.update(delta)

            Thread.sleep(1L)
        }
    }

    protected fun setup(environment: TestEnvironment) {
        createGame(environment)

        environment.systems.forEach {
            ecsEngine.addSystem(it)
        }

        for (entityInfo in environment.entities) {
            val e = ecsEngine.createEntity()
            entityInfo.components.forEach {
                e.add(it)
            }
            ecsEngine.addEntity(e)

            val location = environment.game.location
            location.content.entities.add(e)
        }
    }

    protected fun cleanup(environment: TestEnvironment) {
        environment.systems.forEach {
            ecsEngine.removeSystem(it)
        }
        val ecsEntities = ecsEngine.entities.toList()
        environment.entities.forEach {
            val e = ecsEntities.singleOrNull { e -> e.id() == it.id }
            ecsEngine.removeEntity(e)
        }

        val location = environment.game.location
        location.content.entities.removeAll()
    }
}