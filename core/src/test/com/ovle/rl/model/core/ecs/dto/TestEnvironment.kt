package com.ovle.rl.model.core.ecs.dto

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.ai.bt.dto.TestGame
import com.ovle.rl.model.game.core.component.GlobalComponent

data class TestEnvironment(
    val game: TestGame,
    val systems: Array<EntitySystem>,
    val globalComponents: Array<GlobalComponent>,
    val entities: Array<EntityInfo>
)