package com.ovle.rl.model.core.ecs

import com.ovle.rl.model.game.ai.AIComponent
import com.ovle.rl.model.game.ai.AISystem
import com.ovle.rl.model.game.ai.decision.EntityDecisionHelper
import com.ovle.rl.model.game.ai.impl.behaviorTree.BehaviorTreeAIDecisionProcessor
import com.ovle.rl.model.game.skill.helper.PositionHelper
import com.ovle.rl.model.game.skill.helper.SkillHelper
import com.ovle.rl.model.game.social.EnemiesHelper
import com.ovle.rl.model.game.space.body.BodyComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.body.BodySystem
import com.ovle.rl.model.game.space.move.MoveStateHelper
import com.ovle.rl.model.game.space.move.MoveSystem
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.game.time.GlobalTimeComponent
import com.ovle.rl.model.game.time.GlobalTimeSystem
import com.ovle.rl.model.game.time.dto.GlobalTimeInfo


object Systems {
    val time = GlobalTimeSystem()
    val ai = AISystem(
        BehaviorTreeAIDecisionProcessor(),
        EntityDecisionHelper(
            SkillHelper(PositionHelper(PassabilityHelper())),
            EnemiesHelper()
        )
    )
    val move = MoveSystem(MoveStateHelper())
    val body = BodySystem(PassabilityHelper())
}

object Components {
    fun ai() = AIComponent()
    fun move() = MoveComponent(1.0)
    fun body() = BodyComponent()
}

object GlobalComponents {
    fun time() = GlobalTimeComponent(GlobalTimeInfo(), mutableListOf())
}