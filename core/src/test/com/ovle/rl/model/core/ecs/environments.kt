package com.ovle.rl.model.core.ecs

import com.ovle.rl.model.core.ecs.Components.ai
import com.ovle.rl.model.core.ecs.Components.move
import com.ovle.rl.model.core.ecs.Components.body
import com.ovle.rl.model.core.ecs.GlobalComponents.time
import com.ovle.rl.model.core.ecs.Systems.ai
import com.ovle.rl.model.core.ecs.Systems.move
import com.ovle.rl.model.core.ecs.Systems.body
import com.ovle.rl.model.core.ecs.Systems.time
import com.ovle.rl.model.core.ecs.dto.TestEnvironment


val AI = TestEnvironment(
    game = testGame,
    systems = arrayOf(time, ai),
    globalComponents = arrayOf(time()),
    entities = arrayOf(
        entity("aiOwner", ai())
    )
)

val AIAndMovement = TestEnvironment(
    game = testGame,
    systems = arrayOf(time, ai, move, body),
    globalComponents = arrayOf(time()),
    entities = arrayOf(
        entity("aiOwner", ai(), move(), body())
    )
)