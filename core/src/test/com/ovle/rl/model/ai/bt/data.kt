package com.ovle.rl.model.ai.bt

import com.badlogic.gdx.ai.btree.Task.Status.SUCCEEDED
import com.ovle.rl.model.ai.bt.dto.TestCase
import com.ovle.rl.model.ai.bt.dto.step
import com.ovle.rl.model.core.ecs.AIAndMovement
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.utils.gdx.math.point.point


val testCases = arrayOf(
    TestCase(
        description = "move to point",
        environment = AIAndMovement,
        task = Move.ToPosition(
            point(2, 2)
        ),
        expectedResult = arrayOf(
            step("move", SUCCEEDED)
        )
    )
)