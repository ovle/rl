package com.ovle.rl.model.ai.bt

import com.ovle.rl.model.ai.bt.dto.StepResult
import com.ovle.rl.model.ai.bt.dto.TestCase
import com.ovle.rl.model.core.GameTest
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.ai.Components.ai
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.log.debugEventLogHook
import com.ovle.utils.event.EventBus
import ktx.ashley.get
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.provider.MethodSource

//todo Couldn't load shared library 'gdx64.dll' for target: ...
//todo set stop flag
class Test: GameTest() {

    companion object {
        @JvmStatic
        fun args() = testCases

        @JvmStatic
        @BeforeAll
        fun setupGlobal() {
            EventBus.addHook(::debugEventLogHook)
        }
    }

    @Test
    fun `empty test`() {
        Assertions.assertTrue(true)
    }

    //@ParameterizedTest
    @MethodSource("args")
    fun `test behavior tree`(testCase: TestCase) {
        setup(testCase.environment)

        val (description, _, _, expectedResult) = testCase
        val actualResult: MutableCollection<StepResult> = mutableListOf()

        setupAIOwner(testCase)

        runGame(step = SystemUpdateInterval.MIN)

        Assertions.assertArrayEquals(expectedResult, actualResult.toTypedArray(), description)

        cleanup(testCase.environment)
    }

    private fun setupAIOwner(testCase: TestCase) {
        val gameInfo = testCase.environment.game
        val location = gameInfo.location
        val entities = location.content.entities

        val aiOwner = entities.all().single { it.id() == "aiOwner" }
        aiOwner[ai]!!.rootTask = RootTask("","", testCase.task)
    }
}