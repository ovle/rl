package com.ovle.rl.model.ai.bt.dto

import com.ovle.rl.model.game.ai.TaskExecResult


data class StepResult(
    val step: String,
    val result: TaskExecResult
)

fun step(step: String, result: TaskExecResult) = StepResult(step, result)