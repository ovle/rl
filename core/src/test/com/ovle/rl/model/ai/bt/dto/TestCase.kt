package com.ovle.rl.model.ai.bt.dto

import com.ovle.rl.model.core.ecs.dto.TestEnvironment
import com.ovle.rl.model.game.ai.task.AITask

data class TestCase(
    val description: String,
    val environment: TestEnvironment,
    val task: AITask,
    val expectedResult: Array<StepResult>
)