package com.ovle.rl.model.ai.bt

import com.badlogic.gdx.ai.btree.BehaviorTree
import com.badlogic.gdx.ai.btree.Task
import com.ovle.rl.model.ai.bt.dto.StepResult
import com.ovle.rl.model.game.ai.impl.behaviorTree.BaseTask
import com.ovle.rl.model.game.ai.impl.behaviorTree.TaskStatusListener
import com.ovle.rl.model.game.ai.impl.behaviorTree.dto.BTParams


class TestTaskStatusListener(
    tree: BehaviorTree<BTParams>,
    private val actualResult: MutableCollection<StepResult>
): TaskStatusListener(tree) {

    override fun statusUpdated(task: Task<BTParams>, previousStatus: Task.Status?) {
        super.statusUpdated(task, previousStatus)

        if (task is BaseTask) {
            val status = task.status
            if (!isTerminal(status)) return

            actualResult += StepResult(task.name ?: "", status)
        }
    }
}