package com.ovle.rl.model.ai.bt.dto

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.procgen.grid.world.World
import com.ovle.utils.RandomParams

data class TestGame(
    val location: Location, val world: World, val random: RandomParams
)