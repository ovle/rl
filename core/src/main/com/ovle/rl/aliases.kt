package com.ovle.rl

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.effect.EffectInfo
import com.ovle.rl.model.game.effect.EffectParams
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.quest.dto.QuestInfo
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.space.move.impl.path.MovePath
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.game.time.dto.TimerEffectParamsFactoryPayload
import com.ovle.rl.model.procgen.grid.GridsValue
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.array2d.Array2d

typealias GetGame = () -> GameEntity
typealias GameEntity = Entity

typealias TileArray = Array2d<TileTemplate>
typealias TileCheck1 = (TileTemplate) -> Boolean
typealias TileCheck2 = (GridPoint2, TileArray) -> Boolean
typealias GridsValueCheck = (GridsValue) -> Boolean

typealias AreaCheck2 = (AreaInfo, Location) -> Boolean
typealias EntityCheck = (Entity) -> Boolean
typealias GameCheck = (GameEntity) -> Boolean

typealias GetLocationPoint = (Location) -> GridPoint2?

typealias MoveCostFn = (TileTemplate, TileTemplate?) -> Int
typealias MoveCostFn2 = (GridPoint2, GridPoint2) -> Int
typealias PassStopFn = (GridPoint2) -> Boolean

typealias EffectPayload = Any?
typealias Effect = (EffectParams) -> Unit

typealias EntityId = String
typealias PlayerId = String
typealias WorldId = String
typealias LocationId = String

typealias QuestCondition = () -> Boolean
typealias QuestHook = ((QuestInfo) -> Unit)?

typealias Seed = Int
typealias Tick = Long
typealias Hour = Int

typealias RoomTiles = MutableList<Vector2>

typealias GetAOE = (Entity, Int) -> Collection<GridPoint2>
typealias GetPath = (GridPoint2, GridPoint2?) -> MovePath

typealias TimerStep = Int
typealias TimerCheck = (RandomParams) -> Boolean
typealias TimerEffectParamsFactory = (TimerEffectParamsFactoryPayload) -> EffectParams
typealias TriggerEffectParamsFactory = (GameEvent) -> EffectParams
typealias SkillEffectParamsFactory = (Entity, SkillTarget?) -> EffectParams

typealias TimerEffect = EffectInfo<TimerEffectParamsFactory>
typealias TriggerEffect = EffectInfo<TriggerEffectParamsFactory>
typealias SkillEffect = EffectInfo<SkillEffectParamsFactory>

typealias StateToRegionMap = Map<String, TextureRegion>