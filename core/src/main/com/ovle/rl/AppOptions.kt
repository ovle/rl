package com.ovle.rl

object AppOptions {
    const val IS_PROFILE_MODE = true
    const val IS_DEBUG_MODE = true
}