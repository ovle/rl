package com.ovle.rl.model.template

interface NamedTemplate {
    val name: String
}