package com.ovle.rl.model.template.structure

import com.badlogic.gdx.math.GridPoint2

class StructureEntities(
    private val prototype: StructureEntity,
    private val positions: Collection<GridPoint2>
) {
    fun create(): Collection<StructureEntity> {
        return positions.map {
            StructureEntity(prototype, it)
        }
    }
}