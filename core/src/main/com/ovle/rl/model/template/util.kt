package com.ovle.rl.model.template


fun parsedMask(mask: String?): List<List<Char>>? {
    return mask?.trim()
        ?.split("\n")
        ?.map {
            it.trim().split(" ").map { chars -> chars[0] }
        }
}