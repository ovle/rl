package com.ovle.rl.model.template.structure

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.social.dto.FactionTemplate
import com.ovle.rl.model.template.entity.EntityTemplate

data class StructureEntity(
    val template: EntityTemplate,
    val structurePosition: GridPoint2? = null,
    val faction: FactionTemplate? = null,
    val items: Collection<EntityTemplate> = listOf(),
) {

    constructor(prototype: StructureEntity, position: GridPoint2) : this(
        prototype.template,
        position,
        prototype.faction,
        prototype.items
    )
}
