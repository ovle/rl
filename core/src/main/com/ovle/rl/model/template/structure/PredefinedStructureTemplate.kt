package com.ovle.rl.model.template.structure

import com.ovle.rl.TileCheck1
import com.ovle.rl.model.template.entity.SpawnTemplate
import com.ovle.rl.model.template.parsedMask


data class PredefinedStructureTemplate(
    var name: String = "",
    var description: String = "",
    var mask: String = "",
    var spawnTemplates: Collection<SpawnTemplate> = listOf(),
    var entities: Collection<StructureEntity> = listOf(),
    var quests: Collection<StructureQuest> = listOf(),
    val tileFilter: TileCheck1 = { t -> t.props.isFloor }
) {
    val parsedMask: List<List<Char>> by lazy {
        parsedMask(mask)!!
    }
}