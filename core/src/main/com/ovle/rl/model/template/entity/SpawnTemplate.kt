package com.ovle.rl.model.template.entity

import com.ovle.rl.TileCheck1
import com.ovle.rl.model.game.tile.TileTemplate

data class SpawnTemplate(
    val chancePerTile: Float,
    val chancePerHour: Float = 0.0f,
    val tileCheck: TileCheck1,
    val nearTiles: Collection<TileTemplate> = emptySet(),
    val nearRadius: Int = 2
)