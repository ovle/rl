package com.ovle.rl.model.template.structure

data class StructureQuest(
    var questId: String = "",
    var entityId: String = ""
)