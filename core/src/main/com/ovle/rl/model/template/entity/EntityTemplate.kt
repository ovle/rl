package com.ovle.rl.model.template.entity

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.template.NamedTemplate
import com.ovle.utils.gdx.ashley.component.BaseComponent
import kotlin.reflect.KClass


class EntityTemplate(
    override val name: String,
    var shortName: String = name,
    var description: String? = null,
    var parent: EntityTemplate? = null,
    var getState: () -> Collection<EntityComponent> = { listOf() },
    var spawnTemplate: SpawnTemplate? = null,
): NamedTemplate {

    val fullState: Collection<EntityComponent>
        get() = mergeState(parent?.fullState, getState.invoke())


    private fun mergeState(
        parentState: Collection<EntityComponent>?,
        state: Collection<EntityComponent>
    ): Collection<EntityComponent> {
        if (parentState == null) return state

        val parentStateMap = parentState.associateBy { it.javaClass }
        val stateMap = state.associateBy { it.javaClass }
        val mergedMap = parentStateMap + stateMap
        return mergedMap.values
    }

    fun has(clazz: KClass<out BaseComponent>) = fullState.any { it::class == clazz }
}
