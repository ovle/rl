package com.ovle.rl.model.util

import java.util.*

fun randomId() = UUID.randomUUID().toString()