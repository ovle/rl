package com.ovle.rl.model.util

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.ai.task.AITask
import com.ovle.rl.model.game.core.Components.template
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.game.effect.StaticEffectTemplate
import com.ovle.rl.model.game.game.Components.game
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.space.move.MoveParams
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.time.dto.Timer
import com.ovle.rl.model.game.trigger.dto.TriggerTemplate
import ktx.ashley.has

fun Collection<Entity>.info(): String {
    return map { it.info() }
        .groupBy { it }.entries
        .joinToString(", ") { (k, v) -> "${v.count()}x $k" }
}

fun Any?.info(recursive: Boolean = false): String = when {
    this == null -> "no"
    this is Entity -> when {
        this.has(template) -> { name() } // + " (id:${this.id().takeLast(4)})"
        this.has(game) -> "global"
        else -> "unknown entity"
    } + if (recursive) {
        this.components.map { it.info() }
            .filterNot { it.isBlank() }
            .joinToString(
                prefix = " {\n",
                postfix = "\n}",
                separator = "\n",
                transform = { s -> "  $s" }
            )
    } else ""
    //this is EntityComponent -> this.debugInfo() ?: ""
    this is Timer -> "{name ${template.name}, cnt:${repeatCount}/${template.maxRepeats}}"
    this is TriggerTemplate -> "{class: ${key.simpleName}}"
    this is SkillTemplate -> name
    this is StaticEffectTemplate -> name
    this is MoveParams -> "{currentTarget:${path?.currentTarget?.info()}}"
    this is AITask -> debugInfo()
    this is GridPoint2 -> "($x,$y)"
    this is TaskInfo -> "{name:${template.name}, target:${target.info()}, res:${lockedResource ?: "no"}, performer:${performer.info()}, status:${status}}"
    this is Iterable<*> -> joinToString { it.info() }
    this is SkillTarget.Entity -> entity.info()
    this is SkillTarget.Point -> point.info()
    this is SkillTarget.Build -> point.info()
    else -> "(unknown)"
}