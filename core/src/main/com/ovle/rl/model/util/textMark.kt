package com.ovle.rl.model.util


fun String.color(colorName: String) = "[$colorName]$this[]"

fun String.markSelection() = color("db16-white")

fun String.markName() = color("db16-yellow")
fun String.markEntity() = color("db16-yellow")
fun String.markSkill() = color("db16-blue")

fun String.markError() = color("db16-red")

fun String.markDamage() = color("db16-red")
fun String.markHeal() = color("db16-lightGreen")

fun String.markFail() = color("db16-red")
fun String.markSuccess() = color("db16-lightGreen")