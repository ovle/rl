package com.ovle.rl.model.util

//todo modules
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.ai.Components.ai
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.container.Components.carriable
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.game.Components.game
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.gather.Components.source
import com.ovle.rl.model.game.life.Components.age
import com.ovle.rl.model.game.life.Components.hunger
import com.ovle.rl.model.game.life.Components.daySchedule
import com.ovle.rl.model.game.life.Components.sleepPlace
import com.ovle.rl.model.game.material.Components.material
import com.ovle.rl.model.game.material.MaterialTemplate
import com.ovle.rl.model.game.perception.Components.perception
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.house.Components.houseAware
import com.ovle.rl.model.game.life.Components.sleep
import com.ovle.rl.model.game.skill.Components.skill
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.social.Components.social
import com.ovle.rl.model.game.social.dto.FactionTemplate
import com.ovle.rl.model.game.space.Components.aoe
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.accessibility.Accessibility
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.spawn.portal.Components.portal
import com.ovle.rl.model.game.task.Components.taskPerformer
import com.ovle.rl.model.game.time.Components.timer
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.gdx.math.distance
import ktx.ashley.get
import ktx.ashley.has


fun GameEntity.location() = this[game]!!.location
fun GameEntity.world() = this[game]!!.world

fun Collection<Entity>.withAge() = filter { it.has(age) }
fun Collection<Entity>.withDaySchedule() = filter { it.has(daySchedule) }
fun Collection<Entity>.ableToSleep() = filter { it.has(sleep) }
fun Collection<Entity>.asleep() = filter {
    it.has(sleep) && it[sleep]!!.isSleeping
}
fun Collection<Entity>.awaken() = filter {
    !it.has(sleep) || !it[sleep]!!.isSleeping
}
fun Collection<Entity>.sleepPlaces() = filter { it.has(sleepPlace) }
fun Collection<Entity>.ableToEat() = filter { it.has(hunger) }
fun Collection<Entity>.aiOwners() = filter { it.has(ai) }
fun Collection<Entity>.perceptionOwners() = filter { it.has(perception) }
fun Collection<Entity>.taskPerformers() = filter { it.has(taskPerformer) }
fun Collection<Entity>.socials() = filter { it.has(social) }
fun Collection<Entity>.closestTo(p: GridPoint2) = minByOrNull { distance(it.position(), p) }
fun Collection<Entity>.nearTo(p: GridPoint2, r: Int) = filter { distance(it.position(), p) < r }
fun Collection<Entity>.materials() = filter { it.has(material) }
fun Collection<Entity>.skillOwners() = filter { it.has(skill) }
fun Collection<Entity>.movables() = filter { it.has(move) }
fun Collection<Entity>.carriables() = filter { it.has(carriable) }
fun Collection<Entity>.aoeOwners() = filter { it.has(aoe) }
fun Collection<Entity>.timers() = filter { it.has(timer) }
fun Collection<Entity>.sources() = filter { it.has(source) }
fun Collection<Entity>.portals() = filter { it.has(portal) }
fun Collection<Entity>.houseAwares() = filter { it.has(houseAware) }
fun Collection<Entity>.ofFaction(f: FactionTemplate) = filter {
    f == it[social]?.faction
}fun Collection<Entity>.ofPlayer(p: LocationPlayer) = filter {
    p == it[social]?.player
}
fun Collection<Entity>.knownByPlayer(p: LocationPlayer) = filter {
    p.locationProjection.knownMap.isKnownTile(it.position())
}
fun Collection<Entity>.materials(mt: MaterialTemplate? = null) = materials().filter {
    mt == null || it[material]!!.template == mt
}
fun Collection<Entity>.excludeKeeper() = filter {
    !it.has(social) || !it[social]!!.isKeeper
}
fun Collection<Entity>.keepers() = filter {
    it.has(social) && it[social]!!.isKeeper
}

fun Collection<Entity>.ofTemplate(t: EntityTemplate?) =
    if (t == null) this else filter { it.template() == t }

fun Collection<Entity>.accessiblesTo(
    entity: Entity, content: LocationContent
): List<Entity> {
    val key = Accessibility.Companion.Key(entity)
    val accessibility = content.accessibility
    return this.filter {
        accessibility.isAccessible(key, entity.position(), it.position())
    }
}

fun Collection<AreaInfo>.housesTo(entity: Entity): Collection<AreaInfo> {
    return this.filter {
        val host = (it.params as AreaParams.House).host
        host == null || host == entity
    }
}

fun Collection<AreaInfo>.accessibleTo(entity: Entity, content: LocationContent): Collection<AreaInfo> {
    val key = Accessibility.Companion.Key(entity)
    val accessibility = content.accessibility
    return this.filter {
        accessibility.isAccessible(key, entity.position(), it.area.first()) //todo
    }
}

fun Collection<SkillTemplate>.withTag(tag: SkillTag?) = if (tag == null) this else filter { tag in it.tags }
fun Collection<Entity>.positions() = map { it.position() }.toSet()
fun Collection<Entity>.templates() = map { it.template() }.toSet()