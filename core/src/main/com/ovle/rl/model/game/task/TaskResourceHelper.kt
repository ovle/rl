package com.ovle.rl.model.game.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.space.accessibility.Accessibility.Companion.Key
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.util.knownByPlayer
import com.ovle.rl.model.util.materials
import com.ovle.utils.gdx.math.distance

class TaskResourceHelper {

    companion object {
        private const val MAX_PRIORITY = 1.0f
    }

    fun knownResources(location: Location, player: LocationPlayer): MutableSet<Entity> {
        val locked = player.tasks
            .mapNotNull { it.lockedResource }
            .toSet()

        return location.content.entities.all()
            .materials()
            .knownByPlayer(player)
            .subtract(locked)
            .toMutableSet()
    }

    fun bestResource(
        knownFreeResources: MutableSet<Entity>, task: TaskInfo, performer: Entity, location: Location
    ) = knownFreeResources
        .accessibleForTask(task, performer, location)
        .maxByOrNull { resourcePriority(task, performer, it) }


    private fun resourcePriority(task: TaskInfo, performer: Entity, resource: Entity): Float {
        val taskPosition = task.target.positions().firstOrNull()
            ?: return MAX_PRIORITY

        val dstToPerformer = distance(resource.position(), performer.position())
        val dstToTask = distance(resource.position(), taskPosition)

        return MAX_PRIORITY / (dstToPerformer + dstToTask)
    }

    private fun Collection<Entity>.accessibleForTask(
        task: TaskInfo, performer: Entity, location: Location
    ): Collection<Entity> {
        val accessibility = location.content.accessibility
        val resource = task.target.resource() ?: return emptySet()

        return filter {
            it.template() == resource &&
                accessibility.isAccessible(Key(performer), performer.position(), it.position())
        }
    }
}