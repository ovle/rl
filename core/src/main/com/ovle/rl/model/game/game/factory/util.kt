package com.ovle.rl.model.game.game.factory

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity


fun Engine.newEntity(vararg components: Component): Entity =
    createEntity().apply {
        components.forEach { component -> this.add(component) }
        addEntity(this)
    }