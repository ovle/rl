package com.ovle.rl.model.game.game.dto.location.projection

import com.badlogic.gdx.math.GridPoint2

interface KnownTileMap {

    fun isKnownTile(p: GridPoint2): Boolean

    fun markAsKnown(p: GridPoint2)
}