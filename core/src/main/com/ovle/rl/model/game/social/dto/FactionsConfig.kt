package com.ovle.rl.model.game.social.dto

import com.badlogic.ashley.core.Entity

class FactionsConfig(
    val playerFaction: FactionTemplate,
    val aiPlayerFaction: FactionTemplate,
    val relations: Collection<FactionFactionRelation>,
    val getSpawnFaction: (Entity) -> FactionTemplate
)