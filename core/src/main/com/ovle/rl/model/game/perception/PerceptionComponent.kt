package com.ovle.rl.model.game.perception

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.component.EntityComponent

//todo sight, hearing, feel living, feel magic
class PerceptionComponent(
    val sightRadius: Int,
) : EntityComponent() {
    var fov: Set<GridPoint2> = setOf()
}