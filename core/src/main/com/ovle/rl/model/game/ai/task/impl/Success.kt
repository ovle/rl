package com.ovle.rl.model.game.ai.task.impl

import com.ovle.rl.model.game.ai.task.AITask

object Success: AITask() {

    override fun debugInfo() = "success"
}