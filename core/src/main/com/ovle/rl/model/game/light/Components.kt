package com.ovle.rl.model.game.light

import com.ovle.utils.gdx.ashley.component.mapper

object Components {
    val light = mapper<LightSourceComponent>()
}