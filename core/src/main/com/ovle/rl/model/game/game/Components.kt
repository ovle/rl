package com.ovle.rl.model.game.game

import com.ovle.utils.gdx.ashley.component.mapper

object Components {
    val game = mapper<GameComponent>()
}