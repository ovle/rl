package com.ovle.rl.model.game.game

import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.game.dto.FinishGameCondition

typealias FinishGameConditionCheck = (GameEvent, GameEntity) -> Boolean
typealias FinishGameResults = Map<FinishGameCondition, Boolean>