package com.ovle.rl.model.game.area

import com.badlogic.gdx.math.Rectangle
import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.game.game.dto.player.LocationPlayer


class CreateAreaCommand(
    val rect: Rectangle, val template: AreaTemplate, val params: AreaParams,
    val player: LocationPlayer
) : GameCommand()

class MergeAreasCommand(
    val rect: Rectangle, val template: AreaTemplate, val params: AreaParams, val areas: Collection<AreaInfo>,
    val player: LocationPlayer
) : GameCommand()

class DeleteAreaCommand(
    val area: AreaInfo,
    val player: LocationPlayer
) : GameCommand()