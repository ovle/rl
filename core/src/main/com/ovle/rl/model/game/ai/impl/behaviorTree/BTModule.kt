package com.ovle.rl.model.game.ai.impl.behaviorTree

import com.ovle.rl.model.game.ai.decision.DecisionProcessor
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.singleton


val aiBtModule = DI.Module("BT") {
    bind<DecisionProcessor>() with singleton {
        BehaviorTreeAIDecisionProcessor()
    }
}