package com.ovle.rl.model.game.ai

import com.ovle.rl.model.game.ai.decision.AITaskFactory
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode

class AIComponent(
    var aiTaskFactories: Collection<AITaskFactory> = emptyList()
): EntityComponent() {
    var rootTask: RootTask? = null
    var active: Boolean = true

    //implementation-specific
    var aiParams: AIParams? = null

    override fun gameInfo() = GameInfoNode(
        "ai:", listOfNotNull(
            rootTask?.gameInfo(),
            GameInfoNode("active: $active")
        )
    )
}