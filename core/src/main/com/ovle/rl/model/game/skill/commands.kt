package com.ovle.rl.model.game.skill

import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.game.skill.dto.SkillUsage

class UseSkillCommand(val skillUsage: SkillUsage) : GameCommand()