package com.ovle.rl.model.game.projectile.dto

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.EntityId

data class ProjectileInfo(
    val entityId: EntityId,
    val from: GridPoint2,
    val to: GridPoint2? = null,
    val source: Entity? = null,
)