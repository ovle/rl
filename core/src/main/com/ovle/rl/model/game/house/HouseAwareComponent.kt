package com.ovle.rl.model.game.house

import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode

class HouseAwareComponent(
    var closestOwnedHouse: AreaInfo? = null,
    var closestHouse: AreaInfo? = null
) : EntityComponent() {

    override fun gameInfo() = GameInfoNode(
        "house aware: ", listOf()
    )
}