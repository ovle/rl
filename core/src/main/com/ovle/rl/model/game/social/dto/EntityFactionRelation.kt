package com.ovle.rl.model.game.social.dto

//will be stored in entity
data class EntityFactionRelation(
    val faction: FactionTemplate,
    val value: RelationValue
)