package com.ovle.rl.model.game.spawn.portal

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.game.dto.player.LocationPlayer

data class PortalEntitySpawnCheck(
    val check: (LocationPlayer, LocationContent, Entity, Int) -> Boolean,
    val description: String
)