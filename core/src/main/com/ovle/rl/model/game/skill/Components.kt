package com.ovle.rl.model.game.skill

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.skill.Components.skill
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.util.info
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get
import ktx.ashley.has

object Components {
    val skill = mapper<SkillComponent>()
}

fun Entity.skills(): Collection<SkillTemplate> {
    check(has(skill)) { "no skills for entity ${info()}" }
    return this[skill]!!.skills
}

fun Entity.hasSkill(s: SkillTemplate): Boolean {
    check(has(skill)) { "no skills for entity ${info()}" }
    return s in this.skills()
}