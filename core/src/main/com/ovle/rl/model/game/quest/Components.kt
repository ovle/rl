package com.ovle.rl.model.game.quest

import com.ovle.utils.gdx.ashley.component.mapper

object Components {
    val questOwner = mapper<QuestOwnerComponent>()
}