package com.ovle.rl.model.game.game.dto.location

import com.ovle.rl.TileArray
import com.ovle.rl.model.game.game.dto.Entities
import com.ovle.rl.model.game.space.accessibility.Accessibility
import com.ovle.rl.model.procgen.grid.Grids

data class LocationContent(
    val tiles: TileArray,
    val grids: Grids,
    val entities: Entities,
    val accessibility: Accessibility
)