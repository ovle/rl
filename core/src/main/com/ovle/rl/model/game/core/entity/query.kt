package com.ovle.rl.model.game.core.entity

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.Components.core
import com.ovle.rl.model.game.core.isExists
import ktx.ashley.get
import ktx.ashley.has
import kotlin.reflect.KClass


//todo use families?
fun Collection<Entity>.with(componentClass: KClass<out Component>): Collection<Entity> {
    val mapper = ComponentMapper.getFor(componentClass.java)
    return filter {
        it.has(mapper) && (it[core]?.isExists ?: true) //todo remove exists check here
    }
}

fun entityWith(entities: Collection<Entity>, componentClass: KClass<out Component>) =
    entities.with(componentClass)
        .singleOrNull()

