package com.ovle.rl.model.game.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.life.isDead
import com.ovle.rl.model.game.task.Components.taskPerformer
import ktx.ashley.get
import ktx.ashley.has


fun Entity.isTaskPerformer() = isExists() && !isDead() && has(taskPerformer)

fun Entity.isFreeTaskPerformer() = isTaskPerformer() && this[taskPerformer]!!.current == null

fun Entity.isActiveTaskPerformer() = isTaskPerformer() && !isFreeTaskPerformer()
