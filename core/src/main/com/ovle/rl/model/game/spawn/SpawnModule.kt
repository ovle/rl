package com.ovle.rl.model.game.spawn

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.spawn.portal.PortalSystem
import org.kodein.di.*


val spawnModule = DI.Module("spawn") {
    bind<EntitySpawnCheckHelper>() with singleton {
        EntitySpawnCheckHelper()
    }
    bind<EntitySpawnHelper>() with singleton {
        EntitySpawnHelper(instance())
    }

    bind<EntitySystem>().inSet() with singleton {
        PortalSystem()
    }
    bind<EntitySystem>().inSet() with singleton {
        TimedSpawnSystem(instance(), instance())
    }
}