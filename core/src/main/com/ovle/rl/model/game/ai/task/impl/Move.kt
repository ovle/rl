package com.ovle.rl.model.game.ai.task.impl

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.ai.task.AITask
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.skill.helper.PositionHelper
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.info
import com.ovle.rl.model.util.positions

sealed class Move: AITask() {

    abstract fun goals(entity: Entity, content: LocationContent): Collection<GridPoint2>

    abstract val isFlee: Boolean


    data class ToPosition(
        val target: GridPoint2,
        val range: IntRange = 0..0
    ) : Move() {
        override val isFlee = false
        override fun goals(entity: Entity, content: LocationContent)
            = goals(entity, target, range, content)
        override fun debugInfo() = "move to: ${target.info()}"
        override fun toString() = "${super.toString()} ($target)"
    }

    data class ToEntity(
        val target: Entity,
        val range: IntRange = 0..0
    ) : Move() {
        override val isFlee = false
        override fun goals(entity: Entity, content: LocationContent)
            = goals(entity, target.position(), range, content)
        override fun debugInfo() = "move to: ${target.info()}"
        override fun toString() = "${super.toString()} (${target.name()})"
    }

    data class From(
        val entities: Collection<Entity>
    ): Move() {
        override val isFlee = true
        override fun goals(entity: Entity, content: LocationContent) = entities.positions()
        override fun debugInfo() = "flee: ${entities.info()}"
    }


    protected fun goals(entity: Entity, target: GridPoint2, range: IntRange, content: LocationContent): Collection<GridPoint2> {
        val positionHelper = PositionHelper(PassabilityHelper())
        return positionHelper.positionsToTarget(entity, range, target, content)!! //todo if range null
    }
}