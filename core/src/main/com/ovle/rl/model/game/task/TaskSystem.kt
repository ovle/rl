package com.ovle.rl.model.game.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.task.Components.taskPerformer
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskStatus.FAILED
import com.ovle.rl.model.game.task.dto.TaskStatus.SUCCEEDED
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.selection.TaskTargetSelection
import com.ovle.rl.model.util.awaken
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.ofPlayer
import com.ovle.rl.model.util.taskPerformers
import com.ovle.utils.cartesianProduct
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.log.debug

class TaskSystem(
    private val taskCandidatesHelper: TaskCandidatesHelper,
    private val validTaskTargetsHelper: ValidTaskTargetsHelper
): TimedEventSystem() {

    override fun subscribeIntr() {
        subscribe<CheckTaskCommand>(this) { onCheckTaskCommand(it.target, it.template, it.player) }
        subscribe<CancelTaskCommand>(this) { onCancelTaskCommand(it.task, it.player) }
    }


    private fun onCheckTaskCommand(target: TaskTargetSelection, taskTemplate: TaskTemplate, player: LocationPlayer) {
        val targetConfig = taskTemplate.targetConfig
        val targets = target.taskTargets()
        val location = game().location()
        val validTargets = validTaskTargetsHelper.validTargets(
            targets, targetConfig, location, player
        )
        if (validTargets.isEmpty()) return

        enqueueTask(taskTemplate, validTargets, player)
    }

    private fun onCancelTaskCommand(task: TaskInfo, player: LocationPlayer) {
        cleanupTask(task, player)
    }


    override fun updateTime() {
        val players = game().location().players.all()
        players.forEach {
            updatePlayerTasks(it)
        }
    }

    private fun updatePlayerTasks(player: LocationPlayer) {
        validateTasks(player)

        val tasks = player.tasks
        //todo user task >>> area task
        val freeTasks = tasks.filter { it.isFree }
        processFreeTasks(freeTasks, player)

        tasks.filter { !it.isFree }.forEach {
            processActiveTask(it, player)
        }
    }

    private fun processFreeTasks(freeTasks: List<TaskInfo>, player: LocationPlayer) {
        val location = game().location()
        val taskPerformers = getTaskPerformers(location, player)
        if (taskPerformers.isEmpty()) {
            debug(TASK_LOG_TAG) { "no free performers" }
            return
        }

        val tasksPerformers = cartesianProduct(freeTasks, taskPerformers)
            .filter { (t, p) -> t.template.performerFilter.invoke(p, t.target, location) }
            .map { it.first to it.second }
        if (tasksPerformers.isEmpty()) {
            debug(TASK_LOG_TAG) { "no available performers" }
            return
        }

        val bestTaskPerformers = taskCandidatesHelper.bestTaskCandidates(
            tasksPerformers, location, player
        )
        bestTaskPerformers.forEach { (t, p, r) ->
            startTask(t, p, r)
        }
    }

    private fun processActiveTask(task: TaskInfo, player: LocationPlayer) {
        when (task.status) {
            SUCCEEDED -> {
                send(TaskFinishedEvent(task))
                cleanupTask(task, player)
            }
            FAILED -> {
                send(TaskFinishedEvent(task))
                resetTask(task)
            }
            else -> {}
        }
    }

    private fun startTask(task: TaskInfo, performer: Entity, resource: TaskResource?) {
        task.performer = performer
        task.lockedResource = resource

        val performerComponent = performer[taskPerformer]!!
        performerComponent.current = task

        task.start()

        send(TaskStartedEvent(task))
    }

    private fun enqueueTask(
        taskTemplate: TaskTemplate, targets: Collection<TaskTarget>, player: LocationPlayer
    ) {
        for (target in targets) {
            enqueueSingleTask(taskTemplate, target, player)
        }
    }

    private fun enqueueSingleTask(taskTemplate: TaskTemplate, target: TaskTarget, player: LocationPlayer) {
        val task = TaskInfo(taskTemplate, target)
        player.tasks.add(task)

        task.reset()
    }

    private fun validateTasks(player: LocationPlayer) {
        val tasks = player.tasks

        val invalidTasks = tasks.filter { !isValid(it, player) }
        invalidTasks.forEach {
            debug { "task $it removed (is no longer valid)" }
            invalidateTask(it, player)
        }

        val tasksToReset = tasks.filter {
            val performer = it.performer
            performer != null && !performer.isTaskPerformer()
        }
        tasksToReset.forEach {
            debug { "task $it reset (no valid performer)" }
            resetTask(it)
        }
    }

    private fun resetTask(task: TaskInfo) {
        cleanupPerformer(task)

        task.performer = null
        task.reset()
    }

    private fun invalidateTask(task: TaskInfo, player: LocationPlayer) {
        cleanupTask(task, player)
    }

    private fun cleanupTask(task: TaskInfo, player: LocationPlayer) {
        cleanupPerformer(task)
        player.tasks.remove(task)

        send(TaskRemovedEvent(task))
    }

    private fun cleanupPerformer(task: TaskInfo) {
        val performer = task.performer ?: return

        performer[taskPerformer]!!.current = null
    }

    private fun getTaskPerformers(
        location: Location,
        player: LocationPlayer
    ) = location.content.entities.all()
        .ofPlayer(player)
        .taskPerformers()
        .filter { it.isFreeTaskPerformer() }

    /*
    check of tasks in progress are complex (depends on behavior tree step)
    so trees are responsible for that
    here checking only possibility to create the task
    and cancel until not in progress
    */
    private fun isValid(taskInfo: TaskInfo, player: LocationPlayer) = when {
        taskInfo.isFree -> {
            val targetConfig = taskInfo.template.targetConfig
            val location = game().location()
            val targets = setOf(taskInfo.target)

            validTaskTargetsHelper.validTargets(
                targets, targetConfig, location, player
            ).isNotEmpty()
        }
        //todo tasks with unknown targets became known should be validated
        else -> true
    }
}