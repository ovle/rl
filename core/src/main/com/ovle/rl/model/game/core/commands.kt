package com.ovle.rl.model.game.core

import com.ovle.utils.event.Event

/**
 * a command (smth has to be done) related to the game logic
 * only needed for separate systems from each other. may be replaced with direct systems dependencies + method calls
 * usually there's only one processor (one who technically execute the command)
 */
abstract class GameCommand : Event()