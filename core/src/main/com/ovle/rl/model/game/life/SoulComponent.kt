package com.ovle.rl.model.game.life

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.template.entity.EntityTemplate


class SoulComponent(
    val size: Int = 1,
    val yields: EntityTemplate
) : EntityComponent() {
    override fun gameInfo() = GameInfoNode(
        "soul:", listOf(
            GameInfoNode("size: $size"),
        )
    )
}