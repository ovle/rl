package com.ovle.rl.model.game.task.target.selection

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck.Error
import com.ovle.rl.model.game.task.target.TaskTargetCheck.Success
import com.ovle.rl.model.game.build.BuildTemplate

data class BuildTargetSelection(
    var template: BuildTemplate? = null,
    var points: MutableCollection<GridPoint2> = mutableSetOf(),
): TaskTargetSelection() {

    override val isMultiselect = true

    override fun taskTargets() = points.map { TaskTarget.Build(it, template!!) }


    override fun updatePoints(selection: Collection<GridPoint2>) {
        if (template != null) points.addAll(selection)
    }

    override fun updateBuildTemplate(template: BuildTemplate?) {
        this.template = template
    }


    override fun check() = when {
        template == null -> Error("select tile")
        points.isEmpty() -> Error("select positions")
        else -> Success
    }

    override fun copy() = BuildTargetSelection(template, points.toMutableSet())

    override fun reset() {
        //template is selected
        points.clear()
    }
}