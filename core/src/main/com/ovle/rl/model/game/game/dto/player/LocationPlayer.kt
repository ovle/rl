package com.ovle.rl.model.game.game.dto.player

import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.game.dto.location.projection.LocationProjection
import com.ovle.rl.model.game.task.dto.TaskInfo


sealed class LocationPlayer(
    val locationProjection: LocationProjection
) {
    val tasks: MutableCollection<TaskInfo> = mutableListOf()
    val areas: MutableCollection<AreaInfo> = mutableListOf()

    class Human(
        val player: Player,
        locationProjection: LocationProjection
    ): LocationPlayer(locationProjection)

    class AI(
        locationProjection: LocationProjection
    ): LocationPlayer(locationProjection)
}