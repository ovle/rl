package com.ovle.rl.model.game.time.dto

import com.ovle.rl.TimerStep


data class Timer(
    val template: TimerTemplate,

    var finished: Boolean = false,
    var repeatCount: TimerStep = 0,
    var data: TimerData = TimerData()
)