package com.ovle.rl.model.game.farm

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.game.game.DestroyEntityCommand
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe

class FarmSystem(
    private val taskTemplates: Collection<TaskTemplate>
) : BaseSystem() {

    override fun subscribe() {
        subscribe<FarmCommand>(this) { onFarmCommand(it.position, it.template, it.resource) }
    }

    private fun onFarmCommand(
        position: GridPoint2, template: FarmTemplate, resource: Entity?
    ) {
        val spawn = EntitySpawn(
            template = template.crop, position = position
        )
        send(CreateEntityCommand(spawn))

        resource?.let {
            send(DestroyEntityCommand(it))
        }
    }
}