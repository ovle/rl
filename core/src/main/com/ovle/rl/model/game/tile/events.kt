package com.ovle.rl.model.game.tile

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.util.info


class TileChangedEvent(val oldTile: TileTemplate, val tile: TileTemplate, val position: GridPoint2) : GameEvent() {
    override val message: String
        get() = "tile at ${position.info()} changed from '$oldTile' to '$tile'"
}
