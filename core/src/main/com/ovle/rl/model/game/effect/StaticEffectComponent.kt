package com.ovle.rl.model.game.effect

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.util.info

//todo rename to EffectComponent, rename *effects to something else
class StaticEffectComponent(
    val effects: MutableCollection<StaticEffectTemplate> = mutableListOf()
) : EntityComponent()