package com.ovle.rl.model.game.game

import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.procgen.grid.world.World

/**
 * game in the context of player
 */
class GameComponent(
    val location: Location,
    val world: World?
): GlobalComponent()