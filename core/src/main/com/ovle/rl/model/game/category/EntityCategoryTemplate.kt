package com.ovle.rl.model.game.category

import com.ovle.rl.model.template.NamedTemplate
import com.ovle.rl.model.template.entity.EntityTemplate

data class EntityCategoryTemplate(
    override val name: String,
    val description: String,
    val entities: Collection<EntityTemplate>
): NamedTemplate
