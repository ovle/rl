package com.ovle.rl.model.game.story

import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.story.dto.StoryEvent

class StoryComponent(
    val events: MutableCollection<StoryEvent> = mutableListOf()
): GlobalComponent()