package com.ovle.rl.model.game.schedule

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode


class DayScheduleComponent(
    val config: Collection<DaySchedule>,
) : EntityComponent() {

    var current: DaySchedule? = null

    override fun gameInfo() = GameInfoNode("schedule: $config")
}