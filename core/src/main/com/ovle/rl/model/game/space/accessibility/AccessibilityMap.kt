package com.ovle.rl.model.game.space.accessibility

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.space.accessibility.AccessibilityMapFactory.Companion.INACCESSIBLE_MAP_VALUE
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.point.adjHV

data class AccessibilityMap(
    var map: Array2d<Int>? = null,
    var dirty: Boolean = false,
) {

    fun isAccessible(from: GridPoint2, target: GridPoint2): Boolean {
        val m = map!!
        if (!m.isValid(target)) return false

        //need check as position/point is occupied by entity, so is 'inaccessible'
        if (from == target) return true

        val adjValues = accessibleAdjValues(from, m)

        return m[target.x, target.y] in adjValues
    }


    private fun accessibleAdjValues(position: GridPoint2, m: Array2d<Int>): Set<Int> {
        //- entity itself is inaccessible
        //- entity may divide the space for different areas on acc map gen
        return position.adjHV()
            .filter { m.isValid(it) }
            .map { m[it.x, it.y] }
            .filter { it != INACCESSIBLE_MAP_VALUE }
            .toSet()
    }
}