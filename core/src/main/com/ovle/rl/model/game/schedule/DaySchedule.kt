package com.ovle.rl.model.game.schedule

import com.ovle.rl.Hour

data class DaySchedule(
    val type: DayScheduleType,
    val start: Hour,
    val duration: Hour
) {
    val finish: Int = (start + duration).let {
        if (it >= 24) (it % 24) - 1 else it
    }
}