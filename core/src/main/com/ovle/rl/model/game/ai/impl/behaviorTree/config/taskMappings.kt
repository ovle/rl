package com.ovle.rl.model.game.ai.impl.behaviorTree.config

import com.badlogic.gdx.ai.btree.Task
import com.badlogic.gdx.ai.btree.branch.Selector
import com.badlogic.gdx.ai.btree.branch.Sequence
import com.ovle.rl.model.game.ai.GetTask
import com.ovle.rl.model.game.ai.GetTaskExec
import com.ovle.rl.model.game.ai.TaskCancelExec
import com.ovle.rl.model.game.ai.impl.behaviorTree.BaseTask
import com.ovle.rl.model.game.ai.impl.behaviorTree.config.task.*
import com.ovle.rl.model.game.ai.impl.behaviorTree.config.task.stub.failTask
import com.ovle.rl.model.game.ai.impl.behaviorTree.config.task.stub.successTask
import com.ovle.rl.model.game.ai.impl.behaviorTree.dto.BTParams
import com.ovle.rl.model.game.ai.task.AITask
import com.ovle.rl.model.game.ai.task.Composite
import com.ovle.rl.model.game.ai.task.impl.*
import com.ovle.rl.model.game.container.EntityDropCarryItemCommand
import com.ovle.rl.model.game.container.carriedItem
import com.ovle.rl.model.game.space.EntityStopMovementCommand
import com.ovle.utils.event.EventBus.send


val taskMappings = mapOf<Class<out AITask>, GetTask>(
    Composite.Sequence::class.java to { at ->
        val tasks = (at as Composite.Sequence).children
        val mappedTasks = tasks.map { t -> mapTask(t) }.toTypedArray()
        Sequence(*mappedTasks)
    },
    Composite.Selector::class.java to { at ->
        val tasks = (at as Composite.Selector).children
        val mappedTasks = tasks.map { t -> mapTask(t) }.toTypedArray()
        Selector(*mappedTasks)
    },
    UseSkill::class.java to { at ->
        baseTask(at) { useSkillTask(at as UseSkill) }
    },
    Move::class.java to { at ->
        baseTask(at, ::cancelMove) { moveTask(at as Move) }
    },
    Take::class.java to { at ->
        baseTask(at) { takeTask(at as Take) }
    },
    Drop::class.java to { at ->
        baseTask(at) { dropTask(at as Drop) }
    },
    Eat::class.java to { at ->
        baseTask(at) { eatTask(at as Eat) }
    },
    Sleep::class.java to { at ->
        baseTask(at) { sleep(at as Sleep) }
    },
    Success::class.java to { s ->
        baseTask(s) { successTask() }
    },
    Fail::class.java to { s ->
        baseTask(s) { failTask() }
    }
)

fun mapTask(task: AITask): Task<BTParams> {
    val mapping = if (task is Move) taskMappings[Move::class.java]
        else taskMappings[task::class.java]
    checkNotNull(mapping) { "no mapping found for task $task" }

    return mapping.invoke(task)
}


private fun baseTask(task: AITask, cancel: TaskCancelExec? = null, getTaskExec: GetTaskExec): BaseTask {
    val name = task::class.java.simpleName
    return BaseTask(name, { getTaskExec.invoke(task) }, cancel)
}


private fun cancelMove(params: BTParams) {
    val entity = params.owner
    send(EntityStopMovementCommand(entity))

    entity.carriedItem()?.let {
        send(EntityDropCarryItemCommand(entity, it))
    }
}