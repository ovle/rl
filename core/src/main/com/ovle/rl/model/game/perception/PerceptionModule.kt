package com.ovle.rl.model.game.perception

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.*


val perceptionModule = DI.Module("perception") {
    bind<FOVHelper>() with singleton {
        FOVHelper()
    }
    bind<EntitySystem>().inSet() with singleton {
        PerceptionSystem(instance())
    }
}