package com.ovle.rl.model.game.time.dto

import com.ovle.rl.Tick

data class TimerData(
    var lastUpdate: Tick = 0,
    var time: Tick = 0
) {
    val diff
        get() = time - lastUpdate

    fun update() {
        lastUpdate = time
    }

    fun reset() {
        time = 0
        lastUpdate = 0
    }
}