package com.ovle.rl.model.game.ai.decision

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.game.dto.player.Players
import com.ovle.rl.model.game.skill.helper.SkillHelper
import com.ovle.rl.model.game.time.dto.GameTime
import com.ovle.utils.RandomParams

data class AITaskFactoryContext(
    val entities: Collection<Entity>,
    val aiOwners: Collection<Entity>,
    val enemies: Map<Entity, Collection<Entity>>,
    val skillHelper: SkillHelper,
    val content: LocationContent,
    val players: Players,
    val gameTime: GameTime,
    val random: RandomParams
)