package com.ovle.rl.model.game.skill

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.skill.helper.PositionHelper
import com.ovle.rl.model.game.skill.helper.SkillHelper
import com.ovle.rl.model.game.skill.helper.ValidPlayerSkillTargetsHelper
import org.kodein.di.*


val skillModule = DI.Module("skill") {
    bind<PositionHelper>() with singleton {
        PositionHelper(instance())
    }
    bind<ValidPlayerSkillTargetsHelper>() with singleton {
        ValidPlayerSkillTargetsHelper()
    }
    bind<SkillHelper>() with singleton {
        SkillHelper(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        SkillSystem()
    }
}