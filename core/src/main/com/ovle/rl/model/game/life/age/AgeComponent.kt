package com.ovle.rl.model.game.life.age

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode

class AgeComponent(
    val maxAgeHours: Int? = null,
    val dieChancePerHour: Float? = null,
    val transform: TransformConfig? = null
) : EntityComponent() {
    var ageHours: Int = 0

    override fun gameInfo() = GameInfoNode(
        "age:", listOfNotNull(
            GameInfoNode("age: $ageHours"),
            transform?.let { GameInfoNode("transform: ${it.entity.name}") }
        )
    )
}