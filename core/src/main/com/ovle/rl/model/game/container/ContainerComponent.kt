package com.ovle.rl.model.game.container

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.util.info

//todo
//capacity
//resource filter? i.e. stash can't contain liquids; non-item entities can't be stored (need item concept)
//key? i.e. stash needs a (specific) key to be opened
//states? opened, closed, empty
class ContainerComponent(
    val items: MutableCollection<Entity> = mutableListOf()
) : EntityComponent() {

    override fun gameInfo() = GameInfoNode("contains:", items.map { GameInfoNode(it.name()) })
}