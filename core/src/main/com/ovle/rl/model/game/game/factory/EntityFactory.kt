package com.ovle.rl.model.game.game.factory

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.ovle.rl.EntityId
import com.ovle.rl.model.game.core.component.CoreComponent
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.template.TemplateComponent
import com.ovle.rl.model.template.entity.EntityTemplate


class EntityFactory {

    fun newTemplatedEntity(id: EntityId, template: EntityTemplate, source: Entity?, engine: Engine): Entity {
        val components = basicComponents(id, template, source) +
            templatedComponents(template)

        return engine.newEntity(*components.toTypedArray())
    }

    private fun basicComponents(id: EntityId, template: EntityTemplate, source: Entity?): List<EntityComponent> {
        val coreComponent = CoreComponent(id, source)
        val templateComponent = TemplateComponent(template)
        return listOf(coreComponent, templateComponent)
    }

    private fun templatedComponents(template: EntityTemplate): List<EntityComponent> = template.fullState.toList()
}