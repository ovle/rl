package com.ovle.rl.model.game.core.component

import com.badlogic.ashley.core.Entity
import com.ovle.rl.EntityId


class CoreComponent(
    val id: EntityId,
    val source: Entity? = null,

    var isExists: Boolean = true, //todo remove
    var isSkipped: Boolean = false,
) : EntityComponent() {

    override fun gameInfo() = GameInfoNode(
        "core:", listOfNotNull(
            GameInfoNode("id: $id"),
        )
    )
}