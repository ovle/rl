package com.ovle.rl.model.game.task.target.selection

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.game.task.target.TaskTargetCheck.*

data class AttackTargetSelection(
    var entity: Entity? = null
): TaskTargetSelection() {

    override fun taskTargets(): Collection<TaskTarget> =
        setOf(TaskTarget.Entity(entity!!))


    override fun updateEntities(entities: Collection<Entity>) {
        entity = entities.firstOrNull()
    }


    override fun check(): TaskTargetCheck =
        if (entity != null) Success
        else Error("select entity")

    override fun copy() = AttackTargetSelection(entity)

    override fun reset() {
        entity = null
    }
}