package com.ovle.rl.model.game.task.target.selection

import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck.*
import com.ovle.rl.model.template.entity.EntityTemplate

data class CraftTargetSelection(
    var template: CraftTemplate? = null,
    var resource: EntityTemplate? = null,
    var approved: Boolean = false,
): TaskTargetSelection() {

    override fun taskTargets(): Collection<TaskTarget> =
        setOf(TaskTarget.Craft(template!!, resource))


    override fun updateCraftTemplate(template: CraftTemplate?) {
        this.template = template
    }

    override fun updateResource(resource: EntityTemplate?) {
        this.resource = resource
    }

    override fun check() = when {
        template == null -> Error("select entity")
        resource == null -> Error("select resource")
        !approved -> NotApproved
        else -> Success
    }

    override fun copy() = CraftTargetSelection(template, resource)

    override fun reset() {
        resource = null
        approved = false
    }
}