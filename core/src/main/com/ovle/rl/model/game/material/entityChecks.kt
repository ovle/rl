package com.ovle.rl.model.game.material

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.material.Components.material
import ktx.ashley.get
import ktx.ashley.has

fun Entity.isMadeOf(mt: MaterialTemplate) = this.has(material) && this[material]!!.template == mt
