package com.ovle.rl.model.game.task.target.selection

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.game.task.target.TaskTargetCheck.*

data class ExploreTargetSelection(
    var point: GridPoint2? = null
): TaskTargetSelection() {

    override fun taskTargets(): Collection<TaskTarget> =
        setOf(TaskTarget.Point(point!!))


    override fun updatePoints(selection: Collection<GridPoint2>) {
        this.point = selection.single()
    }


    override fun check(): TaskTargetCheck =
        if (point != null) Success
        else Error("select position")

    override fun copy() = ExploreTargetSelection(point)

    override fun reset() {
        point = null
    }
}