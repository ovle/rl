package com.ovle.rl.model.game.time

import com.badlogic.ashley.core.Entity
import com.ovle.utils.event.EventBus.send
import com.ovle.rl.Tick
import com.ovle.rl.TimerEffect
import com.ovle.rl.TimerStep
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.LocationLoadedEvent
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.time.Components.globalTime
import com.ovle.rl.model.game.time.Components.timer
import com.ovle.rl.model.game.time.dto.Timer
import com.ovle.rl.model.game.time.dto.TimerEffectParamsFactoryPayload
import com.ovle.rl.model.util.*
import com.ovle.utils.RandomParams
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get


class TimerSystem : TimedEventSystem() {

    companion object {
        const val TIMER_UPDATE_STEP = SystemUpdateInterval.BASE
    }


    override fun subscribeIntr() {
        subscribe<LocationLoadedEvent>(this) { onLocationLoadedEvent(it.location) }
    }

    private fun onLocationLoadedEvent(location: Location) {
        val game = game()
        val globalTimers = game[globalTime]!!.timers
        val globalTemplates = location.template.globalTimerTemplates

        globalTimers += globalTemplates.map { Timer(it) }
    }


    override fun updateTime() {
        val game = game()
        val location = game.location()
        val random = location.random
        val content = location.content
        val timerEntities = content.entities.all().timers()

        for (e in timerEntities) {
            val timers = e[timer]!!.timers
            processTimers(timers, updateInterval, e, random)
        }

        val globalTimers = game[globalTime]!!.timers
        processTimers(globalTimers, updateInterval, game, random)
    }

    private fun processTimers(
        timers: MutableCollection<Timer>, deltaTicks: Tick, entity: Entity, random: RandomParams
    ) {
        if (timers.isEmpty()) return

        val finished = mutableListOf<Timer>()
        for (t in timers) {
            processTimer(t, deltaTicks, entity, random)
            if (t.finished) {
                finished += t
            }
        }
        timers.removeAll(finished.toSet())
    }

    private fun processTimer(
        timer: Timer, deltaTicks: Tick, entity: Entity, random: RandomParams
    ) {
        check(!timer.finished) { "trying to process finished timer: ${timer.info()}" }

        val data = timer.data
        data.time += deltaTicks

        val template = timer.template
        if (data.diff < TIMER_UPDATE_STEP) return

        data.update()
        timer.repeatCount++
        val repeatCount = timer.repeatCount

        if (template.fireCheck.invoke(random)) {
            processEffects(template.onFire, entity, repeatCount)
        }

        send(TimerFiredEvent(entity, timer))

        val maxRepeats = template.maxRepeats ?: return
        if (repeatCount >= maxRepeats) {
            if (template.finishCheck.invoke(random)) {
                processEffects(template.onFinish, entity, repeatCount)
            }

            timer.finished = true

            send(TimerFinishedEvent(entity, timer))
        }
    }

    private fun processEffects(effects: Collection<TimerEffect>, entity: Entity, repeatCount: TimerStep) {
        effects.forEach { (effect, getParams) ->
            val payload = TimerEffectParamsFactoryPayload(entity, repeatCount)
            effect.invoke(getParams.invoke(payload))
        }
    }
}