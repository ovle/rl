package com.ovle.rl.model.game.life

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.gather.Components.source
import com.ovle.rl.model.game.life.Components.health
import com.ovle.rl.model.game.life.Components.hunger
import com.ovle.rl.model.game.life.Components.sleep
import com.ovle.rl.model.game.life.age.AgeComponent
import com.ovle.rl.model.game.life.hunger.HungerComponent
import com.ovle.rl.model.game.schedule.DayScheduleComponent
import com.ovle.rl.model.game.life.sleep.SleepComponent
import com.ovle.rl.model.game.life.sleep.SleepPlaceComponent
import com.ovle.rl.model.game.material.Components.material
import com.ovle.rl.model.game.material.MaterialTemplate
import com.ovle.rl.model.game.material.material
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get
import ktx.ashley.has


object Components {
    val age = mapper<AgeComponent>()
    val health = mapper<HealthComponent>()
    val daySchedule = mapper<DayScheduleComponent>()
    val sleep = mapper<SleepComponent>()
    val sleepPlace = mapper<SleepPlaceComponent>()
    val hunger = mapper<HungerComponent>()
}

fun Entity.hp(): Int {
    check (has(health)) { }

    return this[health]!!.health
}

fun Entity.sleepPlacePointOrNull(): GridPoint2? {
    return this[sleep]?.place?.position()
}

fun Entity.maxHp(): Int {
    check (has(health)) { }

    return this[health]!!.maxHealth
}


fun Entity.consumes(e: Entity): Boolean {
    if (!e.has(material)) return false
    val materialTemplate = e[material]!!.template ?: return false
    return consumes(materialTemplate)
}

fun Entity.consumes(et: EntityTemplate): Boolean {
    val consumeMaterials = this[hunger]?.consumes ?: emptySet()
    return et.material() in consumeMaterials
}

fun Entity.consumes(mt: MaterialTemplate): Boolean {
    return mt in (this[hunger]?.consumes ?: emptySet())
}

fun Entity.consumesGatherResult(e: Entity): Boolean {
    val gatherTemplates = e[source]!!.info.map { it.yield }
    return gatherTemplates.any { consumes(it) }
}
