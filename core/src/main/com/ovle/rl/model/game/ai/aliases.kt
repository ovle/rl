package com.ovle.rl.model.game.ai

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.ai.btree.Task
import com.ovle.rl.model.game.ai.impl.behaviorTree.dto.BTParams
import com.ovle.rl.model.game.ai.task.AITask

typealias GetTask = (AITask) -> Task<BTParams>

typealias TaskResource = Entity
typealias TaskExecResult = Task.Status
typealias TaskExec = (BTParams) -> TaskExecResult
typealias TaskCancelExec = (BTParams) -> Unit
typealias TaskExecFactory = (BTParams) -> TaskExec
typealias GetTaskExec = (AITask) -> TaskExec
