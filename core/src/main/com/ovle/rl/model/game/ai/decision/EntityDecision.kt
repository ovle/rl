package com.ovle.rl.model.game.ai.decision

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.task.RootTask

data class EntityDecision(
    val entity: Entity,
    val aiTask: RootTask
)