package com.ovle.rl.model.game.ai.decision

import com.badlogic.ashley.core.Entity
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.ai.task.RootTask

interface DecisionProcessor {
    fun setTask(entity: Entity, game: GameEntity, rootTask: RootTask)
    fun process(entity: Entity, game: GameEntity)

    fun onEntityAIDisabled(entity: Entity) { }
    fun onEntityAIEnabled(entity: Entity) { }
}