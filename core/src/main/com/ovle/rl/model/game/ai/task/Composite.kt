package com.ovle.rl.model.game.ai.task

import com.ovle.rl.model.game.core.component.GameInfoNode

sealed class Composite(
    vararg val children: AITask
): AITask() {

    class Sequence(vararg children: AITask?): Composite(*children.filterNotNull().toTypedArray()) {
        override fun gameInfo() = GameInfoNode("Seq", children.map { it.gameInfo() })
    }

    class Selector(vararg children: AITask?): Composite(*children.filterNotNull().toTypedArray()) {
        override fun gameInfo() = GameInfoNode("Sel", children.map { it.gameInfo() })
    }


    override fun debugInfo() = "[${children.joinToString(", ") { it.debugInfo() }}]"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Sequence
        if (!children.contentEquals(other.children)) return false

        return true
    }

    override fun hashCode(): Int {
        return children.contentHashCode()
    }
}