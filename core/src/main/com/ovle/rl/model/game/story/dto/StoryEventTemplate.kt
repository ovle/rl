package com.ovle.rl.model.game.story.dto

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.template.NamedTemplate

class StoryEventTemplate(
    override val name: String,
    val condition: StoryEventCondition,
    val effectConfig: StoryEventEffectConfig,
    val effect: (StoryEventEffectConfig, Location) -> Unit
): NamedTemplate