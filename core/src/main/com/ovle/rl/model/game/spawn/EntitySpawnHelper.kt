package com.ovle.rl.model.game.spawn

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.positions
import com.ovle.utils.RandomParams
import com.ovle.utils.chancedIndex


class EntitySpawnHelper(
    private val spawnChecker: EntitySpawnCheckHelper
) {

    fun spawnEntities(
        location: Location,
        entityTemplates: Collection<EntityTemplate>,
        isPerHourSpawn: Boolean
    ): Collection<EntitySpawn> {
        val random = location.random
        val tiles = location.content.tiles
        val claimedPoints = location.content.entities.all().positions()
        val allowedEntities = location.template.entitySpawnConfig.allowedEntities
        val checkEntities = entityTemplates.filter {
            it.spawnTemplate != null && it in allowedEntities
        }

        val result = mutableListOf<EntitySpawn>()
        val points = tiles.points

        for (point in points) {
            if (point in claimedPoints) continue

            val entity = spawnEntity(
                checkEntities, tiles, point, isPerHourSpawn, random
            ) ?: continue

            result += EntitySpawn(
                template = entity, position = point
            )
        }

        return result
    }

    private fun spawnEntity(
        entities: Collection<EntityTemplate>,
        tiles: TileArray,
        point: GridPoint2,
        isPerHourSpawn: Boolean,
        random: RandomParams
    ): EntityTemplate? {
        val spawnableTemplates = entities.filter {
            isSpawnable(it, random, tiles, point, isPerHourSpawn)
        }
        if (spawnableTemplates.isEmpty()) return null

        val chanceTotal = spawnableTemplates.size
        val coeff = 1.0f / chanceTotal.toFloat() //todo wtf
        val table = spawnableTemplates.map { it.spawnTemplate!!.chancePerTile * coeff }
        val value = random.kRandom.nextFloat()
        val spawnIndex = table.chancedIndex(value) ?: return null

        return spawnableTemplates[spawnIndex]
    }

    private fun isSpawnable(
        template: EntityTemplate, random: RandomParams, tiles: TileArray, point: GridPoint2, isPerHourSpawn: Boolean
    ): Boolean {
        val spawnTemplate = template.spawnTemplate!!
        val perHourCheck = !isPerHourSpawn || random.kRandom.nextDouble() < spawnTemplate.chancePerHour
        if (!perHourCheck) return false

        return spawnChecker.isCanSpawn(spawnTemplate, tiles, point)
    }
}