package com.ovle.rl.model.game.tile

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.effect.Components.staticEffect
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.space.EntityChangedPositionEvent
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.Components.timer
import com.ovle.rl.model.game.time.dto.Timer

import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import ktx.ashley.get


class TileSystem(
    private val config: Collection<TileEffectsTemplate>
) : BaseSystem() {

    override fun subscribe() {
        subscribe<ChangeTileCommand>(this) { onChangeTileCommand(it.tile, it.position) }
        subscribe<EntityChangedPositionEvent>(this) { onEntityChangedPositionEvent(it.entity, it.oldPosition) }
    }

    private fun onChangeTileCommand(newTile: TileTemplate, position: GridPoint2) {
        val location = game().location()
        val tiles = location.content.tiles
        check(tiles.isValid(position)) { "invalid tile position: $position" }

        val (x, y) = position
        val oldTile = tiles[x, y]

        val actualNewTile = if (newTile != TileTemplate.NOTHING) newTile
            else newTile(position, location) ?: newTile
        tiles[x, y] = actualNewTile

        send(TileChangedEvent(oldTile, actualNewTile, position))
    }

    private fun onEntityChangedPositionEvent(entity: Entity, oldPosition: GridPoint2) {
        val location = game().location()
        val tiles = location.content.tiles
        val position = entity.position()

        //todo how to process EntityChangedPositionEvent on game init?
        val oldTiles = setOf(tiles[oldPosition.x, oldPosition.y])
        val newTiles = setOf(tiles[position.x, position.y])
        val exitTiles = oldTiles.subtract(newTiles)
        val enterTiles = newTiles.subtract(oldTiles)

        exitTiles.forEach { cleanupEffects(entity, it) }
        enterTiles.forEach { applyEffects(entity, it) }
    }

    private fun cleanupEffects(entity: Entity, tile: TileTemplate) {
        val effectsConfigs = effectConfigs(tile, entity)

        //todo check by timer name only?
        val timers = entity[timer]!!.timers
        val timerTemplates = effectsConfigs.flatMap { it.timerEffects }.toSet()

        timers.removeAll { it.template in timerTemplates }

        val effects = entity[staticEffect]!!.effects
        val staticEffectTemplates = effectsConfigs.flatMap { it.staticEffects }.toSet()
        val effectsToRemove = effects.filter { it in staticEffectTemplates }
            .onEach { it.revert.invoke(entity) }

        effectsToRemove.forEach { effects.remove(it) }
    }


    private fun applyEffects(entity: Entity, tile: TileTemplate) {
        val effectsConfigs = effectConfigs(tile, entity)

        val timers = entity[timer]!!.timers
        val timerTemplates = timers.map { it.template }.toSet()
        val newTimerTemplates = effectsConfigs.flatMap { it.timerEffects }
            .filterNot { it in timerTemplates }

        timers += newTimerTemplates.map { Timer(template = it) }

        val staticEffectTemplates = entity[staticEffect]!!.effects
        val newStaticEffectTemplates = effectsConfigs.flatMap { it.staticEffects }
            .filterNot { it in staticEffectTemplates }
            .onEach { it.apply.invoke(entity) }

        staticEffectTemplates += newStaticEffectTemplates
    }

    private fun effectConfigs(tile: TileTemplate, entity: Entity) =
        config.filter { tile in it.tiles && it.entityCheck.invoke(entity) }

    
    private fun newTile(point: GridPoint2, location: Location): TileTemplate? {
        val grids = location.content.grids
        val tileMapper = location.template.tileMapper

        return tileMapper.map(grids.value(point))
    }
}