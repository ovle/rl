package com.ovle.rl.model.game.game.dto.location

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.LocationId
import com.ovle.rl.model.game.game.dto.player.Players
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.grid.world.WorldPlace
import com.ovle.utils.RandomParams


data class Location(
    val id: LocationId,
    val template: LocationTemplate,
    val name: String,
    val worldPoint: GridPoint2?,
    val worldPlace: WorldPlace?,
    val random: RandomParams,

    val content: LocationContent,
    val players: Players
)