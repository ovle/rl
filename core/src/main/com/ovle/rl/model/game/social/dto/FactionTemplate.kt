package com.ovle.rl.model.game.social.dto

import com.ovle.rl.model.template.NamedTemplate

data class FactionTemplate(
    override val name: String,
): NamedTemplate