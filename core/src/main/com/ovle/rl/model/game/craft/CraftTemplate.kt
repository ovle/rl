package com.ovle.rl.model.game.craft

import com.ovle.rl.TileCheck1
import com.ovle.rl.model.template.NamedTemplate
import com.ovle.rl.model.template.entity.EntityTemplate

data class CraftTemplate(
    override val name: String,
    val entity: EntityTemplate,
    val station: EntityTemplate? = null,
    //todo any of?
    val resources: Collection<EntityTemplate>? = null,
    val check: TileCheck1? = null
): NamedTemplate