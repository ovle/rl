package com.ovle.rl.model.game.collision

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.collision.Components.collision
import ktx.ashley.get
import ktx.ashley.has

class CollisionHelper {

    fun collisions(entity: Entity, entities: Collection<Entity>): Collection<Entity> {
        return entities.subtract(listOf(entity).toSet())
            .filter { isEntityCollidesWithEntity(entity, it) }
    }

    fun collisions(entity: Entity, points: Collection<GridPoint2>, tiles: TileArray): Collection<GridPoint2> {
        if (!isEntityCollides(entity)) return listOf()

        return points.filter {
            tiles.isValid(it) && tiles[it.x, it.y].props.isCollides
        }
    }


    private fun isEntityCollidesWithEntity(entity: Entity, checkEntity: Entity): Boolean {
        return isEntityCollides(entity) || isEntityCollides(checkEntity)
    }

    private fun isEntityCollides(entity: Entity): Boolean {
        if (!entity.has(collision)) return false

        val collisionComponent = entity[collision]!!
        return collisionComponent.active
    }
}