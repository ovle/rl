package com.ovle.rl.model.game.life.hunger

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.game.material.MaterialTemplate


class HungerComponent(
    val maxHunger: Int,
    val consumes: Collection<MaterialTemplate> = emptySet()
) : EntityComponent() {
    var hunger: Int = 0

    //todo statuses
    val isHungry
        get() = hunger >= maxHunger * 0.75
    val isStarved
        get() = hunger >= maxHunger

    override fun gameInfo() = GameInfoNode(
        "hunger:", listOf(
            GameInfoNode("hunger: ${hunger}/${maxHunger}"),
            GameInfoNode("consumes: [${consumes.joinToString { it.name }}]"),
        )
    )
}