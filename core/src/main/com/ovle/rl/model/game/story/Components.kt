package com.ovle.rl.model.game.story

import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.story.Components.story
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get

object Components {
    val story = mapper<StoryComponent>()
}

fun GameEntity.storyEvents() = this[story]!!.events