package com.ovle.rl.model.game.trigger

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.inSet
import org.kodein.di.singleton


val triggerModule = DI.Module("trigger") {
    bind<EntitySystem>().inSet() with singleton {
        TriggerSystem()
    }
}