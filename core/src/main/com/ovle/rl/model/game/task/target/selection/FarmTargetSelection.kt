package com.ovle.rl.model.game.task.target.selection

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck.*

data class FarmTargetSelection(
    var template: FarmTemplate? = null,
    val points: MutableCollection<GridPoint2> = mutableSetOf(),
    var approved: Boolean = false,
): TaskTargetSelection() {

    override val isMultiselect = true

    override fun taskTargets() = points.map { TaskTarget.Farm(it, template!!) }


    override fun updatePoints(selection: Collection<GridPoint2>) {
        if (template != null) points.addAll(selection)
    }

    override fun updateFarmTemplate(template: FarmTemplate?) {
        this.template = template
    }

    override fun check() = when {
        template == null -> Error("select crop")
        points.isEmpty() -> Error("select positions")
        !approved -> NotApproved
        else -> Success
    }

    override fun copy() = FarmTargetSelection(template, points.toMutableSet())

    override fun reset() {
        points.clear()
        approved = false
    }
}