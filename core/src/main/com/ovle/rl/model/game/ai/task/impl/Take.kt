package com.ovle.rl.model.game.ai.task.impl

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.task.AITask
import com.ovle.rl.model.util.info


data class Take(val target: Entity) : AITask() {
//    override fun gameInfo() = GameInfoNode("take: ${target.info()}")
    override fun debugInfo() = "take: ${target.info()}"
}