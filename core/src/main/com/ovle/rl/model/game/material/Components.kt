package com.ovle.rl.model.game.material

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.material.Components.material
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get
import ktx.ashley.has


object Components {
    val material = mapper<MaterialComponent>()
}

fun Entity.material(): MaterialTemplate? {
    check(has(material)) { }

    return this[material]!!.template
}

fun EntityTemplate.material() = getState()
    .filterIsInstance<MaterialComponent>()
    .singleOrNull()?.template