package com.ovle.rl.model.game.factory

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.GameCommand

class EntityFactoryCreateEntityCommand(val source: Entity) : GameCommand()