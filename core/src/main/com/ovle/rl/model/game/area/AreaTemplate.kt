package com.ovle.rl.model.game.area

import com.ovle.rl.model.game.tile.TileCheck
import com.ovle.rl.model.template.NamedTemplate

data class AreaTemplate(
    override val name: String,
    val processor: AreaProcessor? = null,
    val tileCheck: TileCheck,
    val checks: Collection<AreaCheck> = emptyList()
): NamedTemplate