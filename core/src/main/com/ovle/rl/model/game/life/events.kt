package com.ovle.rl.model.game.life

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.EntityGameEvent
import com.ovle.rl.model.util.info
import com.ovle.rl.model.util.markDamage
import com.ovle.rl.model.util.markEntity
import com.ovle.rl.model.util.markHeal


class EntityDiedEvent(val entity: Entity) : EntityGameEvent(entity) {
    override val playerAwareMessage: String
        get() {
            val statusStr = "died".markDamage()
            return "${entity.info().markEntity()} $statusStr"
        }

    override val message: String
        get() = "${entity.info()} died!"
}

class EntityResurrectedEvent(val entity: Entity) : EntityGameEvent(entity) {
    override val playerAwareMessage: String
        get() {
            val statusStr = "resurrected".markHeal()
            return "${entity.info().markEntity()} $statusStr"
        }

    override val message: String
        get() = "${entity.info()} resurrected"
}

class EntityStartSleepEvent(val entity: Entity) : EntityGameEvent(entity) {
    override val playerAwareMessage: String
        get() {
            val statusStr = "fell asleep"
            return "${entity.info().markEntity()} $statusStr"
        }

    override val message: String
        get() = "${entity.info()} fell asleep"
}

class EntityAwakenEvent(val entity: Entity) : EntityGameEvent(entity) {
    override val playerAwareMessage: String
        get() {
            val statusStr = "awaken"
            return "${entity.info().markEntity()} $statusStr"
        }

    override val message: String
        get() = "${entity.info()} awaken"
}

class EntityTakenDamageEvent(val entity: Entity, val source: Entity?, val amount: Int) : EntityGameEvent(entity) {
    override val playerAwareMessage: String
        get() {
            val amountStr = "[$amount dmg]".markDamage()
            return "${entity.info().markEntity()} takes $amountStr" +
                if (source == null) "" else " from ${source.info().markEntity()}"
        }

    override val message: String
        get() = "${entity.info()} takes $amount damage from ${source.info()}"
}

class EntityHealedEvent(val entity: Entity, val source: Entity?, val amount: Int) : EntityGameEvent(entity) {
    override val playerAwareMessage: String
        get() {
            val amountStr = "[$amount dmg]".markHeal()
            return "${entity.info().markEntity()} heals $amountStr" +
                if (source == null) "" else " from ${source.info().markEntity()}"
        }

    override val message: String
        get() = "${entity.info()} heals $amount damage from ${source.info()}"
}