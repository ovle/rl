package com.ovle.rl.model.game.ai.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.decision.AITaskFactoryContext
import com.ovle.rl.model.game.ai.task.impl.Fail
import com.ovle.rl.model.game.ai.task.impl.Success
import com.ovle.rl.model.game.task.target.TaskTarget

interface TaskFactory {

    companion object {
        val SUCCESS = RootTask("success", "", Success)
        val FAIL = RootTask("success", "", Fail)
    }

    fun task(
        entity: Entity, target: TaskTarget, resource: TaskResource?, context: AITaskFactoryContext
    ): RootTask
}