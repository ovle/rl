package com.ovle.rl.model.game.game.dto.location.projection

import com.badlogic.gdx.math.GridPoint2


class AIKnownTileMap: KnownTileMap {

    override fun isKnownTile(p: GridPoint2) = true

    override fun markAsKnown(p: GridPoint2) {}
}