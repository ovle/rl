package com.ovle.rl.model.game.build

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.template.NamedTemplate
import com.ovle.rl.model.template.entity.EntityTemplate

data class BuildTemplate(
    override val name: String,
    val tile: TileTemplate,
    val material: EntityTemplate? = null,
    val check: BuildCheck? = null,
    val isAvailable: (Location) -> Boolean = { l -> true } //todo technology check
): NamedTemplate