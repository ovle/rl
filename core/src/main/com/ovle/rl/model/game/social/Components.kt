package com.ovle.rl.model.game.social

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.social.Components.social
import com.ovle.rl.model.game.social.dto.FactionTemplate
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get
import ktx.ashley.has

object Components {
    val social = mapper<SocialComponent>()
}


fun Entity.personalNameOrNull(): String? {
    return this[social]?.name
}

fun Entity.faction(): FactionTemplate? {
    if (!has(social)) return null

    return this[social]!!.faction
}

fun Entity.player(): LocationPlayer? {
    if (!has(social)) return null

    return this[social]!!.player
}