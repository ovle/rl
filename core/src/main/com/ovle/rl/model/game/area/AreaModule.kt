package com.ovle.rl.model.game.area

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.*


val areaModule = DI.Module("area") {
    bind<ValidAreasHelper>() with singleton {
        ValidAreasHelper()
    }
    bind<AreasHelper>() with singleton {
        AreasHelper(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        AreaSystem(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        AreaProcessingSystem(instance())
    }
}