package com.ovle.rl.model.game.game.dto.location.projection

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.Array2d.Companion.array


class PlayerVisibleTileMap(
    val content: LocationContent
): VisibleTileMap {

    private var visibleTilesMask: Array2d<Int> = array(0 , content.tiles.size)


    override fun isVisibleTile(p: GridPoint2) = visibleTilesMask.isValid(p)
        && visibleTilesMask[p.x, p.y] > 0

    override fun incVisibility(p: GridPoint2) {
        visibleTilesMask[p.x, p.y]++
    }

    override fun decVisibility(p: GridPoint2) {
        visibleTilesMask[p.x, p.y]--
    }
}