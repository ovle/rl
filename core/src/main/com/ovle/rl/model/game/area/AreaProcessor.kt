package com.ovle.rl.model.game.area

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer

interface AreaProcessor {

    fun process(
        areas: Collection<AreaInfo>, player: LocationPlayer, location: Location
    )
}