package com.ovle.rl.model.game.transform

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.Components.template
import com.ovle.rl.model.game.core.component.CoreComponent
import com.ovle.rl.model.game.core.component.template.TemplateComponent
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.social.SocialComponent
import com.ovle.rl.model.game.space.body.BodyComponent
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get


class TransformSystem : BaseSystem() {

    companion object {
        private val COMPONENT_TYPES_TO_KEEP = setOf(
            CoreComponent::class,
            TemplateComponent::class,
            BodyComponent::class,
            SocialComponent::class,
        )
    }

    override fun subscribe() {
        subscribe<TransformEntityCommand>(this) { onEntityTransformCommand(it.entity, it.target) }
    }

    private fun onEntityTransformCommand(entity: Entity, target: EntityTemplate) {
        val keepComps = entity.components
            .filter { it::class in COMPONENT_TYPES_TO_KEEP }

        entity.removeAll()

        val newComps = target.fullState
            .filter { it::class !in COMPONENT_TYPES_TO_KEEP } + keepComps
        newComps.forEach {
            entity.add(it)
        }

        entity[template]!!.template = target

        send(EntityTransformedEvent(entity, target))
    }
}