package com.ovle.rl.model.game.game.dto.location.projection

import com.badlogic.gdx.math.GridPoint2


class AIVisibleTileMap: VisibleTileMap {

    override fun isVisibleTile(p: GridPoint2) = true

    override fun incVisibility(p: GridPoint2) {}

    override fun decVisibility(p: GridPoint2) {}
}