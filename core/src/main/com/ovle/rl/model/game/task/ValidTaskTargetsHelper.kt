package com.ovle.rl.model.game.task

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetConfig
import kotlin.reflect.KFunction1

class ValidTaskTargetsHelper {

    fun validTargets(
        targets: Collection<TaskTarget>,
        targetConfig: TaskTargetConfig, location: Location, player: LocationPlayer
    ): Collection<TaskTarget> {
        val content = location.content
        val isKnownTile = player.locationProjection.knownMap::isKnownTile
        fun filterKnownTargets(t: Collection<TaskTarget>) = t.filter { targetConfig.targetFilter.invoke(it, location, player) }
        fun filterAllTargets(t: Collection<TaskTarget>) = t.filter { it.positions().all { p -> content.tiles.isValid(p) } }

        val allTargets = filterAllTargets(targets)
        val knownTargets = knownTargets(allTargets, isKnownTile)
        val unknownTargets = if (targetConfig.allowUnknownTargets) allTargets - knownTargets
            else emptySet()

        return filterKnownTargets(knownTargets) + unknownTargets
    }


    private fun knownTargets(
        allTargets: List<TaskTarget>, isKnownTile: KFunction1<GridPoint2, Boolean>
    ) = allTargets.filter {
        val ps = it.positions()
        ps.isEmpty() || ps.any { p -> isKnownTile(p) }
    }.toSet()
}