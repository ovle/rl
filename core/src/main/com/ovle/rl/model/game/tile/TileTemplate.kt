package com.ovle.rl.model.game.tile

import com.ovle.rl.model.template.NamedTemplate


class TileTemplate(
    val char: Char,
    val displayName: String? = null,
    val props: TileProps = TileProps()
): NamedTemplate {
    override val name: String
        get() = char.toString()

    companion object {
        val WHATEVER = TileTemplate('`', null, TileProps())
        val NOTHING = TileTemplate(' ', null, TileProps())
    }
}