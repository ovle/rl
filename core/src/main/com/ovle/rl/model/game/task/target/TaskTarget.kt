package com.ovle.rl.model.game.task.target

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.build.BuildTemplate
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.space.positionOrNull
import com.ovle.rl.model.template.entity.EntityTemplate


sealed class TaskTarget {

    data class Point(val point: GridPoint2): TaskTarget() {
        override fun positions() = setOf(point)
    }

    data class Entity(val entity: com.badlogic.ashley.core.Entity): TaskTarget() {
        override fun positions() = setOfNotNull(entity.positionOrNull())
    }

    data class Build(
        val point: GridPoint2, val template: BuildTemplate
    ): TaskTarget() {
        override fun positions() = setOf(point)
        override fun resource() = template.material
    }

    data class Craft(
        val template: CraftTemplate, val resource: EntityTemplate?
    ): TaskTarget() {
        override fun positions() = emptySet<GridPoint2>()
        override fun resource() = resource
    }

    data class Carry(
        val point: GridPoint2, val entity: com.badlogic.ashley.core.Entity
    ): TaskTarget() {
        override fun positions() = setOf(point)
    }

    data class Farm(
        val point: GridPoint2, val template: FarmTemplate
    ): TaskTarget() {
        override fun positions() = setOf(point)
        override fun resource() = template.resource
    }

    abstract fun positions(): Collection<GridPoint2>

    //todo build/craft specific
    open fun resource(): EntityTemplate? = null
}
