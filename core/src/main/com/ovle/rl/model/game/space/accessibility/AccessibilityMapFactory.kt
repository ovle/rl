package com.ovle.rl.model.game.space.accessibility

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.areas

/**
 *
 */
class AccessibilityMapFactory(
    private val accessibilityHelper: AccessibilityHelper,
) {
    companion object {
        const val INACCESSIBLE_MAP_VALUE = -1
    }

    fun accessibilityMap(
        moveType: MoveType, unknownTiles: Collection<GridPoint2>, content: LocationContent, checkEntities: Boolean
    ): AccessibilityMap {
        val points = content.tiles.points
        val size = content.tiles.size

        val accessiblePoints = accessibilityHelper.accessiblePoints(
            points, content, moveType, checkEntities
        ) + unknownTiles
        val inaccessiblePoints = points - accessiblePoints.toSet()
        
        val areas = areas(size, inaccessiblePoints)
        val result = Array2d.array(INACCESSIBLE_MAP_VALUE, size)
        val areasMap = areas.mapIndexed { i, ps -> i to ps }.toMap()
        areasMap.forEach { (i, ps) ->
            ps.forEach { p ->
                result[p.x, p.y] = i
            }
        }

        return AccessibilityMap(result)
    }
}