package com.ovle.rl.model.game.game.dto.location.projection

import com.badlogic.gdx.math.GridPoint2


interface VisibleTileMap {

    fun isVisibleTile(p: GridPoint2): Boolean

    fun incVisibility(p: GridPoint2)

    fun decVisibility(p: GridPoint2)
}