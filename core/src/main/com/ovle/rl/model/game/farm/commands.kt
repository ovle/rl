package com.ovle.rl.model.game.farm

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.GameCommand

class FarmCommand(
    val position: GridPoint2, val template: FarmTemplate, val resource: Entity?
) : GameCommand()