package com.ovle.rl.model.game.social

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.Components.core
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.InitEntityCommand
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.life.EntityTakenDamageEvent
import com.ovle.rl.model.game.spawn.portal.Components.portal
import com.ovle.rl.model.game.social.Components.social
import com.ovle.rl.model.procgen.name.entity.EntityNameGenerator

import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.ashley.has

/**
 *
 */
class SocialSystem(
    private val entityNameGenerator: EntityNameGenerator,
) : BaseSystem() {

    override fun subscribe() {
        subscribe<InitEntityCommand>(this) { onInitEntityCommand(it.entity) }
        subscribe<EntityTakenDamageEvent>(this) { onEntityTakenDamageEvent(it.entity, it.source) }
    }

    private fun onInitEntityCommand(entity: Entity) {
        if (!entity.has(social)) return

        val location = game().location()
        val source = entity[core]!!.source

        initFaction(entity, source, location)

        val random = location.random
        entity[social]!!.name = entityNameGenerator.entityName(entity, random)
    }

    private fun onEntityTakenDamageEvent(entity: Entity, source: Entity?) {
        if (!entity.has(social)) return
        source ?: return

        entity[social]!!.personalEnemies += source
    }

    private fun initFaction(entity: Entity, source: Entity?, location: Location) {
        val socialComponent = entity[social]!!
        if (socialComponent.faction != null) return

        val factionsConfig = location.template.factions

        val faction = when {
            source != null -> when {
                source.has(social) -> source[social]!!.faction
                source.has(portal) -> factionsConfig.playerFaction //todo
                else -> null
            }
            else -> null
        }
        val defaultFaction = factionsConfig.getSpawnFaction(entity)

        socialComponent.faction = faction ?: defaultFaction
        socialComponent.player = location.players.byFaction[socialComponent.faction]
    }
}