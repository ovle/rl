package com.ovle.rl.model.game.collision

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.collision.Components.collision
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.life.EntityDiedEvent
import com.ovle.rl.model.game.life.EntityResurrectedEvent
import com.ovle.rl.model.game.space.EntityChangedPositionEvent
import com.ovle.rl.model.game.space.aoeOrNull
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.*
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.ashley.has

/**
 * System for processing entity collision with other entities and tiles
 * 'collision' is about if we should trigger collision event or not
 */
class CollisionSystem(
    private val collisionHelper: CollisionHelper
): BaseSystem() {

    override fun subscribe() {
        subscribe<EntityChangedPositionEvent>(this) { onEntityChangedPositionEvent(it.entity) }
        subscribe<EntityAOEChangedEvent>(this) { onEntityAOEChangedEvent(it.entity) }

        subscribe<EntityDiedEvent>(this) { onEntityDiedEvent(it.entity) }
        subscribe<EntityResurrectedEvent>(this) { onEntityResurrectedEvent(it.entity) }
    }

    private fun onEntityChangedPositionEvent(entity: Entity) {
        checkCollisions(entity)
    }

    private fun onEntityAOEChangedEvent(entity: Entity) {
        //todo how many collisions should be here for the same entities/aoe?
        checkCollisions(entity)
    }

    private fun onEntityDiedEvent(entity: Entity) {
        entity[collision]!!.active = false
    }

    private fun onEntityResurrectedEvent(entity: Entity) {
        entity[collision]!!.active = true
    }

    private fun checkCollisions(entity: Entity) {
        if (!entity.has(collision)) return

        val collisionComponent = entity[collision]!!
        if (!collisionComponent.active) return

        val location = game().location()
        val body = entity.aoeOrNull() ?: setOf(entity.position())

        val entities = location.content.entities.on(body)
        val collisionEntities = collisionHelper.collisions(entity, entities)
        collisionEntities.forEach {
            send(EntityCollidesEntityEvent(entity, it))
            send(EntityCollidesEntityEvent(it, entity))
        }

        val collisionTiles = collisionHelper.collisions(entity, body, location.content.tiles)
        if (collisionTiles.isNotEmpty()) {
            send(EntityCollidesTilesEvent(entity, collisionTiles))
        }
    }
}