package com.ovle.rl.model.game.factory

import com.ovle.rl.model.game.social.dto.FactionTemplate

data class ProductSpawnConfig(
    val chance: Float,
    val number: Int,
    val radius: Int,
    val faction: FactionTemplate? = null
)