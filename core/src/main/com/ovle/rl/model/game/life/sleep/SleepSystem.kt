package com.ovle.rl.model.game.life.sleep

import com.badlogic.ashley.core.Entity
import com.ovle.rl.Tick
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.life.*
import com.ovle.rl.model.game.life.Components.sleep
import com.ovle.rl.model.game.life.Components.sleepPlace
import com.ovle.rl.model.game.schedule.DaySchedule
import com.ovle.rl.model.game.schedule.DayScheduleType
import com.ovle.rl.model.game.schedule.EntityFinishedDayScheduleEvent
import com.ovle.rl.model.game.schedule.EntityStartedDayScheduleEvent
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.time.GAME_HOUR_TICKS
import com.ovle.rl.model.game.time.dto.TimerData
import com.ovle.rl.model.util.*
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get

class SleepSystem : TimedEventSystem() {

    override fun subscribeIntr() {
        subscribe<EntityStartedDayScheduleEvent>(this) { onEntityStartedDayScheduleEvent(it.entity, it.schedule) }
        subscribe<EntityFinishedDayScheduleEvent>(this) { onEntityFinishedDayScheduleEvent(it.entity, it.schedule) }

        subscribe<EntityStartSleepCommand>(this) { onEntityStartSleepCommand(it.entity) }
    }

    override fun updateTime() {
        val game = game()
        val location = game.location()
        val content = location.content
        val entities = content.entities.all()
        val sleepers = entities.asleep()

        for (sleeper in sleepers) {
            sleep(sleeper, updateInterval)
        }
    }


    private fun onEntityStartedDayScheduleEvent(entity: Entity, schedule: DaySchedule) {
        if (schedule.type != DayScheduleType.SLEEP) return
        val sleep = entity[sleep] ?: return

        val content = game().location().content
        val place = sleep.place
        val entities = content.entities.all()
        val sleepers = entities.ableToSleep()
        val busyPlaces = sleepers.mapNotNull { it[Components.sleep]!!.place }.toSet()
        val sleepPlaces = entities.sleepPlaces()
        val freeSleepPlaces = (sleepPlaces - busyPlaces)

        if (place == null) {
            scheduleSleep(entity, schedule, freeSleepPlaces, content)
        }
    }

    private fun onEntityFinishedDayScheduleEvent(entity: Entity, schedule: DaySchedule) {
        if (schedule.type != DayScheduleType.SLEEP) return
        val sleep = entity[sleep] ?: return

        val place = sleep.place
        val isAtPlace = place?.position() == entity.position()
        awake(entity, isAtPlace)
    }

    private fun onEntityStartSleepCommand(entity: Entity) {
        val sleep = entity[sleep]!!
        val place = sleep.place ?: return

        val isAtPlace = place.position() == entity.position()
        if (isAtPlace) {
            startSleep(entity, place)
        }
    }


    private fun scheduleSleep(
        entity: Entity,
        schedule: DaySchedule,
        freeSleepPlaces: Collection<Entity>,
        content: LocationContent
    ) {
        val newPlace = freeSleepPlaces
            .accessiblesTo(entity, content)
            .closestTo(entity.position())
            ?: return

        val sleep = entity[sleep]!!
        sleep.place = newPlace
        sleep.duration = schedule.duration

//        freeSleepPlaces -= newPlace
    }

    private fun startSleep(entity: Entity, place: Entity) {
        val sleep = entity[sleep]!!
        if (sleep.isSleeping) return

        sleep.isSleeping = true
        sleep.timer = TimerData()

        place[sleepPlace]!!.sleeper = entity

        send(EntityStartSleepEvent(entity))
    }

    private fun sleep(entity: Entity, updateInterval: Tick) {
        val sleep = entity[sleep]!!
        val timer = sleep.timer!!

        timer.time += updateInterval
        timer.update()
    }

    private fun awake(entity: Entity, isAtPlace: Boolean) {
        val sleep = entity[sleep]!!
        if (!sleep.isSleeping) return

        sleep.isSleeping = false

        val hoursSlept = sleep.timer!!.time / GAME_HOUR_TICKS
        if (isAtPlace && hoursSlept > sleep.duration!! / 2) {
            send(EntityHealCommand(entity, null, 1))
            //todo heal stress/morale
        }

        sleep.timer = null
        sleep.duration = null

        val place = sleep.place!!
        place[sleepPlace]!!.sleeper = null
        sleep.place = null

        send(EntityAwakenEvent(entity))
    }
}