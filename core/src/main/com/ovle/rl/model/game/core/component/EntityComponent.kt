package com.ovle.rl.model.game.core.component

import com.ovle.utils.gdx.ashley.component.BaseComponent

open class EntityComponent: BaseComponent {
    open fun gameInfo(): GameInfoNode? = null
}

data class GameInfoNode(
    val text: String, val children: Collection<GameInfoNode> = listOf()
)