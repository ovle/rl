package com.ovle.rl.model.game.life.sleep

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.game.time.dto.TimerData

class SleepComponent: EntityComponent() {
    var place: Entity? = null
    var duration: Int? = null
    var isSleeping: Boolean = false
    var timer: TimerData? = null

    override fun gameInfo() = GameInfoNode("sleep: $isSleeping")
}