package com.ovle.rl.model.game.gather

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode

class SourceComponent(
    vararg val info: SourceInfo,
    var integrity: Int = 3
) : EntityComponent() {

    override fun gameInfo() = GameInfoNode(
        "source: ($integrity)", info.map {
            GameInfoNode(
                "yield: ${it.yield.name}, amount: ${it.amount}"
            )
        }
    )
}