package com.ovle.rl.model.game.gather

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.*


val gatherModule = DI.Module("gather") {
    bind<EntitySystem>().inSet() with singleton {
        GatherSystem()
    }
}