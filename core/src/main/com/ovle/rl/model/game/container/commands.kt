package com.ovle.rl.model.game.container

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.game.core.GameEvent

class EntityTakeItemsCommand(val entity: Entity, val owner: Entity?, val items: Collection<Entity>) : GameCommand()

class EntityTakeCarryItemCommand(val entity: Entity, val item: Entity) : GameEvent()
class EntityDropCarryItemCommand(val entity: Entity, val item: Entity, val position: GridPoint2? = null) : GameCommand()