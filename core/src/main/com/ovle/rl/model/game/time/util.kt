package com.ovle.rl.model.game.time

import com.ovle.rl.Tick
import kotlin.math.roundToLong

fun timeToTicks(timeSecs: Double): Tick = (timeSecs / TICK_SIZE).roundToLong()

fun ticksToTime(ticks: Tick): Double = ticks * TICK_SIZE