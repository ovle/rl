package com.ovle.rl.model.game.area

import com.badlogic.gdx.math.GridPoint2


class AreaInfo(
    //todo remove mutability
    val area: MutableCollection<GridPoint2>,

    var name: String,
    var template: AreaTemplate,
    var params: AreaParams,
) {
    var invalidChecks: Collection<AreaCheck> = emptyList()

    val isValid: Boolean
        get() = invalidChecks.isEmpty()
}