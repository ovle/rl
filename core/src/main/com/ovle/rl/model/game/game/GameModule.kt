package com.ovle.rl.model.game.game

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.PooledEngine
import com.ovle.rl.model.game.game.factory.EntityFactory
import com.ovle.rl.model.game.game.factory.GameFactory
import com.ovle.rl.model.game.game.factory.LocationFactory
import com.ovle.rl.model.procgen.grid.world.WorldFactory
//import com.ovle.rl.model.procgen.grid.world.RegionsHelper
//import com.ovle.rl.model.procgen.grid.world.WorldFactory
//import com.ovle.rl.model.procgen.name.region.RegionNameGenerator
import org.kodein.di.*


val gameModule = DI.Module("game") {
    bind<Engine>() with singleton {
        PooledEngine()
    }
    bind<GameContext>() with singleton {
        GameContext(instance(), provider())
    }

    bind<EntityFactory>() with singleton {
        EntityFactory()
    }
    bind<GameFactory>() with singleton {
        GameFactory(provider())
    }
    bind<LocationFactory>() with singleton {
        LocationFactory()
    }
    bind<EntitySystem>().inSet() with singleton {
        GameSystem(instance(), instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        EntityLifecycleSystem(instance())
    }
}