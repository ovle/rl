package com.ovle.rl.model.game.time.dto

import com.badlogic.ashley.core.Entity
import com.ovle.rl.TimerStep

data class TimerEffectParamsFactoryPayload(
    val entity: Entity,
    val timerStep: TimerStep
)