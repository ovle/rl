package com.ovle.rl.model.game.core.system

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.game.GameLoadedEvent
import com.ovle.utils.event.EventBus
import com.ovle.utils.event.EventBus.subscribe


abstract class BaseSystem: EntitySystem() {

    private lateinit var game: GameEntity

    protected fun game() = game


    override fun addedToEngine(engine: Engine) {
        super.addedToEngine(engine)
        subscribe<GameLoadedEvent>(this) { onGameLoadedEvent(it.game) }
        subscribe()
    }

    private fun onGameLoadedEvent(newGame: GameEntity) {
        game = newGame
    }

    override fun removedFromEngine(engine: Engine) {
        super.removedFromEngine(engine)
        EventBus.clearSubscriptions(this)
    }

    abstract fun subscribe()
}