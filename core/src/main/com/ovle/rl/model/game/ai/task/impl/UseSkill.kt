package com.ovle.rl.model.game.ai.task.impl

import com.ovle.rl.model.game.ai.task.AITask
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.util.info

data class UseSkill(
    val skill: SkillTemplate,
    val target: SkillTarget,
    val isLoop: Boolean = false,
) : AITask() {
    override fun debugInfo() = "use skill: ${skill.info()} on: ${target.info()}"
}