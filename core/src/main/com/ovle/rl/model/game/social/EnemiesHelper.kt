package com.ovle.rl.model.game.social

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.life.isAlive
import com.ovle.rl.model.game.social.Components.social
import com.ovle.rl.model.game.social.dto.FactionFactionRelation
import com.ovle.rl.model.game.social.dto.FactionTemplate
import com.ovle.rl.model.game.social.dto.RelationValue
import com.ovle.rl.model.util.ofFaction
import com.ovle.rl.model.util.socials
import ktx.ashley.get
import ktx.ashley.has

class EnemiesHelper {

    fun enemies(location: Location): Map<Entity, Collection<Entity>> {
        val entities = location.content.entities.all()
            .socials()
        val factionsConfig = location.template.factions
        val relations = factionsConfig.relations
        val factions = relations
            .flatMap { listOf(it.faction1, it.faction2) }
            .distinct()
        val enemyFactionsByFaction = factions.associateWith { enemyFactions(it, relations) }
//        val factionsByEntities = entities.associateWith { it.faction() }
        val entitiesByFactions = factions.associateWith { entities.ofFaction(it) }

        return entities.associateWith {
            val enemyFactions = enemyFactionsByFaction[it.faction()] ?: emptyList()
            return@associateWith enemies(it, enemyFactions, entitiesByFactions)
        }
    }

    private fun enemies(
        entity: Entity,
        enemyFactions: Collection<FactionTemplate>,
        entitiesByFaction: Map<FactionTemplate, Collection<Entity>>,
    ): Collection<Entity> {
        val personalEnemies = personalEnemies(entity)
        val factionEnemies = enemyFactions.flatMap { entitiesByFaction[it] ?: emptyList() }
        val enemies = personalEnemies + factionEnemies

        return enemies.filter { e -> e.isExists() && e.isAlive() }
    }

    private fun enemyFactions(
        faction: FactionTemplate, relations: Collection<FactionFactionRelation>
    ): Collection<FactionTemplate> {
        val factionRelations = relations.filter {
            it.faction1 == faction || it.faction2 == faction
        }
        return factionRelations
            .filter { (_, _, r) -> r == RelationValue.BAD }
            .mapNotNull { (f1, f2, _) ->
                when (faction) {
                    f1 -> f2
                    f2 -> f1
                    else -> null
                }
            }
    }

    private fun personalEnemies(entity: Entity, ): Collection<Entity> {
        if (!entity.has(social)) return emptyList()
        return entity[social]!!.personalEnemies
    }
}