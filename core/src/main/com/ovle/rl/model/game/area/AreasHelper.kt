package com.ovle.rl.model.game.area

import com.badlogic.gdx.math.Rectangle
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.utils.gdx.math.points

class AreasHelper(
    private val validAreasHelper: ValidAreasHelper
) {

    fun createArea(
        template: AreaTemplate, rectangle: Rectangle, params: AreaParams, player: LocationPlayer, location: Location
    ): AreaInfo? {
        val tiles = location.content.tiles
        val locationProjection = player.locationProjection
        val areas = player.areas.filter { it.template == template }
        val index = areas.size + 1 //(sameTypeAreas.maxOfOrNull { it.typeIndex } ?: 0) + 1
        val name = "${template.name.lowercase()} $index"

        val area = rectangle.points()
            .filter {
                tiles.isValid(it)
                    && locationProjection.knownMap.isKnownTile(it)
                    && validAreasHelper.isTileValid(template, it, location)
            }
            .toMutableSet()
        if (area.isEmpty()) return null

        val result = AreaInfo(area, name, template, params)
        player.areas += result

        return result
    }

    fun deleteArea(area: AreaInfo, player: LocationPlayer) {
        val areas = player.areas
        areas -= area
    }
}