package com.ovle.rl.model.game.perception

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.perception.Components.perception
import ktx.ashley.get
import ktx.ashley.has


fun Entity.isSees(position: GridPoint2) = has(perception) && position in this[perception]!!.fov

