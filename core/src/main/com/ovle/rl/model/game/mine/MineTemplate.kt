package com.ovle.rl.model.game.mine

import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.template.NamedTemplate
import com.ovle.rl.model.template.entity.EntityTemplate

data class MineTemplate(
    override val name: String,
    val tileTemplate: TileTemplate,
    val minedTileTemplate: TileTemplate,
    val yield: EntityTemplate? = null,
    val durability: Int
): NamedTemplate