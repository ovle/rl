package com.ovle.rl.model.game.task.target

sealed class TaskTargetCheck {
    object Success: TaskTargetCheck()
    object NotApproved: TaskTargetCheck()
    class Error(val message: String): TaskTargetCheck()
}