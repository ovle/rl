package com.ovle.rl.model.game.effect

import com.badlogic.ashley.core.Entity
import com.ovle.rl.EffectPayload

data class EffectParams(
    val source: Entity?,
    val payload: EffectPayload = null
)