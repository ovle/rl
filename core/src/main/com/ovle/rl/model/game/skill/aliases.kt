package com.ovle.rl.model.game.skill

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.game.dto.location.LocationContent

typealias SkillTargetCheck = (SkillTarget, LocationContent) -> Boolean
typealias SkillOwnerCheck = (Entity, LocationContent) -> Boolean