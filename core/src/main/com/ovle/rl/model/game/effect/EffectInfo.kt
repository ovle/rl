package com.ovle.rl.model.game.effect

import com.ovle.rl.Effect

//collides with entity action?
//action is the stuff entity doing (owner is mandatory, target is optional)
//effect is the stuff no one doing (source is optional, target is mandatory)
data class EffectInfo<T>(
    val effect: Effect,
    val getParams: T,
)