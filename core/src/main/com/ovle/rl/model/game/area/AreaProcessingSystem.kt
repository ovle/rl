package com.ovle.rl.model.game.area

import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.util.location


class AreaProcessingSystem(
    private val validAreasHelper: ValidAreasHelper
) : TimedEventSystem() {

    override fun updateTime() {
        val location = game().location()
        val players = location.players.all()

        players.forEach { processAreas(it, location) }
    }

    private fun processAreas(player: LocationPlayer, location: Location) {
        val areas = player.areas
        areas.forEach {
            validAreasHelper.validateArea(it, location)
        }

        val areasByTemplate = areas.groupBy { it.template }
        for (areaTemplate in areasByTemplate.keys) {
            val templateAreas = areasByTemplate[areaTemplate]
                ?.filter { it.isValid } ?: emptyList()
            val processor = areaTemplate.processor ?: continue

            processor.process(templateAreas, player, location)
        }
    }
}