package com.ovle.rl.model.game.container

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.inSet
import org.kodein.di.singleton


val containerModule = DI.Module("container") {
    bind<EntitySystem>().inSet() with singleton {
        ContainerSystem()
    }
    bind<EntitySystem>().inSet() with singleton {
        CarrierSystem()
    }
}