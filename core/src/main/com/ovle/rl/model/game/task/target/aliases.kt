package com.ovle.rl.model.game.task.target

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.task.target.selection.TaskTargetSelection

typealias GetTaskTargetSelection = () -> TaskTargetSelection

typealias TaskTargetFilter = (TaskTarget, Location, LocationPlayer) -> Boolean