package com.ovle.rl.model.game.skill

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.build.BuildTemplate
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.space.position

sealed class SkillTarget {

    data class Point(val point: GridPoint2): SkillTarget() {
        override fun position() = point
    }

    data class Entity(val entity: com.badlogic.ashley.core.Entity): SkillTarget() {
        override fun position() = entity.position()
    }

    data class Build(
        val point: GridPoint2,
        val template: BuildTemplate,
        val resource: com.badlogic.ashley.core.Entity? = null,
    ): SkillTarget() {
        override fun position() = point
    }

    data class Craft(
        val point: GridPoint2,
        val template: CraftTemplate,
        val resource: com.badlogic.ashley.core.Entity? = null,
    ): SkillTarget() {
        override fun position() = point
    }

    data class Farm(
        val point: GridPoint2,
        val template: FarmTemplate,
        val resource: com.badlogic.ashley.core.Entity? = null,
    ): SkillTarget() {
        override fun position() = point
    }

    abstract fun position(): GridPoint2
}