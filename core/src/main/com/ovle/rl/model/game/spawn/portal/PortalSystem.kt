package com.ovle.rl.model.game.spawn.portal

import com.badlogic.ashley.core.Entity
import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.util.*
import com.ovle.utils.RandomParams
import com.ovle.utils.event.EventBus.send


class PortalSystem : TimedEventSystem() {

    override val updateInterval: Tick = SystemUpdateInterval.GAME_HOUR


    override fun updateTime() {
        val location = game().location()
        val content = location.content
        val portals = content.entities.all()
            .portals()
//            .atInfluenceZone(player)
        val random = location.random

        portals.forEach {
            processPortal(it, location, random)
        }
    }

    private fun processPortal(portal: Entity, location: Location, random: RandomParams) {
        val portalEntities = location.template.entitySpawnConfig.portalEntities
        val content = location.content
        val player = location.players.human
        val countByTemplate = portalEntities.map { it.entity }
            .associateWith { count(it, content) }

        val validTemplates = portalEntities.filter {
            val count = countByTemplate[it.entity] ?: 0
            it.check.check(player, content, portal, count)
        }
        val spawnTemplates = validTemplates.filter {
            val count = countByTemplate[it.entity] ?: 0
            val threshold = it.chancePerHour(count)
            random.chance(threshold)
        }

        val position = portal.position()
        spawnTemplates.forEach {
            val spawn = EntitySpawn(
                template = it.entity, position = position, source = portal
            )
            send(CreateEntityCommand(spawn))
        }
    }

    private fun count(template: EntityTemplate, content: LocationContent) =
        content.entities.all().ofTemplate(template).count()
}