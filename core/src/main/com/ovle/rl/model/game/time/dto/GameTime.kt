package com.ovle.rl.model.game.time.dto

import com.ovle.rl.model.game.time.HOURS_IN_DAY
import com.ovle.rl.model.game.time.SUNRISE_HOUR
import com.ovle.rl.model.game.time.SUNSET_HOUR
import com.ovle.utils.gdx.math.hourBetween

data class GameTime(
    val day: Int,
    val hour: Int,
    val minute: Int,
) {
    val totalHours: Int
        get() = day * HOURS_IN_DAY + hour

    val isDay: Boolean
        get() = hour.hourBetween(SUNRISE_HOUR, SUNSET_HOUR)
    val isNight: Boolean
        get() = !isDay
}