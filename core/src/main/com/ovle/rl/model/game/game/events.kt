package com.ovle.rl.model.game.game

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.core.EntityGameEvent
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.util.info


class GameStartedEvent(val location: Location) : GameEvent() {
    override val playerAwareMessage: String
        get() = "welcome to ${location.name}!"
}

class GameSavedEvent : GameEvent()

class GameFinishedEvent(val results: FinishGameResults) : GameEvent()

class GameLoadedEvent(val game: GameEntity) : GameEvent()

class LocationLoadedEvent(val location: Location) : GameEvent()

class EntityLoadedEvent(
    val entity: Entity, val location: Location, val position: GridPoint2
) : EntityGameEvent(entity) {
    override val message: String
        get() = "entity loaded: ${entity.info()}"
}

class EntityDestroyedEvent(val entity: Entity) : EntityGameEvent(entity) {
//    override val playerAwareMessage: String
//        get() = "${entity.info().markEntity()} is destroyed"

    override val message: String
        get() = "entity destroyed: ${entity.info()}"
}

class EntityLeavedLocationEvent(val entity: Entity, val leavePosition: GridPoint2) : EntityGameEvent(entity)
