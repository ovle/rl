package com.ovle.rl.model.game.space.body

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.util.info
import com.ovle.utils.gdx.math.point.point


class BodyComponent : EntityComponent() {

    var position: GridPoint2 = point(0, 0)

    override fun gameInfo() = GameInfoNode(
        "body:", listOf(
            GameInfoNode("pos: ${position.info()}"),
        )
    )
}