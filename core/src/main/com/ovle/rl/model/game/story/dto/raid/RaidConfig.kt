package com.ovle.rl.model.game.story.dto.raid

import com.ovle.rl.GetLocationPoint
import com.ovle.rl.model.game.story.dto.StoryEventEffectConfig
import com.ovle.rl.model.template.entity.EntityTemplate

class RaidConfig(
    val getStartPoint: GetLocationPoint,
    val members: Collection<RaidMemberConfig>
): StoryEventEffectConfig

class RaidMemberConfig(
    val entityTemplate: EntityTemplate,
    val countRange: IntRange
)