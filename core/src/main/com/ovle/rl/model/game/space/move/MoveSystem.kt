package com.ovle.rl.model.game.space.move

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemPriority
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.space.*
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.move.MoveState.*

import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.movables
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.math.point.point
import ktx.ashley.get
import ktx.log.info
import kotlin.math.abs
import kotlin.math.sign

/**
 * System for iterating-based processing of entity movement
 */
class MoveSystem(
    private val moveStateHelper: MoveStateHelper
): TimedEventSystem() {

    override val updateInterval = SystemUpdateInterval.MOVE
    override val systemPriority = SystemPriority.MOVE


    override fun subscribeIntr() {
        subscribe<EntityStartMovementCommand>(this) { onEntityStartMovementCommand(it.entity, it.params) }
        subscribe<EntityStopMovementCommand>(this) { onEntityStopMovementCommand(it.entity) }
    }

    private fun onEntityStartMovementCommand(entity: Entity, params: MoveParams) {
        val moveComponent = entity[move]!!
        moveComponent.params = params
        moveComponent.time = 0
        moveComponent.state = MOVING

        send(EntityStartedMovementEvent(entity))
    }

    private fun onEntityStopMovementCommand(entity: Entity) {
        val moveComponent = entity[move]!!
        moveComponent.params = null
        moveComponent.state = IDLE

        send(EntityStoppedMovementEvent(entity))
    }


    override fun updateTime() {
        val content = game().location().content
        val entities = content.entities.all().movables()

        for (entity in entities) {
            val state = moveStateHelper.moveState(entity, content)
            val moveComponent = entity[move]!!
            moveComponent.state = state

            val params = moveComponent.params

            if (state in setOf(DIRTY, DIRTY_MOVING)) {
                params?.dirty = true
            }
            if (state !in setOf(MOVING, DIRTY_MOVING)) continue

            val isMoved = move(entity, updateInterval) ?: continue
            if (!isMoved) {
                info(MOVE_LOG_TAG) { "${entity.name()} not moved" }
                params?.dirty = true
            }
        }
    }


    private fun move(entity: Entity, deltaTicks: Tick): Boolean? {
        val moveComponent = entity[move]!!
        val path = moveComponent.params?.path ?: return false

        moveComponent.time += deltaTicks

        val moveStep = moveComponent.moveStep
        val steps = (moveComponent.time / moveStep).toInt()
        val position = entity.position()

        for (i in 0 until steps) {
            val target = path.currentTarget ?: break
            val newPosition = newPosition(entity.position(), target)

            send(ChangeEntityPositionCommand(entity, newPosition))

            path.next()
        }

        val newPosition = entity.position()
        val haveSteps = steps > 0

        if (haveSteps) {
            moveComponent.time %= moveStep
        }

        return if (haveSteps) position != newPosition else null
    }

    private fun newPosition(currentPosition: GridPoint2, target: GridPoint2): GridPoint2 {
        val dx = target.x - currentPosition.x
        val dy = target.y - currentPosition.y
        val useX = abs(dx) > abs(dy)
        val dxStep = if (!useX) 0 else dx.sign
        val dyStep = if (useX) 0 else dy.sign

        return point(currentPosition.x + dxStep, currentPosition.y + dyStep)
    }
}