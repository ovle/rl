package com.ovle.rl.model.game.area

import com.badlogic.gdx.math.Rectangle
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.math.points

//todo no different type areas on same point?
class AreaSystem(
    private val areasHelper: AreasHelper
): BaseSystem() {

    override fun subscribe() {
        subscribe<CreateAreaCommand>(this) { onCreateAreaCommand(it.rect, it.template, it.params, it.player) }
        subscribe<MergeAreasCommand>(this) { onMergeAreasCommand(it.rect, it.template, it.params, it.areas, it.player) }
        subscribe<DeleteAreaCommand>(this) { onDeleteAreaCommand(it.area, it.player) }
    }


    private fun onCreateAreaCommand(
        rectangle: Rectangle, template: AreaTemplate, params: AreaParams, player: LocationPlayer
    ) {
        areasHelper.createArea(template, rectangle, params, player, game().location())
    }

    private fun onMergeAreasCommand(
        rectangle: Rectangle, template: AreaTemplate, params: AreaParams, areas: Collection<AreaInfo>, player: LocationPlayer
    ) {
        val newPoints = rectangle.points()
        if (areas.any { it.area.containsAll(newPoints) }) return

        val newArea = areasHelper.createArea(template, rectangle, params, player, game().location()) ?: return

        areas.forEach {
            newArea.area.addAll(it.area)
            areasHelper.deleteArea(it, player)
        }
    }

    private fun onDeleteAreaCommand(area: AreaInfo, player: LocationPlayer) {
        areasHelper.deleteArea(area, player)
    }
}