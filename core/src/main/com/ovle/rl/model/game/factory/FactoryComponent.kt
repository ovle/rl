package com.ovle.rl.model.game.factory

import com.ovle.rl.model.game.core.component.EntityComponent

class FactoryComponent(
    val config: ProductConfig
): EntityComponent()