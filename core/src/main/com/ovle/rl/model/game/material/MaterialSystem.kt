package com.ovle.rl.model.game.material

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.InitEntityCommand
import com.ovle.rl.model.game.material.Components.material
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.ashley.has


class MaterialSystem : BaseSystem() {

    override fun subscribe() {
        subscribe<InitEntityCommand>(this) { onInitEntityCommand(it.entity, it.spawn) }
    }


    private fun onInitEntityCommand(entity: Entity, spawn: EntitySpawn?) {
        spawn ?: return
        if (!entity.has(material)) return

        val materialTemplate = entity[material]!!.template
        if (materialTemplate != null) return

        entity[material]!!.template = spawn.material
    }
}
