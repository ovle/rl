package com.ovle.rl.model.game.task.dto

import com.ovle.rl.model.game.ai.task.TaskFactory
import com.ovle.rl.model.template.NamedTemplate
import com.ovle.rl.model.game.task.TaskPerformerFilter
import com.ovle.rl.model.game.task.isTaskPerformer
import com.ovle.rl.model.game.task.target.TaskTargetConfig


class TaskTemplate (
    override val name: String,
    val isAutoTask: Boolean = false,
    val taskFactory: TaskFactory,
    val targetConfig: TaskTargetConfig,
    val performerFilter: TaskPerformerFilter = { e, _, _ -> e.isTaskPerformer() },
): NamedTemplate