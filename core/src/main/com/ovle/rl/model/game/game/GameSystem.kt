package com.ovle.rl.model.game.game

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.factory.GameFactory
import com.ovle.rl.model.game.game.factory.LocationFactory
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.procgen.config.GridFactoryPayload
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.grid.world.World
import com.ovle.rl.model.game.spawn.EntitySpawn

import com.ovle.rl.model.util.location
import com.ovle.utils.RandomParams
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe


class GameSystem(
    private val gameFactory: GameFactory,
    private val locationFactory: LocationFactory,
) : BaseSystem() {

    override fun subscribe() {
        subscribe<GameEvent>(this, checkInheritance = true) { onGameEvent(it) }

        subscribe<StartGameCommand>(this) { onStartGameCommand(it.world, it.locationPoint, it.locationTemplate) }
        subscribe<StartPlaygroundCommand>(this) { onStartPlaygroundCommand(it.locationTemplate) }
        subscribe<StartSavedGameCommand>(this) { onStartSavedGameCommand(it.game) }
    }


    private fun onGameEvent(event: GameEvent) {
        val game = game()
        val finishGameConditions = game.location().template.finishGameConditionsCheck
        val finishGameResults = finishGameConditions.associateWith { it.check(event, game) }
        if (true !in finishGameResults.values) return

        send(GameFinishedEvent(finishGameResults))
    }

    private fun onStartGameCommand(world: World, locationPoint: GridPoint2, locationTemplate: LocationTemplate) {
        val gridFactoryPayload = GridFactoryPayload(world, locationPoint)
        val random = world.random

        startNewGame(locationTemplate, gridFactoryPayload, random, world)
    }

    private fun onStartPlaygroundCommand(locationTemplate: LocationTemplate) {
        startNewGame(locationTemplate, null, RandomParams(), null)
    }

    private fun onStartSavedGameCommand(game: GameEntity) {
        send(GameLoadedEvent(game))

        val location = game.location()
        val entities = location.content.entities.all()
        entities.forEach {
            send(InitEntityCommand(it, null))
            send(EntityLoadedEvent(it, location, it.position()))
        }

        send(LocationLoadedEvent(location))
        send(GameStartedEvent(location))
    }


    private fun startNewGame(
        locationTemplate: LocationTemplate, gridFactoryPayload: GridFactoryPayload?, random: RandomParams, world: World?
    ) {
        val location = locationFactory.newLocation(locationTemplate, gridFactoryPayload, world, random)
        val game = gameFactory.newGame(location, world, engine)
        send(GameLoadedEvent(game))

        postProcessLocation(location)

        send(LocationLoadedEvent(location))
        send(GameStartedEvent(location))
    }

    private fun postProcessLocation(location: Location) {
        val spawns = mutableListOf<EntitySpawn>()

        val worldPlaceProcessor = location.worldPlace?.template?.locationProcessor
        spawns += worldPlaceProcessor?.process(location) ?: emptyList()

        val postProcessors = location.template.postProcessors
        postProcessors.forEach {
            spawns += it.process(location)
        }

        spawns.forEach {
            send(CreateEntityCommand(it))
        }
    }
}