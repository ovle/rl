package com.ovle.rl.model.game.ai.decision

import com.badlogic.ashley.core.Entity
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.ai.Components.ai
import com.ovle.rl.model.game.ai.isAIActive
import com.ovle.rl.model.game.skill.helper.SkillHelper
import com.ovle.rl.model.game.social.EnemiesHelper
import com.ovle.rl.model.game.time.globalTime
import com.ovle.rl.model.util.aiOwners
import com.ovle.rl.model.util.location
import ktx.ashley.get

class EntityDecisionHelper(
    private val skillHelper: SkillHelper,
    private val enemiesHelper: EnemiesHelper
) {

    fun entityDecisions(game: GameEntity): Collection<EntityDecision> {
        val context = decisionMakerContext(game)
        val aiOwners = context.aiOwners

        return aiOwners.mapNotNull {
            entityDecision(it, context)
        }
    }


    private fun entityDecision(
        entity: Entity, context: AITaskFactoryContext
    ): EntityDecision? {
        val decisions = entity[ai]!!.aiTaskFactories
        val decision = decisions.firstNotNullOfOrNull { d -> d.process(entity, context) }

        return if (decision == null) null else EntityDecision(entity, decision)
    }

    private fun decisionMakerContext(game: GameEntity): AITaskFactoryContext {
        val location = game.location()
        val content = location.content
        val entities = content.entities.all()

        return AITaskFactoryContext(
            entities = entities,
            aiOwners = entities.aiOwners().filter { it.isAIActive() },
            enemies = enemiesHelper.enemies(location),
            skillHelper = skillHelper,
            content = content,
            players = location.players,
            gameTime = game.globalTime().toGameTime(),
            random = location.random
        )
    }
}