package com.ovle.rl.model.game.core

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.Components.core
import ktx.ashley.get

fun Entity.isExists() = this[core]!!.isExists
//fun Entity.isExists() = this.components.size() > 0