package com.ovle.rl.model.game.perception

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.TileCheck1
import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType
import com.ovle.utils.gdx.math.discretization.bresenham.filledCircle
import com.ovle.utils.gdx.math.lineOfSight.rayTracing.trace

class FOVHelper {

    fun fieldOfView(
        center: GridPoint2, radius: Int, checkTile: TileCheck1, tiles: TileArray, obstacles: Collection<GridPoint2>
    ): Collection<GridPoint2> {
        val area = filledCircle(center, radius, DiscreetCircleType.DISTANCE_BASED)
            .filter { tiles.isValid(it) }
        val obstaclesTiles = area
            .filterNot { checkTile(tiles[it.x, it.y]) } - center

        return traceFov(center, area, obstaclesTiles + obstacles)
    }

    private fun traceFov(center: GridPoint2, area: List<GridPoint2>, obstacles: Collection<GridPoint2>): Collection<GridPoint2> {
        val result = mutableSetOf<GridPoint2>()
        for (point in area) {
            if (point in result) continue

            val ray = trace(center, point, obstacles)
            result.addAll(ray)
        }
        return result.toList()
    }
}