package com.ovle.rl.model.game.ai

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.ai.decision.EntityDecisionHelper
import org.kodein.di.*


val aiModule = DI.Module("AI") {
    bind<EntitySystem>().inSet() with singleton {
        AISystem(instance(), instance())
    }

    bind<EntityDecisionHelper>() with singleton {
        EntityDecisionHelper(instance(), instance())
    }
}