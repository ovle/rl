package com.ovle.rl.model.game.skill.helper

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate

class ValidPlayerSkillTargetsHelper {

    fun validTarget(skill: PlayerSkillTemplate, point: GridPoint2, location: Location): SkillTarget? {
        val humanPlayer = location.players.human
        val content = location.content
        val targets = skill.getTargets(point, content)
        val validTargets = targets.filter {
            val p = it.position()
            humanPlayer.locationProjection.knownMap.isKnownTile(p) &&
            skill.skill.isTargetValid(it, content)
        }

        return when (validTargets.size) {
            0 -> null
            1 -> validTargets.single()
            else -> {
                //todo
                null
            }
        }
    }
}