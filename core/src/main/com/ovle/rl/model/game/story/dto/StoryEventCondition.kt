package com.ovle.rl.model.game.story.dto

import com.ovle.rl.GameCheck

class StoryEventCondition(
    val startCheck: GameCheck,
    val maxRepeat: Int? = null,
    val cooldownHours: Int = 0
)