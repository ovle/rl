package com.ovle.rl.model.game.container

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.container.Components.carriable
import com.ovle.rl.model.game.container.Components.carrier
import com.ovle.utils.gdx.ashley.component.mapper
import com.ovle.rl.model.game.container.Components.container
import com.ovle.rl.model.util.info
import ktx.ashley.get
import ktx.ashley.has

object Components {
    val container = mapper<ContainerComponent>()
    val carrier = mapper<CarrierComponent>()
    val carriable = mapper<CarriableComponent>()
}

fun Entity.items(): MutableCollection<Entity> {
    check(has(container)) { "no container for entity ${info()}" }
    return this[container]!!.items
}

fun Entity.carriedItem(): Entity? {
    return this[carrier]?.item
}

fun Entity.carrier(): Entity? {
    return this[carriable]?.carrier
}