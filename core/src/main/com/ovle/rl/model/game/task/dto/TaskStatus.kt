package com.ovle.rl.model.game.task.dto

enum class TaskStatus(val displayName: String) {
    /**
     * Waiting for performer to take
     */
    WAITING("?"),

    /**
     * Has performer, behavior tree is in work
     */
    RUNNING("!"),

    /**
     * Successful finishing
     */
    SUCCEEDED("V"),

    /**
     * Can't be finished by given performer
     */
    FAILED("X"),
}
