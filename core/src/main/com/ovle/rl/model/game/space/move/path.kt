package com.ovle.rl.model.game.space.move

//todo
//fun path(entity: Entity, targetEntity: Entity, location: LocationInfo): PathResult {
//    val obstacles = location.content.entities
//        .subtract(listOf(targetEntity))
//        .impassableFor(entity)
//        .positions()
//
//    val target = targetEntity.body().minByOrNull { distance(it, entity.position()) }!!
//
//    return path(entity, target, location, obstacles)
//}
//
//fun path(entity: Entity, target: GridPoint2, location: LocationInfo, obstacles: Collection<GridPoint2>): PathResult {
//    val costFn: (Tile, Tile?) -> Int = { from, to ->
//        when {
//            to == null -> maxMoveCost
//            !isTilePassableForEntity(entity, to) -> maxMoveCost
//            else -> basicCostFn(from, to)
//        }
//    }
//    val stopConditionFn: (GridPoint2) -> Boolean = { p -> p == target }
//
//    return path(
//        PathParams(entity.position(), location.content.tiles, obstacles, ::basicHeuristicsFn, costFn, stopConditionFn)
//    )
//}