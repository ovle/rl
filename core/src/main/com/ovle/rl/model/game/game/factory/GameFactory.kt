package com.ovle.rl.model.game.game.factory

import com.badlogic.ashley.core.Engine
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.game.GameComponent
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.procgen.grid.world.World


class GameFactory(
    private val getGlobalComponents: () -> Set<GlobalComponent>,
) {

    fun newGame(location: Location, world: World?, engine: Engine): GameEntity {
        val components = getGlobalComponents.invoke() + GameComponent(location, world)
        return engine.newEntity(*components.toTypedArray())
    }
}