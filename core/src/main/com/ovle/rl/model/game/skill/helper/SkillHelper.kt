package com.ovle.rl.model.game.skill.helper

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.SkillTag
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.skill.skills
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.nearTo
import com.ovle.rl.model.util.withTag
import com.ovle.utils.gdx.math.distance
import kotlin.math.abs

//todo only supports skill with entity targets
class SkillHelper(
    private val positionHelper: PositionHelper
) {

    fun bestSkillWithTarget(
        entity: Entity, targets: Collection<Entity>, content: LocationContent, tag: SkillTag?
    ): Pair<SkillTemplate, Entity?>? {
        val skills = accessibleSkills(entity, content, tag)
        val skillsWithTargets = skills.associateWith {
            bestTarget(entity, it, targets, content)
        }.filterValues { it != null }

        val result = skillsWithTargets.entries.maxByOrNull { (s, t) ->
            skillAndTargetPriority(s, t!!, entity)
        }

        return result?.toPair()
    }

    private fun bestTarget(
        entity: Entity, skill: SkillTemplate, targets: Collection<Entity>, content: LocationContent
    ): Entity? {
        val checkRadius = 20
        return targets
            .nearTo(entity.position(), checkRadius) //todo check FOV?
            .filter { isTargetValid(skill, it, content, entity) }
            .maxByOrNull { skillAndTargetPriority(skill, it, entity) }
    }

    fun bestSkill(
        entity: Entity, target: Entity, content: LocationContent, tag: SkillTag? = null
    ): SkillTemplate? {
        return accessibleSkills(entity, target, content, tag)
            .filter { isTargetValid(it, target, content, entity) }
            .maxByOrNull { skillAndTargetPriority(it, target, entity) }
    }


    private fun accessibleSkills(
        entity: Entity, content: LocationContent, tag: SkillTag?
    ): List<SkillTemplate> {
        return entity.skills()
            .withTag(tag)
            .filter { it.isOwnerValid(entity, content) }
    }

    private fun accessibleSkills(
        entity: Entity, target: Entity, content: LocationContent, tag: SkillTag?
    ): List<SkillTemplate> {
        return accessibleSkills(entity, content, tag)
            .filter {
                it.isOwnerValid(entity, content)
                    && positionHelper.isTargetAccessible(entity, it.range, target.position(), content)
            }
    }

    private fun isTargetValid(
        skill: SkillTemplate, target: Entity, content: LocationContent, entity: Entity
    ) = (skill.isTargetValid(SkillTarget.Entity(target), content)
        && positionHelper.isTargetAccessible(entity, skill.range, target.position(), content))


    private fun skillAndTargetPriority(skill: SkillTemplate, target: Entity, entity: Entity): Float {
        val distance = distance(entity.position(), target.position())
        val avgRange = skill.range?.average()?.toFloat() ?: Float.MAX_VALUE
        val distanceDiff = abs(distance - avgRange) + 1 //to avoid div by zero
        val distanceCheck = 1.0f / distanceDiff

        //todo other aspects
        val result = distanceCheck
        //println("skillAndTargetPriority: $result (${entity.info()}, target: ${target.info()})")
        return result
    }
}