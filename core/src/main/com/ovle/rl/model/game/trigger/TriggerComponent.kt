package com.ovle.rl.model.game.trigger

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.trigger.dto.TriggerTemplate

data class TriggerComponent(
    val triggers: MutableList<TriggerTemplate> = mutableListOf()
): EntityComponent()