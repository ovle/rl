package com.ovle.rl.model.game.space.move.impl.path

import com.ovle.rl.GetPath
import com.ovle.rl.model.template.NamedTemplate

data class PathTemplate(
    override val name: String,
    val getPath: GetPath
): NamedTemplate