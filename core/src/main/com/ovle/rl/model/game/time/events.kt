package com.ovle.rl.model.game.time

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.time.dto.Timer
import com.ovle.rl.model.util.info
import com.ovle.utils.event.Event


object AbsoluteTimeChangedEvent : GameEvent()

object GameTimeChangedEvent : GameEvent()


class TimerFiredEvent(val entity: Entity, val timer: Timer) : GameEvent()

class TimerFinishedEvent(val entity: Entity, val timer: Timer) : GameEvent() {
    override val message: String
        get() = "timer finished: ${timer.info()} for entity: ${entity.info()}"
}

class GameSpeedChangedEvent(val speed: Double) : Event()

class ChangeGameSpeedCommand(val multiplier: Double) : Event()

class ToggleGamePauseCommand : Event()

class ToggleGameMainMenuCommand : Event()

class ShowEntityDetailsCommand(val entity: Entity) : Event()