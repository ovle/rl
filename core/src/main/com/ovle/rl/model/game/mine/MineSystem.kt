package com.ovle.rl.model.game.mine

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.game.gather.MineTileCommand
import com.ovle.rl.model.game.tile.ChangeTileCommand
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.game.spawn.EntitySpawn

import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe


class MineSystem(
    private val mineTileHelper: MineTileHelper,
    private val mineTemplates: Collection<MineTemplate>
) : BaseSystem() {


    override fun subscribe() {
        subscribe<MineTileCommand>(this) { onMineTileCommand(it.position) }
    }

    private fun onMineTileCommand(position: GridPoint2) {
        val location = game().location()
        val tiles = location.content.tiles
        check(tiles.isValid(position)) { "invalid tile position: $position" }

        val tile = tiles[position.x, position.y]
        val isMined = mineTileHelper.gatherTile(position, tile)
        if (!isMined) return

        processMinedTile(position, tile)
    }

    private fun processMinedTile(position: GridPoint2, tile: TileTemplate) {
        val mineTemplate = mineTemplate(tile)
        val newTile = mineTemplate?.minedTileTemplate ?: TileTemplate.NOTHING
        send(ChangeTileCommand(newTile, position))

        val template = mineTileHelper.mineTemplate(tile)?.yield ?: return
        val spawn = EntitySpawn(
            template = template, position = position
        )

        send(CreateEntityCommand(spawn))
    }

    fun mineTemplate(tile: TileTemplate) = mineTemplates
        .find { it.tileTemplate == tile }
}
