package com.ovle.rl.model.game.life.age

import com.badlogic.ashley.core.Entity
import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.DestroyEntityCommand
import com.ovle.rl.model.game.life.Components.age
import com.ovle.rl.model.game.transform.TransformEntityCommand

import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.withAge
import com.ovle.utils.RandomParams
import com.ovle.utils.event.EventBus.send
import ktx.ashley.get


class AgeSystem : TimedEventSystem() {

    override val updateInterval: Tick = SystemUpdateInterval.AGE


    override fun updateTime() {
        val location = game().location()
        val content = location.content
        val entities = content.entities.all().withAge()
        val random = location.random

        entities.forEach {
            processEntity(it, random)
        }
    }

    private fun processEntity(entity: Entity, random: RandomParams) {
        val age = entity[age]!!
        age.ageHours++

        val maxAge = age.maxAgeHours
        if (maxAge != null && age.ageHours >= maxAge) {
            send(DestroyEntityCommand(entity))
        }

        val dieChancePerTurn = age.dieChancePerHour
        if (dieChancePerTurn != null && random.chance(dieChancePerTurn)) {
            send(DestroyEntityCommand(entity))
        }

        val transform = age.transform
        if (transform != null && random.chance(transform.chancePerHour)) {
            send(TransformEntityCommand(entity, transform.entity))
        }
    }
}