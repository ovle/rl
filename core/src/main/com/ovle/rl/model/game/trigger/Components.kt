package com.ovle.rl.model.game.trigger

import com.ovle.utils.gdx.ashley.component.mapper

object Components {
    val trigger = mapper<TriggerComponent>()
}