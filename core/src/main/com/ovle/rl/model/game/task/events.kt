package com.ovle.rl.model.game.task

import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskStatus.*
import com.ovle.rl.model.util.info
import com.ovle.rl.model.util.markEntity
import com.ovle.rl.model.util.markFail
import com.ovle.rl.model.util.markSuccess


class TaskStartedEvent(val task: TaskInfo) : GameEvent() {
    override val message: String
        get() {
            val performerInfo = task.performer.info()
            return "$performerInfo started task: ${task.template.name}"
        }

    override val playerAwareMessage: String
        get() {
            val performerInfo = task.performer.info().markEntity()
            return "$performerInfo started task: ${task.template.name}"
        }
}

class TaskFinishedEvent(val task: TaskInfo) : GameEvent() {
    override val message: String?
        get() {
            val performerInfo = task.performer.info().markEntity()
            val status = when (task.status) {
                SUCCEEDED -> "succeeded"
                FAILED -> "failed"
                else -> null
            }

            return "$performerInfo $status task: ${task.template.name}"
        }

    override val playerAwareMessage: String?
        get() {
            val performerInfo = task.performer.info().markEntity()
            val status = when (task.status) {
                SUCCEEDED -> "succeeded".markSuccess()
                FAILED -> "failed".markFail()
                else -> null
            }

            return if (status == null) null
                else "$performerInfo $status task: ${task.template.name}"
        }
}

class TaskRemovedEvent(val task: TaskInfo) : GameEvent()