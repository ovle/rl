package com.ovle.rl.model.game.gather

import com.ovle.rl.model.template.entity.EntityTemplate

data class SourceInfo(
    val yield: EntityTemplate,
    val amount: IntRange = 1..1,
)