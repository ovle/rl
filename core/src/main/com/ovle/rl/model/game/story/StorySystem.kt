package com.ovle.rl.model.game.story

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.story.dto.StoryEvent
import com.ovle.rl.model.game.story.dto.StoryEventTemplate
import com.ovle.rl.model.game.time.globalTime

import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send

class StorySystem(
    private val templates: Collection<StoryEventTemplate>
) : TimedEventSystem() {

    override fun updateTime() {
        val game = game()
        val totalHours = game.globalTime().toGameTime().totalHours
        val candidates = templates.filter {
            isCandidateToStart(it, game, totalHours)
        }
        val candidate = candidates.firstOrNull() ?: return

        startEvent(candidate, game, totalHours)
    }

    private fun isCandidateToStart(
        template: StoryEventTemplate, game: Entity, totalHours: Int
    ): Boolean {
        val condition = template.condition
        if (!condition.startCheck(game)) return false

        val sameEvents = game.storyEvents()
            .filter { it.template == template }
        val maxRepeat = condition.maxRepeat
        if (maxRepeat != null && sameEvents.size > maxRepeat) return false

        val latestEvent = sameEvents.maxByOrNull { it.startHour }
        val cooldownHours = condition.cooldownHours
        val isRefreshedCooldown = latestEvent == null
            || latestEvent.startHour + cooldownHours <= totalHours
        if (!isRefreshedCooldown) return false

        return true
    }

    private fun startEvent(
        candidate: StoryEventTemplate, game: Entity, totalHours: Int
    ) {
        val storyEvent = StoryEvent(candidate, totalHours)

        candidate.effect(candidate.effectConfig, game.location())

        val events = game.storyEvents()
        events += storyEvent

        send(StoryEventStartedEvent(storyEvent))
    }
}