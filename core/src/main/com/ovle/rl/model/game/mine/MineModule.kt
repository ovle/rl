package com.ovle.rl.model.game.mine

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.mine.MineTileHelper
import org.kodein.di.*


val mineModule = DI.Module("mine") {
    bind<MineTileHelper>() with singleton {
        MineTileHelper(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        MineSystem(instance(), instance())
    }
}