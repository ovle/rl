package com.ovle.rl.model.game.task

import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.selection.TaskTargetSelection


class CancelTaskCommand(val task: TaskInfo, val player: LocationPlayer) : GameCommand()

class CheckTaskCommand(
    val target: TaskTargetSelection, val template: TaskTemplate, val player: LocationPlayer
) : GameCommand()
