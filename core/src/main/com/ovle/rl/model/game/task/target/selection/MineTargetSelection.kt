package com.ovle.rl.model.game.task.target.selection

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.game.task.target.TaskTargetCheck.*

data class MineTargetSelection(
    val points: MutableCollection<GridPoint2> = mutableSetOf()
): TaskTargetSelection() {

    override val isMultiselect = true

    override fun taskTargets() = points.map { TaskTarget.Point(it) }


    override fun updatePoints(selection: Collection<GridPoint2>) {
        points += selection
    }


    override fun check(): TaskTargetCheck = when {
        points.isEmpty() -> Error("select positions")
        else -> Success
    }

    override fun copy() = MineTargetSelection(points.toMutableSet())

    override fun reset() {
        points.clear()
    }
}