package com.ovle.rl.model.game.space

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.space.accessibility.AccessibilityHelper
import com.ovle.rl.model.game.space.accessibility.AccessibilityMapFactory
import com.ovle.rl.model.game.space.accessibility.AccessibilitySystem
import com.ovle.rl.model.game.space.aoe.AOESystem
import com.ovle.rl.model.game.space.body.BodySystem
import com.ovle.rl.model.game.space.move.MovePathSystem
import com.ovle.rl.model.game.space.move.MoveStateHelper
import com.ovle.rl.model.game.space.move.MoveSystem
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.game.space.move.impl.moveMap.MoveMapFactory
import com.ovle.rl.model.game.space.move.impl.path.MovePathFactory
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMapFactory
import org.kodein.di.*


val spaceModule = DI.Module("space") {
    bind<PassabilityHelper>() with singleton {
        PassabilityHelper()
    }
    bind<AccessibilityHelper>() with singleton {
        AccessibilityHelper(instance())
    }
    bind<DijkstraMapFactory>() with singleton {
        DijkstraMapFactory()
    }
    bind<MoveMapFactory>() with singleton {
        MoveMapFactory(instance())
    }
    bind<AccessibilityMapFactory>() with singleton {
        AccessibilityMapFactory(instance())
    }
    bind<MovePathFactory>() with singleton {
        MovePathFactory(instance())
    }
    bind<MoveStateHelper>() with singleton {
        MoveStateHelper()
    }
    bind<EntitySystem>().inSet() with singleton {
        MovePathSystem(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        MoveSystem(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        BodySystem(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        AccessibilitySystem(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        AOESystem()
    }
}