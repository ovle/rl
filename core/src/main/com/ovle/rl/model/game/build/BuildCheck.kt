package com.ovle.rl.model.game.build

import com.ovle.rl.TileCheck2

data class BuildCheck(
    val check: TileCheck2,
    val description: String,
)