package com.ovle.rl.model.game.space.move

enum class MoveType {
    DEFAULT,
    SWIMMING,
    FLYING
}