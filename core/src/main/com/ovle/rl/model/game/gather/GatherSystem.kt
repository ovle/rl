package com.ovle.rl.model.game.gather

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.game.game.DestroyEntityCommand
import com.ovle.rl.model.game.gather.Components.source
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.util.location
import com.ovle.utils.RandomParams
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get


class GatherSystem : BaseSystem() {


    override fun subscribe() {
        subscribe<GatherEntityCommand>(this) { onGatherEntityCommand(it.entity) }
    }

    private fun onGatherEntityCommand(entity: Entity) {
        val sourceComponent = entity[source]!!

        sourceComponent.integrity--
        if (sourceComponent.integrity > 0) return

        val random = game().location().random
        processGatheredEntity(entity, random)
    }

    private fun processGatheredEntity(entity: Entity, random: RandomParams) {
        val sourceComponent = entity[source]!!
        val position = entity.position()
        sourceComponent.info.forEach {
            val spawn = EntitySpawn(
                template = it.yield, position = position
            )
            val times = it.amount.random(random.kRandom)

            repeat(times) {
                send(CreateEntityCommand(spawn))
            }
        }

        send(DestroyEntityCommand(entity))
    }
}
