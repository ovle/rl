package com.ovle.rl.model.game.tile

import com.ovle.rl.model.game.space.move.MoveType

data class TileProps(
    val isWater: Boolean = false,
    val isLava: Boolean = false,
    val isIce: Boolean = false,
    val isDeep: Boolean = false,
    val isDeposit: Boolean = false,
    val isWall: Boolean = false,
    val isFloor: Boolean = false,
    val isDoor: Boolean = false,
    val isWindow: Boolean = false,
    val isArtificial: Boolean = false,
    val isNatural: Boolean = false,
    val isSoil: Boolean = false,
) {

    val isLiquid: Boolean = isWater || isLava
    val isOpaque: Boolean = isWall || isDoor
    val isCollides: Boolean = isWall || isDoor
    val isPassable: Boolean = !(isWall || isWindow)

    fun isAccessible(moveType: MoveType): Boolean = when (moveType) {
        MoveType.FLYING -> !isWall
        MoveType.SWIMMING -> isWater
        MoveType.DEFAULT -> !(isWall || isWindow || isLava || isDeep)
    }
}
