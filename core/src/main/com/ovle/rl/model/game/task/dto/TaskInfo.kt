package com.ovle.rl.model.game.task.dto

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.TaskResource
import com.ovle.rl.model.game.ai.task.AITaskSource
import com.ovle.rl.model.game.task.target.TaskTarget

/**
 * task for ai
 * no duration (varies depend on conditions)
 * single performer (null if not taken yet)
 * performer is still presents after task finishing (for history)
 */
class TaskInfo(
    val template: TaskTemplate,
    val target: TaskTarget,
): AITaskSource {

    var performer: Entity? = null

    var lockedResource: TaskResource? = null

    var status: TaskStatus = TaskStatus.WAITING
        private set


    fun start() {
        status = TaskStatus.RUNNING
    }

    fun reset() {
        status = TaskStatus.WAITING
    }

    override fun success() {
        status = TaskStatus.SUCCEEDED
    }

    override fun fail() {
        status = TaskStatus.FAILED
    }

    val isFree: Boolean
        get() = performer == null
}