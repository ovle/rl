package com.ovle.rl.model.game.factory

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.factory.Components.factory
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.social.Components.social
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.spawn.EntitySpawnCheckHelper
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.positions
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.math.discretization.bresenham.filledCircle
import ktx.ashley.get


class FactorySystem(
    private val spawnChecker: EntitySpawnCheckHelper
) : BaseSystem() {

    override fun subscribe() {
        subscribe<EntityFactoryCreateEntityCommand>(this) { onEntityCreateEntityCommand(it.source) }
    }

    private fun onEntityCreateEntityCommand(source: Entity) {
        createEntity(source)
    }


    private fun createEntity(source: Entity) {
        val game = game()
        val random = game.location().random
        val productConfig = source[factory]!!.config
        val productTemplate = productConfig.product
        val locationContent = game.location().content

        val spawnConfigs = productConfig.spawnConfigs
        val value = random.kRandom.nextDouble()
        var accum = 0.0f
        val spawnConfig = spawnConfigs.takeWhile {
            val result = accum <= value
            accum += it.chance
            result
        }.last()

        val availableSpawnPoints = availableSpawnPoints(
            source.position(), spawnConfig.radius, productTemplate, locationContent
        )
        availableSpawnPoints.shuffle(random.kRandom)

        val spawnPoints = availableSpawnPoints.take(
            minOf(availableSpawnPoints.size, spawnConfig.number)
        )

        spawnPoints.forEach { sp ->
            val spawn = EntitySpawn(
                template = productTemplate,
                position = sp
            ) { e, l ->
                val faction = spawnConfig.faction
                if (faction != null) {
                    val sc = e[social]!!
                    sc.faction = faction
                    sc.player = l.players.byFaction[faction]
                }
            }

            send(CreateEntityCommand(spawn))
        }
    }

    private fun availableSpawnPoints(
        position: GridPoint2, radius: Int, entityTemplate: EntityTemplate, content: LocationContent,
    ): MutableList<GridPoint2> {
        val tiles = content.tiles
        val occupiedPositions = content.entities.all().positions()
        val spawnTemplate = entityTemplate.spawnTemplate
        val spawnArea = filledCircle(position, radius).filter {
            tiles.isValid(it) && it !in occupiedPositions
        }

        return (
            if (spawnTemplate == null) spawnArea
            else spawnArea.filter { p ->
                spawnChecker.isCanSpawn(spawnTemplate, tiles, p)
            }
        ).toMutableList()
    }
}