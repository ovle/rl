package com.ovle.rl.model.game.quest.dto

import com.badlogic.ashley.core.Entity

/**
 * quest taken by the holder
 */
data class QuestInfo(
    val id: String,
    val description: QuestDescription,
    val performer: Entity,
    val holder: Entity,

    var status: QuestStatus = QuestStatus.IN_PROCESS
)

enum class QuestStatus(val isTerminal: Boolean) {
    IN_PROCESS(false),
    COMPLETED(false),
    REWARDED(true),
    FAILED(true);
}