package com.ovle.rl.model.game.space.aoe

import com.ovle.rl.GetAOE
import com.ovle.rl.model.template.NamedTemplate

class AOETemplate(
    override val name: String,
    val factory: GetAOE,
): NamedTemplate