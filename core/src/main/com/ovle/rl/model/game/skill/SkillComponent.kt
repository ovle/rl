package com.ovle.rl.model.game.skill

import com.ovle.rl.Tick
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.skill.dto.SkillUsage
import com.ovle.rl.model.util.info

class SkillComponent(
    val skills: Collection<SkillTemplate> = listOf(),
) : EntityComponent() {

    var current: SkillUsage.Entity? = null
    var ticksLeft: Tick = 0

    override fun gameInfo() = GameInfoNode(
        "skills:", skills.map {
            GameInfoNode(it.info())
        }
    )
}