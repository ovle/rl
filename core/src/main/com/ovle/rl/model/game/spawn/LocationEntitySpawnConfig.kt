package com.ovle.rl.model.game.spawn

import com.ovle.rl.model.game.spawn.portal.PortalEntitySpawnTemplate
import com.ovle.rl.model.template.entity.EntityTemplate

data class LocationEntitySpawnConfig(
    val allowedEntities: Collection<EntityTemplate>,
    val portalEntities: Collection<PortalEntitySpawnTemplate>,
)