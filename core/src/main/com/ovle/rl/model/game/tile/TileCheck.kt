package com.ovle.rl.model.game.tile

import com.ovle.rl.TileCheck1

data class TileCheck(
    val check: TileCheck1,
    val description: String,
)