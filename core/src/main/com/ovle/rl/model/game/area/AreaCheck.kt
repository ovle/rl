package com.ovle.rl.model.game.area

import com.ovle.rl.AreaCheck2

data class AreaCheck(
    val check: AreaCheck2,
    val description: String,
)