package com.ovle.rl.model.game.trigger

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.EntityGameEvent
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.trigger.Components.trigger
import com.ovle.rl.model.util.info
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.ashley.has
import ktx.log.info

class TriggerSystem: BaseSystem() {

    override fun subscribe() {
        subscribe<GameEvent>(this, checkInheritance = true) { onGameEvent(it) }
    }

    private fun onGameEvent(event: GameEvent) {
        if (event !is EntityGameEvent) return

        val entities = listOfNotNull(event.subject)
        entities.forEach {
            checkTriggers(it, event)
        }
    }

    private fun checkTriggers(entity: Entity, event: GameEvent) {
        if (!entity.has(trigger)) return

        val triggers = entity[trigger]!!.triggers
            .filter { it.key == event::class.java }

        triggers.forEach { t ->
            info(TRIGGER_LOG_TAG) { "fired trigger: ${t.info()} of entity: ${entity.info()}" }

            val effects = t.effects
            effects.forEach {
                val params = it.getParams.invoke(event)
                it.effect.invoke(params)
            }
        }
    }
}