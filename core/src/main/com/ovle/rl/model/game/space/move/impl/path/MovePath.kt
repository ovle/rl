package com.ovle.rl.model.game.space.move.impl.path

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.point.adjHV


class MovePath(val path: List<GridPoint2>) {

    private var step = 0

    var currentTarget: GridPoint2? = path.firstOrNull()?.cpy()
        private set
    val last: GridPoint2?
        get() = path.lastOrNull()
    val left: List<GridPoint2>
        get() = path.drop(step)

    fun next() {
        step++
        currentTarget = point(step)
    }

    fun isAtCurrentTarget(p: GridPoint2): Boolean {
        val ct = currentTarget
        return if (ct == null) false else p in ct.adjHV()
    }

    fun point(step: Int) =
        if (step < path.size) path[step] else null
}

