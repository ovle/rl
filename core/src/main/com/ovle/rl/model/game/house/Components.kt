package com.ovle.rl.model.game.house

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.house.Components.houseAware
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get


object Components {
    val houseAware = mapper<HouseAwareComponent>()
}

fun Entity.closestOwnedHouseOrNull(): AreaInfo? {
    return this[houseAware]?.closestOwnedHouse
}

fun Entity.closestHouseOrNull(): AreaInfo? {
    return this[houseAware]?.closestHouse
}