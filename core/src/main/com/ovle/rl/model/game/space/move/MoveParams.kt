package com.ovle.rl.model.game.space.move

import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.space.move.impl.path.MovePath

class MoveParams(
    val task: Move? = null,
    var path: MovePath? = null,

    var dirty: Boolean = true,
) {
    val isFixedPathMove: Boolean
        get() = task == null
}