package com.ovle.rl.model.game.task.dto

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.TaskResource

data class TaskCandidate(
    val task: TaskInfo,
    val performer: Entity,
    val resource: TaskResource?
)
