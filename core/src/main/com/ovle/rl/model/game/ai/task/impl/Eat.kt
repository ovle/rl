package com.ovle.rl.model.game.ai.task.impl

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.task.AITask
import com.ovle.rl.model.util.info


data class Eat(val target: Entity) : AITask() {
//    override fun gameInfo() = GameInfoNode("eat: ${target.info()}")
    override fun debugInfo() = "eat: ${target.info()}"
}