package com.ovle.rl.model.game.time.dto

import com.ovle.rl.Tick
import com.ovle.rl.model.game.time.*

class GlobalTimeInfo {
    var time: Double = 0.0
    var gameSpeed: Double = DEFAULT_GAME_SPEED
    var paused: Boolean = false

    val tick: Tick
        get() = timeToTicks(time)


    fun toGameTime(): GameTime {
        var ticksLeft = tick
        val day = ticksLeft / GAME_DAY_TICKS + 1 //start from 1
        ticksLeft %= GAME_DAY_TICKS
        val hour = ticksLeft / GAME_HOUR_TICKS
        ticksLeft %= GAME_HOUR_TICKS
        val minute = ticksLeft / GAME_MINUTE_TICKS

        return GameTime(
            day = day.toInt(),
            hour = hour.toInt(),
            minute = minute.toInt()
        )
    }
}