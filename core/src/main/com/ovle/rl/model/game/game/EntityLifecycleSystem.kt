package com.ovle.rl.model.game.game

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.Components.core
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.factory.EntityFactory
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get


class EntityLifecycleSystem(
    private val entityFactory: EntityFactory,
) : BaseSystem() {

    override fun subscribe() {
        subscribe<CreateEntityCommand>(this) { onCreateEntityCommand(it.spawn) }
        subscribe<DestroyEntityCommand>(this) { onDestroyEntityCommand(it.entity) }
        subscribe<EntityLeavedLocationEvent>(this) { onEntityLeavedLocationEvent(it.entity, it.leavePosition) }
    }


    private fun onCreateEntityCommand(spawn: EntitySpawn) {
        createEntity(spawn)
    }

    private fun onDestroyEntityCommand(entity: Entity) {
        destroyEntity(entity)
    }

    private fun onEntityLeavedLocationEvent(entity: Entity, leavePosition: GridPoint2) {
        //todo
        destroyEntity(entity)
    }


    private fun createEntity(spawn: EntitySpawn) {
        val location = game().location()
        val entity = entityFactory.newTemplatedEntity(
            spawn.id, spawn.template, spawn.source, engine
        )
        spawn.init(entity, location)

        location.content.entities.add(entity)

        send(InitEntityCommand(entity, spawn))

        send(EntityLoadedEvent(entity, location, spawn.position))
    }

    private fun destroyEntity(entity: Entity) {
        send(EntityDestroyedEvent(entity))

        val location = game().location()
        location.content.entities.remove(entity)

        entity[core]!!.isExists = false
        //todo cleanup
        //engine.removeEntity(entity)
    }
}