package com.ovle.rl.model.game.life

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.life.age.AgeSystem
import com.ovle.rl.model.game.life.hunger.HungerSystem
import com.ovle.rl.model.game.life.sleep.SleepSystem
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.inSet
import org.kodein.di.singleton


val lifeModule = DI.Module("life") {
    bind<EntitySystem>().inSet() with singleton {
        HealthSystem()
    }
    bind<EntitySystem>().inSet() with singleton {
        HungerSystem()
    }
    bind<EntitySystem>().inSet() with singleton {
        SleepSystem()
    }
    bind<EntitySystem>().inSet() with singleton {
        AgeSystem()
    }
}