package com.ovle.rl.model.game.life

import com.badlogic.ashley.core.Entity
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.life.Components.health
import com.ovle.rl.model.game.life.hunger.EntityStarvedEvent
import ktx.ashley.get
import ktx.ashley.has
import kotlin.math.max
import kotlin.math.min

class HealthSystem : BaseSystem() {

    override fun subscribe() {
        subscribe<EntityTakeDamageCommand>(this) { onEntityTakeDamageCommand(it.entity, it.source, it.amount) }
        subscribe<EntityHealCommand>(this) { onEntityHealedCommand(it.entity, it.source, it.amount) }
        subscribe<KillEntityCommand>(this) { onKillSelectedEntityCommand(it.entity) }
        subscribe<ResurrectEntityCommand>(this) { onResurrectEntityCommand(it.entity) }

        subscribe<EntityStarvedEvent>(this) { onEntityStarvedEvent(it.entity) }
    }

    private fun onKillSelectedEntityCommand(entity: Entity) {
        if (!entity.has(health)) return

        applyDamage(entity, null, entity[health]!!.health)
    }

    private fun onResurrectEntityCommand(entity: Entity) {
        if (!entity.has(health)) return

        applyHeal(entity, null, entity[health]!!.maxHealth)
    }

    private fun onEntityTakeDamageCommand(entity: Entity, source: Entity?, amount: Int) {
        if (!entity.has(health)) return

        applyDamage(entity, source, amount)
    }

    private fun onEntityHealedCommand(entity: Entity, source: Entity?, amount: Int) {
        if (!entity.has(health)) return

        applyHeal(entity, source, amount)
    }

    private fun onEntityStarvedEvent(entity: Entity) {
        if (!entity.has(health)) return

        applyDamage(entity, null, 1)
    }


    private fun applyDamage(entity: Entity, source: Entity?, amount: Int) {
        check(amount >= 0) { "damage amount is negative: $amount" }

        val life = entity[health]!!
        if (life.isDead) return

        life.health = max(life.health - amount, 0)

        send(EntityTakenDamageEvent(entity, source, amount))

        if (life.health == 0) {
            send(EntityDiedEvent(entity))

//            createSoul(entity)
        }
    }

    private fun applyHeal(entity: Entity, source: Entity?, amount: Int) {
        check(amount >= 0) { "heal amount is negative: $amount" }

        val life = entity[health]!!
        val wasDead = life.isDead

        life.health = min(life.health + amount, life.maxHealth)

        if (wasDead) {
//            if (!consumeSoul(entity)) return

            send(EntityResurrectedEvent(entity))
        }

        send(EntityHealedEvent(entity, source, amount))
    }
/*
    private fun createSoul(entity: Entity) {
        if (!entity.has(soul)) return

        val spawn = EntitySpawn(
            template = entity[soul]!!.yields,
            position = entity.position(),
            source = entity
        )
        send(CreateEntityCommand(spawn))
    }

    private fun consumeSoul(entity: Entity): Boolean {
        if (!entity.has(soul)) return true //no need to consume

        val content = game().location().content
        val entities = content.entities.on(entity.position())
        val yields = entity[soul]!!.yields
        val soul = entities.find { it.template() == yields } ?: return false

        send(DestroyEntityCommand(soul))

        return true
    }
*/
}