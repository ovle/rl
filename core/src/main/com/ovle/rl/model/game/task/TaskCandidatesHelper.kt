package com.ovle.rl.model.game.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.task.dto.TaskCandidate
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.utils.gdx.math.distance

//todo dynamic programming for optimal combination of all tasks + performers
class TaskCandidatesHelper(
    private val taskResourceHelper: TaskResourceHelper
) {

    companion object {
        private const val MAX_PRIORITY = 1.0f
    }

    fun bestTaskCandidates(
        taskCandidates: Collection<Pair<TaskInfo, Entity>>, location: Location, player: LocationPlayer
    ): TaskCandidates {
        val result = mutableListOf<TaskCandidate>()
        val knownFreeResources = taskResourceHelper.knownResources(location, player)
        val tasksTaken = mutableSetOf<TaskInfo>()
        val performersTaken = mutableSetOf<Entity>()

        while (true) {
            val bestTaskAndPerformer = taskCandidates
                .filter { (t, p) ->
                    t !in tasksTaken && p !in performersTaken
                }.map { (t, p) ->
                    val br = taskResourceHelper.bestResource(knownFreeResources, t, p, location)
                    TaskCandidate(t, p, br)
                }.filter { (t, _, r) ->
                    val mt = t.target.resource()
                    mt == null || r?.template() == mt
                }.maxByOrNull { (t, p) ->
                    priority(t, p)
                }

            bestTaskAndPerformer ?: break

            val (task, performer, resource) = bestTaskAndPerformer

            tasksTaken += task
            performersTaken += performer
            resource?.let { knownFreeResources -= it }

            result += bestTaskAndPerformer
        }

        return result
    }

    private fun priority(task: TaskInfo, performer: Entity): Float {
        val taskPosition = task.target.positions().firstOrNull()
            ?: return MAX_PRIORITY

        val distance = distance(performer.position(), taskPosition)

        return MAX_PRIORITY / distance.toFloat()
    }
}