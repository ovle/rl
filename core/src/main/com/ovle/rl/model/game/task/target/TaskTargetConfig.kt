package com.ovle.rl.model.game.task.target


class TaskTargetConfig(
    val getTarget: GetTaskTargetSelection,
    val targetFilter: TaskTargetFilter = { _, _, _ -> true },
    val allowUnknownTargets: Boolean = false
)