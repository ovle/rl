package com.ovle.rl.model.game.gather

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.material.Components.material
import ktx.ashley.get
import ktx.ashley.has

fun Entity.isGatherable() = this.has(Components.source)
