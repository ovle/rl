package com.ovle.rl.model.game.time.dto

import com.ovle.rl.TimerCheck
import com.ovle.rl.TimerEffect
import com.ovle.rl.TimerStep
import com.ovle.rl.model.template.NamedTemplate


class TimerTemplate(
    override val name: String,
    val maxRepeats: TimerStep? = null,
    val fireCheck: TimerCheck = { true },
    val finishCheck: TimerCheck = { true },
    val onFire: Collection<TimerEffect> = listOf(),
    val onFinish: Collection<TimerEffect> = listOf()
): NamedTemplate