package com.ovle.rl.model.game.tile

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.build.BuildTemplate
import com.ovle.rl.model.game.craft.CraftTemplate

fun TileArray.isPlaceToCraftOn(
    p: GridPoint2, t: CraftTemplate
) = check(p) { t.check?.invoke(it) ?: true }

fun TileArray.isPlaceToBuildOn(
    p: GridPoint2, t: BuildTemplate
) = check(p) { t.check?.check?.invoke(p, this) ?: true }

fun TileArray.isMineable(p: GridPoint2) = check(p) { it.props.isWall }