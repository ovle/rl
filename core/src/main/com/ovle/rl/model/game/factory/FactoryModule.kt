package com.ovle.rl.model.game.factory

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.spawn.EntitySpawnCheckHelper
import org.kodein.di.*


val factoryModule = DI.Module("factory") {
    bind<EntitySystem>().inSet() with singleton {
        FactorySystem(EntitySpawnCheckHelper())
    }
}