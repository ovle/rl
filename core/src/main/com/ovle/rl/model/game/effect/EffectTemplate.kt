package com.ovle.rl.model.game.effect

import com.ovle.rl.Effect

class EffectTemplate(
    val name: String,
    val effect: Effect
)