package com.ovle.rl.model.game.task.target.selection

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.game.build.BuildTemplate
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.material.MaterialTemplate
import com.ovle.rl.model.template.entity.EntityTemplate

sealed class TaskTargetSelection {

    open val isMultiselect: Boolean = false

    abstract fun taskTargets(): Collection<TaskTarget>

    open fun updatePoints(selection: Collection<GridPoint2>) { }
    open fun updateEntities(entities: Collection<Entity>) { }

    //todo
    open fun updateBuildTemplate(template: BuildTemplate?) { }
    open fun updateCraftTemplate(template: CraftTemplate?) { }
    open fun updateResource(resource: EntityTemplate?) { }
    open fun updateFarmTemplate(template: FarmTemplate?) { }

    abstract fun check(): TaskTargetCheck
    abstract fun copy(): TaskTargetSelection
    abstract fun reset()
}
