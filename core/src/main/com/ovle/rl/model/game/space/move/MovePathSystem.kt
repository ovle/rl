package com.ovle.rl.model.game.space.move

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.SystemPriority
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.social.player
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.move.impl.path.MovePath
import com.ovle.rl.model.game.space.move.impl.path.MovePathFactory

import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.movables
import ktx.ashley.get

/**
 * System for move path calculation
 */
class MovePathSystem(
    private val movePathFactory: MovePathFactory,
) : TimedEventSystem() {

    override val updateInterval = SystemUpdateInterval.MOVE_CALC_PATH
    override val systemPriority = SystemPriority.MOVE


    override fun updateTime() {
        val content = game().location().content
        val entities = content.entities.all().movables()

        for (entity in entities) {
            calcPath(entity)
        }
    }

    private fun calcPath(entity: Entity) {
        val params = entity[move]!!.params ?: return
        if (params.isFixedPathMove) return
        if (!params.dirty) return
        val task = params.task ?: return

        val location = game().location()
        val goals = task.goals(entity, location.content)
        val forceAccessiblePoints = forceAccessiblePoints(location, entity)

        val path = movePathFactory.path(
            entity,
            goals,
            task.isFlee,
            location,
            forceAccessiblePoints
        )

        params.dirty = false
        params.path = if (path != null) MovePath(path) else null
    }

    private fun forceAccessiblePoints(location: Location, entity: Entity): Collection<GridPoint2> {
        val locationProjection = entity.player()?.locationProjection
            ?: return emptyList()

        val isKnownTile = locationProjection.knownMap::isKnownTile
        val tiles = location.content.tiles

        return tiles.points.filterNot(isKnownTile)
    }
}