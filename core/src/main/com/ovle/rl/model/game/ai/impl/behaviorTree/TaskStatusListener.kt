package com.ovle.rl.model.game.ai.impl.behaviorTree

import com.badlogic.gdx.ai.btree.BehaviorTree
import com.badlogic.gdx.ai.btree.Task
import com.badlogic.gdx.ai.btree.Task.Status.FAILED
import com.badlogic.gdx.ai.btree.Task.Status.SUCCEEDED
import com.ovle.rl.model.game.ai.AI_LOG_TAG
import com.ovle.rl.model.game.ai.impl.behaviorTree.dto.BTParams
import com.ovle.rl.model.util.info
import ktx.log.debug
import ktx.log.info


open class TaskStatusListener(private val tree: BehaviorTree<BTParams>): BehaviorTree.Listener<BTParams> {

    override fun childAdded(task: Task<BTParams>?, index: Int) {}

    override fun statusUpdated(task: Task<BTParams>, previousStatus: Task.Status?) {
        val status = task.status
        val isTerminalStatus = isTerminal(status)
        val btParams = task.`object`
        val aiTask = btParams.rootTask

        if (task is BaseTask) {
            debug(AI_LOG_TAG) { "$status: ${task.name} (${btParams.owner.info()}), (${aiTask.debugInfo()})" }
        }

        val root = tree.getChild(0)
        if (task == root && isTerminalStatus) {
            val source = aiTask.source
            when (status) {
                SUCCEEDED -> source?.success()
                FAILED -> source?.fail()
                else -> { }
            }
        }
    }

    protected fun isTerminal(status: Task.Status) = status in arrayOf(SUCCEEDED, FAILED)
}