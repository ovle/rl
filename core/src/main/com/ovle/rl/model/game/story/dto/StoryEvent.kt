package com.ovle.rl.model.game.story.dto


data class StoryEvent(
    val template: StoryEventTemplate,
    val startHour: Int,
)