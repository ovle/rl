package com.ovle.rl.model.game.quest

import com.ovle.rl.model.game.core.component.EntityComponent

class QuestOwnerComponent(
    var questIds: MutableList<String> = mutableListOf()
) : EntityComponent()