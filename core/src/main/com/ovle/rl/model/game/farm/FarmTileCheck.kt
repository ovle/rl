package com.ovle.rl.model.game.farm

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray

data class FarmTileCheck(
    val check: (GridPoint2, TileArray) -> Boolean,
    val description: String
)