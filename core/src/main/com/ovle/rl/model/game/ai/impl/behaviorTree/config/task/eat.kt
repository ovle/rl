package com.ovle.rl.model.game.ai.impl.behaviorTree.config.task

import com.badlogic.gdx.ai.btree.Task.Status.SUCCEEDED
import com.ovle.rl.model.game.ai.TaskExec
import com.ovle.rl.model.game.ai.task.impl.Eat
import com.ovle.rl.model.game.life.hunger.EntityEatCommand
import com.ovle.utils.event.EventBus.send

fun eatTask(task: Eat): TaskExec = { btParams ->
    val owner = btParams.owner
    val food = task.target
    send(EntityEatCommand(owner, food))

    SUCCEEDED
}