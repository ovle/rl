package com.ovle.rl.model.game.time

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.time.dto.GlobalTimeInfo
import org.kodein.di.*


val timeModule = DI.Module("time") {
    bind<EntitySystem>().inSet() with singleton {
        GlobalTimeSystem()
    }
    bind<EntitySystem>().inSet() with singleton {
        TimerSystem()
    }
    bind<GlobalComponent>().inSet() with provider {
        GlobalTimeComponent(GlobalTimeInfo())
    }
}