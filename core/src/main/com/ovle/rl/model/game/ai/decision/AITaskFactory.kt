package com.ovle.rl.model.game.ai.decision

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.task.RootTask

interface AITaskFactory {

    fun process(entity: Entity, context: AITaskFactoryContext): RootTask?
}