package com.ovle.rl.model.game.light

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.util.AOEData

class LightSourceComponent(
    val area: AOEData
): EntityComponent()