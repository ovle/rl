package com.ovle.rl.model.game.social

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.procgen.name.entity.EntityNameGenerator
import org.kodein.di.*


val socialModule = DI.Module("social") {
    bind<EntityNameGenerator>() with singleton {
        EntityNameGenerator(instance())
    }
    bind<EnemiesHelper>() with singleton {
        EnemiesHelper()
    }

    bind<EntitySystem>().inSet() with singleton {
        SocialSystem(instance())
    }
}