package com.ovle.rl.model.game.material

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.mine.MineTileHelper
import org.kodein.di.*


val materialModule = DI.Module("material") {
    bind<EntitySystem>().inSet() with singleton {
        MaterialSystem()
    }
}