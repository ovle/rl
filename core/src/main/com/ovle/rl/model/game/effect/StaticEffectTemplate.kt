package com.ovle.rl.model.game.effect

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.template.NamedTemplate

data class StaticEffectTemplate(
    override val name: String,
    val apply: (Entity) -> Unit,
    val revert: (Entity) -> Unit,
): NamedTemplate