package com.ovle.rl.model.game.ai

import com.ovle.utils.gdx.ashley.component.mapper

object Components {
    val ai = mapper<AIComponent>()
}