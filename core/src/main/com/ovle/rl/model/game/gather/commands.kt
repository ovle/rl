package com.ovle.rl.model.game.gather

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.GameCommand


class MineTileCommand(val position: GridPoint2, val source: Entity?) : GameCommand()

class GatherEntityCommand(val entity: Entity, val source: Entity?) : GameCommand()