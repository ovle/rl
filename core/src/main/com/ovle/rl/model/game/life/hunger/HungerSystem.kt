package com.ovle.rl.model.game.life.hunger

import com.badlogic.ashley.core.Entity
import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.DestroyEntityCommand
import com.ovle.rl.model.game.life.*
import com.ovle.rl.model.game.life.Components.hunger
import com.ovle.rl.model.util.*
import com.ovle.utils.RandomParams
import ktx.ashley.get


class HungerSystem : TimedEventSystem() {

    companion object {
        private const val HUNGER_INCREASE_CHANCE_PER_UPDATE = 0.5f
    }

    override val updateInterval: Tick = SystemUpdateInterval.HUNGER


    override fun subscribeIntr() {
        subscribe<EntityEatCommand>(this) { onEntityEatCommand(it.entity, it.food) }
    }

    override fun updateTime() {
        val location = game().location()
        val content = location.content
        val entities = content.entities.all().ableToEat()
        val random = location.random

        entities.forEach {
            processEntity(it, random)
        }
    }

    private fun processEntity(entity: Entity, random: RandomParams) {
        if (entity.isDead()) return

        val hunger = entity[hunger]!!
        if (hunger.isStarved) {
            send(EntityStarvedEvent(entity))
            return
        }

        if (random.chance(HUNGER_INCREASE_CHANCE_PER_UPDATE)) {
            hunger.hunger++
        }
    }

    private fun onEntityEatCommand(entity: Entity, food: Entity) {
        val hunger = entity[hunger]!!
        //todo depends on food
        hunger.hunger = 0

        send(EntityConsumedEvent(entity, food))
        send(DestroyEntityCommand(food))
    }
}