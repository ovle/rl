package com.ovle.rl.model.game.area

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.Location

class ValidAreasHelper {

    fun isTileValid(template: AreaTemplate, p: GridPoint2, l: Location): Boolean {
        val tile = l.content.tiles[p.x, p.y]
        return tile.props.isFloor && template.tileCheck.check(tile)
    }

    fun validateArea(areaInfo: AreaInfo, location: Location) {
        val checks = areaInfo.template.checks
        areaInfo.invalidChecks = checks.filter {
            !it.check(areaInfo, location)
        }
    }
}