package com.ovle.rl.model.game.perception

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.projection.LocationProjection
import com.ovle.rl.model.game.perception.Components.perception
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.info
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get
import ktx.ashley.has

object Components {
    val perception = mapper<PerceptionComponent>()
}

fun Entity.fov(): Collection<GridPoint2> {
    check(has(perception)) { "no perception for entity ${info()}" }
    return this[perception]!!.fov
}

fun Entity.isVisible(lp: LocationProjection)
    = lp.visibleMap.isVisibleTile(position())

//not valid for movable entities
fun Entity.isKnown(lp: LocationProjection)
    = lp.knownMap.isKnownTile(position())

//todo move check is hack
// for known+invisible territory need render entities on their last know positions
fun Entity.isVisibleWhileKnown(): Boolean {
    return !has(move)
}

fun Entity.isVisibleForInteraction(tilesInfo: LocationProjection): Boolean {
    val isVisible = isVisible(tilesInfo)
    val isKnown = isKnown(tilesInfo)
    return isVisible || (isKnown && isVisibleWhileKnown())
}