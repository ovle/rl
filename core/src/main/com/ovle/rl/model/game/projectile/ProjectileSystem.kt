package com.ovle.rl.model.game.projectile

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.collision.Components.collision
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.projectile.dto.ProjectileInfo
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.move.MoveParams

import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get

class ProjectileSystem: BaseSystem() {

    override fun subscribe() {
        subscribe<CreateProjectileCommand>(this) { onCreateProjectileCommand(it.info) }
    }

    private fun onCreateProjectileCommand(info: ProjectileInfo) {
        val content = game().location().content
        val projectile = content.entities.entity(info.entityId)!!
        initProjectile(projectile, info)
    }

    private fun initProjectile(newProjectile: Entity, info: ProjectileInfo) {
        //enable only here to skip collision on entity create
        val collisionComponent = newProjectile[collision]!!
        collisionComponent.active = true

        val moveComponent = newProjectile[move]!!
        val pathTemplate = moveComponent.pathTemplate!!
        val path = pathTemplate.getPath.invoke(info.from, info.to)
        path.next()

        //todo cleanup the path when FINISHED
        moveComponent.params = MoveParams(path = path)
        moveComponent.time = 0
    }
}