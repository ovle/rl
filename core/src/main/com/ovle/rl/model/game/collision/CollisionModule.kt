package com.ovle.rl.model.game.collision

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.*


val collisionModule = DI.Module("collision") {
    bind<CollisionHelper>() with singleton {
        CollisionHelper()
    }
    bind<EntitySystem>().inSet() with singleton {
        CollisionSystem(instance())
    }
}