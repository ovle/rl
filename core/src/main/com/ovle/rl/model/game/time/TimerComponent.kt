package com.ovle.rl.model.game.time

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.time.dto.Timer

class TimerComponent(
    val timers: MutableList<Timer> = mutableListOf()
): EntityComponent()