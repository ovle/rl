package com.ovle.rl.model.game.game.dto.location.projection

/**
 * player's projection of location content
 * main use case is fog-of-war implementation
 */
class LocationProjection(
    val knownMap: KnownTileMap,
    val visibleMap: VisibleTileMap,
)