package com.ovle.rl.model.game

import com.ovle.utils.event.DEFAULT_PRIORITY

//todo other systems?
object SystemPriority {
    const val ACCESSIBILITY = -20
    const val TASK = -10
    const val SKILL = -5
    const val DEFAULT = DEFAULT_PRIORITY // 0
    const val AI = 10
    const val MOVE = 100
}