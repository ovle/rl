package com.ovle.rl.model.game.mine

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.tile.TileTemplate


class MineTileHelper(
    private val mineTemplates: Collection<MineTemplate>
) {

    //todo move out
    private val mineMap = mutableMapOf<GridPoint2, Int>()

    fun gatherTile(point: GridPoint2, tile: TileTemplate): Boolean {
        val mineTemplate = mineTemplate(tile) ?: return false

        val value = mineMap.computeIfAbsent(point) { 2 }
//        val value = mineMap.computeIfAbsent(point) { mineTemplate.durability }
        val newValue = value - 1
        if (newValue == 0) {
            mineMap.remove(point)
            return true
        }

        mineMap[point] = newValue
        return false
    }

    fun mineTemplate(tile: TileTemplate) = mineTemplates
        .find { it.tileTemplate == tile }
}