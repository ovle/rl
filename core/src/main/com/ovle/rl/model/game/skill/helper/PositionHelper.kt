package com.ovle.rl.model.game.skill.helper

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.TileCheck1
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.space.accessibility.Accessibility.Companion.Key
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.util.positions
import com.ovle.utils.gdx.math.discretization.bresenham.DiscreetCircleType
import com.ovle.utils.gdx.math.discretization.bresenham.circle
import com.ovle.utils.gdx.math.lineOfSight.rayTracing.hasLos

class PositionHelper(
    private val passabilityHelper: PassabilityHelper
) {

    /**
     * @return true if the [range] is accessible for given [owner] and target in [targetPosition]
     */
    fun isTargetAccessible(
        owner: Entity, range: IntRange?, targetPosition: GridPoint2, content: LocationContent
    ) = positionsToTarget(owner, range, targetPosition, content)
        ?.isNotEmpty() ?: true

    /**
     * @return null if no restrictions, empty collection if no points fit, non-empty collection otherwise
     */
    fun positionsToTarget(
        owner: Entity, range: IntRange?, targetPosition: GridPoint2, content: LocationContent
    ): Collection<GridPoint2>? {
        if (range == null) return null

        val accessibility = content.accessibility
        val key = Key(owner)
        val position = owner.position()

        val rangePoints = pointRange(targetPosition, range)
            .filter { accessibility.isAccessible(key, position, it) }
        if (range.max() <= 1) return rangePoints //no obstacles check

        val tileObstacles = content.tiles
            .filterTiles { t -> t.props.isWall }.map { it.first }
        val entityObstacles = content.entities.all()
            .filterNot (passabilityHelper::isEntityPassable)
            .positions()
        val skillObstacles = tileObstacles + entityObstacles
        val losObstacles = skillObstacles - targetPosition - position

        //todo LOS map? unlike moving, LOS doesn't seems to be individual
        return rangePoints.filter { hasLos(it, targetPosition, losObstacles) }
    }


    private fun pointRange(p: GridPoint2, range: IntRange) =
        if (range == 0..0) setOf(p)
        else range.flatMap { circle(p, it, DiscreetCircleType.DISTANCE_BASED) }

    private fun TileArray.filterTiles(filter: TileCheck1): Collection<Pair<GridPoint2, TileTemplate>> {
        return indexedElements()
            .map { (i, t) -> point(i) to t }
            .filter { (p, t) -> filter.invoke(t) }
    }
}