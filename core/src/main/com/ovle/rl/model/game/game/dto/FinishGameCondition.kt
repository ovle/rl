package com.ovle.rl.model.game.game.dto

import com.ovle.rl.model.game.game.FinishGameConditionCheck

data class FinishGameCondition(
    val description: String,
    val result: GameResult,
    val check: FinishGameConditionCheck,
)