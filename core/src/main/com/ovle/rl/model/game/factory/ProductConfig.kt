package com.ovle.rl.model.game.factory

import com.ovle.rl.model.template.entity.EntityTemplate

data class ProductConfig(
    val product: EntityTemplate,
    val spawnConfigs: Collection<ProductSpawnConfig>
) {
    init {
        check(spawnConfigs.map { it.chance }.sum() == 1.0f)
    }
}