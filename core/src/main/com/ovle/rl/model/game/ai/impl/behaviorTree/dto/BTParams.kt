package com.ovle.rl.model.game.ai.impl.behaviorTree.dto

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.task.AITask
import com.ovle.rl.model.game.game.dto.location.Location

data class BTParams(
    val owner: Entity,
    val location: Location,
    val rootTask: AITask
)