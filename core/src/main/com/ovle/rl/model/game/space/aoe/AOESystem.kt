package com.ovle.rl.model.game.space.aoe

import com.badlogic.ashley.core.Entity
import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.collision.EntityAOEChangedEvent
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.DestroyEntityCommand
import com.ovle.rl.model.game.space.ChangeEntityPositionCommand
import com.ovle.rl.model.game.space.Components.aoe
import com.ovle.rl.model.game.space.setAoe
import com.ovle.rl.model.util.aoeOwners

import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus
import ktx.ashley.get

/**
 * System for changing entity position/size (checking passability first)
 * 'passability' is about if we can place object at target position
 * [ChangeEntityPositionCommand] supposed to be the only way to do that
 *
 * the only special case is entity leaving the location
 */
class AOESystem : TimedEventSystem() {

    override val updateInterval: Tick = SystemUpdateInterval.AOE

    override fun updateTime() {
        val location = game().location()
        val entities = location.content.entities.all()
            .aoeOwners()
        entities.forEach { process(it) }
    }


    private fun process(entity: Entity) {
        val aoeComponent = entity[aoe]!!
        val aoeTemplate = aoeComponent.template
        val params = aoeComponent.params

        when {
            params == null -> startAOE(entity)
            params.count == aoeComponent.duration -> stopAOE(entity)
            else -> {
                params.count++

                val newAoe = aoeTemplate.factory.invoke(entity, params.count)
                entity.setAoe(newAoe)

                EventBus.send(EntityAOEChangedEvent(entity))
            }
        }
    }

    private fun startAOE(entity: Entity) {
        val aoeComponent = entity[aoe]!!
        aoeComponent.params = AOEParams(0)
    }

    private fun stopAOE(entity: Entity) {
        EventBus.send(DestroyEntityCommand(entity))
    }
}