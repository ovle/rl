package com.ovle.rl.model.game.ai.task

import com.ovle.rl.model.game.core.component.GameInfoNode


abstract class AITask {

    var source: AITaskSource? = null

    open fun gameInfo() = GameInfoNode(this.javaClass.simpleName)

    abstract fun debugInfo(): String
}