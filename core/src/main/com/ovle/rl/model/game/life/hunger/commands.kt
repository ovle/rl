package com.ovle.rl.model.game.life.hunger

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.GameCommand

class EntityEatCommand(val entity: Entity, val food: Entity) : GameCommand()
