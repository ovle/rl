package com.ovle.rl.model.game.ai.impl.behaviorTree

import com.badlogic.gdx.ai.btree.LeafTask
import com.badlogic.gdx.ai.btree.Task
import com.badlogic.gdx.ai.btree.Task.Status.FAILED
import com.ovle.rl.model.game.ai.TaskCancelExec
import com.ovle.rl.model.game.ai.TaskExecFactory
import com.ovle.rl.model.game.ai.TaskExecResult
import com.ovle.rl.model.game.ai.impl.behaviorTree.dto.BTParams
import com.ovle.rl.model.game.task.InvalidTargetException
import ktx.log.debug

class BaseTask(
    var name: String? = "", var execFactory: TaskExecFactory? = null, var cancel: TaskCancelExec? = null
): LeafTask<BTParams>() {

    override fun copyTo(otherTask: Task<BTParams>): Task<BTParams> {
        otherTask as BaseTask
        otherTask.name = name
        otherTask.execFactory = execFactory
        otherTask.cancel = cancel
        return otherTask
    }

    override fun execute(): Status {
        val btParams = this.`object`
//        info { "execute $name for entity ${btParams.owner.info()}" }

        val execResult: TaskExecResult
        try {
            val taskExec = this.execFactory!!.invoke(btParams)
            execResult = taskExec.invoke(btParams)
        } catch (ex: InvalidTargetException) {
            debug { "${name}: target is no longer valid" }
            return FAILED
        }

        return execResult
    }

    override fun cancelRunningChildren(startIndex: Int) {
        super.cancelRunningChildren(startIndex)

        val btParams = this.`object`
        cancel?.invoke(btParams)
    }
}