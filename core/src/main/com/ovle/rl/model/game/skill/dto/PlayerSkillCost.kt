package com.ovle.rl.model.game.skill.dto

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer


sealed class PlayerSkillCost {

    object Free: PlayerSkillCost() {
        override fun isAbleToPay(player: LocationPlayer, location: Location) = true

        override fun pay(player: LocationPlayer, location: Location) = true
    }
/*
    class Resource(
        private val resourceTemplate: ResourceTemplate, private val amount: Int
    ): PlayerSkillCost() {
        override fun isAbleToPay(player: LocationPlayer, location: Location): Boolean {
            val resources = resources(location, player)
            return resources.count() >= amount
        }

        override fun pay(player: LocationPlayer, location: Location): Boolean {
            val resourcesToPay = resources(location, player).take(amount)
            if (resourcesToPay.size < amount) return false

            resourcesToPay.forEach {
                send(DestroyEntityCommand(it))
            }
            return true
        }

        private fun resources(location: Location, player: LocationPlayer): Collection<Entity> {
            val entities = location.content.entities.all()
            return entities//.atInfluenceZone(player)
                .ofTemplate(resourceTemplate.entityRaw)
        }
    }

 */

    abstract fun isAbleToPay(player: LocationPlayer, location: Location): Boolean

    abstract fun pay(player: LocationPlayer, location: Location): Boolean
}