package com.ovle.rl.model.game.story

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.core.component.GlobalComponent
import org.kodein.di.*


val storyModule = DI.Module("story") {
    bind<EntitySystem>().inSet() with singleton {
        StorySystem(instance())
    }

    bind<GlobalComponent>().inSet() with provider {
        StoryComponent()
    }
}