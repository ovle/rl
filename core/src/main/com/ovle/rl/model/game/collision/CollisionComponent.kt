package com.ovle.rl.model.game.collision

import com.ovle.rl.model.game.core.component.EntityComponent

/**
 * @property active     is need to check collision/fire appropriate events for the entity
 * @property hasBody    is allowed for other entities to occupy the same space
 * @property collisionDamage    damage amount on collision (applied by external sources, like triggers)
 */
class CollisionComponent(
    var active: Boolean = true,
    var hasBody: Boolean = true,
    var collisionDamage: Int = 1
) : EntityComponent()