package com.ovle.rl.model.game.transform

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.*


val transformModule = DI.Module("transform") {
    bind<EntitySystem>().inSet() with singleton {
        TransformSystem()
    }
}