package com.ovle.rl.model.game.skill.dto

import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.skill.SkillTarget

sealed class SkillUsage {
    class Entity(
        val skill: SkillTemplate,
        val target: SkillTarget?,
        val source: com.badlogic.ashley.core.Entity
    ): SkillUsage()

    class Player(
        val playerSkill: PlayerSkillTemplate,
        val target: SkillTarget?,
        val source: LocationPlayer
    ): SkillUsage()
}