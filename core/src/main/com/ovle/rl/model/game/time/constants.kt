package com.ovle.rl.model.game.time

import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemUpdateInterval

/**
 * tick duration in seconds
 */
const val TICK_SIZE = 1.0/60.0

const val DEFAULT_MOVE_TIME_TILE = 60

/**
 * DEFAULT_GAME_SPEED should be matched with minActionUpdateInterval
 */
const val DEFAULT_GAME_SPEED = 2.0

const val BASE_SKILL_DURATION: Tick = SystemUpdateInterval.BASE
const val MIN_ANIMATION_FRAME_LENGTH = 0.25

const val MINUTES_IN_HOUR = 60
const val HOURS_IN_DAY = 24

const val SUNSET_HOUR = 22
const val SUNRISE_HOUR = 6

const val GAME_MINUTE_TICKS = 25
const val GAME_HOUR_TICKS = GAME_MINUTE_TICKS * MINUTES_IN_HOUR
const val GAME_DAY_TICKS = GAME_HOUR_TICKS * HOURS_IN_DAY
