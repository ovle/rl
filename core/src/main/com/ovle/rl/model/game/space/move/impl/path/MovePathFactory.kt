package com.ovle.rl.model.game.space.move.impl.path

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.space.aoeOrNull
import com.ovle.rl.model.game.space.move.MOVE_LOG_TAG
import com.ovle.rl.model.game.space.move.impl.moveMap.MoveMapFactory
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.info
import com.ovle.utils.gdx.math.array2d.gradientDescent
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMap
import ktx.log.debug
import ktx.log.info


class MovePathFactory(
    private val moveMapFactory: MoveMapFactory
) {

    /**
     * @return null if no path, empty collection if already at goal, path otherwise
     */
    fun path(
        entity: Entity,
        goals: Collection<GridPoint2>,
        isFromGoals: Boolean,
        location: Location,
        forceAccessiblePoints: Collection<GridPoint2>
    ): List<GridPoint2>? {
        debug(MOVE_LOG_TAG) { "calc path for ${entity.info()} (${entity.position()}), goals: ${goals}, isFromGoals: $isFromGoals" }

        val isEntityOnGoals = entity.position() in goals.toSet()
        if (!isFromGoals && isEntityOnGoals) {
            info(MOVE_LOG_TAG) { "empty path!" }
            return emptyList()
        }

        fun fleeGoals() = moveMapFactory.fleeMap(entity, goals, location, forceAccessiblePoints).goals
        val resultGoals = if (isFromGoals) fleeGoals() else goals

        val map = moveMapFactory.moveMap(entity, resultGoals, location, forceAccessiblePoints)

        val result = path(entity, map)

        debug(MOVE_LOG_TAG) { "path for ${entity.info()} (${entity.position()}): $result" }
        if (result == null) {
            info(MOVE_LOG_TAG) { "no path!" }
        }

        return result
    }

    private fun path(entity: Entity, map: DijkstraMap): List<GridPoint2>? {
        val body = entity.aoeOrNull() ?: setOf(entity.position())

        var p = entity.position().cpy()
        val b = body.map { it.cpy() }
        val result = mutableListOf<GridPoint2>()

        while (true) {
            val diff = map.data.gradientDescent(b) ?: break
            b.forEach { it.add(diff) }

            p = p.add(diff)
            result += p.cpy()
        }

        return if (result.isEmpty()) null else result
    }
}