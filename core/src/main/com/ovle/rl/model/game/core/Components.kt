package com.ovle.rl.model.game.core

import com.badlogic.ashley.core.Entity
import com.ovle.utils.gdx.ashley.component.mapper
import com.ovle.rl.EntityId
import com.ovle.rl.model.game.core.Components.core
import com.ovle.rl.model.game.core.Components.template
import com.ovle.rl.model.game.core.component.CoreComponent
import com.ovle.rl.model.game.core.component.template.TemplateComponent
import com.ovle.rl.model.game.social.personalNameOrNull
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.info
import ktx.ashley.get
import ktx.ashley.has


object Components {
    val core = mapper<CoreComponent>()
    val template = mapper<TemplateComponent>()
}

fun Entity.id(): EntityId {
    check(has(core)) { "no core for entity ${info()}" }
    return this[core]!!.id
}

fun Entity.template(): EntityTemplate {
    check(has(template)) { "no template for entity ${info()}" }
    return this[template]!!.template
}

fun Entity.templateName() = template().name

fun Entity.name(): String {
    check(has(template)) { "no template for entity ${info()}" }

    val personalName = this.personalNameOrNull()
    val templateName = this[template]!!.template.name
    return if (personalName != null) "$personalName, $templateName" else templateName
}