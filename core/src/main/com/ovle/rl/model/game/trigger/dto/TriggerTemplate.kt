package com.ovle.rl.model.game.trigger.dto

import com.ovle.rl.TriggerEffect
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.template.NamedTemplate

data class TriggerTemplate(
    override val name: String,
    val key: Class<out GameEvent>,
    val effects: Collection<TriggerEffect>
): NamedTemplate
