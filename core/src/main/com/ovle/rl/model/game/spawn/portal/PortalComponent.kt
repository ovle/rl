package com.ovle.rl.model.game.spawn.portal

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode

class PortalComponent : EntityComponent() {

    override fun gameInfo() = GameInfoNode(
        "portal:", listOf()
    )
}