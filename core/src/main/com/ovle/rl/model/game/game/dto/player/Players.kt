package com.ovle.rl.model.game.game.dto.player

import com.ovle.rl.model.game.social.dto.FactionTemplate

data class Players(
    val human: LocationPlayer.Human,
    val byFaction: Map<FactionTemplate, LocationPlayer>
) {
    fun all() = byFaction.values
}