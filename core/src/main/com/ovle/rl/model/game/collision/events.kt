package com.ovle.rl.model.game.collision

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.EntityGameEvent
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.util.info


class EntityCollidesEntityEvent(val entity: Entity, val obstacle: Entity) : EntityGameEvent(entity) {
    override val message: String
        get() = "${entity.info()} collides with ${obstacle.info()}"
}

class EntityCollidesTilesEvent(val entity: Entity, private val collisions: Collection<GridPoint2>) : EntityGameEvent(entity) {
    override val message: String
        get() = "${entity.info()} collides with tile(s) at ${collisions.info()}"
}

class EntityAOEChangedEvent(val entity: Entity) : GameEvent()