package com.ovle.rl.model.game.game

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.core.EntityGameEvent
import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.grid.world.World
import com.ovle.rl.model.game.spawn.EntitySpawn

class StartPlaygroundCommand(val locationTemplate: LocationTemplate) : GameCommand()
class StartGameCommand(val world: World, val locationPoint: GridPoint2, val locationTemplate: LocationTemplate) : GameCommand()
class StartSavedGameCommand(val game: GameEntity) : GameEvent()

class CreateEntityCommand(val spawn: EntitySpawn) : GameCommand()
class DestroyEntityCommand(val entity: Entity) : GameCommand()

class InitEntityCommand(val entity: Entity, val spawn: EntitySpawn?): EntityGameEvent(entity)