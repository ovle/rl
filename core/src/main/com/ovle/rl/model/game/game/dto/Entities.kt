package com.ovle.rl.model.game.game.dto

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.EntityId
import com.ovle.rl.model.game.core.Components.core
import com.ovle.rl.model.game.space.position
import ktx.ashley.get

class Entities {

    private val entities: MutableList<Entity> = mutableListOf()

    private val entitiesByPosition: MutableMap<GridPoint2, MutableCollection<Entity>> = mutableMapOf()


    fun add(entity: Entity) {
        entities += entity
        updatePosition(entity, null, entity.position())
    }

    fun remove(entity: Entity) {
        entities -= entity
        entitiesByPosition[entity.position()]?.remove(entity)
    }

    fun removeAll() {
        entities.clear()
        entitiesByPosition.clear()
    }

    fun all(): List<Entity> = entities

    fun entity(id: EntityId) = all().singleOrNull { it[core]?.id == id }

    fun on(point: GridPoint2): Collection<Entity> {
        return entitiesByPosition[point] ?: emptyList()
    }

    fun on(points: Collection<GridPoint2>): Collection<Entity> {
        return points.flatMap { entitiesByPosition[it] ?: emptyList() }
    }

    fun updatePosition(
        entity: Entity, oldPosition: GridPoint2?, newPosition: GridPoint2
    ) {
        entitiesByPosition[oldPosition]?.remove(entity)

        val entitiesOnPosition = entitiesByPosition
            .computeIfAbsent(newPosition) { mutableListOf() }
        entitiesOnPosition += entity
    }
}