package com.ovle.rl.model.game.ai.task

interface AITaskSource {
    fun success()
    fun fail()
}