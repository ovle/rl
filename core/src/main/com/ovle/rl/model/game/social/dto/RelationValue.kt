package com.ovle.rl.model.game.social.dto

enum class RelationValue {
    GOOD,
    NEUTRAL,
    BAD
}