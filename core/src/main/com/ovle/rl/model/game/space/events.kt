package com.ovle.rl.model.game.space

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.EntityGameEvent
import com.ovle.rl.model.util.info


class EntityChangedPositionEvent(val entity: Entity, val oldPosition: GridPoint2) : EntityGameEvent(entity) {
    override val message: String
        get() = "${entity.info()} changed position to ${entity.position().info()}"
}

class EntityStartedMovementEvent(val entity: Entity) : EntityGameEvent(entity)

class EntityStoppedMovementEvent(val entity: Entity) : EntityGameEvent(entity)
