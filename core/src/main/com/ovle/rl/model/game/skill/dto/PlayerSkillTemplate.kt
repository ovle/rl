package com.ovle.rl.model.game.skill.dto

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.template.NamedTemplate

class PlayerSkillTemplate(
    override val name: String = "",
    val skill: SkillTemplate,
    val cost: Collection<PlayerSkillCost> = listOf(PlayerSkillCost.Free),
    val getTargets: (GridPoint2, LocationContent) -> Collection<SkillTarget>,
    val isAvailable: (Location) -> Boolean = { l -> true } //todo technology check
): NamedTemplate