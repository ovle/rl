package com.ovle.rl.model.game.space.move

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.collision.hasBody
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.space.moveType

/**
 * entity or tile is 'passable' if actor entity is allowed to stand on/over it
 *
 * 'impassable' tile is always 'unmovable'
 * a tile may be 'passable' but not 'movable' for entity
 * i.e. a fish may get to the ground (by external force/teleportation), but is unable to move on it/from it
 */
class PassabilityHelper {

    fun isPassable(point: GridPoint2, entity: Entity, content: LocationContent): Boolean {
        if (!isPassable(point, content.tiles)) return false

        val entities = content.entities.on(point)
        val impassableEntities = impassableEntities(entity, entities)
        if (impassableEntities.isNotEmpty()) return false

        return true
    }


    private fun impassableEntities(entity: Entity, entities: Collection<Entity>): Collection<Entity> {
        return entities.subtract(setOf(entity))
            .filterNot { isEntityPassableForEntity(entity, it) }
    }

    private fun isPassable(point: GridPoint2, tiles: TileArray): Boolean {
        val checkTile = tiles[point.x, point.y]
        return tiles.isValid(point) && checkTile.props.isPassable
    }

    private fun isEntityPassableForEntity(entity: Entity, checkEntity: Entity): Boolean {
        return isEntityPassable(entity) || isEntityPassable(checkEntity)
    }

    fun isEntityPassable(entity: Entity): Boolean {
        val alwaysPassableMoveTypes = setOf(MoveType.FLYING, MoveType.SWIMMING)

        return when {
            entity.moveType() in alwaysPassableMoveTypes -> true
            !entity.hasBody() -> true
            else -> false
        }
    }
}