package com.ovle.rl.model.game.task.target.selection

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.game.task.target.TaskTargetCheck.*

data class GatherTargetSelection(
    var entities: MutableCollection<Entity> = mutableSetOf()
): TaskTargetSelection() {

    override val isMultiselect = true

    override fun taskTargets() = entities.map { TaskTarget.Entity(it) }


    override fun updateEntities(entities: Collection<Entity>) {
        this.entities += entities
    }


    override fun check(): TaskTargetCheck =
        if (entities.isEmpty()) Error("select entities")
        else Success

    override fun copy() = GatherTargetSelection(entities.toMutableSet())

    override fun reset() {
        entities.clear()
    }
}