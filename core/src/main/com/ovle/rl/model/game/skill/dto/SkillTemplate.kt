package com.ovle.rl.model.game.skill.dto

import com.ovle.rl.SkillEffect
import com.ovle.rl.Tick
import com.ovle.rl.model.template.NamedTemplate
import com.ovle.rl.model.game.skill.SkillOwnerCheck
import com.ovle.rl.model.game.skill.SkillTargetCheck

/**
 * @property name               skill name
 * @property tags               additional tags, see [SkillTag]
 * @property range              from what range the skill may be applied
 * @property ticks              ticks taken on usage
 * @property effects            skill effects, applied on successful finish
 * @property isTargetValid      target availability (exclude space-related availability)
 */
class SkillTemplate(
    override val name: String = "",
    val tags: Collection<SkillTag> = listOf(),
    val range: IntRange? = null,
    val ticks: Tick = 0,
    val effects: Collection<SkillEffect>,
    val isOwnerValid: SkillOwnerCheck = { _, _ -> true },
    val isTargetValid: SkillTargetCheck = { _, _ -> true },
): NamedTemplate