package com.ovle.rl.model.game.collision

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.collision.Components.collision
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get

object Components {
    val collision = mapper<CollisionComponent>()
}

fun Entity.hasBody(): Boolean {
    val collisionComponent = this[collision] ?: return false
    return collisionComponent.active && collisionComponent.hasBody
}