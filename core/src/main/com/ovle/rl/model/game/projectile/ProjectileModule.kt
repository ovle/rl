package com.ovle.rl.model.game.projectile

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.inSet
import org.kodein.di.singleton

val projectileModule = DI.Module("projectile") {
    bind<EntitySystem>().inSet() with singleton {
        ProjectileSystem()
    }
}