package com.ovle.rl.model.game.tile

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.GameCommand

class ChangeTileCommand(val tile: TileTemplate, val position: GridPoint2) : GameCommand()