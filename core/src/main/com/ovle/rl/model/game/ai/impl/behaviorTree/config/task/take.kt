package com.ovle.rl.model.game.ai.impl.behaviorTree.config.task

import com.badlogic.gdx.ai.btree.Task.Status.FAILED
import com.badlogic.gdx.ai.btree.Task.Status.SUCCEEDED
import com.ovle.rl.model.game.ai.TaskExec
import com.ovle.rl.model.game.ai.task.impl.Take
import com.ovle.rl.model.game.container.EntityTakeCarryItemCommand
import com.ovle.rl.model.game.container.carriedItem
import com.ovle.utils.event.EventBus.send

//todo drop item on task cancellation
fun takeTask(task: Take): TaskExec = { btParams ->
    val owner = btParams.owner
    val entity = task.target

    send(EntityTakeCarryItemCommand(owner, entity))

    if (owner.carriedItem() == entity) SUCCEEDED else FAILED
}