package com.ovle.rl.model.game.game.dto.player

import com.ovle.rl.PlayerId

data class Player(
    val id: PlayerId
) {

    companion object {
        val DEFAULT = Player("player1")
    }
}