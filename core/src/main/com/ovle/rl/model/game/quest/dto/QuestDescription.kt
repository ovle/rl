package com.ovle.rl.model.game.quest.dto

import com.ovle.rl.QuestCondition
import com.ovle.rl.QuestHook


class QuestDescription(
    val id: String,
    val title: String,
    val description: String,
    val precondition: QuestCondition,
    val successCondition: QuestCondition,
    val failCondition: QuestCondition?,
    val onSuccess: QuestHook = null,
    val onFail: QuestHook = null,
    val onFinish: QuestHook = null
) {
}