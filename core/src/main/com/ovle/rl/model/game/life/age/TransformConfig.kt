package com.ovle.rl.model.game.life.age

import com.ovle.rl.model.template.entity.EntityTemplate

data class TransformConfig(
    val entity: EntityTemplate,
    val chancePerHour: Float
)