package com.ovle.rl.model.game.schedule

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.inSet
import org.kodein.di.singleton


val dayScheduleModule = DI.Module("daySchedule") {
    bind<EntitySystem>().inSet() with singleton {
        DayScheduleSystem()
    }
}