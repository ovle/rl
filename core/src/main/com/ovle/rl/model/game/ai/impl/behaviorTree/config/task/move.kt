package com.ovle.rl.model.game.ai.impl.behaviorTree.config.task

import com.badlogic.gdx.ai.btree.Task.Status.*
import com.ovle.rl.model.game.ai.TaskExec
import com.ovle.rl.model.game.ai.task.impl.Move
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.EntityStartMovementCommand
import com.ovle.rl.model.game.space.EntityStopMovementCommand
import com.ovle.rl.model.game.space.move.MoveParams
import com.ovle.rl.model.game.space.move.MoveState.*
import com.ovle.utils.event.EventBus.send
import ktx.ashley.get


fun moveTask(task: Move): TaskExec = { btParams ->
    val owner = btParams.owner
    val moveComponent = owner[move]!!

    val status = when(moveComponent.state) {
        IDLE -> {
            val params = MoveParams(task)
            send(EntityStartMovementCommand(owner, params))
            RUNNING
        }
        MOVING, DIRTY, DIRTY_MOVING -> RUNNING
        FINISHED -> {
            send(EntityStopMovementCommand(owner))
            SUCCEEDED
        }
        UNAVAILABLE -> {
            send(EntityStopMovementCommand(owner))
            FAILED
        }
    }

    status
}