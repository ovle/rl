package com.ovle.rl.model.game.game.dto.location.projection

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.Array2d.Companion.array


class PlayerKnownTileMap(
    val content: LocationContent
): KnownTileMap {

    private var knownTilesMask: Array2d<Boolean> = array(false, content.tiles.size)

    //val lastKnownPositions: Map<EntityId, GridPoint2> = mutableMapOf()

    override fun isKnownTile(p: GridPoint2) = knownTilesMask.isValid(p)
        && knownTilesMask[p.x, p.y]

    override fun markAsKnown(p: GridPoint2) {
        knownTilesMask[p.x, p.y] = true
    }
}