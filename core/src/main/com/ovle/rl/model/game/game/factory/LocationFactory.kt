package com.ovle.rl.model.game.game.factory

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.*
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.game.dto.location.projection.*
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.game.dto.player.Player
import com.ovle.rl.model.game.game.dto.player.Players
import com.ovle.rl.model.game.space.accessibility.Accessibility
import com.ovle.rl.model.procgen.config.GridFactoryPayload
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.grid.Grids
import com.ovle.rl.model.procgen.grid.world.World
import com.ovle.rl.model.util.randomId
import com.ovle.utils.RandomParams


class LocationFactory {

    fun newLocation(
        template: LocationTemplate, gridFactoryPayload: GridFactoryPayload?, world: World?, random: RandomParams
    ): Location {
        val point = gridFactoryPayload?.point
        val locationSeed = locationSeed(random, point, world?.tiles?.size)
        val locationRandom = RandomParams(locationSeed)

        val gridFactories = template.getGridFactories.invoke(gridFactoryPayload)
        val grids = Grids(gridFactories.map { it.create(random) }.toTypedArray())
        val tiles = grids.toTileArray(template.tileMapper)
        val content = LocationContent(tiles, grids, Entities(), Accessibility())
        val factionsConfig = template.factions

        val humanPlayer = LocationPlayer.Human(
            player = Player.DEFAULT,
            locationProjection = LocationProjection(
                knownMap = PlayerKnownTileMap(content),
                visibleMap = PlayerVisibleTileMap(content),
            ),
        )
        val aiPlayer = LocationPlayer.AI(
            locationProjection = LocationProjection(
                knownMap = AIKnownTileMap(),
                visibleMap = AIVisibleTileMap(),
            )
        )

        return Location(
            id = randomId(),
            template = template,
            name = locationName(world, point, template),
            worldPoint = point,
            worldPlace = world?.place(point),
            random = locationRandom,
            content = content,
            players = Players(
                human = humanPlayer,
                byFaction = mapOf(
                    factionsConfig.playerFaction to humanPlayer,
                    factionsConfig.aiPlayerFaction to aiPlayer
                )
            )
        )
    }

    private fun locationSeed(globalRandom: RandomParams, locationPoint: GridPoint2?, worldSize: Int?): Long {
        val globalSeed = globalRandom.seed
        locationPoint ?: return globalSeed
        worldSize ?: return globalSeed

        val pointPart = (locationPoint.x + locationPoint.y * worldSize)
        return globalSeed + pointPart
    }

    private fun locationName(
        world: World?, locationPoint: GridPoint2?, template: LocationTemplate
    ): String {
        locationPoint ?: return template.name
        world ?: return template.name

        return world.region(locationPoint)!!.name
    }
}