package com.ovle.rl.model.game.spawn.portal

import com.ovle.utils.gdx.ashley.component.mapper


object Components {
    val portal = mapper<PortalComponent>()
}