package com.ovle.rl.model.game.schedule

import com.ovle.utils.gdx.ashley.component.mapper


object Components {
    val daySchedule = mapper<DayScheduleComponent>()
}