package com.ovle.rl.model.game.ai.impl.behaviorTree.config.task.stub

import com.badlogic.gdx.ai.btree.Task
import com.ovle.rl.model.game.ai.TaskExec
import ktx.log.info


fun successTask(message: String? = null): TaskExec = { (_) ->
    message?.let { info { it } }

    Task.Status.SUCCEEDED
}