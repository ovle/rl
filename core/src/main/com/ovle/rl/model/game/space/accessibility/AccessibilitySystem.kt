package com.ovle.rl.model.game.space.accessibility

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.SystemPriority
import com.ovle.rl.model.game.collision.hasBody
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.EntityDestroyedEvent
import com.ovle.rl.model.game.game.EntityLoadedEvent
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.life.EntityDiedEvent
import com.ovle.rl.model.game.space.EntityChangedPositionEvent
import com.ovle.rl.model.game.space.accessibility.Accessibility.Companion.Key
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.game.tile.TileChangedEvent
import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.subscribe

/**
 *
 */
class AccessibilitySystem(
    private val mapFactory: AccessibilityMapFactory
) : TimedEventSystem() {

    override val systemPriority = SystemPriority.ACCESSIBILITY


    override fun subscribeIntr() {
        subscribe<EntityLoadedEvent>(this) { onEntityLoadedEvent(it.entity) }
        subscribe<EntityDestroyedEvent>(this) { onEntityDestroyedEvent(it.entity) }
        subscribe<EntityDiedEvent>(this) { onEntityDiedEvent(it.entity) }
        subscribe<EntityChangedPositionEvent>(this) { onEntityChangedPositionEvent(it.entity) }

        subscribe<TileChangedEvent>(this) { onTileChangedEvent(it.position) }
    }

    private fun onEntityDiedEvent(entity: Entity) {
        markMapsDirty(entity)
    }

    private fun onEntityChangedPositionEvent(entity: Entity) {
        markMapsDirty(entity)
    }

    private fun onEntityLoadedEvent(entity: Entity) {
        markMapsDirty(entity)
    }

    private fun onEntityDestroyedEvent(entity: Entity) {
        markMapsDirty(entity)
    }

    private fun onTileChangedEvent(position: GridPoint2) {
        val accessibility = accessibility()

        accessibility.maps.values.forEach {
            it.dirty = true
        }
    }

    override fun updateTime() {
        val location = game().location()
        val content = location.content
        val accessibility = content.accessibility
        val players = accessibilityPlayers()
        val maps = accessibility.maps

        players.forEach { player ->
            MoveType.values().forEach { moveType ->
                val checkEntities = moveType == MoveType.DEFAULT
                val playerKey = Key(moveType, player)
                if (maps[playerKey]?.dirty != false) {
                    val unknownTiles = unknownTiles(player, content)
                    maps[playerKey] = mapFactory.accessibilityMap(
                        playerKey.moveType, unknownTiles, content, checkEntities
                    )
                }
            }
        }
    }


    private fun unknownTiles(
        player: LocationPlayer?, content: LocationContent
    ): List<GridPoint2> {
        if (player !is LocationPlayer.Human) return emptyList()

        val locationProjection = player.locationProjection
        val playerUnknownTiles = content.tiles.points.filterNot {
            locationProjection.knownMap.isKnownTile(it)
        }
        return playerUnknownTiles
    }

    //todo simplify?
    //todo partial update?
    private fun markMapsDirty(source: Entity) {
        val players = accessibilityPlayers()
        val accessibility = accessibility()
        val hasBody = source.hasBody()

        players.forEach { player ->
            when {
                player is LocationPlayer.Human -> {
                    //this includes player-visibility-based update, even without body
                    //so need update all player maps
                    MoveType.values().forEach { moveType ->
                        val key = Key(moveType, player)
                        accessibility.maps[key]?.dirty = true
                    }
                }

                hasBody -> {
                    //flying/swimming maps doesn't depends on entities
                    //flying/swimming entities doesn't affect default map
                    val key = Key(MoveType.DEFAULT, player)
                    accessibility.maps[key]?.dirty = true
                }

                else -> { }
            }
        }
    }

    private fun accessibilityPlayers() = game().location().players.all() + null

    private fun accessibility() = game().location().content.accessibility
}