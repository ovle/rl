package com.ovle.rl.model.game.ai

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.ai.GdxAI
import com.ovle.rl.model.game.SystemPriority
import com.ovle.rl.model.game.ai.Components.ai
import com.ovle.rl.model.game.ai.decision.EntityDecisionHelper
import com.ovle.rl.model.game.ai.decision.DecisionProcessor
import com.ovle.rl.model.game.ai.decision.EntityDecision
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.life.EntityAwakenEvent
import com.ovle.rl.model.game.life.EntityDiedEvent
import com.ovle.rl.model.game.life.EntityResurrectedEvent
import com.ovle.rl.model.game.life.EntityStartSleepEvent
import com.ovle.rl.model.game.time.ticksToTime
import com.ovle.rl.model.util.info
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.log.info

/**
 * AI processing, not tied to implementation
 */
class AISystem(
    private val decisionProcessor: DecisionProcessor,
    private val entityDecisionHelper: EntityDecisionHelper
) : TimedEventSystem() {

    override val systemPriority = SystemPriority.AI

    override fun updateTime() {
        val deltaTime = ticksToTime(updateInterval).toFloat()
        GdxAI.getTimepiece().update(deltaTime);

        val game = game()
        val entityDecisions = entityDecisionHelper.entityDecisions(game)
        entityDecisions.forEach {
            processEntityDecision(it, game)
        }
    }

    override fun subscribeIntr() {
        subscribe<EntityDiedEvent>(this) { onEntityDiedEvent(it.entity) }
        subscribe<EntityResurrectedEvent>(this) { onEntityResurrectedEvent(it.entity) }

        subscribe<EntityStartSleepEvent>(this) { onEntityStartSleepEvent(it.entity) }
        subscribe<EntityAwakenEvent>(this) { onEntityAwakenEvent(it.entity) }
    }


    private fun onEntityDiedEvent(entity: Entity) {
        val aiComponent = entity[ai] ?: return
        aiComponent.active = false

        decisionProcessor.onEntityAIDisabled(entity)
    }

    private fun onEntityResurrectedEvent(entity: Entity) {
        val aiComponent = entity[ai] ?: return
        aiComponent.active = true

        decisionProcessor.onEntityAIEnabled(entity)
    }

    private fun onEntityStartSleepEvent(entity: Entity) {
        val aiComponent = entity[ai] ?: return
        aiComponent.active = false

        decisionProcessor.onEntityAIDisabled(entity)
    }

    private fun onEntityAwakenEvent(entity: Entity) {
        val aiComponent = entity[ai] ?: return
        aiComponent.active = true

        decisionProcessor.onEntityAIEnabled(entity)
    }


    private fun processEntityDecision(entityDecision: EntityDecision, game: Entity) {
        val (entity, rootTask) = entityDecision
        val currentRootTask = entity[ai]!!.rootTask
        val isNameDiffers = rootTask.name != currentRootTask?.name
        val isIdDiffers = rootTask.id != currentRootTask?.id
        if (isNameDiffers || isIdDiffers) {
            info(AI_LOG_TAG) { "ai task: { ${rootTask.info()} }, is set for ${entity.info()}" }
            entity[ai]!!.rootTask = rootTask
            decisionProcessor.setTask(entity, game, rootTask)
        }

        decisionProcessor.process(entity, game)
    }
}
