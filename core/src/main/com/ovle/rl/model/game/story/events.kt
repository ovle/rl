package com.ovle.rl.model.game.story

import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.story.dto.StoryEvent

class StoryEventStartedEvent(val event: StoryEvent) : GameEvent() {
    override val message = "event ${event.template.name} started!"
    override val playerAwareMessage = message
}
