package com.ovle.rl.model.game.time

import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.time.dto.GlobalTimeInfo
import com.ovle.rl.model.game.time.dto.Timer

class GlobalTimeComponent (
    val time: GlobalTimeInfo,
    val timers: MutableCollection<Timer> = mutableListOf()
): GlobalComponent()
