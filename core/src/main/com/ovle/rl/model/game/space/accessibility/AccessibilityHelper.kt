package com.ovle.rl.model.game.space.accessibility

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.game.space.move.PassabilityHelper

/**
 * entity or tile is 'accessible' if actor entity is willing to move on it during the pathfinding
 *
 * inaccessible tile may not be possible for entity to move on/move from/stand on,
 * or it just can be dangerous for entity in any way ('accessible' level may vary between 1 and 0 in the last case?)
 */
class AccessibilityHelper(
    private val passabilityHelper: PassabilityHelper
) {

    /**
     *
     */
    fun accessiblePoints(
        points: Collection<GridPoint2>, content: LocationContent, moveType: MoveType, checkEntities: Boolean
    ): Collection<GridPoint2> = points.filter {
        isAccessible(it, content, moveType, checkEntities)
    }


    private fun isAccessible(point: GridPoint2, content: LocationContent, moveType: MoveType, checkEntities: Boolean): Boolean {
        val tile = content.tiles[point.x, point.y]
        val isTileAccessible = tile.props.isAccessible(moveType)

        return isTileAccessible && (!checkEntities || entityCheck(content, point))
    }

    private fun entityCheck(content: LocationContent, point: GridPoint2): Boolean {
        val entitiesOnPoint = content.entities.on(point)
        return entitiesOnPoint.all { passabilityHelper.isEntityPassable(it) }
    }
}