package com.ovle.rl.model.game.task

import com.ovle.rl.model.game.task.target.selection.TaskTargetSelection

class InvalidTargetException(val target: TaskTargetSelection): RuntimeException()