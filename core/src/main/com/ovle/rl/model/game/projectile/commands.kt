package com.ovle.rl.model.game.projectile

import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.game.projectile.dto.ProjectileInfo


class CreateProjectileCommand(val info: ProjectileInfo) : GameCommand()
