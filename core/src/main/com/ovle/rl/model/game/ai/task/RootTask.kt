package com.ovle.rl.model.game.ai.task

import com.ovle.rl.model.game.core.component.GameInfoNode

class RootTask(
    val name: String, val id: String, val task: AITask
): AITask() {

    override fun debugInfo() = "root $id $name"

    override fun gameInfo() = GameInfoNode("Root", listOf(task.gameInfo()))
}