package com.ovle.rl.model.game.game.dto

enum class GameResult {
    WIN,
    LOSE,
}