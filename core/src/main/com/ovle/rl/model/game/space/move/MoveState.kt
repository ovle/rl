package com.ovle.rl.model.game.space.move

enum class MoveState {

    /**
     * No moving
     */
    IDLE,

    /**
     * Entity is moving
     */
    MOVING,

    /**
     * will recalc path, but goal is still available
     */
    DIRTY,

    /**
     * same as [DIRTY] but goal is still in path,
     * so we can move even while move is dirty
     */
    DIRTY_MOVING,

    /**
     * Moving is finished
     */
    FINISHED,

    /**
     * Moving is not available (i.e. move target is not valid)
     */
    UNAVAILABLE,
}