package com.ovle.rl.model.game.ai.impl.behaviorTree.config.task

import com.badlogic.gdx.ai.btree.Task.Status.SUCCEEDED
import com.ovle.rl.model.game.ai.TaskExec
import com.ovle.rl.model.game.ai.task.impl.Drop
import com.ovle.rl.model.game.container.Components.carrier
import com.ovle.rl.model.game.container.EntityDropCarryItemCommand
import com.ovle.rl.model.game.space.position
import com.ovle.utils.event.EventBus.send
import ktx.ashley.get


fun dropTask(drop: Drop): TaskExec = { btParams ->
    val owner = btParams.owner
    val to = owner.position()
    val carried = owner[carrier]!!.item

    if (carried == null) SUCCEEDED else {
        send(EntityDropCarryItemCommand(owner, carried, to))
        SUCCEEDED
    }
}