package com.ovle.rl.model.game.tile

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.*


val tileModule = DI.Module("tile") {
    bind<EntitySystem>().inSet() with singleton {
        TileSystem(instance())
    }
}