package com.ovle.rl.model.game.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.task.dto.TaskCandidate
import com.ovle.rl.model.game.task.target.TaskTarget

typealias TaskPerformerFilter = (Entity, TaskTarget, Location) -> Boolean
typealias TaskCandidates = Collection<TaskCandidate>
