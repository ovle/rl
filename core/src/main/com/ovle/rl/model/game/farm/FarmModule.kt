package com.ovle.rl.model.game.farm

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.*


val farmModule = DI.Module("farm") {
    bind<EntitySystem>().inSet() with singleton {
        FarmSystem(instance())
    }
}