package com.ovle.rl.model.game.ai

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.ai.Components.ai
import ktx.ashley.get
import ktx.ashley.has


fun Entity.isAIActive() = has(ai) && this[ai]!!.active
