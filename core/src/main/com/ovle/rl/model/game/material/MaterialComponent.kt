package com.ovle.rl.model.game.material

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode

class MaterialComponent(
    var template: MaterialTemplate? = null,
) : EntityComponent() {

    override fun gameInfo() = GameInfoNode(
        "material: ${template?.name}"
    )
}