package com.ovle.rl.model.game.game

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.core.entity.entityWith
import com.ovle.rl.model.log.debugEventLogHook
import com.ovle.rl.model.log.gameLogHook
import com.ovle.rl.model.log.timestampLogHook
import com.ovle.utils.event.EventBus
import kotlin.math.min

class GameContext(
    private val engine: Engine,
    private val getSystems: () -> Set<EntitySystem>,
    val log: MutableList<GameLogItem> = mutableListOf(),
) {

    private var isStarted: Boolean = false


    fun start() {
        getSystems.invoke().forEach { engine.addSystem((it)) }

        EventBus.addHook(::debugEventLogHook)
        EventBus.addHook { timestampLogHook(it) { game() } }
        EventBus.addHook { gameLogHook(it, log) }

        isStarted = true
    }

    fun update(delta: Float) {
        if (!isStarted) return

        val interval = min(delta, 1 / 60f) //todo why max-bounded?
        engine.update(interval)
    }

    fun finish() {
        isStarted = false

        engine.removeAllSystems()
        engine.removeAllEntities()

        EventBus.clearHooks()

        log.clear()
    }

    fun game() = entityWith(engine.entities.toList(), GameComponent::class)!!
}