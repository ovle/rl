package com.ovle.rl.model.game.schedule

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.GameCommand

class EntityStartedDayScheduleEvent(val entity: Entity, val schedule: DaySchedule) : GameCommand()
class EntityFinishedDayScheduleEvent(val entity: Entity, val schedule: DaySchedule) : GameCommand()
