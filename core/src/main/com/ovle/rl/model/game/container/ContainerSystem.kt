package com.ovle.rl.model.game.container

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.container.Components.container
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.space.ChangeEntityPositionCommand
import com.ovle.rl.model.game.space.EntityChangedPositionEvent
import com.ovle.rl.model.game.space.position
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.ashley.has


class ContainerSystem : BaseSystem() {

    override fun subscribe() {
        subscribe<EntityTakeItemsCommand>(this) { onEntityTakeItems(it.entity, it.owner, it.items) }
        subscribe<EntityChangedPositionEvent>(this) { onEntityChangedPositionEvent(it.entity) }
    }

    private fun onEntityTakeItems(entity: Entity, owner: Entity?, items: Collection<Entity>) {
        val containerItems = entity[container]!!.items
        val ownerContainerItems = owner?.get(container)?.items

        items.forEach {
            containerItems += it
            if (ownerContainerItems != null) {
                ownerContainerItems -= it
            }

            send(ChangeEntityPositionCommand(it, entity.position()))
        }
    }

    private fun onEntityChangedPositionEvent(entity: Entity) {
        if (!entity.has(container)) return

        val position = entity.position()
        val items = entity[container]!!.items
        items.forEach {
            send(ChangeEntityPositionCommand(it, position))
        }
    }
}