package com.ovle.rl.model.game.farm

import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.template.NamedTemplate
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.templates

data class FarmTemplate(
    override val name: String,
    val resource: EntityTemplate? = null,
    val gatherResource: EntityTemplate,
    val lifecycle: Collection<EntityTemplate>,
    val farmTileCheck: FarmTileCheck
): NamedTemplate {

    init {
        check(lifecycle.isNotEmpty()) { "lifecycle is empty for template $name" }
    }

    val crop: EntityTemplate
        get() = lifecycle.first()


    fun isResourceAvailable(l: Location): Boolean {
        val r = resource ?: return true
        val ts = l.content.entities.all().templates()
        return r in ts
    }

    fun isAreaAvailable(player: LocationPlayer.Human): Boolean {
        return player.areas
            .any {
                val params = it.params
                params is AreaParams.Farm && params.template == this
            }
    }

    //todo technology check
}