package com.ovle.rl.model.game.space.aoe

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.component.EntityComponent

class AOEComponent(
    val template: AOETemplate,
    val duration: Int,
    val gridPositions: MutableCollection<GridPoint2> = mutableSetOf(),

    var params: AOEParams? = null,
) : EntityComponent()

