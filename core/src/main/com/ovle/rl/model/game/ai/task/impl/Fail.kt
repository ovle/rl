package com.ovle.rl.model.game.ai.task.impl

import com.ovle.rl.model.game.ai.task.AITask

object Fail: AITask() {

    override fun debugInfo() = "fail"
}