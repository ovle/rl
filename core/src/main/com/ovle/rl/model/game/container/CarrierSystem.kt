package com.ovle.rl.model.game.container

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.collision.Components.collision
import com.ovle.rl.model.game.container.Components.carriable
import com.ovle.rl.model.game.container.Components.carrier
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.EntityDestroyedEvent
import com.ovle.rl.model.game.space.ChangeEntityPositionCommand
import com.ovle.rl.model.game.space.EntityChangedPositionEvent
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.space.setPosition
import com.ovle.rl.model.util.info
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.utils.gdx.math.distance
import ktx.ashley.get
import ktx.ashley.has
import ktx.log.info


class CarrierSystem : BaseSystem() {

    override fun subscribe() {
        subscribe<EntityTakeCarryItemCommand>(this) { onEntityTakeCarryItemCommand(it.entity, it.item) }
        subscribe<EntityDropCarryItemCommand>(this) { onEntityDropCarryItemCommand(it.entity, it.item, it.position) }

        subscribe<EntityChangedPositionEvent>(this) { onEntityChangedPositionEvent(it.entity) }
        subscribe<EntityDestroyedEvent>(this) { onEntityDestroyedEvent(it.entity) }
    }


    private fun onEntityTakeCarryItemCommand(entity: Entity, item: Entity) {
        val isTooFarToTake = distance(entity.position(), item.position()) > 1
        if (isTooFarToTake) {
            info(CARRY_LOG_TAG) { "item ${item.info()} is too far to take" }
            return
        }
        if (entity.carriedItem() == item) {
            info(CARRY_LOG_TAG) { "item ${item.info()} is carried already by the ${entity.info()}" }
            return
        }
        if (item.carrier() != null) {
            info(CARRY_LOG_TAG) { "item ${item.info()} is carried already by another entity" }
            return
        }

        carry(entity, item)
    }

    private fun onEntityDropCarryItemCommand(entity: Entity, item: Entity, position: GridPoint2?) {
        if (entity.carriedItem() != item) {
            info(CARRY_LOG_TAG) { "item ${item.info()} is not carried by ${entity.info()}" }
            return
        }

        drop(entity, item, position)
    }

    private fun onEntityChangedPositionEvent(entity: Entity) {
        if (!entity.has(carrier)) return

        val carrierPosition = entity.position()
        val item = entity[carrier]!!.item ?: return

        send(ChangeEntityPositionCommand(item, carrierPosition))
    }

    private fun onEntityDestroyedEvent(entity: Entity) {
        entity[carrier]?.item?.let {
            drop(entity, it, entity.position())
        }

        entity[carriable]?.let {
            val carrier = it.carrier
            if (carrier != null) {
                drop(carrier, entity, carrier.position())
            }
        }
    }


    private fun carry(entity: Entity, item: Entity) {
        entity[carrier]!!.item = item

        item.setPosition(entity.position().cpy())
        item[carriable]!!.carrier = entity
        item[collision]?.active = false
    }

    private fun drop(entity: Entity, item: Entity, position: GridPoint2?) {
        entity[carrier]!!.item = null

        item[collision]?.active = true
        item[carriable]!!.carrier = null

        if (position != null) {
            send(ChangeEntityPositionCommand(item, position))
        }
    }
}
