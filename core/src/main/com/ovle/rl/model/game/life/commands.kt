package com.ovle.rl.model.game.life

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.GameCommand

class EntityStartSleepCommand(val entity: Entity) : GameCommand()
class EntityTakeDamageCommand(val entity: Entity, val source: Entity?, val amount: Int) : GameCommand()
class EntityHealCommand(val entity: Entity, val source: Entity?, val amount: Int) : GameCommand()
class KillEntityCommand(val entity: Entity) : GameCommand()
class ResurrectEntityCommand(val entity: Entity) : GameCommand()