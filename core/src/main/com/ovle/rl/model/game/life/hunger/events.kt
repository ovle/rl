package com.ovle.rl.model.game.life.hunger

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.util.info
import com.ovle.rl.model.util.markEntity


class EntityStarvedEvent(val entity: Entity) : GameEvent() {
    override val message: String
        get() = "${entity.info()} starved"
}

class EntityConsumedEvent(val entity: Entity, val food: Entity) : GameEvent() {
    override val playerAwareMessage: String
        get() {
            val entityStr = entity.info().markEntity()
            val foodStr = food.info().markEntity()
            return "$entityStr consumed $foodStr"
        }

    override val message: String
        get() = "${entity.info()} consumed ${food.info()}"
}