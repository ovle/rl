package com.ovle.rl.model.game.social

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.social.dto.FactionTemplate

class SocialComponent(
    val isKeeper: Boolean = false, //todo

    var name: String? = null,
    var faction: FactionTemplate? = null,
    var player: LocationPlayer? = null,
    val personalEnemies: MutableSet<Entity> = mutableSetOf()
) : EntityComponent() {

    override fun gameInfo() = GameInfoNode(
        "social: ", listOf(
            GameInfoNode("""faction: ${faction?.name ?: "no"}"""),
            GameInfoNode("""player: ${player ?: "no"}"""),
        )
    )
}