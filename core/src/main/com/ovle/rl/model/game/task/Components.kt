package com.ovle.rl.model.game.task

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.task.Components.taskPerformer
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.util.info
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get
import ktx.ashley.has

object Components {
    val taskPerformer = mapper<TaskPerformerComponent>()
}

fun Entity.currentTask(): TaskInfo? {
    check(has(taskPerformer)) { "no taskPerformer for entity ${info()}" }
    return this[taskPerformer]!!.current
}