package com.ovle.rl.model.game.spawn.portal

import com.ovle.rl.model.template.entity.EntityTemplate

data class PortalEntitySpawnTemplate(
    val entity: EntityTemplate,
    val check: PortalEntitySpawnCheck,
    val chancePerHour: (Int) -> Float
)
