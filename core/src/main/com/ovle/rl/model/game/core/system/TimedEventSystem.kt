package com.ovle.rl.model.game.core.system

import com.ovle.rl.AppOptions
import com.ovle.utils.event.EventBus.subscribe
import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemPriority
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.time.GameTimeChangedEvent
import com.ovle.utils.event.Priority
import ktx.log.debug
import kotlin.system.measureTimeMillis

/**
 * the system which tracks ticks changing and updates on given tick interval
 * decoupled from the real time with [GameTimeChangedEvent]
 */
abstract class TimedEventSystem: BaseSystem() {

    protected open val updateInterval: Tick = SystemUpdateInterval.BASE

    protected open val systemPriority: Priority = SystemPriority.DEFAULT

    protected var time: Tick = 0


    final override fun subscribe() {
        subscribe<GameTimeChangedEvent>(this, priority = systemPriority) {
            onTimeChangedEvent()
        }

        subscribeIntr()
    }

    private fun onTimeChangedEvent() {
        time ++

        val diff = time - updateInterval
        if (diff < 0) return

        if (AppOptions.IS_PROFILE_MODE) {
            val t = measureTimeMillis { updateTime() }
            if (t > 1) {
                debug { "${this.javaClass.simpleName}: updateTime = $t" }
            }
        } else {
            updateTime()
        }

        time = diff
    }

    open fun subscribeIntr() {}

    /**
     * update time for [updateInterval]
     */
    abstract fun updateTime()
}