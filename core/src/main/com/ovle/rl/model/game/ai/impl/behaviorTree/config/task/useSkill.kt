package com.ovle.rl.model.game.ai.impl.behaviorTree.config.task

import com.badlogic.gdx.ai.btree.Task.Status.*
import com.ovle.rl.model.game.ai.TaskExec
import com.ovle.rl.model.game.ai.task.impl.UseSkill
import com.ovle.rl.model.game.skill.UseSkillCommand
import com.ovle.rl.model.game.skill.helper.PositionHelper
import com.ovle.rl.model.game.skill.dto.SkillUsage
import com.ovle.rl.model.game.space.move.PassabilityHelper
import com.ovle.rl.model.game.space.position
import com.ovle.utils.event.EventBus.send


fun useSkillTask(task: UseSkill): TaskExec = { btParams ->
    val (owner, location, _) = btParams
    val (skill, target, isLoop) = task
    val range = skill.range

    val content = location.content
    val targetPositions = target.position()
    val positionHelper = PositionHelper(PassabilityHelper())
    val skillPositions = positionHelper.positionsToTarget(owner, range, targetPositions, content)

    fun isAccessible() = skill.isOwnerValid(owner, content) && skill.isTargetValid(target, content)
        && positionHelper.isTargetAccessible(owner, range, targetPositions, content)
    fun isAtSkillPosition() = skillPositions == null || owner.position() in skillPositions

    when {
        !isAccessible() -> FAILED
        !isAtSkillPosition() -> FAILED
        else -> {
            val usage = SkillUsage.Entity(skill, target, owner)
            send(UseSkillCommand(usage))

            if (isLoop) RUNNING else SUCCEEDED
        }
    }
}