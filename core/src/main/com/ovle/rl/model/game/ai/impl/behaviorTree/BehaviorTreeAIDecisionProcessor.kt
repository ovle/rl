package com.ovle.rl.model.game.ai.impl.behaviorTree

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.ai.btree.BehaviorTree
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.ai.AIParams
import com.ovle.rl.model.game.ai.AI_LOG_TAG
import com.ovle.rl.model.game.ai.Components.ai
import com.ovle.rl.model.game.ai.decision.DecisionProcessor
import com.ovle.rl.model.game.ai.impl.behaviorTree.config.mapTask
import com.ovle.rl.model.game.ai.impl.behaviorTree.dto.BTParams
import com.ovle.rl.model.game.ai.task.RootTask
import com.ovle.rl.model.util.info
import com.ovle.rl.model.util.location
import ktx.ashley.get
import ktx.log.info


class BehaviorTreeAIDecisionProcessor : DecisionProcessor {

    override fun process(entity: Entity, game: GameEntity) {
        val aiComponent = entity[ai]!!
        aiComponent.aiParams?.behaviorTree?.step()
    }

    override fun setTask(entity: Entity, game: GameEntity, rootTask: RootTask) {
        cancelBT(entity)

        val behaviorTree = newBehaviorTree(entity, rootTask, game)
        entity[ai]!!.aiParams = AIParams(behaviorTree)
    }

    override fun onEntityAIDisabled(entity: Entity) {
        cancelBT(entity)
    }


    private fun newBehaviorTree(entity: Entity, rootTask: RootTask, game: Entity): BehaviorTree<BTParams> {
        info(AI_LOG_TAG) { "new behavior tree created for ${entity.info()}, rootTask: ${rootTask.info()}" }

        val result: BehaviorTree<BTParams> = BehaviorTree(mapTask(rootTask.task))

        return result.apply {
                `object` = BTParams(entity, game.location(), rootTask)
                addListener(TaskStatusListener(this))
            }
    }

    private fun cancelBT(entity: Entity) {
        val aiComponent = entity[ai]!!
        aiComponent.aiParams?.behaviorTree?.cancel()
    }
}