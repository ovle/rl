package com.ovle.rl.model.game.schedule

import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.life.Components.daySchedule
import com.ovle.rl.model.game.time.globalTime
import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.withDaySchedule
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.gdx.math.hourBetween
import ktx.ashley.get

class DayScheduleSystem : TimedEventSystem() {

    override val updateInterval = SystemUpdateInterval.GAME_HOUR

    override fun updateTime() {
        val game = game()
        val location = game.location()
        val gameTime = game.globalTime().toGameTime()
        val content = location.content
        val entities = content.entities.all()
        val gameHour = gameTime.hour
        val scheduleEntities = entities.withDaySchedule()

        for (scheduleEntity in scheduleEntities) {
            val dsc = scheduleEntity[daySchedule]
                ?: continue

            val current = dsc.config.firstOrNull {
                gameHour.hourBetween(it.start, it.finish)
            }

            val previous = dsc.current
            dsc.current = current

            if (previous != current) {
                previous?.let {
                    send(EntityFinishedDayScheduleEvent(scheduleEntity, it))
                }
                current?.let {
                    send(EntityStartedDayScheduleEvent(scheduleEntity, it))
                }
            }
        }
    }
}