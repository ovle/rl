package com.ovle.rl.model.game.social.dto

data class FactionFactionRelation(
    val faction1: FactionTemplate,
    val faction2: FactionTemplate,
    val value: RelationValue
)