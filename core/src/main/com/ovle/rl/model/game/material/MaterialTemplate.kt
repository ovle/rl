package com.ovle.rl.model.game.material

class MaterialTemplate(
    val name: String,
    val grade: Int,
    val color: String? = null,
)