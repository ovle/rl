package com.ovle.rl.model.game.perception

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.isExists
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.EntityDestroyedEvent
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.life.EntityDiedEvent
import com.ovle.rl.model.game.life.EntityResurrectedEvent
import com.ovle.rl.model.game.life.isDead
import com.ovle.rl.model.game.perception.Components.perception
import com.ovle.rl.model.game.social.player
import com.ovle.rl.model.game.space.EntityChangedPositionEvent
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.game.tile.TileChangedEvent
import com.ovle.rl.model.game.tile.TileTemplate

import com.ovle.rl.model.util.location
import com.ovle.rl.model.util.perceptionOwners
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get


class PerceptionSystem(
    private val fovHelper: FOVHelper
) : BaseSystem() {

    override fun subscribe() {
        subscribe<EntityDiedEvent>(this) { onEntityDiedEvent(it.entity) }
        subscribe<EntityResurrectedEvent>(this) { onEntityResurrectedEvent(it.entity) }
        subscribe<EntityDestroyedEvent>(this) { onEntityDestroyedEvent(it.entity) }

        subscribe<EntityChangedPositionEvent>(this) { onEntityChangedPosition(it.entity) }
        subscribe<TileChangedEvent>(this) { onTileChangedEvent(it.oldTile, it.tile, it.position) }
    }

    private fun onEntityDiedEvent(entity: Entity) {
        updateFov(entity)
    }

    private fun onEntityResurrectedEvent(entity: Entity) {
        updateFov(entity)
    }

    private fun onEntityDestroyedEvent(entity: Entity) {
        updateFov(entity)
    }

    private fun onEntityChangedPosition(entity: Entity) {
        updateFov(entity)
    }

    private fun onTileChangedEvent(oldTile: TileTemplate, tile: TileTemplate, position: GridPoint2) {
        val isOldTilePassable = !oldTile.props.isOpaque
        val isTilePassable = !tile.props.isOpaque
        if (isOldTilePassable == isTilePassable) return

        val content = game().location().content
        val entities = content.entities.all().perceptionOwners()
            .filter { position in it.fov() }

        entities.forEach {
            updateFov(it)
        }
    }


    private fun updateFov(entity: Entity) {
        val perceptionComponent = entity[perception] ?: return

        val oldFov = perceptionComponent.fov
        val fov = fov(entity)
        perceptionComponent.fov = fov

        val pointsToAdd = fov - oldFov
        val pointsToRemove = oldFov - fov

        updateTileMasks(pointsToAdd, pointsToRemove, entity.player())
    }

    private fun updateTileMasks(
        pointsToAdd: Set<GridPoint2>, pointsToRemove: Set<GridPoint2>, player: LocationPlayer?
    ) {
        player ?: return

        val locationProjection = player.locationProjection
        val visibleMap = locationProjection.visibleMap
        val knownMap = locationProjection.knownMap

        pointsToAdd.forEach {
            visibleMap.incVisibility(it)
            knownMap.markAsKnown(it)
        }

        pointsToRemove.forEach {
            visibleMap.decVisibility(it)
        }
    }


    private fun fov(entity: Entity): Set<GridPoint2> {
        if (entity.isDead()) return setOf()
        if (!entity.isExists()) return setOf()

        val perceptionComponent = entity[perception]!!
        val tiles = game().location().content.tiles

        return fovHelper.fieldOfView(
            entity.position(),
            perceptionComponent.sightRadius,
            { !it.props.isOpaque },
            tiles,
            listOf() //no entity obstacles for now
        ).toSet()
    }
}