package com.ovle.rl.model.game

import com.ovle.rl.Tick
import com.ovle.rl.model.game.time.GAME_HOUR_TICKS


object SystemUpdateInterval {
    const val MIN: Tick = 1
    const val BASE: Tick = 60
    const val GAME_HOUR: Tick = GAME_HOUR_TICKS.toLong()

    const val AOE: Tick = 10 * MIN
    const val RENDER: Tick = MIN
    const val MOVE: Tick = MIN
    const val MOVE_CALC_PATH: Tick = BASE
    const val SPAWN: Tick = GAME_HOUR
    const val AGE: Tick = GAME_HOUR
    const val HUNGER: Tick = GAME_HOUR
    const val PORTAL: Tick = GAME_HOUR
}