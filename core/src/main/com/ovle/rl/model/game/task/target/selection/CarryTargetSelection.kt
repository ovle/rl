package com.ovle.rl.model.game.task.target.selection

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.task.target.TaskTargetCheck
import com.ovle.rl.model.game.task.target.TaskTargetCheck.*

data class CarryTargetSelection(
    var point: GridPoint2? = null,
    var entity: Entity? = null,
): TaskTargetSelection() {

    override fun updatePoints(selection: Collection<GridPoint2>) {
        if (entity != null) this.point = selection.single()
    }

    override fun updateEntities(entities: Collection<Entity>) {
        if (entities.isNotEmpty()) entity = entities.first()
    }

    //todo taskTargetFilter? seems not work for single-tile tasks (carry, scout)
    override fun check(): TaskTargetCheck = when {
        entity == null -> Error("select entity")
        point == null -> Error("select position")
        else -> Success
    }


    override fun taskTargets(): Collection<TaskTarget> =
        setOf(TaskTarget.Carry(point!!, entity!!))

    override fun copy() = CarryTargetSelection(point, entity)

    override fun reset() {
        point = null
        entity = null
    }
}