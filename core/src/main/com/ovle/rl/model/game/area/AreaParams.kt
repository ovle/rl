package com.ovle.rl.model.game.area

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.category.EntityCategoryTemplate
import com.ovle.rl.model.game.core.template
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.social.personalNameOrNull

sealed class AreaParams {

    data class House(
        val host: Entity? = null
    ): AreaParams() {
        override fun equalsByParams(p: AreaParams?) = p is House
            && p.host == host

        override fun info(area: AreaInfo, content: LocationContent): String = "host: ${host?.personalNameOrNull() ?: "anyone"}"
    }

    data class Storage(
        val categoryTemplate: EntityCategoryTemplate? = null
    ): AreaParams() {
        override fun equalsByParams(p: AreaParams?) = p is Storage
            && p.categoryTemplate == categoryTemplate

        override fun info(area: AreaInfo, content: LocationContent): String {
            val entityTemplates = categoryTemplate?.entities ?: emptyList()
            val areaEntities = content.entities.on(area.area)
            val storedEntities = areaEntities.filter { it.template() in entityTemplates }
            val name = categoryTemplate?.name

            return "store: " + when {
                categoryTemplate == null -> "nothing"
                else -> "$name X ${storedEntities.size}"
            }
        }
    }

    data class Farm(
        val template: FarmTemplate? = null,
    ): AreaParams() {
        override fun equalsByParams(p: AreaParams?) = p is Farm
            && p.template == template

        override fun info(area: AreaInfo, content: LocationContent): String = "plant: ${template?.name ?: "nothing"}"
    }


    var isActive = true

    abstract fun equalsByParams(p: AreaParams?): Boolean

    open fun info(area: AreaInfo, content: LocationContent): String { return "" }
}