package com.ovle.rl.model.game.task

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.util.info

class TaskPerformerComponent(
    var current: TaskInfo? = null
) : EntityComponent()