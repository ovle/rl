package com.ovle.rl.model.game.craft

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode


class CraftStationComponent : EntityComponent() {

    override fun gameInfo() = GameInfoNode("craft station:", listOf())
}