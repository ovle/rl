package com.ovle.rl.model.game.spawn

import com.ovle.rl.Tick
import com.ovle.rl.model.game.SystemUpdateInterval
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.game.CreateEntityCommand
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send


class TimedSpawnSystem(
    private val entityTemplates: Collection<EntityTemplate>,
    private val entitySpawnHelper: EntitySpawnHelper
) : TimedEventSystem() {

    override val updateInterval: Tick = SystemUpdateInterval.SPAWN


    override fun updateTime() {
        val location = game().location()
        val spawnEntities = entitySpawnHelper.spawnEntities(
            location, entityTemplates, isPerHourSpawn = true
        )

        spawnEntities.forEach {
            send(CreateEntityCommand(it))
        }
    }
}