package com.ovle.rl.model.game.life

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.life.Components.health
import com.ovle.rl.model.game.life.Components.hunger
import ktx.ashley.get
import ktx.ashley.has


fun Entity.isAlive() = has(health) && !this.isDead()

fun Entity.isDead() = has(health) && this[health]!!.isDead

fun Entity.isHungry() = has(hunger) && this[hunger]!!.isHungry

fun Entity.isFullHealth() = has(health) && this[health]!!
    .let { it.health == it.maxHealth }

fun Entity.isLowHealth(part: Float) = has(health)
    && this[health]!!.let { it.health < it.maxHealth * part }

