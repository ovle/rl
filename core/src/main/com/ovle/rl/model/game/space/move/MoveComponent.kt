package com.ovle.rl.model.game.space.move

import com.ovle.rl.Tick
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.game.space.move.impl.path.PathTemplate
import com.ovle.rl.model.game.time.DEFAULT_MOVE_TIME_TILE
import kotlin.math.roundToLong

class MoveComponent(
    val defaultSpeed: Double,
    val pathTemplate: PathTemplate? = null,

    var params: MoveParams? = null,
    var time: Tick = 0,
    var state: MoveState = MoveState.IDLE,

    var speed: Double = defaultSpeed,
    var type: MoveType = MoveType.DEFAULT
) : EntityComponent() {

    val moveStep: Tick
        get() = (DEFAULT_MOVE_TIME_TILE.toDouble() / speed).roundToLong()

    override fun gameInfo() = GameInfoNode(
        "move:", listOf(
            GameInfoNode("state: ${state.toString().lowercase()}"),
            GameInfoNode("speed: $speed"),
        )
    )
}