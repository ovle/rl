package com.ovle.rl.model.game.transform

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.template.entity.EntityTemplate

class EntityTransformedEvent(val entity: Entity, val target: EntityTemplate) : GameCommand()