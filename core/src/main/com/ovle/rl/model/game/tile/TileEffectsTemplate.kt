package com.ovle.rl.model.game.tile

import com.ovle.rl.EntityCheck
import com.ovle.rl.model.game.effect.StaticEffectTemplate
import com.ovle.rl.model.game.time.dto.TimerTemplate


class TileEffectsTemplate(
    val tiles: Collection<TileTemplate>,
    val entityCheck: EntityCheck = { true },
    val timerEffects: Collection<TimerTemplate> = listOf(),
    val staticEffects: Collection<StaticEffectTemplate> = listOf(),
)