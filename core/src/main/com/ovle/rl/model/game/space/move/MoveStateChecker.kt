package com.ovle.rl.model.game.space.move

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.name
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.accessibility.Accessibility.Companion.Key
import com.ovle.rl.model.game.space.move.MoveState.*
import com.ovle.rl.model.game.space.move.MoveStateHelper.Companion.GoalStatus.*
import com.ovle.rl.model.game.space.position
import ktx.ashley.get
import ktx.log.debug
import ktx.log.info

class MoveStateHelper {

    companion object {
        private enum class GoalStatus {
            NOT_CHANGED,
            CHANGED_STILL_IN_PATH,
            CHANGED_OUT_OF_PATH,
        }
    }

    fun moveState(entity: Entity, content: LocationContent): MoveState {
        val moveComponent = entity[move]!!
        val params = moveComponent.params
        val path = params?.path

        return when {
            params == null -> IDLE
            path == null -> when {
                params.dirty -> MOVING //path is yet to be calc
                else -> UNAVAILABLE //no path exists
            }
            params.isFixedPathMove -> {
                val isAtGoal = entity.position() == path.last
                if (isAtGoal) FINISHED else MOVING
            }
            path.path.isEmpty() -> FINISHED //already at any of the move goals
            else -> {
                val isAtGoal = entity.position() == path.last
                val isGoalAccessible = true //checked by AI, reset move if need  //entity.isAnyAccessible(setOf(goal))
                val accessibility = content.accessibility
                val isCurrentPathStepAccessible = if (path.currentTarget == null) false
                else {
                    accessibility.isAccessible(Key(entity), entity.position(), path.currentTarget!!)
                }

                val isAtPath = path.isAtCurrentTarget(entity.position())
                val goalStatus = goalStatus(params, entity, content)

                when {
                    isAtGoal -> FINISHED
                    !isGoalAccessible -> {
                        info(MOVE_LOG_TAG) { "goal not accessible: ${entity.name()}" }
                        UNAVAILABLE
                    }
                    !isCurrentPathStepAccessible || !isAtPath || (goalStatus == CHANGED_OUT_OF_PATH) -> {
                        debug(MOVE_LOG_TAG) { "dirty move ($isCurrentPathStepAccessible, $isAtPath, $goalStatus): ${entity.name()}" }
                        DIRTY
                    }
                    goalStatus == CHANGED_STILL_IN_PATH -> {
                        debug(MOVE_LOG_TAG) { "dirty move, goal still in path" }
                        DIRTY_MOVING
                    }
                    else -> MOVING
                }
            }
        }
    }

    private fun goalStatus(params: MoveParams, entity: Entity, content: LocationContent): GoalStatus {
        val task = params.task!!
        if (task.isFlee) return NOT_CHANGED //no particular 'goal' for flee

        val path = params.path!!
        val taskGoals = task.goals(entity, content).toSet()
        if (path.last in taskGoals) return NOT_CHANGED

        val isGoalInPath = path.left.intersect(taskGoals).isNotEmpty()
        return if (isGoalInPath) CHANGED_STILL_IN_PATH else CHANGED_OUT_OF_PATH
    }
}