package com.ovle.rl.model.game.core.component.template

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.template.entity.EntityTemplate

class TemplateComponent(
    var template: EntityTemplate
) : EntityComponent()