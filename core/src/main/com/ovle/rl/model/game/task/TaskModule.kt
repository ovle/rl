package com.ovle.rl.model.game.task

import com.badlogic.ashley.core.EntitySystem
import org.kodein.di.*


val taskModule = DI.Module("task") {
    bind<ValidTaskTargetsHelper>() with singleton {
        ValidTaskTargetsHelper()
    }
    bind<TaskResourceHelper>() with singleton {
        TaskResourceHelper()
    }
    bind<TaskCandidatesHelper>() with singleton {
        TaskCandidatesHelper(instance())
    }
    bind<EntitySystem>().inSet() with singleton {
        TaskSystem(instance(), instance())
    }
}