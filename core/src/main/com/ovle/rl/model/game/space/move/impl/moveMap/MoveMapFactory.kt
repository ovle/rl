package com.ovle.rl.model.game.space.move.impl.moveMap

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.space.accessibility.Accessibility.Companion.Key
import com.ovle.rl.model.game.space.move.MOVE_LOG_TAG
import com.ovle.rl.model.game.space.position
import com.ovle.rl.model.util.info
import com.ovle.utils.DijkstraMapValue
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.localMins
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMap
import com.ovle.utils.gdx.math.dijkstraMap.DijkstraMapFactory
import com.ovle.utils.gdx.math.dijkstraMap.scanGoals
import ktx.log.debug
import ktx.log.info

/**
 * factory for move maps
 */
class MoveMapFactory(
    private val dijkstraMapFactory: DijkstraMapFactory,
) {
    companion object {
        private const val DEFAULT_FLEE_MAP_MULTIPLIER: Float = -1.2f
    }

    fun moveMap(
        entity: Entity,
        goals: Collection<GridPoint2>,
        location: Location,
        forceAccessiblePoints: Collection<GridPoint2>
    ): DijkstraMap {
        debug(MOVE_LOG_TAG) { "calc moveMap for ${entity.info()}, goals: ${goals.info()}" }

        return dijkstraMap(entity, goals, location, forceAccessiblePoints)
    }

    fun fleeMap(
        entity: Entity,
        goals: Collection<GridPoint2>,
        location: Location,
        forceAccessiblePoints: Collection<GridPoint2>
    ): DijkstraMap {
        debug(MOVE_LOG_TAG) { "calc fleeMap for ${entity.info()}, from goals: ${goals.info()}" }

        val map = dijkstraMap(
            entity, goals, location, forceAccessiblePoints + goals
        )

        return fleeMap(map)
    }


    private fun dijkstraMap(
        entity: Entity,
        goals: Collection<GridPoint2>,
        location: Location,
        forceAccessiblePoints: Collection<GridPoint2>
    ): DijkstraMap {
        val content = location.content
        val tiles = content.tiles
        val size = tiles.size

        if (goals.isEmpty()) {
            info { "no accessible goals for dijkstraMap" }
            val emptyMap = Array2d(Array(size * size) { DijkstraMap.UNINITIALIZED_MAP_VALUE }, size)
            return DijkstraMap(goals, emptyMap)
        }

        val accessibility = content.accessibility
        val points = tiles.points
        val key = Key(entity)
        val accessiblePoints = points.filter {
            accessibility.isAccessible(key, entity.position(), it)
        } + forceAccessiblePoints
        val inaccessiblePoints = tiles.points - accessiblePoints.toSet()

        return dijkstraMapFactory.dijkstraMap(size, 1, goals, inaccessiblePoints)
    }

    /**
     * creates flee-from-goals map
     * inverse the [initialMap] using given [multiplier], then scan, using local minimums as new goals
     *
     * @param initialMap    map to work with. we'll flee from its goals
     * @param multiplier    the more(the less if negative) the value - the more algorithm tends to move
     *      towards the global min instead of the local ones
     *
     * @return flee map
     */
    private fun fleeMap(initialMap: DijkstraMap, multiplier: Float = DEFAULT_FLEE_MAP_MULTIPLIER): DijkstraMap {
        val map = initialMap.data

        val multipliedData = map.data
            .map { if (it == DijkstraMap.UNINITIALIZED_MAP_VALUE) DijkstraMap.UNINITIALIZED_MAP_VALUE else it * multiplier }
            .toTypedArray()
        val multipliedMap = Array2d(multipliedData, map.size)
        /*
            todo it's unclear what to do next
            desired behavior - will try to move to the global min,
                  BUT may decide to move to the local ones instead, if road to the global one is unoptimal

            no second scan - will stuck in the corners, and has no meaningful movement at all, as gradient will be danger-centric
            second scan - local mins - may stuck in the corners, as almost every corner is a local min
            second scan - global min - will lead to the weird movement if we have to travel through the danger to reach safe zone
            second scan - non-goal-based scan - ???
        */

        val localMins = localMins(multipliedMap)
        val globalMin = localMins.minByOrNull { it.value }!!
        val globalMins = mapOf(globalMin.toPair())

        val fleeGoalsToValues = globalMins.toMutableMap()

        val inaccessiblePoints = map.points.filter { map.isAccessible(it) } - fleeGoalsToValues.keys
        val fleeMap = scanGoals(fleeGoalsToValues, inaccessiblePoints, multipliedMap.size)
        val fleeLocalMins = localMins(fleeMap)

        return DijkstraMap(fleeLocalMins.keys, fleeMap)
    }


    private fun localMins(map: Array2d<DijkstraMapValue>) =
        map.localMins { map[it.x, it.y] != DijkstraMap.UNINITIALIZED_MAP_VALUE }

    private fun Array2d<DijkstraMapValue>.isAccessible(point: GridPoint2) = isValid(point)
        && this[point.x, point.y] != DijkstraMap.UNINITIALIZED_MAP_VALUE
}