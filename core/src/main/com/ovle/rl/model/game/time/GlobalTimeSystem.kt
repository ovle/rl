package com.ovle.rl.model.game.time

import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.time.dto.GlobalTimeInfo

import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe

/**
 * the system which quantize time
 * supports pause / game speed change
 */
class GlobalTimeSystem : BaseSystem() {

    override fun subscribe() {
        subscribe<ChangeGameSpeedCommand>(this) { onChangeGameSpeedCommand(it.multiplier) }
        subscribe<ToggleGamePauseCommand>(this) { onSwitchPauseGameCommand() }
    }

    private fun onChangeGameSpeedCommand(multiplier: Double) {
        val time = timeInfo()
        changeGameSpeed(time, multiplier)
    }

    private fun onSwitchPauseGameCommand() {
        val time = timeInfo()
        time.paused = !time.paused
    }

    private fun changeGameSpeed(globalTime: GlobalTimeInfo, multiplier: Double) {
        globalTime.gameSpeed *= multiplier
        send(GameSpeedChangedEvent(globalTime.gameSpeed))
    }


    override fun update(deltaTime: Float) {
        send(AbsoluteTimeChangedEvent)

        val time = timeInfo()
        if (time.paused) return

        val scaledDeltaTime = deltaTime * time.gameSpeed

        val oldTick = time.tick
        time.time += scaledDeltaTime
        val newTick = time.tick

//        if (oldTurn != newTurn) {
//            info(TIME_LOG_TAG) {"---------------------- turn $newTurn ----------------------" }
//        }

        //support for game speed
        //for the high speed update with the large delta will be bad if had to rely on other systems
        //which didn't process yet their big delta
        val deltaTicks = newTick - oldTick
        repeat(deltaTicks.toInt()) {
            send(GameTimeChangedEvent)
        }
    }

    private fun timeInfo() = game().globalTime()
}