package com.ovle.rl.model.game.space.body

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.EntityLeavedLocationEvent
import com.ovle.rl.model.game.game.EntityLoadedEvent
import com.ovle.rl.model.game.space.*
import com.ovle.rl.model.game.space.move.PassabilityHelper

import com.ovle.rl.model.util.info
import com.ovle.rl.model.util.location
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.log.info

/**
 * System for changing entity position (checking passability first)
 * 'passability' is about if we can place object at target position
 * [ChangeEntityPositionCommand] supposed to be the only way to do that
 *
 * the only special case is entity leaving the location
 */
class BodySystem(
    private val passabilityHelper: PassabilityHelper
) : BaseSystem() {

    override fun subscribe() {
        subscribe<EntityLoadedEvent>(this) { onEntityLoadedEvent(it.entity, it.position) }
        subscribe<ChangeEntityPositionCommand>(this) { onChangeEntityPositionCommand(it.entity, it.position) }
    }

    private fun onChangeEntityPositionCommand(entity: Entity, position: GridPoint2) {
        setPosition(entity, position)
    }

    //todo what if loaded entity isn't on passable position?
    private fun onEntityLoadedEvent(entity: Entity, position: GridPoint2) {
        setPosition(entity, position)
    }


    private fun setPosition(entity: Entity, newPosition: GridPoint2) {
        val location = game().location()
        val content = location.content
        val tiles = content.tiles

        if (!tiles.isValid(newPosition)) {
            info(SPACE_LOG_TAG) { "entity ${entity.info()} is out of location!" }
            send(EntityLeavedLocationEvent(entity, newPosition))
            return
        }

        if (!passabilityHelper.isPassable(newPosition, entity, content)) {
            info(SPACE_LOG_TAG) { "point $newPosition aren't passable for entity ${entity.info()}!" }
            return
        }

        val oldPosition = entity.position()
        entity.setPosition(newPosition.cpy())

        content.entities.updatePosition(entity, oldPosition, newPosition)

        send(EntityChangedPositionEvent(entity, oldPosition))
    }
}