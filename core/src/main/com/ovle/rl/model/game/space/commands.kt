package com.ovle.rl.model.game.space

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.core.GameCommand
import com.ovle.rl.model.game.space.move.MoveParams

class ChangeEntityPositionCommand(val entity: Entity, val position: GridPoint2) : GameCommand()
class EntityStartMovementCommand(val entity: Entity, val params: MoveParams) : GameCommand()
class EntityStopMovementCommand(val entity: Entity) : GameCommand()