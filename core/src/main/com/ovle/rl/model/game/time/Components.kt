package com.ovle.rl.model.game.time

import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.time.Components.globalTime
import com.ovle.rl.model.game.time.dto.GlobalTimeInfo
import com.ovle.rl.model.util.info
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get
import ktx.ashley.has

object Components {
    val globalTime = mapper<GlobalTimeComponent>()
    val timer = mapper<TimerComponent>()
}

fun GameEntity.globalTime(): GlobalTimeInfo {
    check(has(globalTime)) { "no globalTime for entity ${info()}" }
    return this[globalTime]!!.time
}