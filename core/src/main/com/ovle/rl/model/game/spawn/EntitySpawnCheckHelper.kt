package com.ovle.rl.model.game.spawn

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.template.entity.SpawnTemplate
import com.ovle.utils.gdx.math.discretization.bresenham.filledCircle
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2


class EntitySpawnCheckHelper {

    fun isCanSpawn(
        template: SpawnTemplate, tiles: TileArray, point: GridPoint2
    ): Boolean {
        val (x, y) = point
        val tile = tiles[x, y]

        val tileCheck = tile == TileTemplate.WHATEVER || template.tileCheck(tile)
        val nearTiles = template.nearTiles
        val nearTilesCheck = nearTiles.isEmpty() ||
            checkArea(point, template, tiles).any { tiles[it.x, it.y] in nearTiles }

        return tileCheck && nearTilesCheck
    }


    private fun checkArea(
        point: GridPoint2, template: SpawnTemplate, tiles: TileArray
    ) = filledCircle(point, template.nearRadius)
        .filter { tiles.isValid(it) }
}