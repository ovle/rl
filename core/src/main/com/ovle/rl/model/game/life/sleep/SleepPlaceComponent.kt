package com.ovle.rl.model.game.life.sleep

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode


class SleepPlaceComponent: EntityComponent() {
    var sleeper: Entity? = null

    override fun gameInfo() = GameInfoNode(
        "sleep place:"
    )
}