package com.ovle.rl.model.game.space.accessibility

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.social.player
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.game.space.moveType

class Accessibility {

    companion object {

        data class Key(
            val moveType: MoveType,
            val player: LocationPlayer?, //todo shouldn't be here, fow shouldn't be part of acc
        ) {
            constructor(entity: Entity): this(entity.moveType(), entity.player())
        }
    }

    val maps: MutableMap<Key, AccessibilityMap> = mutableMapOf()


    fun isAccessible(key: Key, from: GridPoint2, target: GridPoint2): Boolean {
        val map = maps[key]!!
        if (!map.map!!.isValid(target)) return false

        return map.isAccessible(from, target)
    }
}