package com.ovle.rl.model.game.skill

import com.ovle.rl.model.game.core.EntityGameEvent
import com.ovle.rl.model.game.skill.dto.SkillUsage
import com.ovle.rl.model.util.info
import com.ovle.rl.model.util.markEntity
import com.ovle.rl.model.util.markSkill


class EntityStartUseSkillEvent(val skillUsage: SkillUsage.Entity) : EntityGameEvent(skillUsage.source) {
    override val message: String
        get() {
            val skill = skillUsage.skill
            val target = skillUsage.target
            val source = skillUsage.source
            return "${source.info()} started to use skill ${skill.name} on ${target.info()}"
        }
}

class EntityFinishUseSkillEvent(val skillUsage: SkillUsage.Entity) : EntityGameEvent(skillUsage.source) {
    override val playerAwareMessage: String
        get() {
            val skill = skillUsage.skill
            val target = skillUsage.target
            val source = skillUsage.source
            return "${source.info().markEntity()} used skill ${skill.name.markSkill()} on ${target.info().markEntity()}"
        }

    override val message: String
        get() {
            val skill = skillUsage.skill
            val target = skillUsage.target
            val source = skillUsage.source
            return "${source.info()} finished to use skill ${skill.name} on ${target.info()}"
        }
}