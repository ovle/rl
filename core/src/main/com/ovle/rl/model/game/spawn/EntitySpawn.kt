package com.ovle.rl.model.game.spawn

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.EntityId
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.material.MaterialTemplate
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.util.randomId

data class EntitySpawn(
    val template: EntityTemplate,
    val id: EntityId = randomId(),
    val source: Entity? = null,

    var position: GridPoint2,
    var material: MaterialTemplate? = null,

    val init: (Entity, Location) -> Unit = { _, _ -> }
)