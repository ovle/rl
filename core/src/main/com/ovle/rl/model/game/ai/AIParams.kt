package com.ovle.rl.model.game.ai

import com.badlogic.gdx.ai.btree.BehaviorTree
import com.ovle.rl.model.game.ai.impl.behaviorTree.dto.BTParams

class AIParams(val behaviorTree: BehaviorTree<BTParams>? = null)