package com.ovle.rl.model.game.effect

import com.ovle.utils.gdx.ashley.component.mapper

object Components {
    val staticEffect = mapper<StaticEffectComponent>()
}