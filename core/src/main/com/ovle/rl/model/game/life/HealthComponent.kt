package com.ovle.rl.model.game.life

import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode


class HealthComponent(
    var maxHealth: Int = 3,
) : EntityComponent() {
    var health: Int = maxHealth

    val isDead
        get() = health == 0

    override fun gameInfo() = GameInfoNode(
        "health:", listOf(
            GameInfoNode("health: ${health}/${maxHealth}"),
        )
    )
}