package com.ovle.rl.model.game.core

import com.badlogic.ashley.core.Entity
import com.ovle.utils.event.Event

/**
 * an event (smth has been done) related to the game logic
 * usually broadcasted, as there can be any number of processors
 */
abstract class GameEvent : Event() {
    open val message: String? = null
    open val playerAwareMessage: String? = null

    var timestamp: Double? = null

    /**
     * if an event is subject to the game log and observable by the player
     */
    val isPlayerAware: Boolean
        get() = playerAwareMessage != null
}

abstract class EntityGameEvent(val subject: Entity) : GameEvent()