package com.ovle.rl.model.game.factory

import com.ovle.utils.gdx.ashley.component.mapper

object Components {
    val factory = mapper<FactoryComponent>()
}