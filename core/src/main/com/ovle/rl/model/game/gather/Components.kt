package com.ovle.rl.model.game.gather

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.gather.Components.source
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get


object Components {
    val source = mapper<SourceComponent>()
}

fun Entity.yields(et: EntityTemplate): Boolean {
    return this[source]?.info?.any { s -> s.yield == et }
        ?: false
}