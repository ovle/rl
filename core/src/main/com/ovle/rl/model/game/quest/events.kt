package com.ovle.rl.model.game.quest

import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.quest.dto.QuestInfo


class QuestStatusUpdatedEvent(val quest: QuestInfo) : GameEvent()
