package com.ovle.rl.model.game.skill

import com.badlogic.ashley.core.Entity
import com.ovle.rl.Tick
import com.ovle.rl.model.game.core.system.TimedEventSystem
import com.ovle.rl.model.game.life.isDead
import com.ovle.rl.model.game.skill.Components.skill
import com.ovle.rl.model.game.skill.dto.SkillUsage
import com.ovle.rl.model.util.*
import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.ashley.get
import ktx.log.info


class SkillSystem : TimedEventSystem() {

    override fun updateTime() {
        val location = game().location()
        val entities = location.content.entities.all().skillOwners()
        entities.forEach { processEntity(it, updateInterval) }
    }


    private fun processEntity(entity: Entity, updateInterval: Tick) {
        val skillComponent = entity[skill]!!
        val current = skillComponent.current ?: return

        skillComponent.ticksLeft -= updateInterval

        if (skillComponent.ticksLeft <= 0) {
            finishUseSkill(current)
        }
    }


    override fun subscribeIntr() {
        subscribe<UseSkillCommand>(this) { onUseSkillCommand(it.skillUsage) }
    }

    private fun onUseSkillCommand(skillUsage: SkillUsage) {
        when (skillUsage) {
            is SkillUsage.Entity -> onEntitySkillCommand(skillUsage)
            is SkillUsage.Player -> onPlayerSkillCommand(skillUsage)
        }
    }


    private fun onEntitySkillCommand(skillUsage: SkillUsage.Entity) {
        val source = skillUsage.source
        if (!source.hasSkill(skillUsage.skill)) {
            info { "entity ${source.info()} has no skill ${skillUsage.skill.info()}" }
            return
        }

        val skillComponent = source[skill]!!
        if (skillComponent.current != null) {
            return
        }

        startUseSkill(skillUsage)
    }

    //todo
    private fun onPlayerSkillCommand(skillUsage: SkillUsage.Player) {
        /*
        val playerSkill = skillUsage.playerSkill
        val target = skillUsage.target
        val location = game().location()
        val keeper = location.content.entities.all()
            .ofPlayer(skillUsage.source)
            .keepers()
            .singleOrNull() ?: return
        if (keeper.isDead()) return

        val isCostPaid = playerSkill.cost
            .map { it.pay(location.players.human, location) }.all { it }
        if (!isCostPaid) {
            return
        }

        val keeperSkillUsage = SkillUsage.Entity(playerSkill.skill, target, keeper)
        startUseSkill(keeperSkillUsage)
         */
    }

    private fun startUseSkill(skillUsage: SkillUsage.Entity) {
        val skill = skillUsage.skill
        val source = skillUsage.source

        val skillComponent = source[Components.skill]!!
        skillComponent.current = skillUsage
        skillComponent.ticksLeft = skill.ticks

        send(EntityStartUseSkillEvent(skillUsage))
    }

    private fun finishUseSkill(skillUsage: SkillUsage.Entity) {
        val skill = skillUsage.skill
        val target = skillUsage.target
        val source = skillUsage.source

        //val amount = skill.effectAmount.invoke(params)
        skill.effects.forEach { (effect, getParams) ->
            val effectParams = getParams.invoke(source, target)
            effect.invoke(effectParams)
        }

        val skillComponent = source[Components.skill]!!
        skillComponent.current = null

        send(EntityFinishUseSkillEvent(skillUsage))
    }
}