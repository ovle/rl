package com.ovle.rl.model.game.space

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.space.Components.aoe
import com.ovle.rl.model.game.space.Components.body
import com.ovle.rl.model.game.space.Components.move
import com.ovle.rl.model.game.space.aoe.AOEComponent
import com.ovle.rl.model.game.space.body.BodyComponent
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.move.MoveType
import com.ovle.rl.model.util.info
import com.ovle.utils.gdx.ashley.component.mapper
import ktx.ashley.get
import ktx.ashley.has

object Components {
    val body = mapper<BodyComponent>()
    val aoe = mapper<AOEComponent>()
    val move = mapper<MoveComponent>()
}


fun Entity.positionOrNull(): GridPoint2? {
    return this[body]?.position
}

fun Entity.position(): GridPoint2 {
    check(has(body)) { "no position for entity ${info()}" }
    return this[body]!!.position
}

fun Entity.setPosition(newPosition: GridPoint2) {
    check(has(body)) { "no body for entity ${info()}" }
    this[body]!!.position = newPosition
}

fun Entity.moveType(): MoveType {
    return this[move]?.type ?: MoveType.DEFAULT
}

fun Entity.movePath() = this[move]?.params?.path

fun Entity.aoeOrNull(): Collection<GridPoint2>? {
    if (!has(aoe)) return null

    val position = this[body]!!.position
    //todo cache
    return this[aoe]!!.gridPositions.map { it.cpy().add(position) }.toMutableSet()
}

fun Entity.setAoe(newAoe: Collection<GridPoint2>) {
    check(has(aoe)) { "no aoe for entity ${info()}" }
    val positions = this[aoe]!!.gridPositions
    positions.clear()
    positions.addAll(newAoe)
}