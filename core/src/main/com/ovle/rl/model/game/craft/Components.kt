package com.ovle.rl.model.game.craft

import com.ovle.utils.gdx.ashley.component.mapper


object Components {
    val craftStation = mapper<CraftStationComponent>()
}