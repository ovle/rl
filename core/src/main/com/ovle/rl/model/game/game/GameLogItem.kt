package com.ovle.rl.model.game.game

class GameLogItem(private val tick: Long, val message: String) {
    override fun toString() = "[$tick] $message"
}
