package com.ovle.rl.model.game.ai.impl.behaviorTree.config.task

import com.badlogic.gdx.ai.btree.Task.Status.SUCCEEDED
import com.ovle.rl.model.game.ai.TaskExec
import com.ovle.rl.model.game.ai.task.impl.Sleep
import com.ovle.rl.model.game.life.EntityStartSleepCommand
import com.ovle.utils.event.EventBus.send

fun sleep(task: Sleep): TaskExec = { btParams ->
    val owner = btParams.owner
    send(EntityStartSleepCommand(owner))

    SUCCEEDED
}