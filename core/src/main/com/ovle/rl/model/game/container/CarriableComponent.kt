package com.ovle.rl.model.game.container

import com.badlogic.ashley.core.Entity
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GameInfoNode
import com.ovle.rl.model.game.core.name

class CarriableComponent(
    var carrier: Entity? = null
) : EntityComponent() {

    override fun gameInfo() = GameInfoNode(
        "carriable:", listOf(
            GameInfoNode("carrier: ${carrier?.name().orEmpty()}"
            )
        )
    )
}