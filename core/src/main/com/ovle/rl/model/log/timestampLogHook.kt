package com.ovle.rl.model.log

import com.ovle.rl.GetGame
import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.time.Components.globalTime
import com.ovle.rl.model.game.time.globalTime
import com.ovle.utils.event.Event
import ktx.ashley.get


fun timestampLogHook(event: Event, getGame: GetGame) {
    if (event !is GameEvent) return

    val globalTime = getGame().globalTime()
    event.timestamp = globalTime.time
}