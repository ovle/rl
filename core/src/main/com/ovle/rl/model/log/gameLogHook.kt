package com.ovle.rl.model.log

import com.ovle.rl.model.game.core.GameEvent
import com.ovle.rl.model.game.game.GameLogItem
import com.ovle.rl.model.game.time.timeToTicks
import com.ovle.utils.event.Event

private const val GAME_LOG_SIZE = 50

fun gameLogHook(event: Event, gameLog: MutableList<GameLogItem>) {
    if (event !is GameEvent) return
    if (!event.isPlayerAware) return

    val tick = event.timestamp?.let { timeToTicks(it) } ?: 0
    val message = event.playerAwareMessage!!
    gameLog.add(GameLogItem(tick, message))

    if (gameLog.size > GAME_LOG_SIZE) {
        gameLog.removeAt(0)
    }
}