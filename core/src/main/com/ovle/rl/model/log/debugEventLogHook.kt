package com.ovle.rl.model.log

import com.ovle.rl.model.game.core.GameEvent
import com.ovle.utils.event.Event
import ktx.log.info


fun debugEventLogHook(event: Event) {
    if (event !is GameEvent) return

    val message = event.message ?: return

    info("EVENT") { message }

//  "(${event::class.simpleName}) $m"  todo separate logs, files
}