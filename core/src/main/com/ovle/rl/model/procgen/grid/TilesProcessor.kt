package com.ovle.rl.model.procgen.grid

import com.ovle.rl.TileArray
import com.ovle.utils.RandomParams


interface TilesProcessor {

    fun process(tiles: TileArray, random: RandomParams)
}