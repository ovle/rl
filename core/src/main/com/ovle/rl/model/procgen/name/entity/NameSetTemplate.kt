package com.ovle.rl.model.procgen.name.entity

import com.valkryst.VNameGenerator.generator.MarkovGenerator

class NameSetTemplate(
    val maxLength: Int,
    val generator: MarkovGenerator,
)