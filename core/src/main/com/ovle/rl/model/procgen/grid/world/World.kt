package com.ovle.rl.model.procgen.grid.world

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.WorldId
import com.ovle.rl.model.procgen.grid.Grids
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.adjHV

data class World(
    val id: WorldId,
    var name: String,
    val random: RandomParams,
    val grids: Grids,
    val tiles: TileArray? = null,

    var regions: Collection<WorldRegion> = listOf(),
    var places: Collection<WorldPlace> = listOf()
) {

    fun region(point: GridPoint2?) = regions.find { point in it.points }

    //todo several at some point
    fun place(point: GridPoint2?) = places.firstOrNull { point == it.point }
}