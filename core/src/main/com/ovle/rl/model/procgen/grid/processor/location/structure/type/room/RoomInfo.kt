package com.ovle.rl.model.procgen.grid.processor.location.structure.type.room

data class RoomInfo(
    val x: Int, val y: Int, val width: Int, val height: Int
)