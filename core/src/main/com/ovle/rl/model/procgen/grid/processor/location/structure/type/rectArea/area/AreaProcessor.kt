package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.rect.RectArea

interface AreaProcessor {

    fun weight(area: RectArea, layoutValue: Float): Float

    fun process(area: RectArea, layoutValue: Float, random: RandomParams): AreaSpawn
}