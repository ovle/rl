package com.ovle.rl.model.procgen.grid.processor.location

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.procgen.grid.LocationProcessor
import ktx.log.info


class NopLocationProcessor(
    private val params: Params
): LocationProcessor {

    companion object {
        class Params()
    }


    override fun process(location: Location): Collection<EntitySpawn> {
        info { "process location at: ${location.worldPoint}" }
        return emptyList()
    }
}