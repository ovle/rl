package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.TileType
import com.ovle.utils.gdx.math.rect.RectArea

data class AreaSpawn(
    val area: RectArea,
    val parts: Collection<AreaPartSpawn>,
    val pointsByTile: Map<TileType, Collection<GridPoint2>>,
    val entitySpawns: Collection<EntitySpawn> = emptyList()
)