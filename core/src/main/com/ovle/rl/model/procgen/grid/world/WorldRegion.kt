package com.ovle.rl.model.procgen.grid.world

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.tile.TileTemplate

data class WorldRegion(
    val mainTileTemplate: TileTemplate,
    val points: Collection<GridPoint2>
) {
    lateinit var name: String
}