package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout

import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.rect.RectArea

interface RectAreaLayoutMapFactory {
    //todo less specific parameters
    fun layoutMap(
        areas: Collection<RectArea>, size: Int, sizeCoef: Float, random: RandomParams
    ): Array2d<Float>
}