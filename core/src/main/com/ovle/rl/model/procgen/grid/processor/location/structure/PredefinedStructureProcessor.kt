package com.ovle.rl.model.procgen.grid.processor.location.structure

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.social.Components.social
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.game.tile.TileTemplate.Companion.WHATEVER
import com.ovle.rl.model.procgen.grid.LocationProcessor
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.template.structure.PredefinedStructureTemplate
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.array2d.ArrayValidationHelper
import com.ovle.utils.gdx.math.point.component1
import com.ovle.utils.gdx.math.point.component2
import com.ovle.utils.gdx.math.point.point
import ktx.ashley.get
import ktx.log.error
import ktx.log.info

class PredefinedStructureProcessor(
    private val templates: Collection<PredefinedStructureTemplate>,
    private val tileTemplates: Collection<TileTemplate>,
    private val arrayValidationHelper: ArrayValidationHelper
) : LocationProcessor {

    override fun process(location: Location): Collection<EntitySpawn> {
        println("location seed: ${location.random.seed}")

        val random = location.random
        val tiles = location.content.tiles
        val entities = mutableListOf<EntitySpawn>()
        templates.forEach {
            entities += processTemplate(it, tiles, random)
        }

        return entities
    }

    private fun processTemplate(
        template: PredefinedStructureTemplate, tiles: TileArray, random: RandomParams
    ): Collection<EntitySpawn> {
        val mask = template.parsedMask

        val check = random.kRandom.nextDouble()
        val chance = 1.0
        val needSpawn = check <= chance
        if (!needSpawn) return emptyList()

        //spawn point is the left TOP corner of mask
        val structureSize = mask.size
        val validArea = arrayValidationHelper.validAreas(
            array = tiles,
            width = structureSize,
            height = structureSize,
            filter = template.tileFilter
        ).randomOrNull(random.kRandom)

        if (validArea == null) {
            error { "spawn failed: can't get validArea" }
            return emptyList()
        }

        val spawnPoint = point(validArea.x1, validArea.y2)
        info { "validArea: $validArea" }
        info { "spawnPoint: $spawnPoint" }

        spawnStructure(mask, tiles, spawnPoint)

        val structureEntities = template.entities
        //entities spawn point is related to the left bottom corner of mask
        val baseEntitiesSpawnPoint = point(spawnPoint.x, spawnPoint.y - structureSize + 1)

        val spawns = structureEntities.map { se ->
            val position = baseEntitiesSpawnPoint.cpy().add(se.structurePosition)

            EntitySpawn(
                template = se.template,
                position = position
            ) { e, l ->
                e[social]?.let { sc ->
                    sc.faction = se.faction
                    sc.player = l.players.byFaction[se.faction]
                }
            }
        }
        return spawns
    }

    private fun spawnStructure(mask: List<List<Char>>, tiles: TileArray, spawnPoint: GridPoint2): Set<GridPoint2> {
        val (x, y) = spawnPoint
        val result = mutableSetOf<GridPoint2>()

        mask.forEachIndexed { i, _ ->
            mask[i].forEachIndexed { j, _ ->
                val tileChar = mask[i][j]
                if (tileChar != WHATEVER.char) {
                    val resultX = x + j
                    val resultY = y - i

                    tiles[resultX, resultY] = tile(tileChar)
                    result.add(point(resultX, resultY))
                }
            }
        }
        return result
    }

    private fun tile(char: Char): TileTemplate {
        val result = tileTemplates.find { it.char == char }
        checkNotNull(result) { "no tileTemplate for '$char' char " }
        return result
    }
}