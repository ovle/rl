package com.ovle.rl.model.procgen.grid.world

import com.ovle.rl.TileCheck1
import com.ovle.rl.model.procgen.grid.LocationProcessor
import com.ovle.rl.model.template.NamedTemplate

class WorldPlaceTemplate(
    override val name: String,
    val worldTileCheck: TileCheck1,
    val locationProcessor: LocationProcessor
) : NamedTemplate {
}