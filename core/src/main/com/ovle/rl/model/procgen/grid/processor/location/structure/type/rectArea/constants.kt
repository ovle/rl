package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea

//adapted to sizeCoef
const val LAYOUT_LEVEL_HIGH = 0.5f //0.75f
const val LAYOUT_LEVEL_LOW = 0.2f  //0.45f

const val LAYOUT_LEVEL_AVG = (LAYOUT_LEVEL_HIGH + LAYOUT_LEVEL_LOW) / 2.0f