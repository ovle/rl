package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.ovle.utils.gdx.math.rect.RectArea

data class AreaTemplateRoom(
    val area: RectArea,
    val entitiesCount: IntRange,
    val purposes: Collection<AreaPartPurpose>
)