package com.ovle.rl.model.procgen.grid.processor.location

import com.badlogic.gdx.math.GridPoint2
import com.github.czyzby.noise4j.map.Grid
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.Grids
import com.ovle.rl.model.procgen.grid.LocationProcessor
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.utils.noise4j.grid.factory.path.impl.GradientPathFactory


class GradientPathLocationProcessor(
    private val params: Params
): LocationProcessor {

    companion object {
        data class Params(
            val newTile: TileTemplate,
            val filter: (Grids, GridPoint2) -> Boolean,
            val getGrid: (Grids) -> Grid,
            val factoryParams: GradientPathFactory.Companion.Params
        )
    }

    override fun process(location: Location): Collection<EntitySpawn> {
        val grids = location.content.grids
        val targetGrid = params.getGrid(grids)
        val factory = GradientPathFactory(params.factoryParams)
        val result = factory.create(targetGrid, location.random)

        val tiles = location.content.tiles
        val points = result.paths.flatten()
            .filter { params.filter(grids, it) }

        points.forEach {
            tiles[it.x, it.y] = params.newTile
        }

        return emptyList()
    }
}