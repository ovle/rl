package com.ovle.rl.model.procgen.grid

import com.badlogic.gdx.math.GridPoint2
import com.github.czyzby.noise4j.map.Grid
import com.ovle.rl.TileArray
import com.ovle.rl.model.procgen.config.TileMapper

class Grids(
    val grids: Array<Grid>,
) {
    init {
        check(grids.isNotEmpty())
        val sample = grids[0]
        val w = sample.width
        val h = sample.height
        check(w == h)
        check(grids.all { it.width == w && it.height == h })
    }

    fun value(p: GridPoint2) = GridsValue(
        grids.map { it.get(p.x, p.y) }.toTypedArray()
    )

    fun toTileArray(tileMapper: TileMapper): TileArray {
        val size = grids[0].width
        val range = (0 until size * size)

        val gridsValues = range.map { i ->
            val pointValues = grids.map { it.array[i] }.toTypedArray()
            GridsValue(pointValues)
        }

        val tiles = gridsValues
            .map { tileMapper.map(it) }
            .toTypedArray()

        return TileArray(tiles, size)
    }
}