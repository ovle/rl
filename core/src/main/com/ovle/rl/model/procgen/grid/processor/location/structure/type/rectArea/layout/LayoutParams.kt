package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.gdx.math.array2d.Array2d

data class LayoutParams(
    val areaSize: Int,
    val sizeCoef: Float,
    val layoutMap: Array2d<Float>,

    val naturalWalls: Collection<GridPoint2>,
    val naturalWater: Collection<GridPoint2>,
)