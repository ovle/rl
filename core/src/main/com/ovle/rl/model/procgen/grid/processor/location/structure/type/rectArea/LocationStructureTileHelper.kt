package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea

import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.utils.RandomParams

interface LocationStructureTileHelper {
    fun tile(type: TileType, layoutValue: Float, random: RandomParams): TileTemplate?
}