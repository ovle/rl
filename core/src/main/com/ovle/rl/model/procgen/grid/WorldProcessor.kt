package com.ovle.rl.model.procgen.grid

import com.ovle.rl.model.procgen.grid.world.World


interface WorldProcessor {

    fun process(world: World)
}