package com.ovle.rl.model.procgen.grid

import com.ovle.utils.RandomParams


interface GridsProcessor {

    fun process(grids: Grids, random: RandomParams)
}