package com.ovle.rl.model.procgen.name.region


import com.ovle.rl.model.procgen.grid.world.WorldRegion
import com.ovle.rl.model.procgen.name.region.RegionNamePartType.*
import com.ovle.utils.RandomParams

class RegionNameGenerator(
    private val regionNameParts: Collection<RegionNamePart>,
) {
    companion object {
        const val SMALL_SIZE = 30
        const val BIG_SIZE = 1000
    }

    fun regionName(region: WorldRegion, random: RandomParams): String {
        val adj = adj(regionNameParts, random)
        val noun = noun(regionNameParts, random)
        val structure = RegionNameStructure.values().random(random.kRandom)
        val size = region.points.size
        val isSmall = size < SMALL_SIZE
        val isBig = size > BIG_SIZE
        val props = region.mainTileTemplate.props

        val subject = when {
            props.isWater -> when {
                isSmall -> "Lake"
                isBig && props.isDeep -> "Ocean"
                else -> "Sea"
            }
            props.isWall -> when {
                isSmall -> "Rock"
                isBig -> "Great mountains"
                else -> "Mountains"
            }
            else -> when {
                isSmall -> "Piece"
                isBig -> "Country"
                else -> "Land"
            }
        }

        return structure.name(subject, adj, noun)
    }


    private fun adj(nameParts: Collection<RegionNamePart>, random: RandomParams) =
        nameParts.filter { it.type == ADJ }
            .random(random.kRandom).name

    private fun noun(nameParts: Collection<RegionNamePart>, random: RandomParams) =
        nameParts.filter { it.type == NOUN }
            .random(random.kRandom).name
}