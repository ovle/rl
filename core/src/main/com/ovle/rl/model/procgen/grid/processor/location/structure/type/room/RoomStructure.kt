package com.ovle.rl.model.procgen.grid.processor.location.structure.type.room

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray

interface RoomStructure<T> {

    fun initParams(r: kotlin.random.Random): T

    fun processTile(point: GridPoint2, room: RoomInfo, tiles: TileArray, params: T, r: kotlin.random.Random)
}