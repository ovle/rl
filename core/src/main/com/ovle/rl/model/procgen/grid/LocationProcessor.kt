package com.ovle.rl.model.procgen.grid

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.spawn.EntitySpawn

interface LocationProcessor {
    fun process(location: Location): Collection<EntitySpawn>
}