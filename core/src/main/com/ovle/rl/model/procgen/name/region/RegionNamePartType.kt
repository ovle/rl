package com.ovle.rl.model.procgen.name.region

enum class RegionNamePartType {
    ADJ, NOUN
}