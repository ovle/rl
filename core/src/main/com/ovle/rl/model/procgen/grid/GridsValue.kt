package com.ovle.rl.model.procgen.grid

class GridsValue(
    val value: Array<Float>,
) {
    val alt: Float
        get() = value[0]

    val temp: Float
        get() = value[1]

    val hum: Float
        get() = value[2]

    val min: Float
        get() = value[3]

    val cha: Float
        get() = value[4]
}
