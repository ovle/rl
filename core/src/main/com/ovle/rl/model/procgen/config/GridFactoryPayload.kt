package com.ovle.rl.model.procgen.config

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.procgen.grid.world.World

data class GridFactoryPayload(
    val world: World,
    val point: GridPoint2
)