package com.ovle.rl.model.procgen.name.entity

import com.badlogic.ashley.core.Entity
import com.ovle.utils.RandomParams

class EntityNameGenerator(
    private val templates: Collection<NameSetTemplate>
) {

    fun entityName(entity: Entity, random: RandomParams): String {
        val template = nameTemplate(entity, templates)
        return template.generator.generate(template.maxLength)
    }


    private fun nameTemplate(entity: Entity, templates: Collection<NameSetTemplate>): NameSetTemplate {
        //todo select by entity
        return templates.first()
    }
}