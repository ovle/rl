package com.ovle.rl.model.procgen.grid.processor.location.structure

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.TileCheck1

fun TileArray.setArray(index: GridPoint2, value: TileArray, tilePostFilter: TileCheck1?) {
    (0 until value.size).forEach { i ->
        (0 until value.size).forEach { j ->
            val x = index.x + j
            val y = index.y - i
            if (tilePostFilter?.invoke(get(x, y)) != false) {
                set(x, y, value[i, j])
            }
        }
    }
}