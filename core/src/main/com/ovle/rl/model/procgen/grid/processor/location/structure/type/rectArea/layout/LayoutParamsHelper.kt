package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout

import com.ovle.rl.TileArray
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.rect.RectArea

class LayoutParamsHelper(
    private val layoutMapFactories: Collection<RectAreaLayoutMapFactory>
) {

    fun layoutParams(
        areas: List<RectArea>, areaSize: Int, tiles: TileArray, random: RandomParams
    ): LayoutParams {
        val activeAreaSize = areas.size * (areaSize * areaSize)
        val maxSize = tiles.size * tiles.size
        val sizeCoef = activeAreaSize.toFloat() / maxSize
        val layoutMapFactory = layoutMapFactories.random(random.kRandom)
        val layoutMap = layoutMapFactory.layoutMap(
            areas, tiles.size, sizeCoef, random
        )

        val naturalWalls = tiles.filter { it.props.isNatural && it.props.isWall }
        val naturalWater = tiles.filter { it.props.isNatural && it.props.isWater }

        return LayoutParams(
            areaSize = areaSize,
            sizeCoef = sizeCoef,
            layoutMap = layoutMap,
            naturalWalls = naturalWalls,
            naturalWater = naturalWater
        )
    }
}