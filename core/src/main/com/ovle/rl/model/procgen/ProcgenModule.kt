package com.ovle.rl.model.procgen

import com.ovle.rl.model.procgen.grid.world.WorldPlacesHelper
import com.ovle.rl.model.procgen.grid.world.WorldRegionsHelper
import com.ovle.rl.model.procgen.grid.world.WorldFactory
import com.ovle.rl.model.procgen.name.place.PlaceNameGenerator
import com.ovle.rl.model.procgen.name.region.RegionNameGenerator
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.singleton


val procgenModule = DI.Module("procgen") {
    bind<RegionNameGenerator>() with singleton {
        RegionNameGenerator(instance())
    }
    bind<PlaceNameGenerator>() with singleton {
        PlaceNameGenerator()
    }
    bind<WorldRegionsHelper>() with singleton {
        WorldRegionsHelper(instance())
    }
    bind<WorldPlacesHelper>() with singleton {
        WorldPlacesHelper(instance())
    }
    bind<WorldFactory>() with singleton {
        WorldFactory(instance(), instance())
    }
}