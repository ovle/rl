package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea

enum class TileType(val c: Char) {
    FLOOR('_'),
    WALL('#'),
    DOOR('D'),
    WINDOW('d'),
    ROAD('.'),
    SQUARE('0'),
    WATER('w'),
    SOIL(','),

    UNKNOWN('?'),
    ;

    companion object {
        fun byChar(c: Char) = TileType.values().singleOrNull { it.c == c }
    }
}