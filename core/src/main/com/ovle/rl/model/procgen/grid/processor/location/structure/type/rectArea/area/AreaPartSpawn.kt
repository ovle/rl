package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.area.AreaParams

data class AreaPartSpawn(
    val points: Collection<GridPoint2>,
    val areaTemplate: com.ovle.rl.model.game.area.AreaTemplate? = null,
    val areaParams: AreaParams?
)