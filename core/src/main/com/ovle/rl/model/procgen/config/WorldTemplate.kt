package com.ovle.rl.model.procgen.config

import com.ovle.rl.model.procgen.grid.GridsProcessor
import com.ovle.rl.model.procgen.grid.TilesProcessor
import com.ovle.rl.model.procgen.grid.WorldProcessor
import com.ovle.rl.model.procgen.grid.world.WorldPlacesHelper
import com.ovle.utils.RandomParams
import com.ovle.utils.noise4j.grid.factory.GridFactory

class WorldTemplate(
    val name: String,
    val getGridFactories: (RandomParams) -> Array<GridFactory<*>>,
    val gridsProcessors: Array<GridsProcessor> = arrayOf(),
    val tilesProcessors: Array<TilesProcessor> = arrayOf(),
    val worldProcessors: Array<WorldProcessor> = arrayOf(),
    val placesParams: WorldPlacesHelper.Companion.Params,
    val tileMapper: TileMapper
)