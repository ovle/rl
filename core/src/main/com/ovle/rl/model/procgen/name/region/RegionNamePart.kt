package com.ovle.rl.model.procgen.name.region

data class RegionNamePart(
    val name: String, val type: RegionNamePartType
)