package com.ovle.rl.model.procgen.grid.processor.location.structure.type.room

import com.badlogic.gdx.math.Vector2
import com.ovle.rl.RoomTiles
import com.ovle.rl.TileArray
import com.ovle.utils.gdx.math.point.isAdj
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.LocationProcessor
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.utils.gdx.math.point.point
import kotlin.math.roundToInt
import kotlin.random.Random


class RoomsProcessor(
    private val structures: Collection<RoomStructure<*>>,
    private val baseTile: TileTemplate
) : LocationProcessor {

    override fun process(location: Location): Collection<EntitySpawn> {
        val random = location.random
        val tiles = location.content.tiles
        val roomsData = mutableListOf<RoomTiles>()
        var currentRoom: RoomTiles? = null

        for (x in 0 until tiles.size) {
            for (y in 0 until tiles.size) {
                val tile = tiles[x, y]
                val isRoomTile = tile == baseTile
                if (!isRoomTile) {
                    currentRoom = null
                    continue
                }

                if (currentRoom == null) {
                    currentRoom = currentRoom(roomsData, x, y)
                }
                if (currentRoom == null) {
                    currentRoom = mutableListOf()
                    roomsData.add(currentRoom)
                }

                val value = Vector2(x.toFloat(), y.toFloat())
                currentRoom.add(value)
            }
        }

        val kRandom = random.kRandom
        val rooms = roomsData.map { roomInfo(it) }
        rooms.forEach {
            processRoom(location.content.tiles, it, structures.random(kRandom), kRandom)
        }

        return listOf()
    }


    private fun roomInfo(points: RoomTiles): RoomInfo {
        val minX = points.minOf { it.x }.roundToInt()
        val minY = points.minOf { it.y }.roundToInt()
        val maxX = points.maxOf { it.x }.roundToInt()
        val maxY = points.maxOf { it.y }.roundToInt()

        return RoomInfo(minX, minY, maxX - minX, maxY - minY)
    }

    private fun currentRoom(roomsData: List<RoomTiles>, x: Int, y: Int) =
        roomsData.find {
            it.any { p -> isAdj(p.x.roundToInt(), p.y.roundToInt(), x, y) }
        }

    private fun <T> processRoom(tiles: TileArray, room: RoomInfo, roomStructure: RoomStructure<T>, r: Random) {
        val params = roomStructure.initParams(r)

        for (x in room.x until room.x + room.width) {
            for (y in room.y until room.y + room.height) {
                val point = point(x, y)
                roomStructure.processTile(point, room, tiles, params, r)
            }
        }
    }
}