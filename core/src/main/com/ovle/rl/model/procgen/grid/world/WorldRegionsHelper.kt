package com.ovle.rl.model.procgen.grid.world

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.TileArray
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.name.region.RegionNameGenerator
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.floodFill

class WorldRegionsHelper(
    private val nameGenerator: RegionNameGenerator
) {

    fun regions(tiles: TileArray, random: RandomParams): Collection<WorldRegion> {
        val result = mutableListOf<WorldRegion>()
        val allPoints = tiles.points.toSet()
        val points = allPoints.toMutableSet()

        while (points.isNotEmpty()) {
            val startPoint = points.first()
            val startTile = tiles[startPoint.x, startPoint.y]
            val maxAreaSize = null //400
            val regionPoints = floodFill(startPoint, tiles.size, maxAreaSize) {
                if (it !in points) return@floodFill false

                val t = tiles[it.x, it.y]
                t == startTile || isSameGroupTiles(t, startTile)
            }.toSet()

            val region = WorldRegion(mainTile(tiles, regionPoints), regionPoints)
            region.name = nameGenerator.regionName(region, random)
            result.add(region)

            points.removeAll(regionPoints)
        }

        return result
    }

    private fun isSameGroupTiles(t1: TileTemplate, t2: TileTemplate): Boolean {
        val p1 = t1.props
        val p2 = t2.props

        return (p1.isWall && p2.isWall)
            || (p1.isWater && p2.isWater)
//            || (p1.isFloor && p2.isFloor)
            || (p1.isLava && p2.isWall) || (p2.isLava && p1.isWall)
    }

    private fun mainTile(tiles: Array2d<TileTemplate>, regionPoints: Collection<GridPoint2>): TileTemplate {
        val t = regionPoints.map { tiles[it.x, it.y] }.groupBy { it }
        return t.maxBy { it.value.size }.key
    }
}