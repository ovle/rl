package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea

import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaSpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.MergeAreasHelper
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaProcessor
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout.LayoutParams
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.rect.RectArea
import com.ovle.utils.weightedIndex


class RectAreaSpawnHelper(
    private val mergeAreasHelper: MergeAreasHelper,
    private val areaProcessors: List<AreaProcessor>,
    private val emptyChancePerTile: (Float) -> Float,
) {

    fun areaSpawns(
        areas: Collection<RectArea>,
        borders: Collection<RectArea>,
        layoutParams: LayoutParams,
        random: RandomParams
    ): List<AreaSpawn> {
        val mergedAreas = mergeAreasHelper.mergeAreas(areas, layoutParams, random)
        val shrinkedAreas = mergedAreas.map {
            it.copy(x1 = it.x1 + 1, y1 = it.y1 + 1)
        }

        val areasSpawns = shrinkedAreas.mapNotNull {
            areaSpawn(it, layoutParams, random)
        }
        val borderAreaSpawns = borders.map {
            borderAreaSpawn(it)
        }

        return (borderAreaSpawns + areasSpawns)
    }

    private fun areaSpawn(
        area: RectArea, layoutParams: LayoutParams, random: RandomParams
    ): AreaSpawn? {
        val center = area.center
        val layoutValue = layoutParams.layoutMap[center.x, center.y]
        val isEmpty = random.chance(emptyChancePerTile(layoutValue))
        if (isEmpty) return null

        val validAreaProcessors = areaProcessors.filter {
            it.weight(area, layoutValue) > 0.0f
        }
        val checkValue = random.kRandom.nextFloat()
        val weights = validAreaProcessors.map { it.weight(area, layoutValue) }
        val index = weights.weightedIndex(checkValue) ?: return null

        val areaProcessor = validAreaProcessors[index]

        return areaProcessor.process(area, layoutValue, random)
    }

    private fun borderAreaSpawn(area: RectArea) = AreaSpawn(
        area = area,
        parts = listOf(),
        pointsByTile = mapOf(TileType.ROAD to area.points),
        entitySpawns = emptyList()
    )
}