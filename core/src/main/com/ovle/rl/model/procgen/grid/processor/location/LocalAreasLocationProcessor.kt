package com.ovle.rl.model.procgen.grid.processor.location

import com.badlogic.gdx.math.GridPoint2
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.Grids
import com.ovle.rl.model.procgen.grid.GridsValue
import com.ovle.rl.model.procgen.grid.LocationProcessor
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.utils.gdx.math.array2d.floodFill
import com.ovle.utils.gdx.math.point.point
import kotlin.math.min


class LocalAreasLocationProcessor(
    private val params: Params
): LocationProcessor {

    companion object {
        data class Params(
            val baseTile: TileTemplate,
            val onePerTiles: Int,
            val sizeRange: IntRange = (2..16),
            val tileCheck: (GridsValue) -> Boolean
        )
    }


    override fun process(location: Location): Collection<EntitySpawn> {
        val random = location.random
        val ponds = mutableListOf<Collection<GridPoint2>>()
        val content = location.content
        val tiles = content.tiles
        val grids = content.grids
        val validPoints = validPoints(grids, tiles.size)
        var attempts = 0
        var tilesCount = 0
        val maxCount = validPoints.size / params.onePerTiles
        val isValid: (GridPoint2) -> Boolean = { it in validPoints }

        while (tilesCount < maxCount) {
            if (validPoints.isEmpty()) break

            val startPoint = validPoints.random(random.kRandom)
            val paramsBasedSize = params.sizeRange.random(random.kRandom)
            val areaSize = min(paramsBasedSize, validPoints.size)
            val area = floodFill(startPoint, tiles.size, areaSize, isValid)

            if (area.isNotEmpty()) {
                ponds.add(area)
                tilesCount += area.size
                validPoints.removeAll { it in area }
            }

            attempts++
        }

        ponds.flatten().forEach {
            tiles[it.x, it.y] = params.baseTile
        }

        return emptyList()
    }


    private fun validPoints(grids: Grids, size: Int): MutableSet<GridPoint2> {
        val result = mutableSetOf<GridPoint2>()

        for (i in 0 until size) {
            for (j in 0 until size) {
                val value = grids.value(point(i, j))
                if (!params.tileCheck(value)) continue

                result.add(point(i, j))
            }
        }

        return result
    }
}