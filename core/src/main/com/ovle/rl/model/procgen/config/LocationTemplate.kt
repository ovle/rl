package com.ovle.rl.model.procgen.config

import com.ovle.rl.model.game.spawn.LocationEntitySpawnConfig
import com.ovle.rl.model.game.game.dto.FinishGameCondition
import com.ovle.rl.model.game.social.dto.FactionsConfig
import com.ovle.rl.model.game.time.dto.TimerTemplate
import com.ovle.rl.model.procgen.grid.LocationProcessor
import com.ovle.utils.noise4j.grid.factory.GridFactory

class LocationTemplate(
    val name: String,
    val getGridFactories: (GridFactoryPayload?) -> Array<GridFactory<*>>,
    val postProcessors: Array<LocationProcessor> = emptyArray(),
    val tileMapper: TileMapper,
    val globalTimerTemplates: Collection<TimerTemplate> = listOf(),
    val finishGameConditionsCheck: Collection<FinishGameCondition> = emptyList(),
    val factions: FactionsConfig,
    val entitySpawnConfig: LocationEntitySpawnConfig
)