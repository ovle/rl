package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.area.AreaTemplate
import com.ovle.utils.RandomParams


data class AreaPartPurpose(
    val name: String,
    val weight: Float,
    val creatures: Collection<AreaPurposeEntity> = emptyList(),
    val entities: Collection<AreaPurposeEntity> = emptyList(),
    //todo wrap both
    val areaTemplate: AreaTemplate? = null,
    val getAreaParams: (() -> AreaParams)? = null,
)