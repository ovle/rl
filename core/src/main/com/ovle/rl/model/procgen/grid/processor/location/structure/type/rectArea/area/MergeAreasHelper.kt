package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout.LayoutParams
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.point.*
import com.ovle.utils.gdx.math.rect.RectArea


class MergeAreasHelper(
    private val params: Params
) {
    companion object {
        data class Params(
            val mergeChancePerLv: (Float) -> Float,
            val largeAreaThresholdLv: Float,
            val largeAreaChance: Float,
        )
    }

    fun mergeAreas(
        areas: Collection<RectArea>, layoutParams: LayoutParams, random: RandomParams
    ): Collection<RectArea> {
        val areaSize = layoutParams.areaSize
        val areasByIndex = areas.associateBy { indexOfArea(it, areaSize) }
        val filterAreas = mutableSetOf<RectArea>()

        fun isAllowMerge(main: RectArea?, a: RectArea?) =
            main != null && a != null
                && main !in filterAreas && a !in filterAreas
                && a.isAllowMerge(main)

        fun merge(a1: RectArea, a2: RectArea): RectArea {
            filterAreas += a1
            filterAreas += a2
            return a1.merge(a2)
        }

        val newAreas = areas.mapNotNull {
            val center = it.center
            val layoutValue = layoutParams.layoutMap[center.x, center.y]
            val mergeChance = params.mergeChancePerLv(layoutValue)
            val index = indexOfArea(it, areaSize)
            val adjToMergeIndex = index.adjHV().random(random.kRandom)
            val adjToMerge = areasByIndex[adjToMergeIndex]
            val isTryLargeSquare = layoutValue > params.largeAreaThresholdLv
                && random.chance(params.largeAreaChance)

            when {
                it in filterAreas -> null
                !random.chance(mergeChance) -> it
                !isAllowMerge(it, adjToMerge) -> it
                isTryLargeSquare -> {
                    val adj1 = areasByIndex[index.right()]
                    val adj2 = areasByIndex[index.bottom()]
                    val adj3 = areasByIndex[index.bottom().right()]
                    if (isAllowMerge(it, adj1) && isAllowMerge(adj2, adj3)) {
                        merge(
                            merge(it, adj1!!), merge(adj2!!, adj3!!)
                        )
                    } else it
                }
                else -> merge(it, adjToMerge!!)
            }
        }

        return newAreas - filterAreas
    }

    private fun indexOfArea(area: RectArea, size: Int) =
        point(area.x1 / size, area.y1 / size)
}