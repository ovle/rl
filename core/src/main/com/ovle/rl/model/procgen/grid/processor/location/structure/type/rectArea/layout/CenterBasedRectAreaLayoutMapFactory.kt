package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout

import com.badlogic.gdx.math.GridPoint2
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.gradientFloodFill
import com.ovle.utils.gdx.math.array2d.normalize
import com.ovle.utils.gdx.math.point.point
import com.ovle.utils.gdx.math.rect.RectArea
import ktx.log.info
import java.lang.Float.max

class CenterBasedRectAreaLayoutMapFactory(
    private val params: Params
): RectAreaLayoutMapFactory {

    companion object {
        class Params(
            val minSizeCoef: Float,
        )
    }

    override fun layoutMap(
        areas: Collection<RectArea>, size: Int, sizeCoef: Float, random: RandomParams
    ): Array2d<Float> {
        val center = areasCenter(areas)
        info { "sizeCoef: $sizeCoef, center: $center" }

        var valuesMap = Array2d.array(Float.MAX_VALUE, size)
        gradientFloodFill(center to 0.0f, emptyList(), valuesMap)

        normalize(valuesMap)
        val valueMul = max(sizeCoef, params.minSizeCoef)
        valuesMap = valuesMap.map { (1.0f - it) * valueMul }

        return valuesMap
    }

    private fun areasCenter(areas: Collection<RectArea>): GridPoint2 {
        val centers = areas.map { it.center }
        val centerX = centers.map { it.x }.average().toInt()
        val centerY = centers.map { it.y }.average().toInt()

        return point(centerX, centerY)
    }
}