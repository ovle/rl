package com.ovle.rl.model.procgen.grid.world

import com.ovle.rl.TileArray
import com.ovle.rl.model.procgen.name.place.PlaceNameGenerator
import com.ovle.utils.RandomParams
import com.ovle.utils.random
import kotlin.math.min


class WorldPlacesHelper(
    private val nameGenerator: PlaceNameGenerator
) {

    companion object {
        class Params(
            val countByTemplate: Map<WorldPlaceTemplate, IntRange>,
        )
    }

    fun places(tiles: TileArray, params: Params, random: RandomParams): Collection<WorldPlace> {
        return params.countByTemplate.flatMap { (template, countRange) ->
            val count = countRange.random(random.kRandom)
            processTemplate(template, count, tiles, random)
        }
    }

    private fun processTemplate(
        template: WorldPlaceTemplate,
        count: Int,
        tiles: TileArray,
        random: RandomParams
    ): Collection<WorldPlace> {
        val availablePoints = tiles.points.filter {
            template.worldTileCheck(tiles[it.x, it.y])
        }
        val actualCount = min(count, availablePoints.size)

        val result = mutableListOf<WorldPlace>()
        repeat(actualCount) {
            val point = availablePoints.random(random.kRandom)
            val worldPlace = WorldPlace(
                template = template,
                point = point
            )
            worldPlace.name = nameGenerator.placeName(worldPlace, random)

            result += worldPlace
        }
        return result
    }
}