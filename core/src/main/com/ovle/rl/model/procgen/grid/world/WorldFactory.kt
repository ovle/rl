package com.ovle.rl.model.procgen.grid.world

import com.ovle.rl.model.procgen.config.WorldTemplate
import com.ovle.rl.model.procgen.grid.Grids
import com.ovle.rl.model.util.randomId
import com.ovle.utils.RandomParams


class WorldFactory(
    private val regionsHelper: WorldRegionsHelper,
    private val placesHelper: WorldPlacesHelper
) {

    fun get(template: WorldTemplate, random: RandomParams): World {
        val gridFactories = template.getGridFactories(random)
        val gridsArray = gridFactories.map { it.create(random) }

        val grids = Grids(gridsArray.toTypedArray())
        template.gridsProcessors.forEach {
            it.process(grids, random)
        }

        val tiles = grids.toTileArray(template.tileMapper)
        template.tilesProcessors.forEach {
            it.process(tiles, random)
        }

        val result = World(
            id = randomId(),
            name = "test",
            random = random,
            tiles = tiles, grids = grids,
            regions = regionsHelper.regions(tiles, random),
            places = placesHelper.places(tiles, template.placesParams, random),
        )

        template.worldProcessors.forEach {
            it.process(result)
        }

        return result
    }
}