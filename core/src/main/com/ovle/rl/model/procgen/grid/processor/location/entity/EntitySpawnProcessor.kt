package com.ovle.rl.model.procgen.grid.processor.location.entity

import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.procgen.grid.LocationProcessor
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.game.spawn.EntitySpawnHelper

class EntitySpawnProcessor(
    private val entityTemplates: Collection<EntityTemplate>,
    private val entitySpawnHelper: EntitySpawnHelper
) : LocationProcessor {

    override fun process(location: Location): Collection<EntitySpawn> {
        return entitySpawnHelper.spawnEntities(
            location, entityTemplates, isPerHourSpawn = false
        )
    }
}