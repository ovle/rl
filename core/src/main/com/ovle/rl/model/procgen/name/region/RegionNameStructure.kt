package com.ovle.rl.model.procgen.name.region

enum class RegionNameStructure {
    ADJ_SUBJ {
        override fun name(subject: String, adj: String, noun: String) = "The $adj $subject"
    },
    SUBJ_NOUN {
        override fun name(subject: String, adj: String, noun: String) = "The $subject of $noun"
    },
    SUBJ_ADJ_NOUN {
        override fun name(subject: String, adj: String, noun: String) = "$subject of $adj $noun"
    },
    ADJ_SUBJ_NOUN {
        override fun name(subject: String, adj: String, noun: String) = "$adj $subject of $noun"
    };

    abstract fun name(subject: String, adj: String, noun: String): String
}