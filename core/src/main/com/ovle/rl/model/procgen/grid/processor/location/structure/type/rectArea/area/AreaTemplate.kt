package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.ovle.rl.model.template.parsedMask

data class AreaTemplate(
    val name: String,
    val h: Int, val w: Int,
    val rooms: Collection<AreaTemplateRoom>,
    val mask: String = ""
) {
    val parsedMask: List<List<Char>> by lazy {
        parsedMask(mask)!!
    }
}