package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea

import com.ovle.rl.TileCheck1
import com.ovle.rl.model.game.area.AreaInfo
import com.ovle.rl.model.game.game.dto.location.Location
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.LocationProcessor
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area.AreaSpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout.LayoutParams
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.layout.LayoutParamsHelper
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.array2d.Array2d
import com.ovle.utils.gdx.math.array2d.ArrayValidationHelper
import com.ovle.utils.gdx.math.rect.RectArea
import com.ovle.utils.gdx.math.rect.RectAreasHelper

//todo wide streets
//todo population
//todo create actual areas
//todo items in containers
class RectAreasLocationProcessor(
    private val params: Params,
    private val layoutParamsHelper: LayoutParamsHelper,
    private val rectAreasHelper: RectAreasHelper,
    private val rectAreaSpawnHelper: RectAreaSpawnHelper,
    private val locationStructureTileHelper: LocationStructureTileHelper,
    private val arrayValidationHelper: ArrayValidationHelper,
): LocationProcessor {

    companion object {
        class Params(
            val tileCheck: TileCheck1,
            val areaSize: Int
        )
    }


    override fun process(location: Location): Collection<EntitySpawn> {
        val random = location.random
        val tiles = location.content.tiles
        val size = tiles.size

        val areaSize = params.areaSize
        val areas = rectAreasHelper.rectAreas(
            RectArea(x1 = 0, y1 = 0, x2 = size, y2 = size), areaSize
        ).filter {
            isValidArea(it, tiles) && it.isAtLeastOfSize(areaSize)
        }
        if (areas.isEmpty()) return emptyList()

        val borders = rectAreasHelper.borders(areas)
            .filter { isValidArea(it, tiles) }

        val layoutParams = layoutParamsHelper.layoutParams(areas, areaSize, tiles, random)

        val areaSpawns = rectAreaSpawnHelper.areaSpawns(areas, borders, layoutParams, random)
        areaSpawns.forEach {
            setTiles(it, tiles, layoutParams, random)
            addAreas(it, location)
        }

        return areaSpawns.flatMap { it.entitySpawns }
    }

    private fun setTiles(
        areaSpawn: AreaSpawn, tiles: Array2d<TileTemplate>, layoutParams: LayoutParams, random: RandomParams
    ) {
        val center = areaSpawn.area.center
        val layoutValue = layoutParams.layoutMap[center.x, center.y]

        for ((type, points) in areaSpawn.pointsByTile) {
            for (p in points) {
                val tile = locationStructureTileHelper.tile(type, layoutValue, random)
                    ?: continue

                tiles[p.x, p.y] = tile
            }
        }
    }

    private fun addAreas(areaSpawn: AreaSpawn, location: Location) {
        val aiPlayerFaction = location.template.factions.aiPlayerFaction
        val aiPlayer = location.players.byFaction[aiPlayerFaction]
            ?: return

        areaSpawn.parts
            .filter { it.areaTemplate != null }
            .forEach {
                aiPlayer.areas += AreaInfo(
                    area = it.points.toMutableSet(),
                    name = "todo",
                    template = it.areaTemplate!!,
                    params = it.areaParams!!
                )
        }
    }

    private fun isValidArea(a: RectArea, tiles: Array2d<TileTemplate>) =
        arrayValidationHelper.isValidArea(
            a.x1, a.x2, a.y1, a.y2, tiles, params.tileCheck
        )
}