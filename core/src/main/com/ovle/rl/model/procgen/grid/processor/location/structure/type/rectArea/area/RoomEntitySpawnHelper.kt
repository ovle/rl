package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.material.MaterialComponent
import com.ovle.rl.model.game.material.MaterialTemplate
import com.ovle.rl.model.game.material.material
import com.ovle.rl.model.game.social.Components.social
import com.ovle.rl.model.game.social.dto.FactionTemplate
import com.ovle.rl.model.game.spawn.EntitySpawn
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LAYOUT_LEVEL_AVG
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LAYOUT_LEVEL_HIGH
import com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.LAYOUT_LEVEL_LOW
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.gdx.math.point.allBorders
import ktx.ashley.get
import kotlin.random.Random

class RoomEntitySpawnHelper(
    private val craftTemplates: Collection<CraftTemplate>,
    private val faction: FactionTemplate
) {

    fun roomEntitySpawns(room: AreaTemplateRoom, layoutValue: Float, purpose: AreaPartPurpose, r: Random): Collection<EntitySpawn> {

        val purposeEntities = purpose.entities
        if (purposeEntities.isEmpty()) return emptySet()

        val spawns = mutableSetOf<EntitySpawn>()

        //todo scale countRange by room size?
        var entitiesCount = room.entitiesCount.random(r)
        if (layoutValue < LAYOUT_LEVEL_AVG) entitiesCount--
        if (entitiesCount <= 0) return emptySet()

        //todo not block doors
        val roomPoints = room.area.points
        val freeRoomPoints = roomPoints.allBorders()
            .toMutableSet()
        val forceEntities = purposeEntities
            .filter { it.countRange.first > 0 }.toMutableSet()
        val availableEntities = purposeEntities
            .filter { it.countRange.last > 0 }.toMutableSet()

        while (spawns.size < entitiesCount) {
            val purposeEntity = forceEntities.randomOrNull(r)
                ?: availableEntities.randomOrNull(r)
                ?: break
            val roomPoint = freeRoomPoints.randomOrNull(r)
                ?: break

            freeRoomPoints -= roomPoint

            val template = purposeEntity.entity
            spawns += EntitySpawn(
                template = template,
                position = roomPoint,
                material = template.material() ?: craftMaterial(template, layoutValue, r)
            )

            val spawnedCount = spawns.count { it.template == template }
            if (spawnedCount >= purposeEntity.countRange.first) {
                forceEntities -= purposeEntity
            }
            if (spawnedCount >= purposeEntity.countRange.last) {
                availableEntities -= purposeEntity
            }
        }

        val creatures = purpose.creatures.map {
            it.entity to it.countRange.random(r)
        }
        creatures.forEach {
            //todo variations/professions

            val point = roomPoints.random(r)
            spawns += EntitySpawn(
                template = it.first,
                position = point,
            ) { e, l ->
                val sc = e[social]!!
                sc.faction = faction
                sc.player = l.players.byFaction[faction]
            }
        }

        return spawns
    }


    private fun craftMaterial(template: EntityTemplate, layoutValue: Float, r: Random): MaterialTemplate? {
        val craftResources = craftTemplates.find { it.entity == template }?.resources
            ?: return null

        val materials = craftResources.mapNotNull { it.material() }
        return when (materials.size) {
            0 -> null
            1 -> materials.single()
            else -> {
                val gradedMaterials = materials.sortedBy { it.grade }
                return when {
                    layoutValue <= LAYOUT_LEVEL_LOW -> gradedMaterials.first()
                    layoutValue <= LAYOUT_LEVEL_HIGH -> gradedMaterials
                        .dropLast(1).random(r)
                    else -> gradedMaterials.last()
                }
            }
        }
    }
}