package com.ovle.rl.model.procgen.grid.processor.location.structure.type.room

enum class DirectionValue {
    H, V, HV, NO_DIRECTION
}