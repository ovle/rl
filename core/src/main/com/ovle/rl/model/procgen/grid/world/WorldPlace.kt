package com.ovle.rl.model.procgen.grid.world

import com.badlogic.gdx.math.GridPoint2

class WorldPlace(
    val template: WorldPlaceTemplate,
    val point: GridPoint2
) {
    lateinit var name: String
}