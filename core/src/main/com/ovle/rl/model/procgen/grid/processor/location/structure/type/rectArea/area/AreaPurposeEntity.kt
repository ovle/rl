package com.ovle.rl.model.procgen.grid.processor.location.structure.type.rectArea.area

import com.ovle.rl.model.template.entity.EntityTemplate

data class AreaPurposeEntity(
    val entity: EntityTemplate,
    val countRange: IntRange
)