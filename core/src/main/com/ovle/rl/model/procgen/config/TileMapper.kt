package com.ovle.rl.model.procgen.config

import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.procgen.grid.GridsValue

interface TileMapper {
    fun map(v: GridsValue): TileTemplate
}