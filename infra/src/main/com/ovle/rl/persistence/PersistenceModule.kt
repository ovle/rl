package com.ovle.rl.persistence

import com.badlogic.ashley.core.EntitySystem
import com.ovle.rl.model.game.area.AreaTemplate
import com.ovle.rl.model.game.build.BuildTemplate
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.effect.StaticEffectTemplate
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.space.aoe.AOETemplate
import com.ovle.rl.model.game.space.move.impl.path.PathTemplate
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.game.time.dto.TimerTemplate
import com.ovle.rl.model.game.trigger.dto.TriggerTemplate
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.rl.persistence.serialization.GameDeserializer
import com.ovle.rl.persistence.serialization.GameSerializer
import com.ovle.rl.persistence.serialization.jackson.*
import com.ovle.rl.persistence.serialization.jackson.deserializer.*
import com.ovle.rl.persistence.serialization.jackson.serializer.EntitySerializer
import com.ovle.rl.persistence.storage.DataStorage
import com.ovle.rl.persistence.storage.preferences.PreferencesDataStorage
import org.kodein.di.*

val persistenceModule = DI.Module("persistence") {
    bind<EntitySystem>().inSet() with singleton {
        PersistenceSystem(instance())
    }
    bind<SavedGamesManager>() with singleton {
        SavedGamesManager(instance(), instance(), instance())
    }

    bind<DataStorage>() with singleton {
        PreferencesDataStorage()
    }

    bind<EntitySerializer>() with singleton {
        EntitySerializer()
    }
    bind<EntityDeserializePool>() with singleton {
        EntityDeserializePool()
    }
    bind<EntityDeserializer>() with singleton {
        EntityDeserializer(entityPool = instance())
    }

    bind<Collection<NamedTemplateDeserializer<*>>>() with singleton {
        listOf(
            NamedTemplateDeserializer(t = EntityTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = SkillTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = TaskTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = TimerTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = TriggerTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = AOETemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = PathTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = StaticEffectTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = FarmTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = BuildTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = CraftTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = AreaTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = TileTemplate::class.java, templates = instance()),
            NamedTemplateDeserializer(t = PlayerSkillTemplate::class.java, templates = instance()),
        )
    }

    bind<LocationTemplateDeserializer>() with singleton {
        LocationTemplateDeserializer(locationTemplates = instance(), worldLocationTemplate = instance())
    }
    bind<GridDeserializer>() with singleton {
        GridDeserializer()
    }

    bind<GameSerializer>() with singleton {
        JacksonGameSerializer(jacksonMixins, instance())
    }
    bind<GameDeserializer>() with singleton {
        JacksonGameDeserializer(
            jacksonMixins, instance(), instance(), instance(), instance(), instance()
        )
    }
}