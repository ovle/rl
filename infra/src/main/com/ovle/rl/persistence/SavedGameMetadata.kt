package com.ovle.rl.persistence

import com.ovle.rl.Tick
import java.util.Date

data class SavedGameMetadata(
    val name: String,
    val id: String,
    val date: Date,
    val tick: Tick,
    val locationName: String,
    val formatVersion: String,
)