package com.ovle.rl.persistence

import com.ovle.rl.model.game.core.system.BaseSystem
import com.ovle.rl.model.game.game.GameSavedEvent
import com.ovle.rl.model.game.game.StartSavedGameCommand

import com.ovle.utils.event.EventBus.send
import com.ovle.utils.event.EventBus.subscribe
import ktx.log.info


class PersistenceSystem(
    private val savedGamesManager: SavedGamesManager
): BaseSystem() {

    override fun subscribe() {
        subscribe<SaveGameCommand>(this) { onSaveGameCommand(it.meta, it.name) }
        subscribe<LoadGameCommand>(this) { onLoadGameCommand(it.meta) }
    }

    private fun onSaveGameCommand(meta: SavedGameMetadata?, name: String) {
        savedGamesManager.save(meta, name, game())
        info(PERSISTENCE_LOG_TAG) { "game saved: $name" }

        send(GameSavedEvent())
    }

    private fun onLoadGameCommand(meta: SavedGameMetadata) {
        val game = savedGamesManager.load(meta, engine)
        info(PERSISTENCE_LOG_TAG) { "game loaded: $meta" }

        send(StartSavedGameCommand(game))
    }
}