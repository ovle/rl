package com.ovle.rl.persistence

import com.ovle.rl.model.game.core.GameCommand

class SaveGameCommand(val meta: SavedGameMetadata?, val name: String) : GameCommand()

class LoadGameCommand(val meta: SavedGameMetadata) : GameCommand()
