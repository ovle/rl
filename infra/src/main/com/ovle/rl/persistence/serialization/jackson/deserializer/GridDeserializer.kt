package com.ovle.rl.persistence.serialization.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.github.czyzby.noise4j.map.Grid


class GridDeserializer @JvmOverloads constructor(
    t: Class<Grid?>? = null,
) : StdDeserializer<Grid>(t) {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Grid {
        val node: JsonNode = p.codec.readTree(p)
        val width = node.get("width").intValue()
        val height = node.get("height").intValue()
        val array = node.get("array").asIterable()
            .map { it.floatValue() }
            .toFloatArray()

        return Grid(array, width, height)
    }
}