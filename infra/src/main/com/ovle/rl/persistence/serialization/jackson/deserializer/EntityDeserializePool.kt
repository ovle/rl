package com.ovle.rl.persistence.serialization.jackson.deserializer

import com.badlogic.ashley.core.Entity
import com.ovle.rl.EntityId

data class EntityDeserializePool(
    val entities: MutableMap<EntityId, Entity> = mutableMapOf()
)