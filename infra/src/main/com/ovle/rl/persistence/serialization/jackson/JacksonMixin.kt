package com.ovle.rl.persistence.serialization.jackson

import kotlin.reflect.KClass

data class JacksonMixin(val target: KClass<out Any>, val mixin: KClass<out Any>)