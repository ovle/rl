package com.ovle.rl.persistence.serialization

import com.badlogic.ashley.core.Engine
import com.ovle.rl.GameEntity
import com.ovle.rl.persistence.SavedGameMetadata
import com.ovle.rl.persistence.SerializedGame
import com.ovle.rl.persistence.SerializedMeta

interface GameDeserializer {
    fun deserializeMeta(data: SerializedMeta): Collection<SavedGameMetadata>

    fun deserialize(data: SerializedGame, engine: Engine): GameEntity
}