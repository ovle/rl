package com.ovle.rl.persistence.serialization.jackson

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.util.location
import com.ovle.rl.persistence.SavedGameMetadata
import com.ovle.rl.persistence.SerializedGame
import com.ovle.rl.persistence.SerializedMeta
import com.ovle.rl.persistence.serialization.GameSerializer
import com.ovle.rl.persistence.serialization.jackson.serializer.EntitySerializer


class JacksonGameSerializer(
    private val jacksonMixins: Array<JacksonMixin>,
    private val entitySerializer: EntitySerializer
) : GameSerializer {

    override fun serializeMeta(meta: Collection<SavedGameMetadata>): SerializedMeta {
        val mapper = jacksonObjectMapper()
        return mapper.writeValueAsString(meta)
    }

    override fun serialize(game: GameEntity): SerializedGame {
        val mapper = objectMapper()
        SimpleModule().apply {
            addSerializer(Entity::class.java, entitySerializer)
            mapper.registerModule(this)
        }

        val content = game.location().content
        val entitiesMap = content.entities.all().associate {
            it.id() to it.components.toArray(EntityComponent::class.java)
                .filter { c -> c.isModel }
                .toTypedArray()
        }
        val gameComponents = game.components.toArray(GlobalComponent::class.java)
            .filter { gc -> gc.isModel }
            .toTypedArray()

        val saveData = SavedGame(gameComponents, entitiesMap)

        return mapper.writeValueAsString(saveData)
    }


    private fun objectMapper(): ObjectMapper {
        val mapper = jacksonObjectMapper()
        jacksonMixins.forEach { mapper.addMixIn(it.target.java, it.mixin.java) }
        return mapper
    }
}