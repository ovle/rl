package com.ovle.rl.persistence.serialization.jackson

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.github.czyzby.noise4j.map.Grid
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.id
import com.ovle.rl.model.template.NamedTemplate
import com.ovle.rl.model.game.game.factory.newEntity
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.move.MoveState
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.persistence.SavedGameMetadata
import com.ovle.rl.persistence.SerializedGame
import com.ovle.rl.persistence.SerializedMeta
import com.ovle.rl.persistence.serialization.GameDeserializer
import com.ovle.rl.persistence.serialization.jackson.deserializer.*

class JacksonGameDeserializer(
    private val jacksonMixins: Array<JacksonMixin>,

    private val entityDeserializer: EntityDeserializer,
    private val entityPool: EntityDeserializePool,
    private val locationTemplateDeserializer: LocationTemplateDeserializer,
    private val gridDeserializer: GridDeserializer,
    private val templateDeserializers: Collection<NamedTemplateDeserializer<*>>
) : GameDeserializer {


    override fun deserializeMeta(data: SerializedMeta): Collection<SavedGameMetadata> {
        val mapper = jacksonObjectMapper()
        val typeReference: TypeReference<*> = object : TypeReference<List<SavedGameMetadata>>() {}
        return mapper.readValue(data, typeReference)
    }

    override fun deserialize(data: SerializedGame, engine: Engine): GameEntity {
        val mapper = objectMapper()
        entityPool.entities.clear()

        SimpleModule().apply {
            addDeserializer(Entity::class.java, entityDeserializer)
            addDeserializer(LocationTemplate::class.java, locationTemplateDeserializer)
            addDeserializer(Grid::class.java, gridDeserializer)

            templateDeserializers.forEach {
                val handledType: Class<NamedTemplate> = it.handledType() as Class<NamedTemplate>
                addDeserializer(handledType, it)
            }

            mapper.registerModule(this)
        }

        val saveData = mapper.readValue(data, SavedGame::class.java)
        //todo load format versions
        //todo WorldInfo - restore tiles, heightGrid, heatGrid from seed

        val globalComponents = saveData.gameComponents
        val game = engine.newEntity(*globalComponents)

        val entities = entityPool.entities.values
        val entitiesMap = saveData.entitiesMap
        entities.forEach { e ->
            val components = entitiesMap[e.id()]!!
            components.forEach { c ->
                postProcess(c)
                e.add(c)
            }

            engine.addEntity(e)
        }

        return game
    }

    //todo separate as component for move module
    private fun postProcess(entityComponent: EntityComponent) {
        when (entityComponent) {
            is MoveComponent -> {
                entityComponent.params?.let {
                    //reset ai-base moves (due to ai reset)
                    if (!it.isFixedPathMove) {
                        entityComponent.params = null
                        entityComponent.state = MoveState.IDLE
                    }
                }
            }
        }
    }

    private fun objectMapper(): ObjectMapper {
        val mapper = jacksonObjectMapper()
        jacksonMixins.forEach { mapper.addMixIn(it.target.java, it.mixin.java) }
        return mapper
    }
}