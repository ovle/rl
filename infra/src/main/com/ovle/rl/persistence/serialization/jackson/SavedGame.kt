package com.ovle.rl.persistence.serialization.jackson

import com.ovle.rl.EntityId
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GlobalComponent

class SavedGame(
    val gameComponents: Array<GlobalComponent>,
    val entitiesMap: Map<EntityId, Array<EntityComponent>>
)