package com.ovle.rl.persistence.serialization

import com.ovle.rl.GameEntity
import com.ovle.rl.persistence.SavedGameMetadata
import com.ovle.rl.persistence.SerializedGame
import com.ovle.rl.persistence.SerializedMeta

interface GameSerializer {
    fun serializeMeta(meta: Collection<SavedGameMetadata>): SerializedMeta

    fun serialize(game: GameEntity): SerializedGame
}