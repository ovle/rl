package com.ovle.rl.persistence.serialization.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.node.TextNode
import com.ovle.rl.model.procgen.config.LocationTemplate


class LocationTemplateDeserializer
@JvmOverloads constructor(
    t: Class<LocationTemplate?>? = null,
    private val worldLocationTemplate: LocationTemplate,
    private val locationTemplates: Collection<LocationTemplate>
) : StdDeserializer<LocationTemplate>(t) {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): LocationTemplate {
        val node: JsonNode = p.codec.readTree(p)
        val name = (node as TextNode).textValue()

        return (locationTemplates + worldLocationTemplate)
            .single { it.name == name }
    }
}