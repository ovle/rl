package com.ovle.rl.persistence.serialization.jackson.deserializer

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.node.TextNode
import com.ovle.rl.model.game.core.component.CoreComponent


class EntityDeserializer @JvmOverloads constructor(
    t: Class<Entity?>? = null,
    private val entityPool: EntityDeserializePool
) : StdDeserializer<Entity>(t) {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Entity {
        val node: JsonNode = p.codec.readTree(p)
        val id = (node as TextNode).textValue()
         return entityPool.entities[id] ?: newEntity(id)
    }

    private fun newEntity(id: String) = Entity().also {
        it.add(CoreComponent(id = id))
        entityPool.entities[id] = it
    }
}