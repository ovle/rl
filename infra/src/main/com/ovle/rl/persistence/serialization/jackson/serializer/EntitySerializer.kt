package com.ovle.rl.persistence.serialization.jackson.serializer

import com.badlogic.ashley.core.Entity
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.ovle.rl.model.game.core.id
import java.io.IOException

class EntitySerializer @JvmOverloads constructor(t: Class<Entity?>? = null) : StdSerializer<Entity>(t) {

    @Throws(IOException::class, JsonProcessingException::class)
    override fun serialize(value: Entity?, jgen: JsonGenerator, provider: SerializerProvider) {
        jgen.writeString(value?.id())
    }
}