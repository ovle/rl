package com.ovle.rl.persistence.serialization.jackson.deserializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.node.TextNode
import com.ovle.rl.model.template.NamedTemplate

class NamedTemplateDeserializer<T: NamedTemplate>
@JvmOverloads constructor(
    t: Class<T>? = null,
    private val templates: Collection<T>
) : StdDeserializer<T>(t) {

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): T? {
        val node: JsonNode = p.codec.readTree(p)
        val name = (node as TextNode).textValue()

        return templates.singleOrNull { it.name == name }
    }
}