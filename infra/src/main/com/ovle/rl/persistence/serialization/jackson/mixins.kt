package com.ovle.rl.persistence.serialization.jackson

import com.fasterxml.jackson.annotation.*
import com.ovle.rl.model.game.ai.AIComponent
import com.ovle.rl.model.game.ai.decision.AITaskFactory
import com.ovle.rl.model.game.ai.task.AITask
import com.ovle.rl.model.game.ai.task.AITaskSource
import com.ovle.rl.model.game.area.AreaParams
import com.ovle.rl.model.game.area.AreaTemplate
import com.ovle.rl.model.game.build.BuildTemplate
import com.ovle.rl.model.game.core.component.EntityComponent
import com.ovle.rl.model.game.core.component.GlobalComponent
import com.ovle.rl.model.game.craft.CraftTemplate
import com.ovle.rl.model.game.effect.StaticEffectTemplate
import com.ovle.rl.model.game.farm.FarmTemplate
import com.ovle.rl.model.game.game.dto.Entities
import com.ovle.rl.model.game.game.dto.location.LocationContent
import com.ovle.rl.model.game.game.dto.player.LocationPlayer
import com.ovle.rl.model.game.game.dto.player.Player
import com.ovle.rl.model.game.life.HealthComponent
import com.ovle.rl.model.game.perception.PerceptionComponent
import com.ovle.rl.model.game.game.dto.location.projection.LocationProjection
import com.ovle.rl.model.game.skill.SkillTarget
import com.ovle.rl.model.game.skill.dto.PlayerSkillTemplate
import com.ovle.rl.model.game.skill.dto.SkillTemplate
import com.ovle.rl.model.game.social.SocialComponent
import com.ovle.rl.model.game.social.dto.FactionTemplate
import com.ovle.rl.model.game.space.accessibility.Accessibility
import com.ovle.rl.model.game.space.aoe.AOETemplate
import com.ovle.rl.model.game.space.move.MoveComponent
import com.ovle.rl.model.game.space.move.MoveParams
import com.ovle.rl.model.game.space.move.impl.path.MovePath
import com.ovle.rl.model.game.space.move.impl.path.PathTemplate
import com.ovle.rl.model.game.task.TaskPerformerComponent
import com.ovle.rl.model.game.task.dto.TaskInfo
import com.ovle.rl.model.game.task.dto.TaskTemplate
import com.ovle.rl.model.game.task.target.TaskTarget
import com.ovle.rl.model.game.tile.TileTemplate
import com.ovle.rl.model.game.time.dto.GlobalTimeInfo
import com.ovle.rl.model.game.time.dto.TimerData
import com.ovle.rl.model.game.time.dto.TimerTemplate
import com.ovle.rl.model.game.trigger.dto.TriggerTemplate
import com.ovle.rl.model.procgen.config.LocationTemplate
import com.ovle.rl.model.procgen.grid.world.World
import com.ovle.rl.model.template.entity.EntityTemplate
import com.ovle.utils.RandomParams
import com.ovle.utils.gdx.math.array2d.Array2d

val jacksonMixins = arrayOf(
    //utils
    JacksonMixin(Array2d::class, Array2dMixin::class),
    JacksonMixin(RandomParams::class, RandomParamsMixin::class),
    //core
    JacksonMixin(EntityComponent::class, EntityComponentMixin::class),
    JacksonMixin(GlobalComponent::class, GlobalComponentMixin::class),
    //templates
    JacksonMixin(TileTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(LocationTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(EntityTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(TaskTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(SkillTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(TimerTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(TriggerTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(AOETemplate::class, NamedTemplateMixin::class),
    JacksonMixin(PathTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(StaticEffectTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(CraftTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(BuildTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(FarmTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(AreaTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(PlayerSkillTemplate::class, NamedTemplateMixin::class),
    JacksonMixin(FactionTemplate::class, NamedTemplateMixin::class),

    //global data
    JacksonMixin(World::class, WorldMixin::class),
    JacksonMixin(GlobalTimeInfo::class, GlobalTimeInfoMixin::class),
    JacksonMixin(LocationContent::class, LocationContentMixin::class),
    JacksonMixin(Entities::class, EntitiesMixin::class),
    JacksonMixin(LocationPlayer::class, LocationPlayerMixin::class),
    JacksonMixin(LocationProjection::class, LocationProjectionMixin::class),
    JacksonMixin(Player::class, PlayerMixin::class),
    //entity data
    JacksonMixin(HealthComponent::class, LifeComponentMixin::class),
    JacksonMixin(PerceptionComponent::class, PerceptionComponentMixin::class),
    JacksonMixin(SocialComponent::class, SocialComponentMixin::class),
    JacksonMixin(AIComponent::class, AIComponentMixin::class),
    JacksonMixin(MoveComponent::class, MoveComponentMixin::class),
    JacksonMixin(TaskPerformerComponent::class, TaskPerformerComponentMixin::class),

    JacksonMixin(Accessibility::class, AccessibilityMixin::class),
    JacksonMixin(AITask::class, AITaskMixin::class),
    JacksonMixin(AITaskSource::class, AITaskSourceMixin::class),
    JacksonMixin(AITaskFactory::class, AIDecisionMakerMixin::class),
    JacksonMixin(MovePath::class, MovePathMixin::class),
    JacksonMixin(MoveParams::class, MoveParamsMixin::class),
    JacksonMixin(TimerData::class, TimerDataMixin::class),
    JacksonMixin(TaskTarget::class, TaskTargetMixin::class),
    JacksonMixin(TaskInfo::class, TaskInfoMixin::class),
    JacksonMixin(SkillTarget::class, SkillTargetMixin::class),
    JacksonMixin(AreaParams::class, AreaParamsMixin::class),
)


@JsonIgnoreProperties("points")
abstract class Array2dMixin

@JsonIgnoreProperties("krandom", "jrandom")
abstract class RandomParamsMixin

@JsonIdentityReference(alwaysAsId = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "name")
abstract class NamedTemplateMixin

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
@JsonAutoDetect(isGetterVisibility = JsonAutoDetect.Visibility.NONE)
abstract class GlobalComponentMixin

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
@JsonAutoDetect(isGetterVisibility = JsonAutoDetect.Visibility.NONE)
abstract class EntityComponentMixin

@JsonIgnoreProperties("tiles", "heightGrid", "heatGrid", "humidityGrid", "areas")
abstract class WorldMixin

@JsonIgnoreProperties("tick", "turn")
abstract class GlobalTimeInfoMixin

@JsonIdentityInfo(generator = ObjectIdGenerators.StringIdGenerator::class)
abstract class LocationContentMixin

@JsonIdentityInfo(generator = ObjectIdGenerators.StringIdGenerator::class)
@JsonIgnoreProperties("areasByTemplate")
abstract class LocationPlayerMixin

@JsonIdentityInfo(generator = ObjectIdGenerators.StringIdGenerator::class)
abstract class PlayerMixin

@JsonIgnoreProperties("maps")
abstract class AccessibilityMixin

@JsonIgnoreProperties("size")
abstract class InfluenceMixin

@JsonIgnoreProperties("starved", "dead")
abstract class LifeComponentMixin

@JsonIgnoreProperties("fov")
abstract class PerceptionComponentMixin

@JsonIgnoreProperties("enemies")
abstract class SocialComponentMixin

@JsonIgnoreProperties("aiParams", "rootTask")
abstract class AIComponentMixin

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
abstract class TaskTargetMixin

@JsonIgnoreProperties("free", "performer", "lockedResources", "status")
abstract class TaskInfoMixin

@JsonIgnoreProperties("current")
abstract class TaskPerformerComponentMixin

@JsonIdentityInfo(generator = ObjectIdGenerators.StringIdGenerator::class)
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
@JsonIgnoreProperties("flee")
abstract class AITaskMixin

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
abstract class AITaskSourceMixin

@JsonIgnoreProperties("last", "left")
abstract class MovePathMixin

@JsonIgnoreProperties("fixedPathMove")
abstract class MoveParamsMixin

@JsonIgnoreProperties("moveStep")
abstract class MoveComponentMixin

@JsonIgnoreProperties("diff")
abstract class TimerDataMixin

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
abstract class SkillTargetMixin

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
abstract class AIDecisionMakerMixin

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "className")
abstract class AreaParamsMixin

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonIgnoreProperties("visibleTilesMask")
abstract class LocationProjectionMixin

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonIgnoreProperties("entitiesByPosition")
abstract class EntitiesMixin
