package com.ovle.rl.persistence.storage.preferences

import com.badlogic.gdx.Gdx
import com.ovle.rl.persistence.storage.DataStorage

class PreferencesDataStorage: DataStorage {

    companion object {
        private const val APP_PREFERENCES_NAME = "com.ovle.rl"
    }

    override fun saveString(dir: String, key: String, data: String) {
        val preferences = preferences(dir)
        preferences.putString(key, data)
        preferences.flush()
    }

    override fun get(dir: String, key: String): String? {
        val preferences = preferences(dir)
        return preferences.getString(key, null)
    }

    override fun delete(dir: String, key: String) {
        val preferences = preferences(dir)
        preferences.remove(key)
        preferences.flush()
    }

    override fun getAll(dir: String): Map<String, *> {
        return preferences(dir).get()
    }


    private fun preferences(dir: String) = Gdx.app
        .getPreferences("$APP_PREFERENCES_NAME.$dir")
}