package com.ovle.rl.persistence.storage

interface DataStorage {
    fun saveString(dir: String, key: String, data: String)
    fun get(dir: String, key: String): String?
    fun getAll(dir: String): Map<String, *>
    fun delete(dir: String, key: String)
}