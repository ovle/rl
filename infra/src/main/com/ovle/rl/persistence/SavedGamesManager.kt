package com.ovle.rl.persistence

import com.badlogic.ashley.core.Engine
import com.ovle.rl.GameEntity
import com.ovle.rl.model.game.time.globalTime
import com.ovle.rl.model.util.location
import com.ovle.rl.persistence.serialization.GameDeserializer
import com.ovle.rl.persistence.serialization.GameSerializer
import com.ovle.rl.persistence.storage.DataStorage
import java.util.*

class SavedGamesManager(
    private val dataStorage: DataStorage,
    private val gameSerializer: GameSerializer,
    private val gameDeserializer: GameDeserializer,
) {

    companion object {
        private const val META_KEY = "meta"
        private const val FORMAT_VERSION = "0.1"
    }

    /**
     * @return saved games meta list
     */
    fun savedGamesMeta(): Collection<SavedGameMetadata> {
        val serializedMeta = dataStorage.get(SAVE_DIR, META_KEY)
            ?: return emptyList()
        return gameDeserializer.deserializeMeta(serializedMeta)
    }

    /**
     * save the [game] with given [name]
     * rewrites existing game if [meta] is set
     */
    fun save(meta: SavedGameMetadata?, name: String, game: GameEntity) {
        val savedGamesMeta = savedGamesMeta().toMutableList()
        if (meta != null) {
            dataStorage.delete(SAVE_DIR, meta.id)

            savedGamesMeta.remove(meta)
        }

        val newId = meta?.id ?: UUID.randomUUID().toString()
        val saveData = gameSerializer.serialize(game)
        dataStorage.saveString(SAVE_DIR, newId, saveData)

        savedGamesMeta += meta(newId, name, game)
        saveMeta(savedGamesMeta)
    }

    /**
     * load the existing game with given [meta] in context of an [engine]
     * @return initialized game entity
     */
    fun load(meta: SavedGameMetadata, engine: Engine): GameEntity {
        val data = dataStorage.get(SAVE_DIR, meta.id)
            ?: throw IllegalArgumentException("no save with id=${meta.id} found")
        return gameDeserializer.deserialize(data, engine)
    }

    /**
     * delete the existing [game]
     */
    fun delete(game: SavedGameMetadata) {
        dataStorage.delete(SAVE_DIR, game.id)

        val savedGamesMeta = savedGamesMeta().toMutableList()
        savedGamesMeta.remove(game)
        saveMeta(savedGamesMeta)
    }


    private fun saveMeta(savedGamesMeta: MutableList<SavedGameMetadata>) {
        val serializedMeta = gameSerializer.serializeMeta(savedGamesMeta)
        dataStorage.saveString(SAVE_DIR, META_KEY, serializedMeta)
    }

    private fun meta(id: String, name: String, game: GameEntity): SavedGameMetadata {
        val locationName = game.location().name
        val tick = game.globalTime().tick

        return SavedGameMetadata(
            name, id, Date(), tick, locationName, FORMAT_VERSION
        )
    }
}